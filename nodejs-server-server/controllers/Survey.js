'use strict';

var utils = require('../utils/writer.js');
var Survey = require('../service/SurveyService');

module.exports.getAdegver = function getAdegver (req, res, next) {
  var id = req.swagger.params['id'].value;
  var basicInput = req.swagger.params['BasicInput'].value;
  Survey.getAdegver(id,basicInput)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAml = function getAml (req, res, next) {
  var idCliente = req.swagger.params['idCliente'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  var flagRiepilogo = req.swagger.params['flagRiepilogo'].value;
  Survey.getAml(idCliente,idIncarico,flagRiepilogo)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMifid = function getMifid (req, res, next) {
  var id = req.swagger.params['id'].value;
  var flagRiepilogo = req.swagger.params['flagRiepilogo'].value;
  Survey.getMifid(id,flagRiepilogo)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.postAml = function postAml (req, res, next) {
  var amlSavePost = req.swagger.params['amlSavePost'].value;
  Survey.postAml(amlSavePost)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.putAml = function putAml (req, res, next) {
  var amlSavePut = req.swagger.params['amlSavePut'].value;
  Survey.putAml(amlSavePut)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveAdegver = function saveAdegver (req, res, next) {
  var adegVerDataModel = req.swagger.params['AdegVerDataModel'].value;
  Survey.saveAdegver(adegVerDataModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveMifid = function saveMifid (req, res, next) {
  var mifidDataModel = req.swagger.params['MifidDataModel'].value;
  Survey.saveMifid(mifidDataModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.surveyAdegverTemplatePOST = function surveyAdegverTemplatePOST (req, res, next) {
  var basicInput = req.swagger.params['BasicInput'].value;
  Survey.surveyAdegverTemplatePOST(basicInput)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.templateAml = function templateAml (req, res, next) {
  Survey.templateAml()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.templateMifid = function templateMifid (req, res, next) {
  Survey.templateMifid()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
