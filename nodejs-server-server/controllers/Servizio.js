'use strict';

var utils = require('../utils/writer.js');
var Servizio = require('../service/ServizioService');

module.exports.searchServizio = function searchServizio (req, res, next) {
  var searchServizioRequest = req.swagger.params['SearchServizioRequest'].value;
  Servizio.searchServizio(searchServizioRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.servizioIdServizioDELETE = function servizioIdServizioDELETE (req, res, next) {
  var idServizio = req.swagger.params['idServizio'].value;
  Servizio.servizioIdServizioDELETE(idServizio)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.servizioIdServizioGET = function servizioIdServizioGET (req, res, next) {
  var idServizio = req.swagger.params['idServizio'].value;
  Servizio.servizioIdServizioGET(idServizio)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.servizioIdServizioPOST = function servizioIdServizioPOST (req, res, next) {
  var idServizio = req.swagger.params['idServizio'].value;
  var servizio = req.swagger.params['Servizio'].value;
  Servizio.servizioIdServizioPOST(idServizio,servizio)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
