'use strict';

var utils = require('../utils/writer.js');
var Customer = require('../service/CustomerService');

module.exports.create = function create (req, res, next) {
  var codFiscale = req.swagger.params['codFiscale'].value;
  Customer.create(codFiscale)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.customerIdCustomerDataRegistryGET = function customerIdCustomerDataRegistryGET (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  Customer.customerIdCustomerDataRegistryGET(idCustomer,idIncarico)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.customerIdCustomerDataRegistryPOST = function customerIdCustomerDataRegistryPOST (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  var dataRegistryModel = req.swagger.params['DataRegistryModel'].value;
  Customer.customerIdCustomerDataRegistryPOST(idCustomer,idIncarico,dataRegistryModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.customerIdCustomerDataRegistryPUT = function customerIdCustomerDataRegistryPUT (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  var dataRegistryModel = req.swagger.params['DataRegistryModel'].value;
  Customer.customerIdCustomerDataRegistryPUT(idCustomer,idIncarico,dataRegistryModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.datiFull = function datiFull (req, res, next) {
  var idCliente = req.swagger.params['idCliente'].value;
  Customer.datiFull(idCliente)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.datiLight = function datiLight (req, res, next) {
  var idCliente = req.swagger.params['idCliente'].value;
  Customer.datiLight(idCliente)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.elencoClienti = function elencoClienti (req, res, next) {
  Customer.elencoClienti()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getDatiAnagraficiCF = function getDatiAnagraficiCF (req, res, next) {
  var cf = req.swagger.params['cf'].value;
  Customer.getDatiAnagraficiCF(cf)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getFatcacrs = function getFatcacrs (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  Customer.getFatcacrs(idCustomer,idIncarico)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getIdCardData = function getIdCardData (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  Customer.getIdCardData(idCustomer,idIncarico)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getIdCardDataAllegati = function getIdCardDataAllegati (req, res, next) {
  var idCliente = req.swagger.params['idCliente'].value;
  Customer.getIdCardDataAllegati(idCliente)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getServdig = function getServdig (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idIncarico = req.swagger.params['idIncarico'].value;
  Customer.getServdig(idCustomer,idIncarico)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveFatcacrsPost = function saveFatcacrsPost (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var dataRegistryRequest = req.swagger.params['dataRegistryRequest'].value;
  Customer.saveFatcacrsPost(idCustomer,dataRegistryRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveFatcacrsPut = function saveFatcacrsPut (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var dataRegistryRequest = req.swagger.params['dataRegistryRequest'].value;
  Customer.saveFatcacrsPut(idCustomer,dataRegistryRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveIdCardDataAllegati = function saveIdCardDataAllegati (req, res, next) {
  var idCardModel = req.swagger.params['idCardModel'].value;
  Customer.saveIdCardDataAllegati(idCardModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveIdCardDataPost = function saveIdCardDataPost (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idCardModel = req.swagger.params['idCardModel'].value;
  Customer.saveIdCardDataPost(idCustomer,idCardModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveIdCardDataPut = function saveIdCardDataPut (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var idCardModel = req.swagger.params['idCardModel'].value;
  Customer.saveIdCardDataPut(idCustomer,idCardModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveServdigPost = function saveServdigPost (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var serviziDigitaliRequest = req.swagger.params['ServiziDigitaliRequest'].value;
  Customer.saveServdigPost(idCustomer,serviziDigitaliRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.saveServdigPut = function saveServdigPut (req, res, next) {
  var idCustomer = req.swagger.params['idCustomer'].value;
  var serviziDigitaliRequest = req.swagger.params['ServiziDigitaliRequest'].value;
  Customer.saveServdigPut(idCustomer,serviziDigitaliRequest)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
