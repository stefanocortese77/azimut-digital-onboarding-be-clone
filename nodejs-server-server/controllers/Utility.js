'use strict';

var utils = require('../utils/writer.js');
var Utility = require('../service/UtilityService');

module.exports.getDominio = function getDominio (req, res, next) {
  var dominioModel = req.swagger.params['DominioModel'].value;
  Utility.getDominio(dominioModel)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sendEmail = function sendEmail (req, res, next) {
  Utility.sendEmail()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sendSms = function sendSms (req, res, next) {
  Utility.sendSms()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.utilityDocfirmaGeneratePOST = function utilityDocfirmaGeneratePOST (req, res, next) {
  Utility.utilityDocfirmaGeneratePOST()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.utilityDocfirmaGetIdGET = function utilityDocfirmaGetIdGET (req, res, next) {
  var id = req.swagger.params['id'].value;
  Utility.utilityDocfirmaGetIdGET(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.utilityDocfirmaListGET = function utilityDocfirmaListGET (req, res, next) {
  Utility.utilityDocfirmaListGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.utilityDocfirmaSavePOST = function utilityDocfirmaSavePOST (req, res, next) {
  Utility.utilityDocfirmaSavePOST()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.utilityDocfirmaSignPOST = function utilityDocfirmaSignPOST (req, res, next) {
  var idIncarico = req.swagger.params['idIncarico'].value;
  Utility.utilityDocfirmaSignPOST(idIncarico)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.verifyEmail = function verifyEmail (req, res, next) {
  Utility.verifyEmail()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.verifySms = function verifySms (req, res, next) {
  Utility.verifySms()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
