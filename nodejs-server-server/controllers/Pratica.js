'use strict';

var utils = require('../utils/writer.js');
var Pratica = require('../service/PraticaService');

module.exports.praticaIdPraticaGET = function praticaIdPraticaGET (req, res, next) {
  var idPratica = req.swagger.params['idPratica'].value;
  Pratica.praticaIdPraticaGET(idPratica)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
