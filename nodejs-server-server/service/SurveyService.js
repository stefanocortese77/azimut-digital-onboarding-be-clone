'use strict';


/**
 * Restituisce il questionario di adeguata verifica con i dati relativi al cliente
 *
 * id String 
 * basicInput BasicInput 
 * returns AdegVerDataModel
 **/
exports.getAdegver = function(id,basicInput) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "datiQuestAdegVer" : "",
  "idConsultant" : "idConsultant",
  "idCustomer" : "idCustomer"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Restituisce il questionario antiriciclaggio con i dati relativi al cliente
 *
 * idCliente Integer 
 * idIncarico Integer 
 * flagRiepilogo Boolean Da valorizzare a false
 * returns AmlDataModel
 **/
exports.getAml = function(idCliente,idIncarico,flagRiepilogo) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "idIncarico" : 5158632,
  "idCliente" : 2328990,
  "listaDomande" : [ {
    "idDomanda" : 45,
    "listaRisposte" : [ {
      "idRisposta" : 1,
      "descRisposta" : "2"
    } ]
  }, {
    "idDomanda" : 48,
    "listaRisposte" : [ {
      "idRisposta" : 1,
      "descRisposta" : "3"
    } ]
  }, {
    "idDomanda" : 51,
    "listaRisposte" : [ {
      "idRisposta" : 63,
      "descRisposta" : "prova"
    } ]
  }, {
    "idDomanda" : 56,
    "listaRisposte" : [ {
      "idRisposta" : 64,
      "descRisposta" : "prova"
    } ]
  } ],
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Restituisce il questionario MiFID con i dati relativi al cliente
 *
 * id String 
 * flagRiepilogo Boolean Da valorizzare a false
 * returns MifidDataModel
 **/
exports.getMifid = function(id,flagRiepilogo) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "idCliente" : "MarioRossi123",
  "listaDomande" : [ {
    "idDomanda" : 1,
    "listaRisposte" : [ {
      "idRisposta" : 2
    } ]
  }, {
    "idDomanda" : 2,
    "listaRisposte" : [ {
      "idRisposta" : 2
    } ]
  } ],
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Inserisce un questionario antiriciclaggio con i dati relativi al cliente
 *
 * amlSavePost AmlDataModel 
 * returns SuccessResponse
 **/
exports.postAml = function(amlSavePost) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "0",
  "description" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Aggiorna un questionario antiriciclaggio con i dati relativi al cliente
 *
 * amlSavePut AmlDataModel 
 * returns SuccessResponse
 **/
exports.putAml = function(amlSavePut) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "0",
  "description" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Inserisce o aggiorna un questionario di adeguata verifica con i dati relativi al cliente
 *
 * adegVerDataModel AdegVerDataModel 
 * no response value expected for this operation
 **/
exports.saveAdegver = function(adegVerDataModel) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Inserisce o aggiorna un questionario MiFID con i dati relativi al cliente
 *
 * mifidDataModel MifidDataModel 
 * no response value expected for this operation
 **/
exports.saveMifid = function(mifidDataModel) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Restituisce l'elenco di ogni domanda con le relative risposte inerenti al questionario di adeguata verifica
 *
 * basicInput BasicInput 
 * returns AdegVerTemplate
 **/
exports.surveyAdegverTemplatePOST = function(basicInput) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "adeguata_verifica" : {
    "info_contesto" : {
      "rischio_riciclaggio" : {
        "name" : "rischio_riciclaggio",
        "options" : [ {
          "value" : "alto",
          "label" : "Alto"
        }, {
          "value" : "medio",
          "label" : "Medio"
        }, {
          "value" : "basso",
          "label" : "Basso"
        } ],
        "type" : "radio",
        "title" : "In base alle informazioni disponibili e a quanto comunque noto circa l’intenzione o la finalità perseguita, il grado di rischio di riciclaggio ateribuibile al Cliente da parte del consulente finanziario è:*"
      },
      "ragionevolezza_attivita" : {
        "name" : "ragionevolezza_attivita",
        "options" : [ {
          "value" : "giustificato",
          "label" : "Giustificato"
        }, {
          "value" : "non_giustificato",
          "label" : "Non giustificato"
        } ],
        "type" : "radio",
        "title" : "Ragionevolezza dell’accensione del rapporto in relazione all’attività svolta dal Cliente *"
      },
      "motivazioni" : {
        "label" : "Motivazioni rischio *",
        "type" : "textarea",
        "help_text" : "In caso di valutazione del rischio medio/alto compilare con le relative motivazioni"
      },
      "title_accordion" : "Informazioni di contesto",
      "comportamento" : {
        "name" : "comportamento",
        "options" : [ "[{\"value\":\"norma\",\"label\":\"Nella norma\"},{\"value\":\"fuori_norma\",\"label\":\"Fuori dalla norma\"}]", "[{\"value\":\"norma\",\"label\":\"Nella norma\"},{\"value\":\"fuori_norma\",\"label\":\"Fuori dalla norma\"}]" ],
        "type" : "radio",
        "title" : "Comportamento del cliente in fase di apertura rapporto *"
      },
      "questions_info" : {
        "questions" : [ {
          "value" : "presenza_terzi",
          "label" : "Presenza di soggetti terzi non giustficata*"
        }, {
          "value" : "info_attendibili",
          "label" : "Le informazioni ricevute dal cliente, in base agli elementi conosciuti, sono da ritenersi attendibili?*"
        }, {
          "value" : "riciclaggio_terrorismo",
          "label" : "In base ai dati e alle informazioni disponibili, si ravvisano elementi di sospettosità ai fini di riciclaggio e/o finanziamento di terrorismo?*"
        } ],
        "type" : "yes_no"
      }
    },
    "questions" : [ {
      "name" : "anni_conoscenza",
      "label" : "Da quanti anni conosci il Cliente? *",
      "type" : "slider",
      "details" : [ {
        "defaultValue" : 2
      } ]
    }, {
      "name" : "primo_contatto",
      "label" : "Come hai conosciuto il Cliente? *",
      "type" : "radio",
      "details" : [ {
        "options" : [ {
          "value" : "spontaneamente",
          "label" : "Presentandosi spontaneamente"
        }, {
          "value" : "professionale",
          "label" : "Canale professionale"
        }, {
          "value" : "privato",
          "label" : "Canale privato"
        } ]
      } ]
    }, {
      "name" : "conoscenza_cliente",
      "label" : "Conosci il contesto familiare del Cliente?",
      "type" : "yes_no"
    }, {
      "name" : "num_componenti",
      "label" : "Quanti sono i componenti del nucleo familiare? *",
      "type" : "slider",
      "details" : [ {
        "defaultValue" : 2
      } ]
    }, {
      "name" : "tenore_vita",
      "label" : "Qual è il tenore di vita del Cliente? *",
      "type" : "radio-heavy",
      "details" : [ {
        "options" : [ {
          "value" : "alto",
          "label" : "Alto"
        }, {
          "value" : "medio",
          "label" : "Medio"
        }, {
          "value" : "basso",
          "label" : "Basso"
        } ]
      } ]
    } ],
    "title_section" : "ADEGUATA VERIFICA",
    "conoscenza_cliente" : {
      "title" : "Conoscenza del cliente"
    }
  },
  "title" : "ADEGUATA VERIFICA"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Restituisce l'elenco di ogni domanda con le relative risposte inerenti al questionario antiriciclaggio
 *
 * returns AmlTemplate
 **/
exports.templateAml = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "sezioni" : [ {
    "idSezione" : "ANTIRICICLAGGIO",
    "descSezione" : "Dati antiriciclaggio",
    "listaDomande" : [ {
      "idDomanda" : 44,
      "testoDomanda" : "Titolo di studio",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_STUDIO",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 45,
      "testoDomanda" : "Natura e scopo del rapporto",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_NATURA_RAPPORTO",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 46,
      "testoDomanda" : "Settore Occupazione",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_SETTORE_OCCUPAZIONE",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 47,
      "testoDomanda" : "Professione",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_PROFESSIONE_ATTUALE",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 48,
      "testoDomanda" : "Nazione principale insediamento attività",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_NAZIONI",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 49,
      "testoDomanda" : "Provincia attività",
      "subTestoDomanda" : null,
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_PROVINCE",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 50,
      "testoDomanda" : "Anno inizio",
      "subTestoDomanda" : null,
      "inputType" : "INPUT_BOX",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 51,
      "testoDomanda" : "Il cliente è un soggetto PEP Nazionale/Estero?",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 52,
      "testoDomanda" : "Il Cliente intrattiene rapporti di qualsiasi natura con soggetti PEP esteri e soggetti PEP nazionali",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 53,
      "testoDomanda" : "Il Cliente dichiara di ricoprire cariche di rilievo in ambito politico/istituzionale",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 54,
      "testoDomanda" : "Il Cliente dichiara di ricoprire cariche di rilievo in ambito societario/associazioni/fondazioni operanti con Paesi extra UE",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 55,
      "testoDomanda" : "Il Cliente dichiara di ricoprire cariche di rilievo in ambito Pubblica Amministrazione",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 56,
      "testoDomanda" : "Il Cliente dichiara di ricoprire cariche di rilievo in enti che gestiscono erogazioni di fondi pubblici",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 63,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 64,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    } ]
  }, {
    "idSezione" : "PRIVACY",
    "descSezione" : "Consensi Privacy",
    "listaDomande" : [ {
      "idDomanda" : 57,
      "testoDomanda" : "Prestazione consenso utilizzo dati personali da parte di Azimut,\\nconsapevole dei diritti previsti dal decreto legislativo xxxx",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 65,
        "descRisposta" : "PRESTO IL CONSENSO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 66,
        "descRisposta" : "NEGO IL CONSENSO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 58,
      "testoDomanda" : "Prestazione consenso trattamento e comunicazione dei dati personali per lo svolgimento di at...",
      "subTestoDomanda" : null,
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 65,
        "descRisposta" : "PRESTO IL CONSENSO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 66,
        "descRisposta" : "NEGO IL CONSENSO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    } ]
  } ],
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Restituisce l'elenco di ogni domanda con le relative risposte inerenti al questionario MiFID
 *
 * returns MifidTemplate
 **/
exports.templateMifid = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "titoloSezione" : "QUESTIONARIO DI PROFILATURA",
  "titoloSubSezione" : "In futuro potresti essere intestatario di un prodotto e/o di un servizio (di Consulenza e/o RTO)?*",
  "titoloSubSubSezione" : "La compilazione del questionario è obbligatoria solo in caso di risposta affermativa",
  "labelClienteNonProfilato" : "Se il cliente non sarà intestatario non è necessario compilare il questionario",
  "sezioni" : [ {
    "idSezione" : "CONOSCENZA_ESPERIENZA",
    "descSezione" : "Conoscenza ed Esperienza",
    "listaDomande" : [ {
      "idDomanda" : 5,
      "testoDomanda" : "Titolo di studio",
      "subTestoDomanda" : "",
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_STUDIO",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 6,
      "testoDomanda" : "Settore di occupazione",
      "subTestoDomanda" : "",
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_OCCUPAZIONE",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 7,
      "testoDomanda" : "Professione",
      "subTestoDomanda" : "se pensionato/a indicare l’attività precedente",
      "inputType" : "DROPDOWN",
      "tipoDominio" : "DOM_PROFESSIONE",
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 8,
      "testoDomanda" : "Ho già sottoscritto altri contratti di consulenza e/o gestione patrimoniale?",
      "subTestoDomanda" : "",
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 1,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 2,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 9,
      "testoDomanda" : "Ho già sottoscritto altri contratti di collocamento?",
      "subTestoDomanda" : "",
      "inputType" : "YES_NO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 3,
        "descRisposta" : "SI",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 4,
        "descRisposta" : "NO",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 10,
      "testoDomanda" : "STRUMENTI",
      "subTestoDomanda" : "",
      "inputType" : "MULTIPLE_CHOICE",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : {
        "domande" : [ {
          "idDomanda" : 11,
          "testoDomanda" : "Mercato Monetario",
          "subTestoDomanda" : "(es: Bot, certificati di deposito e carte commerciali)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 12,
          "testoDomanda" : "Obbligazionari",
          "subTestoDomanda" : "(es: obbligazioni corporate, titoli di Stato)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 13,
          "testoDomanda" : "Azioni",
          "subTestoDomanda" : "",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 14,
          "testoDomanda" : "OICVM",
          "subTestoDomanda" : "(es: fondi comuni di investimento aperti in valori mobiliari, SICAV, ETF)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 15,
          "testoDomanda" : "Strumenti Assicurativi Finanziari",
          "subTestoDomanda" : "(es: polizze unit-linked e index linked)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 16,
          "testoDomanda" : "Fondi di Investimento Alternativi Aperti",
          "subTestoDomanda" : "(es: hedge fund)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 17,
          "testoDomanda" : "Fondi di Investimento Alternativi Chiusi",
          "subTestoDomanda" : "(es: fondi comuni di inv. Immobiliare, fondi di private equity)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 18,
          "testoDomanda" : "Derivati Quotati",
          "subTestoDomanda" : "(es: futures, opzioni su azioni quotate)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 19,
          "testoDomanda" : "Derivati OTC",
          "subTestoDomanda" : "",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 20,
          "testoDomanda" : "Altri Strumenti Complessi",
          "subTestoDomanda" : "(es: obbligazioni convertibili o perpetue)",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : false,
          "listaDipendenze" : null,
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        } ],
        "listaDominiRisposte" : [ {
          "descrizione" : "CONOSCENZA",
          "risposte" : [ {
            "idRisposta" : 8,
            "descRisposta" : "SI",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 9,
            "descRisposta" : "NO",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 10,
            "descRisposta" : "Solo parzialmente",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          } ]
        }, {
          "descrizione" : "OPERAZIONI_TRIMESTRE",
          "risposte" : [ {
            "idRisposta" : 13,
            "descRisposta" : "Frequentemente (più di 10 operazioni al trimestre)",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 14,
            "descRisposta" : "Regolarmente (tra 5 e 10 operazioni al trimestre)",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 15,
            "descRisposta" : "Saltuariamente (meno di 5 operazioni al trimestre)",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 16,
            "descRisposta" : "NO",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 17,
            "descRisposta" : "In modo significativo rispetto al mio patrimonio (per un importo complessivo superiore al 50% del mio patrimonio)",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          } ]
        } ]
      }
    }, {
      "idDomanda" : 21,
      "testoDomanda" : "VERO O FALSO",
      "subTestoDomanda" : "",
      "inputType" : "MULTIPLE_CHOICE_VERO_FALSO",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : {
        "domande" : [ {
          "idDomanda" : 22,
          "testoDomanda" : "Mercato Monetario",
          "subTestoDomanda" : "Gli strumenti del mercato monetario risolvono temporanei fabbisogni di investimento ed offrono la possibilità di smobilizzo nel breve termine",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 11,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 23,
          "testoDomanda" : "Strumenti Obbligazionari",
          "subTestoDomanda" : "In condizioni di mercato normali, il prezzo di una obbligazione o titolo di stato è influenzato da variazioni (positive o negative) del rating dell emittente",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 12,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 24,
          "testoDomanda" : "Azioni",
          "subTestoDomanda" : "Di norma, il valore di una azione è più volatile del valore di una obbligazione di un Emittente Governativo",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 13,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 25,
          "testoDomanda" : "OICVM",
          "subTestoDomanda" : "I fondi comuni di investimento aperti in valori mobiliari diversificano i propri investimenti",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 14,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 26,
          "testoDomanda" : "Assicurativi Finanziari",
          "subTestoDomanda" : "Il valore della prestazione corrisposta a scadenza dall impresa di assicurazione è predeterminata in misura fissa e non è soggetta ad alcuna fluttuazione",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 15,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 27,
          "testoDomanda" : "Fondi di Investimento Alternativi Aperti",
          "subTestoDomanda" : "I Fondi di Investimento Alternativi aperti possono investire in qualsiasi asset class con limiti di concentrazione e di leva finanziaria più ampi rispetto agli OICVM",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 16,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 28,
          "testoDomanda" : "Fondi di Investimento Alternativi Chiusi",
          "subTestoDomanda" : "Avendo investito in un Fondi di Investimento Alternativo chiuso, è possibile disinvestire in qualsiasi momento",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 17,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 29,
          "testoDomanda" : "Derivati Quotati",
          "subTestoDomanda" : "Un prodotto derivato quotato può essere fonte di profitti e perdite che in taluni casi possono essere maggiori del valore iniziale dell investimento",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 18,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 30,
          "testoDomanda" : "Derivati OTC",
          "subTestoDomanda" : "I derivati OTC sono stumenti finanziari quotati sui mercati regolamentati",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 19,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        }, {
          "idDomanda" : 31,
          "testoDomanda" : "Altri Strumenti Complessi",
          "subTestoDomanda" : "Le obbligazioni \"\"perpetue\"\" prevedono il rimborso e il pagamento certo della cedola",
          "inputType" : "",
          "tipoDominio" : null,
          "flagDipendenza" : true,
          "listaDipendenze" : [ {
            "idDomandaDipendenza" : 29,
            "idRispostaDipendenza" : 9,
            "tipoDipendenza" : "disabled"
          } ],
          "listaRisposte" : [ ],
          "strutturaInternaDomande" : null
        } ],
        "listaDominiRisposte" : [ {
          "descrizione" : "YES_NO",
          "risposte" : [ {
            "idRisposta" : 11,
            "descRisposta" : "VERO",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          }, {
            "idRisposta" : 12,
            "descRisposta" : "FALSO",
            "descSubRisposta" : null,
            "flagDipendenza" : false,
            "listaDipendenze" : null
          } ]
        } ]
      }
    } ]
  }, {
    "idSezione" : "INVESTIMENTO",
    "descSezione" : "Obiettivi di investimento",
    "listaDomande" : [ {
      "idDomanda" : 32,
      "testoDomanda" : "Principalmente che OBIETTIVI si prefigge di raggiungere con i suoi investimenti?",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 39,
        "descRisposta" : "Impiegare il patrimonio in prodotti di liquidità",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 40,
        "descRisposta" : "Proteggere il mio capitale investito anche a fronte di un rendimento contenuto",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 41,
        "descRisposta" : "Far crescere moderatamente il mio capitale accettando una variabilità dei rendimenti media o medio-alta",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 42,
        "descRisposta" : "Far crescere discretamente il mio capitale accettando un rischio medio-alto o alto",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 43,
        "descRisposta" : "Far crescere in modo significativo il mio capitale anche a fronte di una variabilità dei endimenti in caso di andamenti negativi dei mercati",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 33,
      "testoDomanda" : "QUALE LIVELLO DI RISCHIO è disposto a correre per raggiungere i suoi obiettivi di investimento?",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 44,
        "descRisposta" : "Sono disposto a correre il minor rischio possibile di perdite del capitale investito",
        "descSubRisposta" : null,
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 39,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 45,
        "descRisposta" : "Sono disposto a correre un rischio contenuto con l obiettivo di conservare il valore del mio capitale investito",
        "descSubRisposta" : null,
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 40,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 46,
        "descRisposta" : "Sono disposto a rischiare perdite di media entità del mio capitale a fronte di una ragionevole crescita dello stesso",
        "descSubRisposta" : null,
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 40,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 41,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 47,
        "descRisposta" : "Sono disposto a rischiare delle perdite anche discrete per puntare a conseguire l obiettivo di una marcata crescita del mio capitale",
        "descSubRisposta" : null,
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 41,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 42,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 48,
        "descRisposta" : "Sono disposto a rischiare che il capitale investito azzeri il suo valore per puntare ad un obiettivo di significativa crescita del mio capitale",
        "descSubRisposta" : null,
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 42,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 43,
          "tipoDipendenza" : "enabled"
        } ]
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 34,
      "testoDomanda" : "L orizzonte temporale dell investimento è:",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 49,
        "descRisposta" : "Breve termine",
        "descSubRisposta" : "(fino a 3 anni)",
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 39,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 50,
        "descRisposta" : "Medio termine",
        "descSubRisposta" : "(da 3 a 8 anni)",
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 40,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 41,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 42,
          "tipoDipendenza" : "enabled"
        } ]
      }, {
        "idRisposta" : 51,
        "descRisposta" : "Lungo termine",
        "descSubRisposta" : "(oltre 8 anni)",
        "flagDipendenza" : true,
        "listaDipendenze" : [ {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 40,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 41,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 42,
          "tipoDipendenza" : "enabled"
        }, {
          "idDomandaDipendenza" : 32,
          "idRispostaDipendenza" : 43,
          "tipoDipendenza" : "enabled"
        } ]
      } ],
      "strutturaInternaDomande" : null
    } ]
  }, {
    "idSezione" : "FINANZIARIA",
    "descSezione" : "Situazione finanziaria",
    "listaDomande" : [ {
      "idDomanda" : 35,
      "testoDomanda" : "Fonte del reddito",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 18,
        "descRisposta" : "Derivante dalla mia professione",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 19,
        "descRisposta" : "Derivante dalla mia professione e Altro in misura marginale",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 20,
        "descRisposta" : "Derivante dalla mia professione e Altro in misura equilibrata",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 21,
        "descRisposta" : "Derivante dalla mia professione e Altro in misura prevalente (esempio: rendite immobiliari, interessi, cedole, dividendi)",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 25,
        "descRisposta" : "Non ho reddito",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 36,
      "testoDomanda" : "Altro (opzionale)",
      "subTestoDomanda" : "Altro:*",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 22,
        "descRisposta" : "IN MISURA MARGINALE",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 23,
        "descRisposta" : "IN MISURA EQUILIBRATA",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 24,
        "descRisposta" : "IN MISURA PREVALENTE",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 37,
      "testoDomanda" : "Prevede che il suo reddito nel prossimo futuro rispetto ad un importo di euro 60.000 sarà:",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 26,
        "descRisposta" : "Superiore",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 27,
        "descRisposta" : "Inferiore",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 28,
        "descRisposta" : "In linea",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 38,
      "testoDomanda" : "Il mio patrimonio finanziario (inclua la liquidità) è nella fascia:",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 29,
        "descRisposta" : "Da euro 100000 a euro 250.000",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 30,
        "descRisposta" : "Da euro 250.000 a euro 500.00",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 31,
        "descRisposta" : "Da euro 500.000 a euro 1.000.000",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 32,
        "descRisposta" : "Oltre euro 1.000.000",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 39,
      "testoDomanda" : "I miei investimenti immobiliari:",
      "subTestoDomanda" : "",
      "inputType" : "CHECKBOX",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 40,
      "testoDomanda" : "Dichiaro di avere impegni finanziari mensili (esempio: mutui, finanziamenti)",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 35,
        "descRisposta" : "Non ho impegni finanziari",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 36,
        "descRisposta" : "Fino a euro 1.000",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 37,
        "descRisposta" : "Fino a euro 2.500",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 38,
        "descRisposta" : "Oltre euro 2.500",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    } ]
  }, {
    "idSezione" : "ALTRO",
    "descSezione" : "Altre informazioni aggiuntive (opzionale)",
    "listaDomande" : [ {
      "idDomanda" : 41,
      "testoDomanda" : "La mia ricchezza finanziaria complessiva:",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 52,
        "descRisposta" : "E ' investita solo parzialmente con la vostra Società (inferiore al 30%)",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 53,
        "descRisposta" : "E ' investita in maniera consistente con la vostra Società (tra il 30% e il 70%)",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 54,
        "descRisposta" : "E ' investita prevalentemente con la vostra Società (oltre il 70%)",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 42,
      "testoDomanda" : "Della parte non investita con la vostra Società, l investimento in strumenti azionari è:",
      "subTestoDomanda" : "",
      "inputType" : "RADIO_BUTTON",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 56,
        "descRisposta" : "Parziale",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 57,
        "descRisposta" : "Consistente",
        "descSubRisposta" : null,
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    }, {
      "idDomanda" : 43,
      "testoDomanda" : "Nel dettaglio, la parte della mia ricchezza finanziaria complessiva non investita con la vostra Società è composta da:",
      "subTestoDomanda" : "",
      "inputType" : "INPUT_BOX",
      "tipoDominio" : null,
      "flagDipendenza" : false,
      "listaDipendenze" : null,
      "listaRisposte" : [ {
        "idRisposta" : 58,
        "descRisposta" : "Strumenti Mercato Monetario (es: Bot, certificati di deposito e carte comerciali",
        "descSubRisposta" : "% sul totale",
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 59,
        "descRisposta" : "Strumenti Obbligazionari (es: obbligazioni corporate, titoli di Stato",
        "descSubRisposta" : "% sul totale",
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 60,
        "descRisposta" : "Strumenti Azionari (es: azioni)",
        "descSubRisposta" : "% sul totale",
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 61,
        "descRisposta" : "Strumenti Complessi (es: FIA, Obbligazioni convertibili o perpetue",
        "descSubRisposta" : "% sul totale",
        "flagDipendenza" : false,
        "listaDipendenze" : null
      }, {
        "idRisposta" : 62,
        "descRisposta" : "OICVM/ Strumenti Assicurativi Finanziari",
        "descSubRisposta" : "% sul totale",
        "flagDipendenza" : false,
        "listaDipendenze" : null
      } ],
      "strutturaInternaDomande" : null
    } ]
  } ],
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

