'use strict';


/**
 * Restituisce i dettagli della pratica
 *
 * idPratica String 
 * returns PraticaResponse
 **/
exports.praticaIdPraticaGET = function(idPratica) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  },
  "pratica" : {
    "idServizi" : [ "idServizi", "idServizi" ],
    "contatti" : {
      "flagEmail" : true,
      "prefCellulare" : "prefCellulare",
      "flagCellulare" : true,
      "numcellulare" : "numcellulare",
      "email" : "email"
    },
    "corrispondenza" : {
      "idProvincia" : {
        "chiave" : "chiave",
        "tipoDominio" : "tipoDominio"
      },
      "idNazione" : {
        "chiave" : "chiave",
        "tipoDominio" : "tipoDominio"
      },
      "cap" : "cap",
      "cognome" : "cognome",
      "indirizzo" : "indirizzo",
      "idComune" : {
        "chiave" : "chiave",
        "tipoDominio" : "tipoDominio"
      },
      "nome" : "nome",
      "civico" : "civico"
    },
    "intestatari" : {
      "cognome" : "cognome",
      "idRuolo" : {
        "chiave" : "chiave",
        "tipoDominio" : "tipoDominio"
      },
      "nome" : "nome",
      "idStato" : {
        "chiave" : "chiave",
        "tipoDominio" : "tipoDominio"
      },
      "id" : "id",
      "codiceFiscale" : "codiceFiscale"
    }
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

