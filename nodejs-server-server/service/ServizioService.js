'use strict';


/**
 * Restituisce l'elenco dei contratti e dei clienti che vi partecipano
 *
 * searchServizioRequest ServizioSearchRequest 
 * returns ServizioSearchResponse
 **/
exports.searchServizio = function(searchServizioRequest) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "servizi" : [ {
    "idStatus" : "idStatus",
    "descStatus" : "descStatus",
    "descTipologia" : "descTipologia",
    "id" : "id",
    "customers" : [ {
      "idStatus" : "idStatus",
      "firstName" : "firstName",
      "lastName" : "lastName",
      "id" : "id",
      "codiceFiscale" : "codiceFiscale"
    }, {
      "idStatus" : "idStatus",
      "firstName" : "firstName",
      "lastName" : "lastName",
      "id" : "id",
      "codiceFiscale" : "codiceFiscale"
    } ],
    "insertionDate" : "insertionDate"
  }, {
    "idStatus" : "idStatus",
    "descStatus" : "descStatus",
    "descTipologia" : "descTipologia",
    "id" : "id",
    "customers" : [ {
      "idStatus" : "idStatus",
      "firstName" : "firstName",
      "lastName" : "lastName",
      "id" : "id",
      "codiceFiscale" : "codiceFiscale"
    }, {
      "idStatus" : "idStatus",
      "firstName" : "firstName",
      "lastName" : "lastName",
      "id" : "id",
      "codiceFiscale" : "codiceFiscale"
    } ],
    "insertionDate" : "insertionDate"
  } ],
  "esito" : {
    "code" : "0",
    "description" : "OK"
  },
  "customers" : [ {
    "idStatus" : "idStatus",
    "lastName" : "lastName",
    "firstname" : "firstname",
    "id" : "id",
    "codiceFiscale" : "codiceFiscale"
  }, {
    "idStatus" : "idStatus",
    "lastName" : "lastName",
    "firstname" : "firstname",
    "id" : "id",
    "codiceFiscale" : "codiceFiscale"
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Elimina una sottoscrizione del servizio di consulenza base o investimento
 *
 * idServizio String 
 * returns SuccessResponse
 **/
exports.servizioIdServizioDELETE = function(idServizio) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "0",
  "description" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Restituisce una sottoscrizione del servizio di consulenza base o investimento
 *
 * idServizio String 
 * returns ServizioResponse
 **/
exports.servizioIdServizioGET = function(idServizio) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  },
  "servizio" : {
    "idIncarico" : "2132",
    "idTipoServizio" : {
      "tipoDominio" : "DOM_SERVIZIO",
      "chiave" : "1"
    },
    "idStatoServizio" : {
      "tipoDominio" : "DOM_STATO_SERVIZIO",
      "chiave" : "1"
    },
    "idRegimeAppartenenza" : {
      "tipoDominio" : "DOM_REGIME_APPARTENENZA",
      "chiave" : "1"
    },
    "pagamento" : {
      "importo" : 15000,
      "idOrigineVersamento" : {
        "tipoDominio" : "DOM_ORIGINE_VERSAMENTO",
        "chiave" : "1"
      },
      "flagBonifico" : true,
      "flagAssegno" : true,
      "dettagliAssegno" : {
        "abi" : "05099",
        "cab" : "14932",
        "numConto" : "100524735516",
        "numAssegno" : "92243856",
        "immagine" : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAC60lEQVR42u3YwaoBcRjG4dc0yWJyTXJjLstW50pcwukkCcnCqFnYHWnG9zw1hYX01vz4myXJendJkmWSLsm8f9zkYfj4+XyoefHaK4v++o8uSRuS5C/JbaSf7dRfY3dLchjxjkPHJNc3v+d+tt5dVkm2b7g5gWnZPL+93fxQUGMCEACgaAA6M0DdAPhXHRwBAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAGAiARABKByApRnAEQAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAGA6gFozQB1A9CZARwBAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQABAAQAAAAQAEABAAQAAAAQAEAPiWABzNAHUDcDYDOAIAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAIACAAAACAAgAIACAAAACAAgAIADA5APwawbwCwAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEABAAQAEAAAAEAAQAEABAAQAAAAQC+OgAHM0DdAFzNAI4AgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIACAAgAAAAgAIAAgAIACAAAACAFQIwMkMUDcAZzOAIwAgAIAAAAIACAAgAIAAAAIACAAgAIAAAAIACAAgAIAAAAIACAAgAIAAAB/XJtkn2ZgCyvm5A5YPGsK14Oh0AAAAAElFTkSuQmCC"
      }
    },
    "percComm" : [ {
      "tipoDominio" : "DOM_TITOLO_COMMISSIONE",
      "chiave" : "1",
      "valore" : 0.5
    }, {
      "tipoDominio" : "DOM_TITOLO_COMMISSIONE",
      "chiave" : "2",
      "valore" : 2.7
    } ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Inserisce o aggiorna una sottoscrizione del servizio di consulenza base o investimento
 *
 * idServizio String 
 * servizio ServizioModel 
 * no response value expected for this operation
 **/
exports.servizioIdServizioPOST = function(idServizio,servizio) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

