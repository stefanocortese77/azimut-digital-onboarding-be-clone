'use strict';


/**
 * Restituisce tutti i valori di ogni dominio specificato in input
 *
 * dominioModel DominioModel 
 * returns DominioResponse
 **/
exports.getDominio = function(dominioModel) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "statusResponse" : {
    "code" : "0",
    "description" : "OK"
  },
  "listaDomini" : [ {
    "codiceDominio" : "DOM_PROVINCE",
    "valoriDominio" : {
      "1" : "Agrigento",
      "2" : "Alessandria",
      "3" : "Ancona",
      "4" : "Aosta",
      "5" : "Arezzo",
      "6" : "Ascoli Piceno",
      "7" : "Asti",
      "8" : "Avellino",
      "9" : "Bari",
      "10" : "Belluno",
      "11" : "Benevento",
      "12" : "Bergamo",
      "13" : "Biella",
      "14" : "Bologna",
      "15" : "Bolzano",
      "16" : "Brescia",
      "17" : "Brindisi",
      "18" : "Cagliari",
      "19" : "Caltanissetta",
      "20" : "Campobasso",
      "21" : "Caserta",
      "22" : "Catania",
      "23" : "Catanzaro",
      "24" : "Chieti",
      "25" : "Como",
      "26" : "Cosenza",
      "27" : "Cremona",
      "28" : "Crotone",
      "29" : "Cuneo",
      "30" : "Enna",
      "31" : "Ferrara",
      "32" : "Firenze",
      "33" : "Foggia",
      "34" : "Forlì-Cesena",
      "35" : "Frosinone",
      "36" : "Genova",
      "37" : "Gorizia",
      "38" : "Grosseto",
      "39" : "Imperia",
      "40" : "Isernia",
      "41" : "La Spezia",
      "42" : "L'Aquila",
      "43" : "Latina",
      "44" : "Lecce",
      "45" : "Lecco",
      "46" : "Livorno",
      "47" : "Lodi",
      "48" : "Lucca",
      "49" : "Macerata",
      "50" : "Mantova",
      "51" : "Massa Carrara",
      "52" : "Matera",
      "53" : "Messina",
      "54" : "Milano",
      "55" : "Modena",
      "56" : "Napoli",
      "57" : "Novara",
      "58" : "Nuoro",
      "59" : "Oristano",
      "60" : "Padova",
      "61" : "Palermo",
      "62" : "Parma",
      "63" : "Pavia",
      "64" : "Perugia",
      "65" : "Pesaro-e-Urbino",
      "66" : "Pescara",
      "67" : "Piacenza",
      "68" : "Pisa",
      "69" : "Pistoia",
      "70" : "Pordenone",
      "71" : "Potenza",
      "72" : "Prato",
      "73" : "Ragusa",
      "74" : "Ravenna",
      "75" : "Reggio di Calabria",
      "76" : "Reggio nell'Emilia",
      "77" : "Rieti",
      "78" : "Rimini",
      "79" : "Roma",
      "80" : "Rovigo",
      "81" : "Salerno",
      "82" : "Sassari",
      "83" : "Savona",
      "84" : "Siena",
      "85" : "Siracusa",
      "86" : "Sondrio",
      "87" : "Taranto",
      "88" : "Teramo",
      "89" : "Terni",
      "90" : "Torino",
      "91" : "Trapani",
      "92" : "Trento",
      "93" : "Treviso",
      "94" : "Trieste",
      "95" : "Udine",
      "96" : "Varese",
      "97" : "Venezia",
      "98" : "Verbano Cusio Ossola",
      "99" : "Vercelli",
      "100" : "Verona",
      "101" : "Vibo Valentia",
      "102" : "Vicenza",
      "103" : "Viterbo",
      "104" : "Monza e della Brianza",
      "105" : "Barletta - Andria - Trani",
      "107" : "Carbonia - Iglesias",
      "108" : "Fermo",
      "111" : "Medio Campidano",
      "112" : "Ogliastra",
      "113" : "Olbia - Tempio",
      "114" : "Estero",
      "115" : "Pola",
      "116" : "Sud Sardegna"
    }
  }, {
    "codiceDominio" : "DOM_NAZIONI",
    "valoriDominio" : {
      "1" : "Afghanistan",
      "2" : "Albania",
      "3" : "Algeria",
      "4" : "Andorra",
      "5" : "Angola",
      "6" : "Anguilla",
      "7" : "Antartide",
      "8" : "Antigua e Barbuda",
      "9" : "Antille Olandesi",
      "10" : "Arabia Saudita",
      "11" : "Argentina",
      "12" : "Armenia",
      "13" : "Aruba",
      "14" : "Australia",
      "15" : "Austria",
      "16" : "Azerbaigian",
      "17" : "Bahamas",
      "18" : "Bahrein",
      "19" : "Bangladesh",
      "20" : "Barbados",
      "21" : "Belgio",
      "22" : "Belize",
      "23" : "Benin",
      "24" : "Bermuda",
      "25" : "Bhutan",
      "26" : "Bielorussia",
      "27" : "Bolivia",
      "28" : "Bosnia ed Erzegovina",
      "29" : "Botswana",
      "30" : "Brasile",
      "31" : "Brunei",
      "32" : "Bulgaria",
      "33" : "Burkina Faso",
      "34" : "Burundi",
      "35" : "Cambogia",
      "36" : "Camerun",
      "37" : "Canada",
      "38" : "Capo Verde",
      "39" : "Ciad",
      "40" : "Cile",
      "41" : "Cina",
      "42" : "Hong Kong",
      "43" : "Macao",
      "44" : "Cipro",
      "45" : "Città del Vaticano",
      "46" : "Colombia",
      "47" : "Comore",
      "48" : "Congo",
      "49" : "Congo, Repubblica Dem. del",
      "50" : "Corea del sud",
      "51" : "Corea del Nord",
      "52" : "Costa d'Avorio",
      "53" : "Costa Rica",
      "54" : "Croazia",
      "55" : "Cuba",
      "56" : "Danimarca",
      "57" : "Dominica",
      "58" : "Ecuador",
      "59" : "Egitto",
      "60" : "El Salvador",
      "61" : "Emirati Arabi Uniti",
      "62" : "Eritrea",
      "63" : "Etiopia",
      "65" : "Fiji",
      "66" : "Filippine",
      "67" : "Finlandia",
      "68" : "Francia",
      "69" : "Gabon",
      "70" : "Gambia",
      "71" : "Georgia",
      "72" : "Georgia del Sud e Isole Sandwich",
      "73" : "Germania Repubblica Federale",
      "74" : "Ghana",
      "75" : "Giamaica",
      "76" : "Giappone",
      "77" : "Gibilterra",
      "78" : "Gibuti",
      "79" : "Giordania",
      "80" : "Gran Bretagna",
      "81" : "Grecia",
      "82" : "Grenada",
      "83" : "Groenlandia",
      "84" : "Guadalupa",
      "85" : "Guam",
      "86" : "Guatemala",
      "87" : "Guyana francese",
      "88" : "Guinea",
      "89" : "Guinea equatoriale",
      "90" : "Guinea Bissau",
      "91" : "Guyana",
      "92" : "Haiti",
      "93" : "Honduras",
      "94" : "India",
      "95" : "Indonesia",
      "96" : "Iran",
      "97" : "Iraq",
      "98" : "Irlanda",
      "99" : "Islanda",
      "100" : "Isola Bouvet",
      "101" : "Isole Cayman",
      "102" : "Isole Christmas",
      "103" : "Isole Cocos (Keeling)",
      "105" : "Isole Falkland (Malvine)",
      "106" : "Isole Faer Oer",
      "108" : "Isole Marianne Settentrionali",
      "109" : "Isole Marshall",
      "110" : "Isole minori degli Stati Uniti",
      "111" : "Isola Norfolk",
      "112" : "Isole Salomone",
      "113" : "Isole Svalbard e Jan Mayen",
      "114" : "Isole Turks e Caicos",
      "115" : "Isole Vergini (GB)",
      "116" : "Isole Vergini (USA)",
      "117" : "Isole Wallis e Futuna",
      "118" : "Israele",
      "119" : "Italia",
      "120" : "Iugoslavia",
      "121" : "Kazakistan",
      "122" : "Kenya",
      "123" : "Kirghizistan",
      "124" : "Kiribati",
      "125" : "Kuwait",
      "126" : "Laos",
      "127" : "Lesotho",
      "128" : "Lettonia",
      "129" : "Libano",
      "130" : "Liberia",
      "131" : "Libia",
      "132" : "Liechtenstein",
      "133" : "Lituania",
      "134" : "Lussemburgo",
      "135" : "Macedonia",
      "136" : "Madagascar",
      "137" : "Malawi",
      "138" : "Maldive",
      "139" : "Malaysia",
      "140" : "Mali",
      "141" : "Malta",
      "142" : "Marocco",
      "143" : "Martinica",
      "144" : "Mauritania",
      "145" : "Mauritius",
      "146" : "Mayotte",
      "147" : "Messico",
      "148" : "Micronesia",
      "149" : "Moldavia",
      "150" : "Monaco",
      "151" : "Mongolia",
      "152" : "Montserrat",
      "153" : "Mozambico",
      "154" : "Myanmar",
      "155" : "Namibia",
      "156" : "Nauru",
      "157" : "Nepal",
      "158" : "Nicaragua",
      "159" : "Niger",
      "160" : "Nigeria",
      "161" : "Niue",
      "162" : "Norvegia",
      "163" : "Nuova Caledonia",
      "164" : "Nuova Zelanda",
      "165" : "Oman",
      "166" : "Paesi Bassi",
      "167" : "Pakistan",
      "168" : "Palau",
      "169" : "Panama",
      "170" : "Papua Nuova Guinea",
      "171" : "Paraguay",
      "172" : "Perù",
      "173" : "Pitcairn",
      "174" : "Polinesia francese",
      "175" : "Polonia",
      "176" : "Portogallo",
      "177" : "Puerto Rico",
      "178" : "Qatar",
      "179" : "Rep. Ceca",
      "180" : "Repubblica Centrafricana",
      "181" : "Repubblica Dominicana",
      "182" : "Reunion",
      "183" : "Romania",
      "184" : "Ruanda",
      "185" : "Russia",
      "186" : "Sahara Occidentale",
      "187" : "Saint Kitts e Nevis",
      "188" : "Saint Lucia",
      "189" : "Saint Pierre e Miquelon",
      "190" : "Saint Vincent e Grenadine",
      "191" : "Samoa",
      "192" : "Samoa Americane",
      "193" : "San Marino",
      "194" : "Sant'Elena",
      "195" : "Sao Tome' e Principe",
      "196" : "Senegal",
      "197" : "Seychelles",
      "198" : "Sierra Leone",
      "199" : "Singapore",
      "200" : "Siria",
      "201" : "Slovacchia",
      "202" : "Slovenia",
      "203" : "Somalia",
      "204" : "Spagna",
      "205" : "Sri Lanka",
      "206" : "Stati Uniti",
      "207" : "Sudafrica",
      "208" : "Sudan",
      "209" : "Suriname",
      "210" : "Svezia",
      "211" : "Svizzera",
      "212" : "Swaziland",
      "213" : "Tagikistan",
      "214" : "Thailandia",
      "215" : "Taiwan",
      "216" : "Tanzania",
      "217" : "Territori australi francesi",
      "218" : "Timor Est",
      "219" : "Togo",
      "220" : "Tokelau",
      "221" : "Tonga",
      "222" : "Trinidad e Tobago",
      "223" : "Tunisia",
      "224" : "Turchia",
      "225" : "Turkmenistan",
      "226" : "Tuvalu",
      "227" : "Ucraina",
      "228" : "Uganda",
      "229" : "Ungheria",
      "230" : "Uruguay",
      "231" : "Uzbekistan",
      "232" : "Vanuatu",
      "233" : "Venezuela",
      "234" : "Vietnam",
      "235" : "Yemen",
      "236" : "Zambia",
      "237" : "Zimbabwe",
      "238" : "Cecoslovacchia",
      "239" : "Germania Repubblica Democratica",
      "240" : "Isola di Man",
      "241" : "Isole Normanne",
      "242" : "Urss",
      "245" : "Estonia",
      "246" : "Arabia Meridionale Federazione",
      "247" : "Arabia Meridionale Protettorato",
      "248" : "Territorio di Gaza",
      "249" : "Malesia",
      "250" : "Isole Ryukyu",
      "251" : "Sikkim",
      "252" : "Vietnam Del Sud",
      "253" : "Vietnam Del Nord",
      "255" : "Basutoland-Sud Africa Britannico",
      "256" : "Beciuania-Sud Africa Britannico",
      "257" : "Ifni",
      "258" : "Somalia Francese",
      "259" : "Tanganica",
      "260" : "Zanzibar",
      "261" : "Bophuthatswana",
      "262" : "Ciskei",
      "263" : "Antille Britanniche",
      "264" : "Panama Zona del Canale",
      "265" : "Isole Caroline",
      "266" : "Isole Cook",
      "267" : "Isole Gilbert e Ellice",
      "268" : "Irian Occidentale",
      "269" : "Isole Macquarie",
      "270" : "Isole Marcus",
      "271" : "Isole Midway",
      "272" : "Nuove Ebridi Condom. Franco-Ingl.",
      "273" : "Papuasia",
      "274" : "Isola di Pasqua",
      "275" : "Dipendenze Canadesi",
      "276" : "Dipendenze Russe",
      "277" : "Dipendenze Australiane",
      "278" : "Dipendenze Neozelandesi",
      "279" : "Dipendenze Sudafricane"
    }
  }, {
    "codiceDominio" : "DOM_CITTADINANZE",
    "valoriDominio" : {
      "1" : "Italiana",
      "2" : "UE",
      "3" : "Europea (Non UE)",
      "4" : "Altro",
      "5" : "USA"
    }
  }, {
    "codiceDominio" : "DOM_TITOLO_STUDIO",
    "valoriDominio" : {
      "1" : "Scuola dell'obbligo",
      "2" : "Scuola Superiore",
      "3" : "Laurea",
      "4" : "Laurea in discipline economico-finanziarie"
    }
  }, {
    "codiceDominio" : "DOM_SETTORE_OCCUPAZIONE_I",
    "valoriDominio" : {
      "1" : "Finanziario/Assicurativo",
      "2" : "Pubblica Amministrazione",
      "3" : "Servizi/Turismo",
      "4" : "Artigianato/Commercio",
      "5" : "Agricoltura",
      "6" : "Industria",
      "7" : "Altro"
    }
  }, {
    "codiceDominio" : "DOM_PROFESSIONE_ATTUALE",
    "valoriDominio" : { }
  }, {
    "codiceDominio" : "DOM_NATURA_RAPPORTO",
    "valoriDominio" : {
      "1" : "Investimento a breve termine",
      "2" : "Investimento a medio/lungo termine",
      "3" : "Finanziamento",
      "4" : "Altro"
    }
  }, {
    "codiceDominio" : "DOM_ORIGINE_VERSAMENTO",
    "valoriDominio" : {
      "1" : "001 Pensione",
      "2" : "002 Reddito lavoro autonomo",
      "3" : "003 Reddito lavoro dipendente",
      "4" : "004 Risparmio",
      "5" : "005 Successione",
      "6" : "006 Donazione",
      "7" : "007 Disinvestimento",
      "8" : "008 Vincita",
      "9" : "009 Liquidazione premi",
      "10" : "010 Vendita beni immobili",
      "11" : "011 Vendita beni mobili",
      "12" : "012 Vendita/acquisto società",
      "13" : "013 Incasso da Clienti"
    }
  }, {
    "codiceDominio" : "DOM_TIPO_DOCUMENTO",
    "valoriDominio" : {
      "1" : "Carta d'identità",
      "2" : "Patente",
      "3" : "Passaporto",
      "4" : "Tesserino Ministeriale",
      "5" : "DocumentiMigrazione",
      "6" : "Altro"
    }
  }, {
    "codiceDominio" : "DOM_CAP",
    "valoriDominio" : {
      "92100" : "166009"
    }
  }, {
    "codiceDominio" : "DOM_COMUNE",
    "valoriDominio" : {
      "166009" : "Agrigento",
      "827907" : "Alessandria Della Rocca",
      "828004" : "Aragona",
      "828103" : "Bivona",
      "828202" : "Burgio",
      "828301" : "Calamonaci",
      "828400" : "Caltabellotta",
      "828509" : "Camastra",
      "828608" : "Cammarata",
      "828707" : "Campobello Di Licata",
      "828806" : "Canicatti",
      "828905" : "Casteltermini",
      "829002" : "Castrofilippo",
      "829101" : "Cattolica Eraclea",
      "829200" : "Cianciana",
      "829309" : "Favara",
      "829408" : "Grotte",
      "829507" : "Joppolo Giancaxio",
      "829606" : "Lampedusa E Linosa",
      "829705" : "Licata",
      "829804" : "Lucca Sicula",
      "829903" : "Menfi",
      "830000" : "Montallegro",
      "830109" : "Montevago",
      "830208" : "Naro",
      "830307" : "Palma Di Montechiaro",
      "830406" : "Porto Empedocle",
      "830505" : "Racalmuto",
      "830604" : "Raffadali",
      "830703" : "Ravanusa",
      "830802" : "Realmonte",
      "830901" : "Ribera",
      "831008" : "Sambuca Di Sicilia",
      "831107" : "San Biagio Platani",
      "831206" : "San Giovanni Gemini",
      "831305" : "Santa Elisabetta",
      "831404" : "Santa Margherita Di Belice",
      "831503" : "Sant Angelo Muxaro",
      "831602" : "Santo Stefano Quisquina",
      "831701" : "Sciacca",
      "831800" : "Siculiana",
      "831909" : "Villafranca Sicula",
      "832006" : "Comitini"
    }
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Genera una OTP e la inoltra via e-mail
 *
 * no response value expected for this operation
 **/
exports.sendEmail = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Genera una OTP e la inoltra via SMS
 *
 * no response value expected for this operation
 **/
exports.sendSms = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Genera il documento da far firmare al cliente a partire dal template e dai dati da inserire dinamicamente
 *
 * no response value expected for this operation
 **/
exports.utilityDocfirmaGeneratePOST = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Restituisce il documento inoltrato a Cesam a seguito della generazione
 *
 * id String 
 * no response value expected for this operation
 **/
exports.utilityDocfirmaGetIdGET = function(id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Restituisce l'elenco di tutti i documenti relativi al servizio di consulenza o investimento da firmare
 *
 * no response value expected for this operation
 **/
exports.utilityDocfirmaListGET = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Inoltra a Cesam il documento generato
 *
 * no response value expected for this operation
 **/
exports.utilityDocfirmaSavePOST = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Inoltra a Namirial i documenti che devono essere firmati. Permette inoltre di recuperare informazioni per atterrare nella sezione dedicata alla firma dei documenti di una pratica
 *
 * idIncarico String 
 * returns DocFirmaResponse
 **/
exports.utilityDocfirmaSignPOST = function(idIncarico) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "listaDocumenti" : [ "Modulo apertura rapporti", "Contratto unico servizi digitali", "Opuscolo informativo" ],
  "listaClienti" : [ {
    "nomeCliente" : "Mario Rossi",
    "codiceFiscale" : "MARRRRRRRRRR",
    "stato" : "FIRMATO",
    "urlIframe" : "http://localhost:3000/Namirial"
  }, {
    "nomeCliente" : "Tommaso Farinetti",
    "codiceFiscale" : "MARRRRRRRRRR",
    "stato" : "DA_FIRMARE",
    "urlIframe" : "http://localhost:3000/Namirial"
  } ],
  "statusComplessivo" : "DA_FIRMARE"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Verifica se l'OTP inserita dall'utente corrisponde a quella inoltrata via e-mail
 *
 * no response value expected for this operation
 **/
exports.verifyEmail = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Verifica se l'OTP inserita dall'utente corrisponde a quella inoltrata via SMS
 *
 * no response value expected for this operation
 **/
exports.verifySms = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

