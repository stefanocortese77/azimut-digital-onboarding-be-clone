'use strict';


/**
 * Servizio per l'autenticazione del consulente finanziario tramite user e password
 *
 * no response value expected for this operation
 **/
exports.authLoginPOST = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Rimuove il token di autenticazione per il consulente finanziario corrente
 *
 * no response value expected for this operation
 **/
exports.authLogoutPOST = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Genera un nuovo token di autenticazione, da utilizzare a seguito della scadenza di quello precedente
 *
 * no response value expected for this operation
 **/
exports.authRefreshtokenPOST = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

