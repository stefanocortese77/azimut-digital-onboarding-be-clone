import axios from 'axios'

let token = ''
const urlMatches = /token=([a-zA-Z0-9_.\-~]*)(&.*)*$/.exec(document.location.href)
if (urlMatches && urlMatches[1] && urlMatches[1].length) {
  token = urlMatches[1]
} else {
  console.error('Authorizaytion token is empty!')
}

axios.defaults.baseURL = 'http://35.187.186.117/api'
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json, text/plain, */*'
axios.defaults.headers.common['Pragma'] = 'no-cache'
axios.defaults.headers.common['Cache-Control'] = 'no-cache'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'http://35.187.186.117'

