import axios from 'axios'

import { defaultHeaders } from './../apiConfig'

export async function get () {
  try {
    const response = await axios({
      url: `/token`,
      method: 'get',
      headers: {
        ...defaultHeaders,
      },
      data: { },
    })
    const { data } = response

    if (response.status !== 200) {
      return { response: data.response, error: data }
    }

    // only proceed once second promise is resolved
    return { response: data }
  } catch (error) {
    return { response: error.response, error }
  }
}

export async function alwaysFail () {
  return {
    error: {
      message: 'Si è verificato un errore',
    },
  }
}
