import * as actionTypes from './constants'

import axios from 'axios'

export function customerCreate (value) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.CUSTOMER_CREATE_START
    })
    try {
      console.log(axios.defaults.headers)
      const response = await axios.get(`/customer/create/?codFiscale=${value}`, {})
      dispatch({
        type: actionTypes.CUSTOMER_CREATE_SUCCESS,
        data: response.data
      }
    )
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.CUSTOMER_CREATE_ERROR,
        error: error
      })
    };
  }
}

export function setCodiceFiscale (cf) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.CUSTOMER_SET_CF,
      data: cf
    })
  }
}

export function setIdCustomer (idCustomer) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.ID_CUSTOMER_SET,
      data: idCustomer
    })
  }
}
export function setIdIncarico (IdIncarico) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.ID_INCARICO_SET,
      data: IdIncarico
    })
  }
}

function dataRegistryCF (codiceFiscale) {
  try {
    return axios.get(`/customer/get/dataRegistryCF/${codiceFiscale}`, {})
  } catch (error) {
    const newError = new Error(error.message)
    newError.apiName = 'dataRegistryCF'
    throw newError
  }
}

function dataRegistry (idCliente, idIncarico, active) {
  if (!active) {
    return new Promise(function (resolve, reject) {
      resolve({ data: {} })
    })
  }
  try {
    return axios.get(`/customer/${idCliente}/dataRegistry/?idIncarico=${idIncarico}`, {})
  } catch (error) {
    const newError = new Error(error.message)
    newError.apiName = 'dataRegistry'
    throw newError
  }
}

function getFatcacrs (idCliente, idIncarico, active) {
  if (!active) {
    return new Promise(function (resolve, reject) {
      resolve({ data: {} })
    })
  }
  try {
    return axios.get(`/customer/${idCliente}/fatcacrs/?idIncarico=${idIncarico}`, {})
  } catch (error) {
    const newError = new Error(error.message)
    newError.apiName = 'fatcacrs'
    throw newError
  }
}

export function getDataRegistry (codiceFiscale, idCliente, idIncarico, dataRegistryBool, fatcacrsBool) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_DATA_REGISTRY_START
    })
    try {
      const [dataCF, dataTMPL, dataFacta] = await Promise.all([dataRegistryCF(codiceFiscale), dataRegistry(idCliente, idIncarico, dataRegistryBool), getFatcacrs(idCliente, idIncarico, fatcacrsBool)])
      dispatch({
        type: actionTypes.GET_DATA_REGISTRY_SUCCESS,
        dataCF: dataCF.data,
        dataTMPL: dataTMPL.data,
        dataFacta: dataFacta.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.GET_DATA_REGISTRY_ERROR,
        error: error
      })
    };
  }
}

export function saveDataRegistry (queryParams, idCliente, idIncarico, active) {
  return async function (dispatch, getState) {
    const query = {}
    dispatch({
      type: actionTypes.POST_INFORMAZIONI_PERSONALI_START,
      query: query
    })
    try {
      const response = await axios({
        method:(getState().customers.dataRegistryStart || active) ? 'PUT' : 'POST',
        url: `/customer/${idCliente}/dataRegistry/?idIncarico=${idIncarico}`,
        data: queryParams
      })
      dispatch({
        type: actionTypes.POST_INFORMAZIONI_PERSONALI_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_INFORMAZIONI_PERSONALI_ERROR,
        error: error
      })
    };
  }
}

// function dataSummaryLight (idCliente) {
//   try {
//     return axios.get(`/customer/dataSummaryLight/${idCliente}`, {})
//   } catch (error) {
//     const newError = new Error(error.message)
//     newError.apiName = 'dataSummaryLight'
//     throw newError
//   }
// }


export function getDataSummaryLight (idCliente, idIncarico) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_SUMMARY_DATA_START
    })
    try {
      const response = await axios.get(`/customer/dataSummaryLight/${idCliente}?idIncarico=${idIncarico}`, {})

      dispatch({
        type: actionTypes.GET_SUMMARY_DATA_SUCCESS,
        data: response.data
      }
    )
    } catch (error) {
      console.log('error summary', error)
      dispatch({
        type: actionTypes.GET_SUMMARY_DATA_ERROR,
        error: error
      })
    };
  }
}

export function getDataSummaryFull (idCliente, idIncarico) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_SUMMARY_DATA_FULL_START
    })
    try {
      const response = await axios.get(`/customer/dataSummaryFull/${idCliente}?idIncarico=${idIncarico}`, {})

      dispatch({
        type: actionTypes.GET_SUMMARY_DATA_FULL_SUCCESS,
        data: response.data
      }
    )
    } catch (error) {
      console.log('error summary', error)
      dispatch({
        type: actionTypes.GET_SUMMARY_DATA_FULL_ERROR,
        error: error
      })
    };
  }
}
//
// function idCardDataAllegati (idCliente, idIncarico, active) {
//   if (active === false) {
//     const dataAllegati = {
//       data:{
//         'idIncarico': idIncarico,
//         'listaAllegati': [
//           {
//             'file': '',
//             'tipoAllegato': ''
//           }
//         ]
//       }
//     }
//     return new Promise(function (resolve, reject) {
//       resolve(dataAllegati)
//     })
//   }
//   try {
//     return axios.get(`/customer/${idCliente}/idCardDataAllegati/?idIncarico=${idIncarico}`, {})
//   } catch (error) {
//     const newError = new Error(error.message)
//     newError.apiName = 'idCardDataAllegati'
//     throw newError
//   }
// }
//
// function idCardData (idCliente, idIncarico, active) {
//   if (active === false) {
//     const data = {
//       data:{
//         'idIncarico': idIncarico,
//         'numeroDocumenti': '',
//         'flagCFRetro': 'false',
//         'flagCFFronte': 'false',
//         'flagDOCRetro': 'false',
//         'flagDOCFronte': 'false',
//
//         'provinciaRilascio': {
//           'codiceDominio': 'DOM_PROVINCE',
//           'chiave': 0
//         },
//         'comuneRilascio': {
//           'codiceDominio': 'DOM_COMUNE',
//           'chiave': 0
//         },
//         'comuneEsteroRilascio': {
//           'codiceDominio': 'DOM_COMUNE',
//           'chiave': 0
//         },
//         'nazioneRilascio': {
//           'codiceDominio': 'DOM_NAZIONI',
//           'chiave': 0
//         },
//         'dataScadenza': '',
//         'dataRilascio': '',
//         'tipoDocumenti': {
//           'codiceDominio': 'DOM_TIPO_DOCUMENTO',
//           'chiave': 0
//         }
//       }
//     }
//     return new Promise(function (resolve, reject) {
//       resolve(data)
//     })
//   }
//   try {
//     return axios.get(`/customer/${idCliente}/idCardData/?idIncarico=${idIncarico}`, {})
//   } catch (error) {
//     const newError = new Error(error.message)
//     newError.apiName = 'idCardData'
//     throw newError
//   }
// }
//
// export function getDocumentsDataAndAllegati (idCliente, idIncarico, idCardDataActive, idCardDataAllegatiActive) {
//   return async function (dispatch) {
//     dispatch({
//       type: actionTypes.GET_DOCUMENTS_DATA_START
//     })
//     try {
//       const [dataIdCardAllegati, dataIdCard] = await Promise.all([idCardDataAllegati(idCliente, idIncarico, idCardDataAllegatiActive), idCardData(idCliente, idIncarico, idCardDataActive)])
//       dispatch({
//         type: actionTypes.GET_DOCUMENTS_DATA_SUCCESS,
//         dataIdCardAllegati: dataIdCardAllegati.data,
//         dataIdCard: dataIdCard.data
//       })
//     } catch (error) {
//       console.log(error)
//       dispatch({
//         type: actionTypes.GET_DOCUMENTS_DATA_ERROR,
//         error: error
//       })
//     };
//   }
// }

export function getDocumentsData (idCliente, idIncarico, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_DOCUMENTS_DATA_START
    })
    try {
      if (active === false) {
        const dataDefault = {
          'idIncarico': idIncarico,
          'flagCFRetro': false,
          'flagCFFronte': false,
          'flagDOCRetro': false,
          'flagDOCFronte': false,
          'provinciaRilascio': {
            'codiceDominio': 'DOM_PROVINCE',
            'chiave': 0
          },
          'comuneRilascio': {
            'codiceDominio': 'DOM_COMUNE',
            'chiave': 0
          },
          'nazioneRilascio': {
            'codiceDominio': 'DOM_NAZIONI',
            'chiave': 0
          },
          'numeroDocumenti': '',
          'dataScadenza': '',
          'dataRilascio': '',
          'tipoDocumenti': {
            'codiceDominio': 'DOM_TIPO_DOCUMENTO',
            'chiave': 4
          }
        }
        // set timeout to intercept fetching
        setTimeout(() => {
          dispatch({
            type: actionTypes.GET_DOCUMENTS_DATA_SUCCESS,
            dataIdCard: dataDefault
          })
        }, 500)
        return
      }

      const response = await axios.get(`/customer/${idCliente}/idCardData/?idIncarico=${idIncarico}`, {})
      dispatch({
        type: actionTypes.GET_DOCUMENTS_DATA_SUCCESS,
        dataIdCard: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.GET_DOCUMENTS_DATA_ERROR,
        error: error
      })
    };
  }
}

// export function postDocumentsScreenshot (idCliente, idIncarico, query, active) {
//   return async function (dispatch) {
//     dispatch({
//       type: actionTypes.POST_DOCUMENTS_SCREEN_START,
//       query: query
//     })
//     try {
//       let response
//       if (active === true) {
//         response = await axios.put(`/customer/${idCliente}/idCardDataAllegati/?idIncarico=${idIncarico}`, query)
//       } else if (active === false) {
//         response = await axios.post(`/customer/${idCliente}/idCardDataAllegati/?idIncarico=${idIncarico}`, query)
//       }
//
//       dispatch({
//         type: actionTypes.POST_DOCUMENTS_SCREEN_SUCCESS,
//         data: response.data
//       })
//     } catch (error) {
//       console.log(error)
//       dispatch({
//         type: actionTypes.POST_DOCUMENTS_SCREEN_ERROR,
//         error: error
//       })
//     };
//   }
// }
//
// function promiseDocumentsData (idCliente, idIncarico, query, active) {
//   try {
//     if (active === false) {
//       return axios.post(`/customer/${idCliente}/idCardData/?idIncarico=${idIncarico}`, query)
//     } else if (active === true) {
//       return axios.put(`/customer/${idCliente}/idCardData/?idIncarico=${idIncarico}`, query)
//     }
//   } catch (error) {
//     const newError = new Error(error.message)
//     newError.apiName = 'idCardData'
//     throw newError
//   }
// }
//
// function promiseDocumentsAllegati (idCliente, idIncarico, query, active) {
//   try {
//     if (active === false) {
//       return axios.post(`/customer/${idCliente}/idCardDataAllegati/?idIncarico=${idIncarico}`, query)
//     } else if (active === true) {
//       return axios.put(`/customer/${idCliente}/idCardDataAllegati/?idIncarico=${idIncarico}`, query)
//     }
//   } catch (error) {
//     const newError = new Error(error.message)
//     newError.apiName = 'idCardDataAllegati'
//     throw newError
//   }
// }
//
// export function postDocumentsDataAndFiles (idCliente, idIncarico, queryData, queryFile, idCardActive, idCardAllegatiActive) {
//   return async function (dispatch) {
//     dispatch({
//       type: actionTypes.POST_DOCUMENTS_DATA_AND_FILE_START,
//       queryData: queryData,
//       queryFile: queryFile
//     })
//     try {
//       const [dataFilePosted, dataPosted] = await Promise.all([promiseDocumentsAllegati(idCliente, idIncarico, queryFile, idCardAllegatiActive), promiseDocumentsData(idCliente, idIncarico, queryData, idCardActive)])
//       dispatch({
//         type: actionTypes.POST_DOCUMENTS_DATA_AND_FILE_SUCCESS,
//         dataFilePosted: dataFilePosted.data,
//         dataPosted: dataPosted.data
//       })
//     } catch (error) {
//       dispatch({
//         type: actionTypes.POST_DOCUMENTS_DATA_AND_FILE_ERROR,
//         error: error
//       })
//     };
//   }
// }

export function postIdCardDataAllegatiCF (idCliente, query, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.POST_DATA_ALLEGATI_CF_START,
      query: query
    })
    try {
      let response
      if (active === false) {
        response = await axios.post(`/customer/${idCliente}/idCardDataAllegati`, query)
      } else if (active === true) {
        response = await axios.put(`/customer/${idCliente}/idCardDataAllegati`, query)
      }

      dispatch({
        type: actionTypes.POST_DATA_ALLEGATI_CF_SUCCESS,
        dataSuccessFlags: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_DATA_ALLEGATI_CF_ERROR,
        error: error
      })
    };
  }
}

export function postIdCardDataAllegatiDOC (idCliente, query, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.POST_DATA_ALLEGATI_DOC_START,
      query: query
    })
    try {
      let response
      if (active === false) {
        response = await axios.post(`/customer/${idCliente}/idCardDataAllegati`, query)
      } else if (active === true) {
        response = await axios.put(`/customer/${idCliente}/idCardDataAllegati`, query)
      }

      dispatch({
        type: actionTypes.POST_DATA_ALLEGATI_DOC_SUCCESS,
        dataSuccessFlags: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_DATA_ALLEGATI_DOC_ERROR,
        error: error
      })
    };
  }
}

export function postIdCardData (idCliente, query, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.POST_DATA_DOCUMENTO_START,
      query: query
    })
    try {
      let response
      if (active === false) {
        response = await axios.post(`/customer/${idCliente}/idCardData`, query)
      } else if (active === true) {
        response = await axios.put(`/customer/${idCliente}/idCardData`, query)
      }

      dispatch({
        type: actionTypes.POST_DATA_DOCUMENTO_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_DATA_DOCUMENTO_ERROR,
        error: error
      })
    };
  }
}

export function postIdCardDataHide (idCliente, query, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.POST_DATA_DOCUMENTO_HIDE_START,
      dataPreviousPosted: query
    })
    try {
      let response
      if (active === false) {
        response = await axios.post(`/customer/${idCliente}/idCardData`, query)
      } else if (active === true) {
        response = await axios.put(`/customer/${idCliente}/idCardData`, query)
      }

      dispatch({
        type: actionTypes.POST_DATA_DOCUMENTO_HIDE_SUCCESS,
        dataPreviousPosted: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_DATA_DOCUMENTO_HIDE_ERROR,
        error: error
      })
    };
  }
}

export function getServiziDigitali (idCliente, idIncarico, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_SERVIZI_DIGITALI_START
    })
    try {
      if (active === false) {
        const dataDefault = {
          'idIncarico': idIncarico
        }
        // set timeout to intercept fetching
        setTimeout(() => {
          dispatch({
            type: actionTypes.GET_SERVIZI_DIGITALI_SUCCESS,
            data: dataDefault
          })
        }, 500)
        return
      }

      const response = await axios.get(`/customer/${idCliente}/servdig/?idIncarico=${idIncarico}`, {})
      dispatch({
        type: actionTypes.GET_SERVIZI_DIGITALI_SUCCESS,
        data: response.data
      }
    )
    } catch (error) {
      console.log('error summary', error)
      dispatch({
        type: actionTypes.GET_SERVIZI_DIGITALI_ERROR,
        error: error
      })
    };
  }
}

export function postServiziDigitali (idCliente, query, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.POST_SERVIZI_DIGITALI_START,
      query: query
    })
    try {
      let response
      if (active === true) {
        response = await axios.put(`/customer/${idCliente}/servdig`, query)
      } else if (active === false) {
        response = await axios.post(`/customer/${idCliente}/servdig`, query)
      }

      dispatch({
        type: actionTypes.POST_SERVIZI_DIGITALI_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_SERVIZI_DIGITALI_ERROR,
        error: error
      })
    };
  }
}

export function postFatca (idCliente, idIncarico, listaDomande) {
  return async function (dispatch, getState) {
    dispatch({
      type: actionTypes.POST_FATCA_START,
    })
    try {
      const response = await axios({
        method: getState().customers.fatcaSent ? 'PUT' : 'POST',
        url: `/customer/${idCliente}/fatcacrs`,
        data: {
          idIncarico,
          listaDomande
        }
      })
      dispatch({
        type: actionTypes.POST_FATCA_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.POST_FATCA_ERROR,
        error: error
      })
    };
  }
}

export function getCustomerList () {
  return async function (dispatch) {
    const query = {}
    dispatch({
      type: actionTypes.GET_LISTA_CLIENTI_START,
      query: query
    })
    try {
      const response = await axios.get(`/customer/list`, query)
      dispatch({
        type: actionTypes.GET_LISTA_CLIENTI_SUCCESS,
        customerListData: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.GET_LISTA_CLIENTI_ERROR,
        error: error
      })
    };
  }
}
