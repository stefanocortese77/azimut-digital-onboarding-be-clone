export const CUSTOMER_CREATE_START = 'CUSTOMER_CREATE_START'
export const CUSTOMER_CREATE_SUCCESS = 'CUSTOMER_CREATE_SUCCESS'
export const CUSTOMER_CREATE_ERROR = 'CUSTOMER_CREATE_ERROR'

export const GET_INFORMAZIONI_PERSONALI_START = 'GET_INFORMAZIONI_PERSONALI_START'
export const GET_INFORMAZIONI_PERSONALI_SUCCESS = 'GET_INFORMAZIONI_PERSONALI_SUCCESS'
export const GET_INFORMAZIONI_PERSONALI_ERROR = 'GET_INFORMAZIONI_PERSONALI_ERROR'

export const POST_INFORMAZIONI_PERSONALI_START = 'POST_INFORMAZIONI_PERSONALI_START'
export const POST_INFORMAZIONI_PERSONALI_SUCCESS = 'POST_INFORMAZIONI_PERSONALI_SUCCESS'
export const POST_INFORMAZIONI_PERSONALI_ERROR = 'POST_INFORMAZIONI_PERSONALI_ERROR'

export const GET_DATA_REGISTRY_START = 'GET_DATA_REGISTRY_START'
export const GET_DATA_REGISTRY_SUCCESS = 'GET_DATA_REGISTRY_SUCCESS'
export const GET_DATA_REGISTRY_ERROR = 'GET_DATA_REGISTRY_ERROR'
export const CUSTOMER_SET_CF = 'CUSTOMER_SET_CF'
export const ID_INCARICO_SET = 'ID_INCARICO_SET'
export const ID_CUSTOMER_SET = 'ID_CUSTOMER_SET'

export const GET_SUMMARY_DATA_START = 'GET_SUMMARY_DATA_START'
export const GET_SUMMARY_DATA_SUCCESS = 'GET_SUMMARY_DATA_SUCCESS'
export const GET_SUMMARY_DATA_ERROR = 'GET_SUMMARY_DATA_ERROR'

export const GET_SUMMARY_DATA_FULL_START = 'GET_SUMMARY_DATA_FULL_START'
export const GET_SUMMARY_DATA_FULL_SUCCESS = 'GET_SUMMARY_DATA_FULL_SUCCESS'
export const GET_SUMMARY_DATA_FULL_ERROR = 'GET_SUMMARY_DATA_FULL_ERROR'

export const GET_DOCUMENTS_DATA_START = 'GET_DOCUMENTS_DATA_START'
export const GET_DOCUMENTS_DATA_SUCCESS = 'GET_DOCUMENTS_DATA_SUCCESS'
export const GET_DOCUMENTS_DATA_ERROR = 'GET_DOCUMENTS_DATA_ERROR'

export const POST_DOCUMENTS_DATA_START = 'POST_DOCUMENTS_DATA_START'
export const POST_DOCUMENTS_DATA_SUCCESS = 'POST_DOCUMENTS_DATA_SUCCESS'
export const POST_DOCUMENTS_DATA_ERROR = 'POST_DOCUMENTS_DATA_ERROR'

export const POST_DOCUMENTS_SCREEN_START = 'POST_DOCUMENTS_SCREEN_START'
export const POST_DOCUMENTS_SCREEN_SUCCESS = 'POST_DOCUMENTS_SCREEN_SUCCESS'
export const POST_DOCUMENTS_SCREEN_ERROR = 'POST_DOCUMENTS_SCREEN_ERROR'

export const POST_DATA_ALLEGATI_CF_START = 'POST_DATA_ALLEGATI_CF_START'
export const POST_DATA_ALLEGATI_CF_SUCCESS = 'POST_DATA_ALLEGATI_CF_SUCCESS'
export const POST_DATA_ALLEGATI_CF_ERROR = 'POST_DATA_ALLEGATI_CF_ERROR'

export const POST_DATA_ALLEGATI_DOC_START = 'POST_DATA_ALLEGATI_DOC_START'
export const POST_DATA_ALLEGATI_DOC_SUCCESS = 'POST_DATA_ALLEGATI_DOC_SUCCESS'
export const POST_DATA_ALLEGATI_DOC_ERROR = 'POST_DATA_ALLEGATI_DOC_ERROR'

export const POST_DATA_DOCUMENTO_HIDE_START = 'POST_DATA_DOCUMENTO_HIDE_START'
export const POST_DATA_DOCUMENTO_HIDE_SUCCESS = 'POST_DATA_DOCUMENTO_HIDE_SUCCESS'
export const POST_DATA_DOCUMENTO_HIDE_ERROR = 'POST_DATA_DOCUMENTO_HIDE_ERROR'

export const POST_DATA_DOCUMENTO_START = 'POST_DATA_DOCUMENTO_START'
export const POST_DATA_DOCUMENTO_SUCCESS = 'POST_DATA_DOCUMENTO_SUCCESS'
export const POST_DATA_DOCUMENTO_ERROR = 'POST_DATA_DOCUMENTO_ERROR'

export const POST_DOCUMENTS_DATA_AND_FILE_START = 'POST_DOCUMENTS_DATA_AND_FILE_START'
export const POST_DOCUMENTS_DATA_AND_FILE_SUCCESS = 'POST_DOCUMENTS_DATA_AND_FILE_SUCCESS'
export const POST_DOCUMENTS_DATA_AND_FILE_ERROR = 'POST_DOCUMENTS_DATA_AND_FILE_ERROR'

export const GET_SERVIZI_DIGITALI_START = 'GET_SERVIZI_DIGITALI_START'
export const GET_SERVIZI_DIGITALI_SUCCESS = 'GET_SERVIZI_DIGITALI_SUCCESS'
export const GET_SERVIZI_DIGITALI_ERROR = 'GET_SERVIZI_DIGITALI_ERROR'

export const POST_SERVIZI_DIGITALI_START = 'POST_SERVIZI_DIGITALI_START'
export const POST_SERVIZI_DIGITALI_SUCCESS = 'POST_SERVIZI_DIGITALI_SUCCESS'
export const POST_SERVIZI_DIGITALI_ERROR = 'POST_SERVIZI_DIGITALI_ERROR'

export const POST_FATCA_START = 'POST_FATCA_START'
export const POST_FATCA_SUCCESS = 'POST_FATCA_SUCCESS'
export const POST_FATCA_ERROR = 'POST_FATCA_ERROR'

export const GET_LISTA_CLIENTI_START = 'GET_LISTA_CLIENTI_START'
export const GET_LISTA_CLIENTI_SUCCESS = 'GET_LISTA_CLIENTI_SUCCESS'
export const GET_LISTA_CLIENTI_ERROR = 'GET_LISTA_CLIENTI_ERROR'
