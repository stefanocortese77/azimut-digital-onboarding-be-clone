import * as actionTypes from './constants'

const initialState = {

  customers: [

  ],

  customer:{
    'statusResponse': {
      'code': null,
      'description': null
    },
    'idCliente': 123, // TODO: remove
    'idIncarico': 123, // TODO: remove
    'codiceFiscale': 'aaaa', // TODO: remove
    'description': null,
    'dataNascita': null,
    'codStat': null,
    'codProvincia': null,
    'codComune': null,
    'cap': null,
    'sesso': null,
    'nome': null,
    'cognome': null,
    'indirizzoResidenza': null,
    'civicoResidenza': null,
    'capResidenza':null,
    'codNazioneResidenza': null,
    'codProvinciaResidenza': null,
    'codComuneResidenza': null,
    'prefissoCellulare': null,
    'cellulare': null,
    'email': null,
    'cittadinanza':[
    ],
    'codCittadinanza': null,
    'codAltraCittadinanza': null,
    'listaResidenzaFiscale': null,
    'caricaRicoperta': null,
    'dataInizioCarica': null,
    'annoInizioAttivita': null,
    'titolareEffettivo': null,
    'CodNaturaRapporto': null,
    'consensoPunto5': null,
    'consensoPunto2': null,
    'altro': null,
    'InAmbitoExtraUE': null,
    'pubblicaAmministrazione': null,
    'codNazione': null,
    'codProfessione': null,
    'rapportoPEPestero': null,
    'PEPnazionale': null,
    'fondiPubblici': null,
    'fonteReddito': null,
    'inAmbitoIstituzionale': null,
    'codSettoreOccupazione': null,
    'indirizzoInvioComunicazioni': null,
    'civicoInvioComunicazioni': null,
    'codProvinciaInvioComunicazioni': null,
    'codComuneInvioComunicazioni': null,
    'capInvioComunicazioni': null,
    'codNazioneInvioComunicazioni': null
  },
  documentsData: {
    tipoDocumenti:{
      chiave:null
    } },
  summaryData: null,
  summaryDataFull: null,
  serviziData: {
    'idIncarico':'',
    'flagCollocamento': false,
    'flagConsulenzaCollocamento': true,
    'immagineFirma': '',
    'immagineFirmaThumb':'',
    'flagImg':false,
    'cellulare':'',
    'mail':''
  },
  dataRegistryStart: false,
  fatcaSent: false,
  customerCreateRequest:{
    fetching: false,
    error: null
  },
  getDataSummaryRequest:{
    fetching: false,
    error: null
  },
  getDataSummaryFullRequest:{
    fetching: false,
    error: null
  },
  getDataRegistryRequest:{
    fetching: false,
    error: null
  },
  getDocumentsDataRequest:{
    fetching: false,
    error: null
  },
  // postDocumentsDataRequest:{
  //   fetching: false,
  //   error: null
  // },
  // postDocumentsScreenRequest:{
  //   fetching: false,
  //   error: null
  // },
  postDataAllegatiCFRequest:{
    fetching: false,
    error: null
  },
  postDataAllegatiDOCRequest:{
    fetching: false,
    error: null
  },
  postDataDocumentoRequest:{
    fetching: false,
    error: null
  },
  postDataDocumentoHideRequest: {
    fetching: false,
    error: null
  },
  // postDocumentsDataAndFilesRequest:{
  //   fetching: false,
  //   error: null
  // },
  getServiziDigitaliRequest:{
    fetching: false,
    error: null
  },
  postServiziDigitaliRequest:{
    fetching: false,
    error: null
  },
  postInfoPersonaliRequest:{
    fetching: false,
    error: null
  },
  postFatcaRequest:{
    fetching: false,
    error: null
  },
  getClientiListRequest: {
    fetching: false,
    error: null
  }

} // TODO: change to reducer specific logic

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.CUSTOMER_CREATE_SUCCESS :

      const customer = Object.assign({}, state.customer, action.data)
      return Object.assign({}, state, { customer:{ ...customer },
        customerCreateRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.CUSTOMER_CREATE_START :
      return Object.assign({}, state, { customerCreateRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.CUSTOMER_CREATE_ERROR :
      return Object.assign({}, state, { customerCreateRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_DATA_REGISTRY_SUCCESS :

      const customerDataCF = Object.assign(action.dataCF, action.dataTMPL, action.dataFacta)
      const customerDataRegistry = Object.assign({}, state.customer, customerDataCF)

      return Object.assign({}, state, {
        customer:{ ...customerDataRegistry },
        dataRegistry: action.dataCF,
        getDataRegistryRequest:{
          fetching: false,
          error: null
        }
      })
    case actionTypes.GET_DATA_REGISTRY_START :
      return Object.assign({}, state, { getDataRegistryRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_DATA_REGISTRY_ERROR :
      return Object.assign({}, state, { getDataRegistryRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.CUSTOMER_SET_CF :
      const customerSet = Object.assign({}, state.customer, { codiceFiscale: action.data })
      return Object.assign({}, state, {
        customer: customerSet
      })

    case actionTypes.ID_CUSTOMER_SET :
      const idClienteSet = Object.assign({}, state.customer, { idCliente: action.data })
      return Object.assign({}, state, {
        customer: idClienteSet
      })

    case actionTypes.ID_INCARICO_SET :
      const idIncaricoSet = Object.assign({}, state.customer, { idIncarico: action.data })
      return Object.assign({}, state, {
        customer: idIncaricoSet
      })
    case actionTypes.GET_SUMMARY_DATA_SUCCESS :
      const summaryData = Object.assign({}, state.summaryData, action.data)
      return Object.assign({}, state, { summaryData:{ ...summaryData },
        getDataSummaryRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.GET_SUMMARY_DATA_START :
      return Object.assign({}, state, { getDataSummaryRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_SUMMARY_DATA_ERROR :
      return Object.assign({}, state, { getDataSummaryRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_SUMMARY_DATA_FULL_SUCCESS :
      const summaryDataFull = Object.assign({}, state.summaryDataFull, action.data)
      return Object.assign({}, state, { summaryDataFull:{ ...summaryDataFull },
        getDataSummaryFullRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.GET_SUMMARY_DATA_FULL_START :
      return Object.assign({}, state, { getDataSummaryFullRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_SUMMARY_DATA_FULL_ERROR :
      return Object.assign({}, state, { getDataSummaryFullRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_DOCUMENTS_DATA_SUCCESS :
      // const dataIdCard = {}
      // dataIdCard.data = action.dataIdCard
      // const responseDocumentsData = Object.assign(action.dataIdCardAllegati, dataIdCard)
      // const customerDocumentsData = Object.assign({}, state.documentsData, responseDocumentsData)
      const customerDocumentsData = Object.assign({}, state.documentsData, action.dataIdCard)
      return Object.assign({}, state, { documentsData:{ ...customerDocumentsData },
        getDocumentsDataRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.GET_DOCUMENTS_DATA_START :
      return Object.assign({}, state, { getDocumentsDataRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_DOCUMENTS_DATA_ERROR :
      return Object.assign({}, state, { getDocumentsDataRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.POST_DOCUMENTS_DATA_START :
      return Object.assign({}, state, { postDocumentsDataRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_DATA_SUCCESS :
      return Object.assign({}, state, { postDocumentsDataRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_DATA_ERROR :
      return Object.assign({}, state, { postDocumentsDataRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.POST_DOCUMENTS_SCREEN_START :
      return Object.assign({}, state, { postDocumentsScreenRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_SCREEN_SUCCESS :
      return Object.assign({}, state, { postDocumentsScreenRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_SCREEN_ERROR :
      return Object.assign({}, state, { postDocumentsScreenRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.POST_DATA_ALLEGATI_CF_START :
      return Object.assign({}, state, { postDataAllegatiCFRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DATA_ALLEGATI_CF_SUCCESS :
      return Object.assign({}, state, { postDataAllegatiCFRequest:{
        fetching: false,
        error: null,
        dataSuccessFlags:action.dataSuccessFlags
      } })
    case actionTypes.POST_DATA_ALLEGATI_CF_ERROR :
      return Object.assign({}, state, { postDataAllegatiCFRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.POST_DATA_ALLEGATI_DOC_START :
      return Object.assign({}, state, { postDataAllegatiDOCRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DATA_ALLEGATI_DOC_SUCCESS :
      return Object.assign({}, state, { postDataAllegatiDOCRequest:{
        fetching: false,
        error: null,
        dataSuccessFlags:action.dataSuccessFlags
      } })
    case actionTypes.POST_DATA_ALLEGATI_DOC_ERROR :
      return Object.assign({}, state, { postDataAllegatiDOCRequest:{
        fetching: false,
        error: action.error
      } })

    case actionTypes.POST_DATA_DOCUMENTO_START :
      return Object.assign({}, state, { postDataDocumentoRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DATA_DOCUMENTO_SUCCESS :
      return Object.assign({}, state, { postDataDocumentoRequest:{
        fetching: false,
        error: null,
        dataPreviousPosted:action.dataPreviousPosted
      } })
    case actionTypes.POST_DATA_DOCUMENTO_ERROR :
      return Object.assign({}, state, { postDataDocumentoRequest:{
        fetching: false,
        error: action.error
      } })

    case actionTypes.POST_DATA_DOCUMENTO_HIDE_START :
      return Object.assign({}, state, { postDataDocumentoHideRequest:{
        fetching: true,
        error: null,
        dataPreviousPosted:action.dataPreviousPosted
      } })
    case actionTypes.POST_DATA_DOCUMENTO_HIDE_SUCCESS :
      return Object.assign({}, state, { postDataDocumentoHideRequest:{
        fetching: false,
        error: null,
        dataPreviousPosted:action.dataPreviousPosted
      } })
    case actionTypes.POST_DATA_DOCUMENTO_HIDE_ERROR :
      return Object.assign({}, state, { postDataDocumentoHideRequest:{
        fetching: false,
        error: action.error
      } })

    case actionTypes.POST_DOCUMENTS_DATA_AND_FILE_START :
      return Object.assign({}, state, { postDocumentsDataAndFilesRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_DATA_AND_FILE_SUCCESS :
      return Object.assign({}, state, { postDocumentsDataAndFilesRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.POST_DOCUMENTS_DATA_AND_FILE_ERROR :
      return Object.assign({}, state, { postDocumentsDataAndFilesRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_SERVIZI_DIGITALI_SUCCESS :
      const serviziData = Object.assign({}, state.serviziData, action.data)
      return Object.assign({}, state, { serviziData:{ ...serviziData },
        getServiziDigitaliRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.GET_SERVIZI_DIGITALI_START :
      return Object.assign({}, state, { getServiziDigitaliRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_SERVIZI_DIGITALI_ERROR :
      return Object.assign({}, state, { getServiziDigitaliRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.POST_SERVIZI_DIGITALI_START :
      return Object.assign({}, state, { postServiziDigitaliRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_SERVIZI_DIGITALI_SUCCESS :
      return Object.assign({}, state, { postServiziDigitaliRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.POST_SERVIZI_DIGITALI_ERROR :
      return Object.assign({}, state, { postServiziDigitaliRequest:{
        fetching: false,
        error: action.error
      } })

    case actionTypes.POST_INFORMAZIONI_PERSONALI_START :
      return Object.assign({}, state, { postInfoPersonaliRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.POST_INFORMAZIONI_PERSONALI_SUCCESS :
      return Object.assign({}, state, {
        postInfoPersonaliRequest:{
          fetching: false,
          error: null,
        },
        dataRegistryStart: true
      })
    case actionTypes.POST_INFORMAZIONI_PERSONALI_ERROR :
      return Object.assign({}, state, { postInfoPersonaliRequest:{
        fetching: false,
        error: action.error
      } })

    case actionTypes.POST_FATCA_START :
      return Object.assign({}, state, {
        postFatcaRequest:{
          fetching: true,
          error: null
        }
      })
    case actionTypes.POST_FATCA_SUCCESS :
      return Object.assign({}, state, {
        postFatcaRequest: {
          fetching: false,
          error: null
        },
        fatcaSent: true
      })
    case actionTypes.POST_FATCA_ERROR :
      return Object.assign({}, state, { postFatcaRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_LISTA_CLIENTI_START :
      return Object.assign({}, state, { getClientiListRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_LISTA_CLIENTI_ERROR :
      return Object.assign({}, state, { getClientiListRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_LISTA_CLIENTI_SUCCESS :
      const clientiArray = action.customerListData.customers
      return Object.assign(
        {},
        state,
        {
          users:clientiArray,
          getClientiListRequest:{
            fetching: false,
            error: null
          }
        })
  }
  return state
}
