import * as actions from './actions'
import * as constants from './constants'

export { default as customersReducer } from './reducer'
export { actions as customersActions }
export { constants as customersConstants }
