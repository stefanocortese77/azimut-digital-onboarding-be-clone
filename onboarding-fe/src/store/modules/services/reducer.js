import * as actionTypes from './constants'

const initialState = {
  services: [ ],
  clienti: [ ],
  query: {
    pageBy: {
      pageRequest: 1,
      pageSize: 20
    },
    sortBy: [{
      field: 'dataIns',
      type: 'asc'
    }],
    conditions: []
  },
  serviceSearchRequest:{
    fetching: false,
    error: null
  },
  serviceSaveRequest:{
    fetching: false,
    error: null
  },
  getServiceInfoRequest:{
    fetching: false,
    error: null
  },
  serviceData:{}
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SERVICE_SEARCH_START:
      return {
        ...state,
        query: action.query,
        serviceSearchRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SERVICE_SEARCH_SUCCESS:
      return {
        ...state,
        services: action.services,
        clienti: action.clienti,
        serviceSearchRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.SERVICE_SEARCH_ERROR:
      return {
        ...state,
        serviceSearchRequest: {
          fetching: false,
          error: action.error
        }
      }
    case actionTypes.SERVICE_SAVE_START :
      return Object.assign({}, state, { serviceSaveRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.SERVICE_SAVE_SUCCESS :
      return Object.assign({}, state, { serviceSaveRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.SERVICE_SAVE_ERROR :
      return Object.assign({}, state, { serviceSaveRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.GET_SERVICE_INFO_START :
      return Object.assign({}, state, { getServiceInfoRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.GET_SERVICE_INFO_SUCCESS :
      const serviceData = Object.assign({}, state.serviceData, action.serviceData)
      return Object.assign({}, state, { services:{ ...serviceData },
        getServiceInfoRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.GET_SERVICE_INFO_ERROR :
      return Object.assign({}, state, { getServiceInfoRequest:{
        fetching: false,
        error: action.error
      } })
  }
  return state
}
