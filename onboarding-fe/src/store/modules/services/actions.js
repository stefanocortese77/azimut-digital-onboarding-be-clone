import * as actionTypes from './constants'

import axios from 'axios'

export function servicesSearch (queryParam) {
  return async function (dispatch) {
    const query = {
      pageBy: {
        pageRequest: 1,
        pageSize: 20
      },
      sortBy: [{
        field: 'insertionDate',
        type: 'asc'
      }],
      conditions: [],
      ...queryParam
    }

    dispatch({
      type: actionTypes.SERVICE_SEARCH_START,
      query: query
    })
    try {
      const response = await axios.post('/servizio/search', query)
      dispatch({
        type: actionTypes.SERVICE_SEARCH_SUCCESS,
        services: response.data.servizi,
        clienti: response.data.clienti
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SERVICE_SEARCH_ERROR,
        error: error
      })
    };
  }
}

export function serviceSave (queryParam) {
  return async function (dispatch) {
    let queryDefault = {
      'genericInfo': {
        'idIncarico': 'string',
        'idCliente': 'string',
        'firstName': 'string',
        'lastName': 'string',
        'codiceFiscale': 'string',
        'idStatus': 0
      },
      'servInvDetails': {
        'idTipoServizio': 0,
        'idHolder': 'string',
        'idCoholders': [
          'string'
        ],
        'idStatoContratto': 0,
        'idRegimeAppartenenza': 0,
        'pagamento': {
          'importo': 0,
          'idOrigineVersamento': 'string',
          'flagBonifico': 'string',
          'flagAssegno': 'string',
          'dettagliAssegno': {
            'abi': 'string',
            'cab': 'string',
            'numConto': 'string',
            'numAssegno': 'string',
            'foto': 'string'
          }
        },
        'corrispondenza': {
          'nome': 'string',
          'cognome': 'string',
          'indirizzo': 'string',
          'civico': 'string',
          'cap': 'string',
          'idComune': 'string',
          'idProvincia': 'string',
          'idNazione': 'string'
        },
        'contatti': {
          'flagEmail': true,
          'email': 'string',
          'flagCellulare': true,
          'prefCellulare': 'string',
          'cellulare': 'string'
        },
        'percCommAdv': 0,
        'percCommTitAz': 0,
        'percCommTitObbl': 0
      }
    }
    const query = Object.assign({}, queryDefault, queryParam)

    dispatch({
      type: actionTypes.SERVICE_SAVE_START,
      query: query
    })
    try {
      const response = await axios.post('/servizio/save', query)
      dispatch({
        type: actionTypes.SERVICE_SAVE_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SERVICE_SAVE_ERROR,
        error: error
      })
    };
  }
}

export function getServizioInfo (idServizio) {
  let query = {
    'idConsultant': 'string',
    'idCustomer': 'string'
  }
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_SERVICE_INFO_START,
      query: query
    })
    try {
      const response = await axios.post(`/servizio/get/${idServizio}`, query)

      dispatch({
        type: actionTypes.GET_SERVICE_INFO_SUCCESS,
        serviceData: response.data
      }
    )
    } catch (error) {
      console.log('error service GET', error)
      dispatch({
        type: actionTypes.GET_SERVICE_INFO_ERROR,
        error: error
      })
    };
  }
}
