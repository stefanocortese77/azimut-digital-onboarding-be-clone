import * as actions from './actions'
import * as constants from './constants'

export { default as praticaReducer } from './reducer'
export { actions as praticaActions }
export { constants as praticaConstants }
