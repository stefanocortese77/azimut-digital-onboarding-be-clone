import * as actionTypes from './constants'

import axios from 'axios'

export function getPratica (idPratica) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.PRATICA_GET_START
    })
    try {
      const response = await axios.get(`/pratica/${idPratica}`, {})

      dispatch({
        type: actionTypes.PRATICA_GET_SUCCESS,
        data: response.data
      }
    )
    } catch (error) {
      console.log('error summary', error)
      dispatch({
        type: actionTypes.PRATICA_GET_ERROR,
        error: error
      })
    };
  }
}

// funzione che salva la pratica e rimanda alla pagina successiva
export function saveAndProceed (paramObject) {
  return async function (dispatch) {
    let queryDefault = {
      'codiciTipoServizio': [
        'string',
        'string'
      ],
      'intestatari': [
        {
          'idCliente': 'string',
          'idRuolo': 0
        },
        {
          'idCliente': 'string',
          'idRuolo': 0
        }
      ]
    }

    const query = Object.assign({}, queryDefault, paramObject)
    dispatch({
      type: actionTypes.PRATICE_SAVE_START,
      query: query
    })
    try {
      const response = await axios.post('/pratica', query)
      dispatch({
        type: actionTypes.PRATICE_SAVE_SUCCESS,
        data: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.PRATICE_SAVE_ERROR,
        error: error
      })
    };
  }
}

