import * as actionTypes from './constants'

const initialState = {
  'pratica': {
    'idStatoPratica': null,
    'idRegimeAppartenenza': null,
    'servizi': [
      {
        'idServizio': null,
        'idTipoServizio': null
      },
      {
        'idServizio': null,
        'idTipoServizio': null
      }
    ],
    'intestatari': {
      'id': null,
      'firstName': null,
      'lastName': null,
      'codiceFiscale': null,
      'idStato': null,
      'idRuolo': null
    },
    'corrispondenza': {
      'nome': null,
      'cognome': null,
      'indirizzo': null,
      'civico': null,
      'cap': {
        'tipoDominio': null,
        'chiave': null
      },
      'idComune': {
        'tipoDominio': null,
        'chiave': null
      },
      'idProvincia': {
        'tipoDominio': null,
        'chiave': null
      },
      'idNazione': {
        'tipoDominio': null,
        'chiave': null
      },
      'comuneEstero': null
    },
    'contatti': {
      'flagEmail': null,
      'email': null,
      'flagCellulare': null,
      'prefCellulare': null,
      'numcellulare': null
    }
  },

  praticaRequest:{
    fetching: false,
    error: null
  },
  savePraticaRequest:{
    fetching: false,
    error: null
  }


}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.PRATICA_GET_SUCCESS :

      const pratica = Object.assign({}, state.pratica, action.data.pratica)
      return Object.assign({}, state, { pratica:{ ...pratica },
        praticaRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.PRATICA_GET_START :
      return Object.assign({}, state, { praticaRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.PRATICA_GET_ERROR :
      return Object.assign({}, state, { praticaRequest:{
        fetching: false,
        error: action.error
      } })
    case actionTypes.PRATICE_SAVE_START :
      return Object.assign({}, state, { savePraticaRequest:{
        fetching: true,
        error: null
      }})
    case actionTypes.PRATICE_SAVE_SUCCESS :
      return Object.assign({}, state, { savePraticaRequest:{
        fetching: false,
        error: null
      } })
    case actionTypes.PRATICE_SAVE_ERROR :
      return Object.assign({}, state, { savePraticaRequest:{
        fetching: false,
        error: action.error
      }})
  }
  return state
}
