import * as actionTypes from './constants'

import axios from 'axios'

export function utilityDocFirmaSign (idIncarico) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.UTILITY_DOCFIRMA_SIGN_START,
    })
    try {
      const response = await axios.post(`/utility/docfirma/sign/?idIncarico=${idIncarico}`, {})
      console.log(response)
      dispatch({
        type: actionTypes.UTILITY_DOCFIRMA_SIGN_SUCCESS,
        data: response.data,

      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.UTILITY_DOCFIRMA_SIGN_ERROR,
        error: error
      })
    };
  }
}

