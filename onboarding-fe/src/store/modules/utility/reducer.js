import * as actionTypes from './constants'

const initialState = {

  listaDocumenti: null,
  listaClienti: null,
  statusComplessivo: null,
  utilityDocFirmaSignRequest:{
    fetching: false,
    error: null
  },
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.UTILITY_DOCFIRMA_SIGN_START:
      return Object.assign({}, state, { utilityDocFirmaSignRequest:{
        fetching: true,
        error: null
      } })
    case actionTypes.UTILITY_DOCFIRMA_SIGN_SUCCESS:
      let utility = action.data
      return Object.assign({}, state, {
        ...utility,
        utilityDocFirmaSignRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.UTILITY_DOCFIRMA_SIGN_ERROR:
      return {
        ...state,
        utilityDocFirmaSignRequest: {
          fetching: false,
          error: action.error
        }
      }
  }
  return state
}
