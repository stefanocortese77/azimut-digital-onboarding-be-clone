import * as actionTypes from './constants'

export function setDataLoading (value) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SET_DATA_LOADING,
      dataLoadingValue: value
    })
  }
}
