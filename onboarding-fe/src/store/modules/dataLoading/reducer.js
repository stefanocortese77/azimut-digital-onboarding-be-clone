import * as actionTypes from './constants'

const initialState = {
  dataLoadingState: 'spinner'
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_DATA_LOADING:
      return Object.assign({}, state, { dataLoadingState:action.dataLoadingValue })
  }
  return state
}
