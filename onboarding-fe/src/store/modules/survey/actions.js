import * as actionTypes from './constants'

import axios from 'axios'

export function fetchSurveyTemplate () {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_START,
    })

    try {
      const response = await axios.get('/survey/mifid/template')
      dispatch({
        type: actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_SUCCESS,
        template: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_ERROR,
        error: error
      })
    };
  }
}

export function fetchSurvey (idCliente, flagRiepilogo, active) {
  return async function (dispatch) {
    if (!active) {
      return null
    }

    dispatch({
      type: actionTypes.SURVEY_MIFID_FETCH_START,
    })

    try {
      const response = await axios.get(`/survey/mifid/get/${idCliente}?flagRiepilogo=${flagRiepilogo}`)
      dispatch({
        type: actionTypes.SURVEY_MIFID_FETCH_SUCCESS,
        previousForm: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SURVEY_MIFID_FETCH_ERROR,
        error: error
      })
    };
  }
}

export function saveSurvey (idCliente, listaDomande) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SURVEY_MIFID_SAVE_START,
    })

    try {
      const response = await axios.post(`/survey/mifid/save`, {
        idCliente,
        listaDomande
      })
      dispatch({
        type: actionTypes.SURVEY_MIFID_SAVE_SUCCESS,
        template: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SURVEY_MIFID_SAVE_ERROR,
        error: error
      })
    };
  }
}

function getAml (idCliente, idIncarico, active) {
  if (!active) {
    return new Promise(function (resolve, reject) {
      resolve({ data: {} })
    })
  }
  try {
    return axios.get(`/survey/aml/${idCliente}/get/?idIncarico=${idIncarico}&flagRiepilogo=false`, {})
  } catch (error) {
    const newError = new Error(error.message)
    newError.apiName = 'getAml'
    throw newError
  }
}
function getTemplateAml () {
  try {
    return axios.get(`/survey/aml/template`, {})
  } catch (error) {
    const newError = new Error(error.message)
    newError.apiName = 'templateAml'
    throw newError
  }
}

export function fetchSurveyAml (idCliente, idIncarico, active) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SURVEY_AML_FETCH_START,
    })

    try {
      const [dataTemplateAml, dataAml] = await Promise.all([getTemplateAml(), getAml(idCliente, idIncarico, active)])
      dispatch({
        type: actionTypes.SURVEY_AML_FETCH_SUCCESS,
        dataTemplateAml: dataTemplateAml.data,
        dataAml: dataAml.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SURVEY_AML_FETCH_ERROR,
        error: error
      })
    };
  }
}

export function saveAml (idCliente, idIncarico, listaDomande) {
  return async function (dispatch, getState) {
    dispatch({
      type: actionTypes.SURVEY_AML_SAVE_START,
    })

    try {
      await axios({
        method: getState().survey.amlSent ? 'PUT' : 'POST',
        url: `/survey/aml/save`,
        data: {
          idCliente,
          idIncarico,
          listaDomande
        }
      })
      dispatch({
        type: actionTypes.SURVEY_AML_SAVE_SUCCESS,
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.SURVEY_AML_SAVE_ERROR,
        error: error
      })
    };
  }
}

export function getAdegVerTemplate () {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_START,
    })

    try {
      const response = await axios.post('/survey/adegver/template', {
        'idConsultant': 'idConsultant',
        'idCustomer': 'idCustomer'
      })
      dispatch({
        type: actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_SUCCESS,
        template: response.data
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_ERROR,
        error: error
      })
    };
  }
}

export function saveAdegVer (idCliente, datiQuestionario) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SAVE_ADEGUATA_VERIFICA_START,
    })

    try {
      const response = await axios.post(`/survey/adegver/save`, {
        idCliente,
        datiQuestionario
      })
      dispatch({
        type: actionTypes.SAVE_ADEGUATA_VERIFICA_SUCCESS,
        dataPost: response.data
      })
    } catch (error) {
      dispatch({
        type: actionTypes.SAVE_ADEGUATA_VERIFICA_ERROR,
        error: error
      })
    };
  }
}
