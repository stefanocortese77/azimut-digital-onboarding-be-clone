import * as actionTypes from './constants'

const initialState = {
  // aml:{},
  // services: [ ],
  // users: [ ],
  // query: {
  //   pageBy: {
  //     pageRequest: 1,
  //     pageSize: 20
  //   },
  //   sortBy: [{
  //     field: 'dataIns',
  //     type: 'asc'
  //   }],
  //   conditions: []
  // },
  // serviceSearchRequest:{
  //   fetching: false,
  //   error: null
  // }
  template: {},
  templateAml: {},
  templateAV:null,
  previousForm: {},
  amlPreviousForm: {},
  amlSent: false,
  mifidTemplateRequest: {
    fetching: false,
    error: null
  },
  amlRequest: {
    fetching: false,
    error: null
  },
  amlSaveRequest: {
    fetching: false,
    error: null
  },
  mifidRequest: {
    fetching: false,
    error: null
  },
  mifidSaveRequest: {
    fetching: false,
    error: null
  },
  adegVerTemplateRequest: {
    fetching: false,
    error: null
  },
  adegVerSaveRequest: {
    fetching: false,
    error: null
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_START:
      return {
        ...state,
        mifidTemplateRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_SUCCESS:
      return {
        ...state,
        template: action.template,
        mifidTemplateRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_TEMPLATE_FETCH_ERROR:
      return {
        ...state,
        mifidTemplateRequest: {
          fetching: false,
          error: action.error
        }
      }

    case actionTypes.SURVEY_MIFID_FETCH_START:
      return {
        ...state,
        mifidRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_FETCH_SUCCESS:
      return {
        ...state,
        previousForm: action.previousForm,
        mifidRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_FETCH_ERROR:
      return {
        ...state,
        mifidRequest: {
          fetching: false,
          error: action.error
        }
      }

    case actionTypes.SURVEY_MIFID_SAVE_START:
      return {
        ...state,
        mifidSaveRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_SAVE_SUCCESS:
      return {
        ...state,
        mifidSaveRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.SURVEY_MIFID_SAVE_ERROR:
      return {
        ...state,
        mifidSaveRequest: {
          fetching: false,
          error: action.error
        }
      }

    case actionTypes.SURVEY_AML_FETCH_START:
      return {
        ...state,
        amlRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SURVEY_AML_FETCH_SUCCESS:
      const surveryDataTemplateAml = Object.assign(action.dataTemplateAml)

      return Object.assign({}, state, {
        amlPreviousForm: action.dataAml,
        templateAml:{ ...surveryDataTemplateAml },
        amlRequest:{
          fetching: false,
          error: null
        } })

    case actionTypes.SURVEY_AML_FETCH_ERROR:
      return {
        ...state,
        amlRequest: {
          fetching: false,
          error: action.error
        }
      }

    case actionTypes.SURVEY_AML_SAVE_START:
      return {
        ...state,
        amlSaveRequest: {
          fetching: true,
          error: null
        }
      }

    case actionTypes.SURVEY_AML_SAVE_SUCCESS:
      return {
        ...state,
        amlSaveRequest: {
          fetching: false,
          error: null
        },
        amlSent: true
      }

    case actionTypes.SURVEY_AML_SAVE_ERROR:
      return {
        ...state,
        amlSaveRequest: {
          fetching: false,
          error: action.error
        }
      }

    case actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_START:
      return {
        ...state,
        adegVerTemplateRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_SUCCESS:
      return {
        ...state,
        templateAV: action.template,
        adegVerTemplateRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.GET_ADEGUATA_VERIFICA_TEMPLATE_ERROR:
      return {
        ...state,
        adegVerTemplateRequest: {
          fetching: false,
          error: action.error
        }
      }
    case actionTypes.SAVE_ADEGUATA_VERIFICA_START:
      return {
        ...state,
        adegVerSaveRequest: {
          fetching: true,
          error: null
        }
      }
    case actionTypes.SAVE_ADEGUATA_VERIFICA_SUCCESS:
      return {
        ...state,
        adegVerSaveRequest: {
          fetching: false,
          error: null
        }
      }
    case actionTypes.SAVE_ADEGUATA_VERIFICA_ERROR:
      return {
        ...state,
        adegVerSaveRequest: {
          fetching: false,
          error: action.error
        }
      }
  }
  return state
}
