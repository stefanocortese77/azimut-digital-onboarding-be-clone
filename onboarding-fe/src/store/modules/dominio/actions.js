import * as actionTypes from './constants'

import axios from 'axios'

export function dominio (queryParam) {
  return async function (dispatch, getState) {
    const query = {
      ...queryParam
    }

    if (!query || !query.listaDomini) {
      console.error('No valid query supplied to dominio')
      return
    }

    let { listaDomini } = queryParam
    const stateDominio = getState().dominio
    const stateDomini = stateDominio.domini
    const stateFiltri = stateDominio.filtri

    // There are some domini on the state
    if (stateDomini) {
      if (stateDomini.listaDomini) {
        const stateListaDomini = stateDomini.listaDomini

        listaDomini = listaDomini.filter(dominio => {
          if (stateListaDomini.find(stateDominio => stateDominio.idDominio === dominio.idDominio)) {
            // The same dominio is already on the state, don't fetch it again unless the filter is different

            if (dominio.filtro) {
              // The dominio is already on the state, but the query is different
              if (stateFiltri[dominio.idDominio] !== dominio.filtro.codice) {
                return true
              }
            }

             // The dominio hasn't a filter, it must not be fetched again
            return false
          }

          // This domain is not already present in state
          return true
        })
      }
    }

    dispatch({
      type: actionTypes.DOMINIO_START,
      query: {
        listaDomini
      }
    })
    try {
      const response = await axios.post('/utility/dominio', {
        listaDomini
      })
      dispatch({
        type: actionTypes.DOMINIO_SUCCESS,
        dominio: response.data,
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.DOMINIO_ERROR,
        error: error
      })
    };
  }
}
