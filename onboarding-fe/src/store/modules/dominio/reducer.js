import * as actionTypes from './constants'

import { unionBy } from 'lodash'

// lista domini   :

// DOM_TIPO_DOCUMENTO,
// DOM_CITTADINANZE,
// DOM_TITOLO_STUDIO,
// DOM_SETTORE_OCCUPAZIONE,
// DOM_PROFESSIONE_ATTUALE,
// DOM_NATURA_RAPPORTO,
// DOM_ORIGINE_VERSAMENTO,
// DOM_PERCENTUALE_COMMISSIONA,
// DOM_COMUNE,
// DOM_PROVINCE,
// DOM_NAZIONI,

const initialState = {
  filtri: {},
  domini: [],
  dominioRequest:{
    fetching: false,
    error: null
  }
}
export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.DOMINIO_START:
      const filtri = {}

      action.query.listaDomini.forEach(dominio => {
        if (dominio.filtro) {
          filtri[dominio.idDominio] = dominio.filtro.codice
        }
      })

      return Object.assign({}, state, {
        dominioRequest: {
          fetching: true,
          error: null
        },
        filtri: Object.assign({}, state.filtri, filtri)
      })
    case actionTypes.DOMINIO_SUCCESS:
      const iteree = function (dominio) {
        if (dominio) {
          return dominio.idDominio
        }
        return null
      }

      // Mergest the state domini and the ones from the action, ginving priority to the
      // action ones in case of conflict
      const listaDomini = unionBy(action.dominio.listaDomini, state.domini.listaDomini || [], iteree)

      return Object.assign({}, state, {
        domini: {
          listaDomini: listaDomini
        },
        dominioRequest:{
          fetching: false,
          error: null
        } })
    case actionTypes.DOMINIO_ERROR:
      return {
        ...state,
        dominioRequest: {
          fetching: false,
          error: action.error
        }
      }
  }
  return state
}
