import * as actionTypes from './constants'

import axios from 'axios'

export function validationCreateOtp (queryParam) {
  return async function (dispatch) {
    const query = {
      ...queryParam
    }

    dispatch({
      type: actionTypes.VALIDATION_CREATE_OTP_START,
      query: query
    })
    try {
      const response = await axios.post('/validation/sms/send', queryParam)
      dispatch({
        type: actionTypes.VALIDATION_CREATE_OTP_SUCCESS,
        data: response.data,
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.VALIDATION_CREATE_OTP_ERROR,
        error: error
      })
    };
  }
}

export function validationVerifyOtp (queryParam) {
  return async function (dispatch) {
    const query = {
      ...queryParam
    }

    dispatch({
      type: actionTypes.VALIDATION_VERIFY_OTP_START,
      query: query
    })
    try {
      const response = await axios.post('/validation/sms/verify', queryParam)
      dispatch({
        type: actionTypes.VALIDATION_VERIFY_OTP_SUCCESS,
        data: response.data,
      })
    } catch (error) {
      console.log(error)
      dispatch({
        type: actionTypes.VALIDATION_VERIFY_OTP_ERROR,
        error: error
      })
    };
  }
}
