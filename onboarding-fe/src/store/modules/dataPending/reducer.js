import * as actionTypes from './constants'

const initialState = {
  dataPendingState: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_DATA_PENDING:
      return Object.assign({}, state, { dataPendingState:action.dataPendingValue })
  }
  return state
}
