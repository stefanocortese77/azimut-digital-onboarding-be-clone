import * as actionTypes from './constants'

export function setDataPending (value) {
  return async function (dispatch) {
    dispatch({
      type: actionTypes.SET_DATA_PENDING,
      dataPendingValue: value
    })
  }
}
