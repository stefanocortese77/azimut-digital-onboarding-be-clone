import { combineReducers } from 'redux'
import { customersReducer } from './modules/customers'
import { servicesReducer } from './modules/services'
import { dominioReducer } from './modules/dominio'
import { surveyReducer } from './modules/survey'
import { verifyReducer } from './modules/validation'
import { praticaReducer } from './modules/pratica'
import { dataPendingReducer } from './modules/dataPending'
import { dataLoadingReducer } from './modules/dataLoading'
import { utilityReducer } from './modules/utility'
import testComponentsReducer from '../routes/TestComponents/modules/testComponentsReducer'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    testComponents: testComponentsReducer,
    customers: customersReducer,
    services: servicesReducer,
    dominio: dominioReducer,
    survey: surveyReducer,
    validation: verifyReducer,
    dataPendings: dataPendingReducer,
    dataLoading: dataLoadingReducer,
    utility:utilityReducer,
    pratica:praticaReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
