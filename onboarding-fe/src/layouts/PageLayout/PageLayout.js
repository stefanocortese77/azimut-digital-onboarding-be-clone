import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import './PageLayout.scss'
import { Row, Col } from 'react-materialize'
import Header from '../../components/Header/'
import Footer from '../../components/Footer/'

export const PageLayout = ({ children }) => (
  <Row className="container paddingless marginless">
    {children}
  </Row>
)
PageLayout.propTypes = {
  children: PropTypes.node,
}

export default PageLayout
