import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-materialize'

class FirmaWrapperComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {

    }
    this.gotoParentDocument = this.gotoParentDocument.bind(this)
  }
  backHome () {
    this.props.history.push('/')
  }
  gotoParentDocument () {
    let tokenNamirial = ''

    // const queryParams = new URLSearchParams(this.props.location.search);
    // console.log('assuming query param is id', queryParams.get('id');


    const urlMatches = /tokenNamirial=([a-zA-Z0-9_.\-~]*)(&.*)*$/.exec(document.location.href)
    if (urlMatches && urlMatches[1] && urlMatches[1].length) {
      tokenNamirial = urlMatches[1]
      parent.document.location.href = `http://localhost:3000/firma?tokenNamirial=${tokenNamirial}`
    }
  }


  render () {
    return (
      <div className="namirial-frame-body">
        <div>loading</div>
        <Button flat waves="light" onClick={this.gotoParentDocument}>concludi</Button>
      </div>
    )
  }
}

FirmaWrapperComponent.propTypes = {
  history: PropTypes.object
}

export default FirmaWrapperComponent
