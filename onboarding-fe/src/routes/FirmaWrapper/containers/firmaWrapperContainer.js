import { connect } from 'react-redux'
import FirmaWrapperComponent from '../components/FirmaWrapperComponent'
import { servicesActions } from '../../../store/modules/services'

const mapDispatchToProps = (dispatch) => {
  return {
    servicesSearch : (query) => dispatch(servicesActions.servicesSearch(query))
  }
}

const mapStateToProps = (state) => ({
  services: state.services.services,
  query: state.services.query
})

export default connect(mapStateToProps, mapDispatchToProps)(FirmaWrapperComponent)
