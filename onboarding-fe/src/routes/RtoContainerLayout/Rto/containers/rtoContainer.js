import { connect } from 'react-redux'
import RtoView from '../components/RtoView'
import { getServizioInfo } from '../../../../store/modules/services/actions'
import { dominio } from '../../../../store/modules/dominio/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    dominio : (queryParam) => dispatch(dominio(queryParam)),
    getServizioInfo : (idServizio) => dispatch(getServizioInfo(idServizio))
  }
}

const mapStateToProps = (state) => ({
  dominioRequest : state.dominio.dominioRequest,
  domini: state.dominio.domini,
  serviceData: state.services.serviceData,
  getServiceInfoRequest: state.services.getServiceInfoRequest
})

export default connect(mapStateToProps, mapDispatchToProps)(RtoView)
