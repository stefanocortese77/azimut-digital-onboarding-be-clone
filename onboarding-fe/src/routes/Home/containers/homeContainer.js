import { connect } from 'react-redux'
import HomeComponent from '../components/HomeComponent'
import { servicesActions } from '../../../store/modules/services'
import { praticaActions } from '../../../store/modules/pratica'
import { setIdCustomer, setIdIncarico, setCodiceFiscale } from '../../../store/modules/customers/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    servicesSearch : (query) => dispatch(servicesActions.servicesSearch(query)),
    getPratica : (idPratica) => dispatch(praticaActions.getPratica(idPratica)),
    setIdCustomer: (idCliente) => dispatch(setIdCustomer(idCliente)),
    setIdIncarico: (idIncarico) => dispatch(setIdIncarico(idIncarico)),
    setCodiceFiscale: (codiceFiscale) => dispatch(setCodiceFiscale(codiceFiscale))
  }
}

const mapStateToProps = (state) => ({
  services: state.services.services,
  query: state.services.query,
  clienti: state.services.clienti,
  pratica: state.pratica.pratica,
  praticaRequest: state.pratica.praticaRequest
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent)
