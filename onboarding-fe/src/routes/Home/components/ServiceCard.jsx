import React from 'react'
import PropTypes from 'prop-types'
import { Card, Button } from 'react-materialize'
import moment from 'moment'

const ServiceCard = ({ service, callBack }) => {
  // variable that changes CSS class for Button
  let stateDescrition
  // variable that changes CSS class for Button
  let cardClass
  // label button
  let labelButton
  // variable that defines (name/surname or fiscal code)
  let componenti = ''

  // set different CSS depending on idStatus
  if (service.idStatus !== undefined) {
    switch (service.idStatus) {
      case 1:
        stateDescrition = 'button-draft'
        cardClass = 'service-card draft'
        labelButton = 'Concludi'
        break
      case 2:
        stateDescrition = 'button-to-sign'
        cardClass = 'service-card to-sign'
        labelButton = 'Firma'
        break
      case 3:
        stateDescrition = 'button-compile-av'
        cardClass = 'service-card compile-av'
        labelButton = 'Compila AV'
        break
      case 4:
        stateDescrition = 'button-complete'
        cardClass = 'service-card complete'
        labelButton = ''
        break
      default:
        stateDescrition = 'service-action'
        cardClass = 'card'
        labelButton = ''
        break
    }

    // check name/surname -> fiscal Code

    service.customers.forEach((customer, index, arr) => {
      if (index > 0 && index !== arr.lenght) {
        componenti += `, `
      }
      if (customer.firstName && customer.lastName) {
        componenti += `${customer.firstName} ${customer.lastName}`
      } else {
        componenti += `${customer.codiceFiscale}`
      }
    })
  } else {
    stateDescrition = 'service-action'
    cardClass = 'card'
    labelButton = ''
    componenti = service.customers[0].codiceFiscale
  }


  let buttonRender = ''
  if (service.idStatus !== 4) {
    buttonRender = (
      <Button className={stateDescrition} onClick={() => { callBack(service.id, service.idStatus, service.idTipologia) }}>
        { labelButton }
      </Button>
    )
  }

  return (

    <div className={cardClass}>
      <Card className="small margin-10">
        <div className="service-card-column">
          <div className="azimut-field margin-b-20">
            <div>Data inserimento</div>
            <div className="service-card-text-upper">{ moment(service.insertionDate).format('DD/MM/YY') }</div>
          </div>
          <div className="azimut-field">
            <div>Servizio</div>
            <div className="service-card-text-lower">{ service.descTipologia }</div>
          </div>
        </div>
        <div className="service-card-column">
          <div className="azimut-field margin-b-20">
            <div>ID pratica</div>
            <div className="service-card-text-upper">{ service.id }</div>
          </div>
          <div className="azimut-field">
            <div>Componenti</div>
            <div className="service-card-text-lower">
              { componenti }
            </div>
          </div>
        </div>
        <div className="service-card-column-longer">
          <div className="azimut-field margin-b-20">
            <div>Stato</div>
            <div className="service-card-text-upper">
              { service.descStatus }
            </div>
          </div>
        </div>
        <div className="service-card-column-button">
          {buttonRender}
        </div>
      </Card>
    </div>
  )
}

ServiceCard.propTypes = {
  service: PropTypes.object,
  callBack: PropTypes.func
}

export default ServiceCard
