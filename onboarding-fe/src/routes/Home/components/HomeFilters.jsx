import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Input } from 'react-materialize'
import HomeFiltersChips from './HomeFiltersChips'
import DatePicker from '../../../components/DatePicker/DatePicker'
import Autocomplete from '../../../components/Autocomplete/Autocomplete'

class HomeFilters extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      searchDrawerCollapse: false,
      descComposed: '',
      startDate: null,
      endDate: null,
      status: '',
      custormerSelected:null,
      custormerSelectedValue: null
    }

    this.updateCondition = this.updateCondition.bind(this)
    this.resetFields = this.resetFields.bind(this)
    this.handleStatusChange = this.handleStatusChange.bind(this)
    this.handleSearchTextChange = this.handleSearchTextChange.bind(this)
    this.handleStartDateChange = this.handleStartDateChange.bind(this)
    this.handleEndDateChange = this.handleEndDateChange.bind(this)
    this.handleCustomerSelect = this.handleCustomerSelect.bind(this)
  }

  componentDidMount () {
    this.props.servicesSearch()
  }
  handleCustomerSelect (value, matchSuggestion) {
    this.setState({ custormerSelected:matchSuggestion, custormerSelectedValue: value })
    if (matchSuggestion) {
      this.updateCondition('idCustomer', matchSuggestion.key)
    }
  }

  updateCondition (field, value) {
    let conditions = this.props.query.conditions.slice()

    const i = conditions.findIndex(c => c.field === field)
    if (i >= 0) {
      conditions.splice(i, 1)
    }

    if (value) {
      conditions.push({ field, value })
    }

    const query = Object.assign({}, this.props.query, {
      conditions
    })

    this.props.servicesSearch(query)
  }

  resetFields () {
    this.setState({
      idCustomer: '',
      descComposed: '',
      startDate: null,
      endDate: null,
      status: '',
    })

    const query = Object.assign({}, this.props.query, {
      conditions: []
    })
    this.setState({ custormerSelectedValue:'' })

    this.props.servicesSearch(query)
  }

  // funzione eseguita ricercando per cliente nella HomePage
  handleSearchTextChange (value) {
    this.updateCondition('idCustomer', '')
    this.setState({ custormerSelectedValue:'' })
  }

  // funzione eseguita ricercando per data iniziale nella HomePage
  handleStartDateChange (value) {
    this.setState({ startDate: value })
    this.updateCondition('startDate', value)
  }

  // funzione eseguita ricercando per data finale nella HomePage
  handleEndDateChange (value) {
    this.setState({ endDate: value })
    this.updateCondition('endDate', value)
  }

  // funzione eseguita ricercando per stato nella HomePage
  handleStatusChange (value) {
    this.setState({ status: value })
    this.updateCondition('idStatus', value)
  }

  // funzione che crea un'array con gli utenti
  mapClientiToAutocomplete (arrayUsers) {
    let usersForAutocomplete = []
    arrayUsers.forEach(function (user) {
      let valueToSearch
      if (user.firstName && user.lastName) {
        valueToSearch = user.firstName + ' ' + user.lastName
      } else {
        valueToSearch = user.codiceFiscale
      }
      usersForAutocomplete.push({
        key: user.id,
        value: valueToSearch,
        label: valueToSearch,
      })
    })
    return usersForAutocomplete
  }

  render () {
    let clienti = this.mapClientiToAutocomplete(this.props.clienti)
    return (
      <div className="search-box padding-r-10 padding-l-10">
        <div className={`search-box-content padding-10 ${this.state.searchDrawerCollapse ? 'closed' : ''}`}>
          <Autocomplete
            id="customer"
            placeholder="Nome, Cognome o CF"
            threshold={0}
            suggestions={clienti}
            onChange={(value, matched) => this.handleCustomerSelect(value, matched)}
            value={this.state.custormerSelectedValue}
          />
          <DatePicker
            placeholder="Data inizio"
            onDateChange={date => this.handleStartDateChange(date)}
            date={this.state.startDate}
          />
          <DatePicker
            placeholder="Data fine"
            onDateChange={date => this.handleEndDateChange(date)}
            date={this.state.endDate}
          />
          <Input
            type="select"
            onChange={event => this.handleStatusChange(event.target.value)}
            value={this.state.status}
          >
            <option value="">Seleziona stato</option>
            <option value="1">Bozza</option>
            <option value="2">Da Firmare</option>
            <option value="3">AV da compilare</option>
            <option value="4">Completata</option>
            <option value="5">Scaduta</option>
          </Input>
        </div>
        <div className={`search-box-handle padding-r-5 padding-l-5 ${this.state.searchDrawerCollapse ? 'closed' : ''}`}>
          <div>
            <HomeFiltersChips
              query={this.props.query}
              resetFields={this.resetFields}
              handleCustomerSelect={this.handleCustomerSelect}
              handleSearchTextChange={this.handleSearchTextChange}
              handleStartDateChange={this.handleStartDateChange}
              handleEndDateChange={this.handleEndDateChange}
              handleStatusChange={this.handleStatusChange}
              custormerSelected={this.state.custormerSelectedValue}
            />
            <span className="search-toggle" onClick={() => this.setState({ searchDrawerCollapse: !this.state.searchDrawerCollapse })}>
              <span>Ricerca</span>
              <Icon className="search-icon">keyboard_arrow_down</Icon>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

HomeFilters.propTypes = {
  servicesSearch: PropTypes.func,
  query: PropTypes.object,
  clienti: PropTypes.array
}

export default HomeFilters
