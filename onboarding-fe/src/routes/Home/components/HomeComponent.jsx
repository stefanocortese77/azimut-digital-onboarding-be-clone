import React from 'react'
import PropTypes from 'prop-types'
import Footer from '../../../components/Footer'
import { Row, Col, Button } from 'react-materialize'
import AsideUsers from '../../../components/AsideUsers'
import staticData from '../static-data/data-main.json'
import HomeFilters from './HomeFilters'
import ServiceCard from './ServiceCard'

class HomeComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      filtersVisibility: true,
      idIncarico : null,
      url: null
    }

    this.onFiltersVisibilityChange = this.onFiltersVisibilityChange.bind(this)
    this.getPratica = this.getPratica.bind(this)
  }

  onFiltersVisibilityChange () {
    this.setState({
      filtersVisibility: !this.state.filtersVisibility
    })
  }
  componentDidUpdate (prevProps) {
    if (prevProps.praticaRequest.fetching === true && this.props.praticaRequest.fetching === false && this.props.praticaRequest.error === null) {
      // this.props.setIdIncarico(this.props.pratica)
      let idCliente = this.props.pratica.intestatari[0].id
      let codiceFiscale = this.props.pratica.intestatari[0].codiceFiscale
      this.props.setIdCustomer(idCliente)
      this.props.setCodiceFiscale(codiceFiscale)
      this.props.setIdIncarico(this.state.idIncarico)
      if (this.state.url) { this.props.history.push(this.state.url) }
    }
  }
  getPratica (idIncarico, idStatus, idTipologia) {
    this.setState({ idIncarico: idIncarico })
    this.props.getPratica(idIncarico)
    this.getUrl(idStatus, idTipologia)
  }
  getUrl (idStatus, idTipologia) {
    // MAPPA LA URL SULLA BASE DELLA TIPOLOGIA SERVIZO E IL SUO STATUS
    let url = null
    // 1. BOZZA [CONSULENZA, INVESITMENTO] 2. FIRMA 3. ADEGUATA VERIFICA
    switch (idStatus) {
      case 1:
        switch (idTipologia) {
          case 'CONSULENZA':
            url = '/consulenza-base/acquisizione-documenti'
            break
          case 'INVESTIMENTO':
            url = '/apertura-servizi/configurazione'
            break
        }
        break
      case 2:
        url = '/Firma'
        break
      case 3:
        url = '/adeguata-verifica'
        break
    }
    this.setState({ url: url })
  }

  render () {
    const { services, servicesSearch, query, clienti } = this.props

    let servicesElements = []

    if (services.lenght === 0) {
      servicesElements.push(
        <Row>
          <Col className="suggestion-card s10 offset-s1">
            <img className="iconographic-folder" src="img/folder.png" />
            {staticData.home_suggestion_servizio}
            <Button flat waves="light">Scopri le funzioni</Button>
          </Col>
        </Row>
      )
    } else {
      servicesElements = services.map((service, i) => (
        <ServiceCard key={i} service={service} callBack={this.getPratica} />
      ))
    }

    return (
      <Row className="container-home paddingless marginless">
        <Col s={9} className="container-home-left">
          <div className="home-scroll">
            <HomeFilters servicesSearch={servicesSearch} query={query} clienti={clienti} />
            { servicesElements }
          </div>
          <Footer />
        </Col>
        <Col s={3} className="paddingless"> <AsideUsers /></Col>
      </Row>
    )
  }
}

HomeComponent.propTypes = {
  services: PropTypes.array,
  servicesSearch: PropTypes.func,
  query: PropTypes.object,
  clienti: PropTypes.array,
  getPratica: PropTypes.func,
  praticaRequest: PropTypes.object,
  setIdIncarico: PropTypes.func,
  pratica: PropTypes.object,
  setIdCustomer: PropTypes.func,
  setCodiceFiscale: PropTypes.func,
  history: PropTypes.object
}

export default HomeComponent
