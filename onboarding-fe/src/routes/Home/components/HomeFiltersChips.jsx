import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-materialize'

// The react-materialize one is so crappy that doesn't
// even expose the close icon click event
const Chip = ({ onClose, children }) => (
  <div className="chip">
    {children}
    <i className="close material-icons" onClick={onClose}>close</i>
  </div>
)

Chip.propTypes = {
  onClose: PropTypes.func,
  children: PropTypes.node,
}

const HomeFiltersChips = (props) => {
  const {
    query: { conditions },
    resetFields,
    handleSearchTextChange,
    handleStartDateChange,
    handleEndDateChange,
    handleStatusChange,
    custormerSelected
  } = props

  let searchingSpan = null
  let removeFiltersButton = null
  if (conditions.length > 0) {
    searchingSpan = (
      <b className="margin-r-10">
        Stai cercando:
      </b>
    )
    removeFiltersButton = (
      <Button className="red" waves="light" icon="clear" onClick={resetFields} />
    )
  }


  let nameChip = null
  const nameCondition = conditions.find(c => c.field === 'idCustomer')
  if (nameCondition) {
    nameChip = (
      <Chip onClose={() => handleSearchTextChange('')}>
        { custormerSelected }
      </Chip>
    )
  }

  let dataInizioChip = null
  const dataInizioCondition = conditions.find(c => c.field === 'startDate')
  if (dataInizioCondition) {
    dataInizioChip = (
      <Chip onClose={() => handleStartDateChange('')}>
        Dal { dataInizioCondition.value.format('DD/MM/YYYY') }
      </Chip>
    )
  }

  let dataFineChip = null
  const dataFineCondition = conditions.find(c => c.field === 'endDate')
  if (dataFineCondition) {
    dataFineChip = (
      <Chip onClose={() => handleEndDateChange(null)}>
        Al { dataFineCondition.value.format('DD/MM/YYYY') }
      </Chip>
    )
  }

  let statusChip = null
  const statusCondition = conditions.find(c => c.field === 'status')
  if (statusCondition) {
    statusChip = (
      <Chip onClose={() => handleStatusChange('')}>
        { statusCondition.value }
      </Chip>
    )
  }

  return (
    <div className="home-filters-chips">
      <div>
        { removeFiltersButton }
        { searchingSpan }
        { nameChip }
        { dataInizioChip }
        { dataFineChip }
        { statusChip }
      </div>
    </div>
  )
}

HomeFiltersChips.propTypes = {
  query: PropTypes.object,
  resetFields: PropTypes.func,
  handleSearchTextChange: PropTypes.func,
  handleStartDateChange: PropTypes.func,
  handleEndDateChange: PropTypes.func,
  handleStatusChange: PropTypes.func,
  custormerSelected: PropTypes.object
}

export default HomeFiltersChips
