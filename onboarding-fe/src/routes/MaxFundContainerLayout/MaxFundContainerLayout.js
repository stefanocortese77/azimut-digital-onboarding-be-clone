import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-materialize'
import { Route, Switch, Link } from 'react-router-dom'

import { MaxFund } from './MaxFund'
import { Intestatari } from '../../components/Intestatari'

export class MaxFundContainerLayout extends React.Component {
  backHome () {
    this.props.history.push('/')
  }

  render () {
    return (

      <Row className="container-home paddingless marginless">
        <Col s={12} className="container-home-left">
          <section className="main-body no-aside">
            <header>
              <Link onClick={() => this.backHome()} to="#" className="torna-pratiche">
                <i className="icon-arrow-back" />Torna a elenco pratiche
              </Link>
              <h1 className="title-macrosezione">Servizio Max Fund</h1>
            </header>
            <article className="page-left-content">
              <div className="background_pattern" />

              <Switch>
                <Route path={`${this.props.match.url}/configurazione`} component={MaxFund} />
                <Route path={`${this.props.match.url}/intestatari`} component={Intestatari} />
              </Switch>

            </article>
          </section>
        </Col>
      </Row>

    )
  }
}

MaxFundContainerLayout.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object
}

export default MaxFundContainerLayout
