import { connect } from 'react-redux'
import RiepilogoCliente from '../components/RiepilogoCliente.jsx'
import { getDataSummaryLight, getDataSummaryFull } from '../../../../store/modules/customers/actions'
import { dominio } from '../../../../store/modules/dominio/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente, idIncarico) => dispatch(getDataSummaryLight(idCliente, idIncarico)),
    getDataSummaryFull : (idCliente, idIncarico) => dispatch(getDataSummaryFull(idCliente, idIncarico)),
    dominio : (queryParam) => dispatch(dominio(queryParam)),
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    summaryData: state.customers.summaryData,
    summaryDataFull: state.customers.summaryDataFull,
    domini: state.dominio.domini,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RiepilogoCliente)
