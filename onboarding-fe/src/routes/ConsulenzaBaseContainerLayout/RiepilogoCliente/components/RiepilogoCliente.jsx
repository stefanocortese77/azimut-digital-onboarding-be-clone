import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Button, Col } from 'react-materialize'
import { Link } from 'react-router-dom'
import { Collapsible, CollapsibleItem } from '../../../../components/Collapsible'
import dataSB from '../../../../static-data/data-servizio-base.json'

export class RiepilogoCliente extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      customer:{},
      openCollapsible: 'uno'
    }

    this.backHome = this.backHome.bind(this)
    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)
    this.goToSection = this.goToSection.bind(this)
  }

  componentDidMount () {
    this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
    this.props.getDataSummaryFull(this.props.customer.idCliente, this.props.customer.idIncarico)
    this.props.dominio({
      'listaDomini': [
        {
          'idDominio': 'DOM_PROVINCE'
        },
        {
          'idDominio': 'DOM_NAZIONI'
        },
        {
          'idDominio': 'DOM_CITTADINANZE'
        },
        {
          'idDominio': 'DOM_TITOLO_STUDIO'
        },
        {
          'idDominio': 'DOM_SETTORE_OCCUPAZIONE_I'
        },
        {
          'idDominio': 'DOM_PROFESSIONE_ATTUALE'
        },
        {
          'idDominio': 'DOM_NATURA_RAPPORTO'
        },
        {
          'idDominio': 'DOM_ORIGINE_VERSAMENTO'
        },
        {
          'idDominio': 'DOM_TIPO_DOCUMENTO'
        }
      ]
    })// carica tutti i domini
  }

  backHome () {
    this.props.history.push('/')
  }

  goToSection (sezione, sottoSezione) {
    // this.props.history.push(sezione)
    this.props.history.push({
      pathname: sezione,
      state: { collapsibleToOpen: sottoSezione }
    })
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({ openCollapsible: itemKey })
  }


  render () {
    if (Object.keys(this.props.domini).length === 0 && this.props.domini.constructor === Object) { return null }
    if (this.props.summaryDataFull === null) { return null }
    const infoCliente = this.props.summaryDataFull

    const torna = dataSB.button_back
    const title = dataSB.title
    const titleSection = dataSB.riepilogo.title_section
    const modificaText = 'Modifica'

    const modificaCF = (
      <span className="accordion-info accordion-modifica" onClick={() => this.goToSection('/consulenza-base/acquisizione-documenti', 'codice-fiscale')}>{modificaText}</span>
    )
    const modificaDoc = (
      <span className="accordion-info accordion-modifica" onClick={() => this.goToSection('/consulenza-base/acquisizione-documenti', 'documento-identita')}>{modificaText}</span>
    )
    const modificaInfo = (
      <span className="accordion-info accordion-modifica" onClick={() => this.goToSection('/consulenza-base/censimento-anagrafico', 0)}>{modificaText}</span>
    )
    const modificaResidenza = (
      <span className="accordion-info accordion-modifica" onClick={() => this.goToSection('/consulenza-base/censimento-anagrafico', 1)}>{modificaText}</span>
    )

    return (
      <section className="main-body">
        <header>
          <Link onClick={this.backHome} to="#" className="torna-pratiche"><i className="icon-arrow-back" />{torna}</Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>
        <article className="page-left-content sezione-riepilogo">
          <div className="background_pattern" />
          <h2 className="title-sezione">{titleSection}</h2>

          <Collapsible autoToggle>
            <CollapsibleItem itemKey="uno" header="Codice Fiscale" icon="keyboard_arrow_down" labelInfo={modificaCF}>
              <p>Foto Codice Fiscale</p>
            </CollapsibleItem>
            <CollapsibleItem itemKey="due" header="Documento d'identità" icon="keyboard_arrow_down" labelInfo={modificaDoc}>
              <p>{infoCliente.sezioneDocumentiIdentita.numeroDocumenti}</p>
            </CollapsibleItem>
            <CollapsibleItem itemKey="tre" header="Informazioni personali" icon="keyboard_arrow_down" labelInfo={modificaInfo}>
              <p>{infoCliente.sezioneInformazioniPersonali.nome}</p>
              <p>{infoCliente.sezioneInformazioniPersonali.cognome}</p>
            </CollapsibleItem>
            <CollapsibleItem itemKey="quattro" header="Residenza" icon="keyboard_arrow_down" labelInfo={modificaResidenza}>
              <p>Telefono</p>
            </CollapsibleItem>
          </Collapsible>

          <Row className="center-align margin-t-40">
            <Col s={12}>
              <Button onClick="" >Firma</Button>
            </Col>
          </Row>
          <Row className="center-align margin-t-40">
            <Col s={12}>
              <Link onClick="" to="#">Salva e firma dopo</Link>
            </Col>
          </Row>
        </article>

      </section>
    )
  }
}

RiepilogoCliente.propTypes = {
  customer: PropTypes.object,
  history: PropTypes.object,
  summaryDataFull: PropTypes.object,
  domini: PropTypes.object,
  getDataSummaryLight: PropTypes.func,
  getDataSummaryFull: PropTypes.func,
  dominio: PropTypes.func
}

export default (RiepilogoCliente)
