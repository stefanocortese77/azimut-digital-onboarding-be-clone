import { connect } from 'react-redux'
import QuestionarioProfilatura from '../components/QuestionarioProfilatura'
import { getDataSummaryLight, getDocumentsData } from '../../../../store/modules/customers/actions'
import { fetchSurveyTemplate, fetchSurvey, saveSurvey } from '../../../../store/modules/survey/actions'
import { setDataPending } from '../../../../store/modules/dataPending/actions'
import { dominio } from '../../../../store/modules/dominio/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente, idIncarico) => dispatch(getDataSummaryLight(idCliente, idIncarico)),
    getDocumentsData : (idCliente, idIncarico) => dispatch(getDocumentsData(idCliente, idIncarico)),
    dominio : (queryParam) => dispatch(dominio(queryParam)),
    fetchSurveyTemplate : () => dispatch(fetchSurveyTemplate()),
    fetchSurvey : (idCliente, flagRiepilogo, active) => dispatch(fetchSurvey(idCliente, flagRiepilogo, active)),
    saveSurvey : (idCliente, listaDomande) => dispatch(saveSurvey(idCliente, listaDomande)),
    setDataPending : (value) => dispatch(setDataPending(value))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    domini: state.dominio.domini,
    template: state.survey.template,
    previousForm: state.survey.previousForm,
    mifidRequest: state.survey.mifidRequest,
    mifidSaveRequest: state.survey.mifidSaveRequest,
    dataPending: state.dataPendings.dataPendingState,
    summaryData : state.customers.summaryData,
    getDataSummaryRequest: state.customers.getDataSummaryRequest
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionarioProfilatura)
