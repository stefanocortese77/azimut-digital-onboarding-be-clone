export default {
  // Conoscesnza ed Esperienza
  TITOLO_STUDIO: {
    id: 'titoloStudio',
    templateId: 5,
  },
  SETTORE_OCCUPAZIONE: {
    id: 'settoreOccupazione',
    templateId: 6,
  },
  SETTORE_PROFESSIONE: {
    id: 'settoreProfessione',
    templateId: 7,
  },
  ALTRI_CONTRATTI_CONSULENZA: {
    id: 'altriContrattiConsulenza',
    templateId: 8,
  },
  ALTRI_CONTRATTI_COLLOCAMENTO: {
    id: 'altriContrattiCollocamento',
    templateId: 9,
  },
  STRUMENTI: {
    id: 'stumenti',
    templateId: 10,
  },
  VERO_FALSO: {
    id: 'veroFalso',
    templateId: 21,
  },
  // ------------------------------


  OBIETTIVI: {
    id: 'obiettivi',
    templateId: 32,
  },
  RISCHIO: {
    id: 'rischio',
    templateId: 33,
  },
  ORIZZONTE_TEMPORALE: {
    id: 'orizzonteTemporale',
    templateId: 34,
  },
  FONTE_REDDITO: {
    id: 'fonteReddito',
    templateId: 35,
  },
  FONTE_REDDITO_ALTRO: {
    id: 'fonteRedditoAltro',
    templateId: 36,
  },
  REDDITO_FUTURO: {
    id: 'redditoFuturo',
    templateId: 37,
  },
  PATRIMONIO_FINANZIARIO: {
    id: 'patrimonioFinanziario',
    templateId: 38,
  },
  INVESTIMENTI_IMMOBILIARI: {
    id: 'investimentiImmobiliari',
    templateId: 39,
  },
  IMPEGNI_FINANZIARI_MENSILI: {
    id: 'impegniFinanziariMensili',
    templateId: 40,
  },

  RICCHEZZA_FINANZIARIA: {
    id: 'ricchezzaFinanziaria',
    templateId: 41,
  },
  INVESTIMENTO_STRUMENTI_AZIONARI: {
    id: 'investimentoStrumentiAzionari',
    templateId: 42,
  },
  RICCHEZZA_NON_INVESTITA: {
    id: 'ricchezzaNonInvestita',
    templateId: 43,
  },
}
