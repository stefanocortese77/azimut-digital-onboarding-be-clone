import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Button } from 'react-materialize'

import withFieldCatch from '../../../../hocs/withFieldCatch/withFieldCatch'
import { CollapsibleItem } from '../../../../components/Collapsible'
import { fieldPropTypes, filterQuestions, loadQuestions, getFieldValue, updateField } from '../../utils'
import fields from '../fields'

const RicchezzaFinanziaria = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const idDomanda = obiettiviTemplate.idDomanda

  const radios = obiettiviTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta }) => {
      const id = `radio-${idDomanda}-${idRisposta}`

      return (
        <p key={idRisposta}>
          <input
            name={id}
            type="radio"
            id={id}
            className="with-gap"
            onChange={(value) => updateField(idDomanda, idRisposta)}
            checked={getFieldValue(idDomanda) === idRisposta} />
          <label htmlFor={id}>{descRisposta}</label>
        </p>
      )
    })

  return (
    <Row s={6}>
      <h6>
        {obiettiviTemplate.testoDomanda}
      </h6>
      <form action="#" className="radio-vertical">
        {radios}
      </form>
    </Row>
  )
}

RicchezzaFinanziaria.propTypes = fieldPropTypes
const WrappedRicchezzaFinanziaria = withFieldCatch(RicchezzaFinanziaria)

const StrumentiAzionari = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const idDomanda = obiettiviTemplate.idDomanda

  const radios = obiettiviTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta }) => {
      const id = `radio-${idDomanda}-${idRisposta}`

      return (
        <p key={idRisposta}>
          <input
            name={id}
            type="radio"
            id={id}
            className="with-gap"
            onChange={(value) => updateField(idDomanda, idRisposta)}
            checked={getFieldValue(idDomanda) === idRisposta} />
          <label htmlFor={id}>{descRisposta}</label>
        </p>
      )
    })

  return (
    <Row s={6}>
      <h6>
        {obiettiviTemplate.testoDomanda}
      </h6>
      <form action="#" className="radio-vertical">
        {radios}
      </form>
    </Row>
  )
}

StrumentiAzionari.propTypes = fieldPropTypes
const WrappedStrumentiAzionari = withFieldCatch(StrumentiAzionari)

const RicchezzaNonInvestita = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const ricchezzaNonInvestitaTemplate = listaDomande.find(c => c.idDomanda === field.templateId)

  const onBarChange = idRisposta => ({ target: { value } }) => {
    let filteredValue = parseInt(value.replace(/[^0-9]/, ''))

    const otherBarsTotal = ricchezzaNonInvestitaTemplate
      .listaRisposte
      .filter(r => r.idRisposta !== idRisposta)
      .reduce((t, { idRisposta }) => t + parseInt(getFieldValue(idRisposta) || 0), 0)

    if (!filteredValue) {
      filteredValue = null
    }

    if (filteredValue > 100) {
      filteredValue = 100
    }

    if (otherBarsTotal + filteredValue > 100) {
      filteredValue = (100 - otherBarsTotal)
    }

    updateField(idRisposta, filteredValue)
  }

  const rows = ricchezzaNonInvestitaTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta }, i) => {
      // const id = `radio-${idDomanda}-${idRisposta}`

      return (
        <div key={idRisposta} className="input-row">
          <div className={`color-key color-key-line color-${i}`} />
          <div>
            { descRisposta }
          </div>
          <div className="input-wrapper">
            <input
              type="number"
              onChange={onBarChange(idRisposta)}
              value={getFieldValue(idRisposta)} />
          </div>
        </div>
      )
    })

  let barItems = []
  let emptyBarMessage = null

  const barTotal = ricchezzaNonInvestitaTemplate
    .listaRisposte
    .reduce((t, { idRisposta }) => t + parseInt(getFieldValue(idRisposta) || 0), 0)

  if (barTotal > 0) {
    barItems = ricchezzaNonInvestitaTemplate
      .listaRisposte
      .map(({ idRisposta }, i) => {
        const width = getFieldValue(idRisposta)

        if (!width) {
          return null
        }

        return (
          <div key={i} className={`color-key color-${i}`} style={{ width: `${width}%` }} />
        )
      })
  } else {
    emptyBarMessage = (
      <p className="empty-bar-msg">{'(compila i campi per popolare la barra indicativa)'}</p>
    )
  }

  return (
    <Row s={6} className="ricchezza-non-investita">
      <h6>
        {ricchezzaNonInvestitaTemplate.testoDomanda}
      </h6>
      <form action="#" className="">
        {rows}
      </form>
      <div className="bar-container">
        <div className="bar">
          { barItems }
        </div>
        { emptyBarMessage }
      </div>
    </Row>
  )
}

RicchezzaNonInvestita.propTypes = fieldPropTypes
const WrappedRicchezzaNonInvestita = withFieldCatch(RicchezzaNonInvestita)

class AltreInformazioni extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
    }, loadQuestions(this.props.previousForm, this.props.template.listaDomande))

    this.save = this.save.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.mifidSaveRequest.fetching && !this.props.mifidSaveRequest.fetching) {
        if (!this.props.mifidSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }

  render () {
    const { domini, template } = this.props
    const { descSezione, listaDomande } = template

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: (idDomanda, value) => {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      }
    }

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="altre-informazioni"
        header={descSezione}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}
      >
        <WrappedRicchezzaFinanziaria
          field={fields.RICCHEZZA_FINANZIARIA}
          {...sectionContext}
        />
        <WrappedStrumentiAzionari
          field={fields.INVESTIMENTO_STRUMENTI_AZIONARI}
          {...sectionContext}
        />
        <WrappedRicchezzaNonInvestita
          field={fields.RICCHEZZA_NON_INVESTITA}
          {...sectionContext}
        />
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

AltreInformazioni.propTypes = {
  domini: PropTypes.object,
  onSave: PropTypes.func,
  setDataPending: PropTypes.func,
  template: PropTypes.object,
  previousForm: PropTypes.object,
  mifidSaveRequest: PropTypes.object,
}

export default(AltreInformazioni)
