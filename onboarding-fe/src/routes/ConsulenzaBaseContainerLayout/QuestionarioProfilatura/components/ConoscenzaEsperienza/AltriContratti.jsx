import React from 'react'
import { PropTypes } from 'prop-types'
import YesNoQuestion from '../../../../../components/YesNoQuestion'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
  fieldPropTypes,
  booleanToFieldId,
  fieldIdToBoolean,
  getFieldValue,
  updateField
} from '../../../utils'

const AltriContratti = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const contrattiTemplate = listaDomande.find(c => c.idDomanda === field.templateId)

  const trueId = contrattiTemplate
    .listaRisposte
    .find(d => d.descRisposta === 'SI')
    .idRisposta
  const falseId = contrattiTemplate
    .listaRisposte
    .find(d => d.descRisposta === 'NO')
    .idRisposta

  const booleanIdMap = booleanToFieldId(trueId, falseId)
  const idBooleanMap = fieldIdToBoolean(trueId, falseId)

  return (<YesNoQuestion
    label={contrattiTemplate.testoDomanda}
    onChange={(value) => updateField(contrattiTemplate.idDomanda, booleanIdMap(value))}
    value={idBooleanMap(getFieldValue(contrattiTemplate.idDomanda))} />)
}

AltriContratti.propTypes = fieldPropTypes

export default withFieldCatch(AltriContratti)