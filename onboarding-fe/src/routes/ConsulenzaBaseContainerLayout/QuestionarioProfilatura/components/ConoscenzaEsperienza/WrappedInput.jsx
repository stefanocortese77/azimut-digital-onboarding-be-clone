import React from 'react'
import { PropTypes } from 'prop-types'
import { Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
  fieldPropTypes,
  mapDominiToOptions,
  getDominio
} from '../../../utils'

const WrappedInput = ({ listaDomande, field, domini, getFieldValue, updateField, dominio }) => {
  const template = listaDomande.find(c => c.idDomanda === field.templateId)
  const options = mapDominiToOptions(getDominio(domini, dominio))

  return (
    <Input
      s={6}
      type="select"
      label={template.testoDomanda}
      onChange={({ target: {
        value
      } }) => updateField(template.idDomanda, value)}
      value={getFieldValue(template.idDomanda)}>
      {options}
    </Input>
  )
}

WrappedInput.propTypes = fieldPropTypes

export default withFieldCatch(WrappedInput)