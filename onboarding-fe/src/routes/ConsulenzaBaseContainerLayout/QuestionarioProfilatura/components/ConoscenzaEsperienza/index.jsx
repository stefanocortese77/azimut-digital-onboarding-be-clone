import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Button } from 'react-materialize'
import { CollapsibleItem } from '../../../../../components/Collapsible'

import fields from '../../fields'
import {
  filterQuestions,
  loadQuestions,
  getFieldValue,
  updateField
} from '../../../utils'
import './index.scss'
import WrappedStrumenti from './Strumenti'
import WrappedInput from './WrappedInput'
import WrappedAltriContratti from './AltriContratti'
import WrappedVeroFalso from './VeroFalso'

// Funzione per controllare la checkbox percentuali
const checkPercentuale = el => el === 17;

class ConoscenzaEsperienza extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
      limitPercentuale: false,
      showError: false,
    }, loadQuestions(this.props.previousForm, this.props.template.listaDomande))

    this.calcPercentuali = this.calcPercentuali.bind(this)
    this.updateFieldStrumenti = this.updateFieldStrumenti.bind(this)
    this.save = this.save.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    const percentuali = this.calcPercentuali(this.state).length
    const prevPercentuali = this.calcPercentuali(prevState).length
    if (this.state.saving === true) {
      if (prevProps.mifidSaveRequest.fetching && !this.props.mifidSaveRequest.fetching) {
        if (!this.props.mifidSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    } else if (percentuali < prevPercentuali) {
      // Rimuove il flag e l'errore in caso una checkbox di percentuale venga deflaggata
      this.setState({ limitPercentuale: false, showError: false })
    }
  }

  // Ritorna un array con le idDomanda delle domande che hanno percentuale = true
  calcPercentuali(state) {
    return this.props.template.listaDomande[5].strutturaInternaDomande.domande.reduce((res, cur) => {
      if (state[cur.idDomanda] && state[cur.idDomanda].some(checkPercentuale)) {
        return [ ...res, cur.idDomanda ]
      }
      return res
    }, [])
  }

  /**
   * 
   * Controlla il numero di risposte 17 (>50 %)
   * Se ce ne è già una, setta il flag limitPercentuale
   * Se sono già due, visualizza il messaggio di errore ed impedisce l'aggiornamento
   */
  updateFieldStrumenti(idDomanda, value) {
    const percentuali = this.calcPercentuali(this.state)
    if (!value.some(checkPercentuale) || percentuali.length === 0) {
      this.setState(updateField(idDomanda, value))
      this.props.setDataPending()
    } else if (this.state.limitPercentuale && value.some(checkPercentuale)) {
      if (percentuali.includes(idDomanda)) {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      } else {
        this.setState({ showError: true })
      }
    } else if (percentuali.length === 1 && value.some(checkPercentuale)) {
      this.setState({
        ...updateField(idDomanda, value),
        limitPercentuale: !percentuali.includes(idDomanda)
      })
      this.props.setDataPending()
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }

  render () {
    const { template, domini } = this.props
    const { descSezione, listaDomande } = template

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: (idDomanda, value) => {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      },
      updateFieldStrumenti: this.updateFieldStrumenti
    }

    const dipendenze = Object
      .keys(this.state)
      .filter(fieldName => !isNaN(parseInt(fieldName)))
      .reduce((a, field) => Object.assign(a, { [field]: this.state[field] }), {})

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="conoscenza-esperienza"
        header={descSezione}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}
      >
        <Row>
          <WrappedInput
            field={fields.TITOLO_STUDIO}
            dominio="DOM_TITOLO_STUDIO"
            {...sectionContext}/>
          <WrappedInput
            field={fields.SETTORE_OCCUPAZIONE}
            dominio="DOM_SETTORE_OCCUPAZIONE_I"
            {...sectionContext}/>
          <WrappedInput
            field={fields.SETTORE_PROFESSIONE}
            dominio="DOM_SETTORE_OCCUPAZIONE_I"
            {...sectionContext}/>
          <span className="padding-10">
            Se pensionato/a indicare la posizione precedente
          </span>
        </Row>
        <Row>
          <WrappedAltriContratti
            field={fields.ALTRI_CONTRATTI_CONSULENZA}
            {...sectionContext} />
          <WrappedAltriContratti
            field={fields.ALTRI_CONTRATTI_COLLOCAMENTO}
            {...sectionContext} />
        </Row>
        <Row>
          <Row>
            <Col s={4}>
              <h2 className="collapsible-section-title">
                Strumenti
              </h2>
            </Col>
            <Col s={8}>
              {this.state.showError && <div className="error-message-percentuale">Attenzione: questa azione è possibile fino ad un massimo di 2 volte</div>}
            </Col>
          </Row>
          <Row>
            <WrappedStrumenti field={fields.STRUMENTI} {...sectionContext} />
          </Row>
        </Row>
        <Row>
          <h2 className="collapsible-section-title">
            Vero o falso?
          </h2>
          <WrappedVeroFalso
            field={fields.VERO_FALSO}
            dipendenze={dipendenze}
            {...sectionContext} />
        </Row>
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

ConoscenzaEsperienza.propTypes = {
  domini: PropTypes.object,
  onSave: PropTypes.func,
  setDataPending: PropTypes.func,
  template: PropTypes.object,
  previousForm: PropTypes.object,
  mifidSaveRequest: PropTypes.object,
}

export default(ConoscenzaEsperienza)
