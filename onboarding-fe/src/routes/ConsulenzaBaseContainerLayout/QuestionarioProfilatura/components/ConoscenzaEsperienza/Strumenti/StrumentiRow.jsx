import React, { Fragment } from 'react'
import { PropTypes } from 'prop-types'
import { Input } from 'react-materialize'

class StrumentiRow extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
        conoscenza: null,
        operazioni: null,
        percentuale: null
      }
    }
  
    componentDidUpdate (prevProps, prevState) {
      const { risposte: { RISPOSTE_OPERAZIONI }, fieldValue } = this.props
      if (prevState.conoscenza !== this.state.conoscenza ||
          prevState.operazioni !== this.state.operazioni ||
          prevState.percentuale !== this.state.percentuale) {
        const { updateFieldStrumenti, risposte: { RISPOSTE_OPERAZIONI } } = this.props
        const { conoscenza, operazioni, percentuale } = this.state

        updateFieldStrumenti([ conoscenza, operazioni, percentuale ])
      } else if (!fieldValue.includes(RISPOSTE_OPERAZIONI.GT50.idRisposta) && this.state.percentuale) {
        this.setState({ percentuale: null })
      }
    }
  
    render () {
      const {
        domanda: {
          idDomanda,
          testoDomanda,
          subTestoDomanda
        },
        risposte: {
          RISPOSTE_CONOSCENZA,
          RISPOSTE_OPERAZIONI
        },
        fieldValue
      } = this.props
  
      return (
        <div className="instruments-row">
          <div>
            <div className="instruments-col-border">
              <b>{ testoDomanda }</b>
              <div>{ subTestoDomanda }</div>
            </div>
          </div>
          <div>
            <div className="instruments-col-border">
              <form action="#" className="radio-horizontal">
                <div className="radio">
                  <input
                    name={`${RISPOSTE_CONOSCENZA.SI.idRisposta}`}
                    type="radio"
                    id={`risposte-conoscenza-si-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ conoscenza: RISPOSTE_CONOSCENZA.SI.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_CONOSCENZA.SI.idRisposta)}
                  />
                  <label htmlFor={`risposte-conoscenza-si-${idDomanda}`} />
                </div>
                <div className="radio">
                  <input
                    name={`${RISPOSTE_CONOSCENZA.NO.idRisposta}`}
                    type="radio"
                    id={`risposte-conoscenza-no-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ conoscenza: RISPOSTE_CONOSCENZA.NO.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_CONOSCENZA.NO.idRisposta)}
                  />
                  <label htmlFor={`risposte-conoscenza-no-${idDomanda}`} />
                </div>
                <div className="radio">
                  <input
                    name={`${RISPOSTE_CONOSCENZA.PARZ.idRisposta}`}
                    type="radio"
                    id={`risposte-conoscenza-parz-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ conoscenza: RISPOSTE_CONOSCENZA.PARZ.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_CONOSCENZA.PARZ.idRisposta)}
                  />
                  <label htmlFor={`risposte-conoscenza-parz-${idDomanda}`} />
                </div>
              </form>
            </div>
          </div>
          <div>
            <div className="instruments-col-border">
              <form action="#" className="radio-horizontal">
                <div className="radio">
                  <input
                    name={`${RISPOSTE_OPERAZIONI.GT10.idRisposta}`}
                    type="radio"
                    id={`risposte-operazioni-gt10-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ operazioni: RISPOSTE_OPERAZIONI.GT10.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_OPERAZIONI.GT10.idRisposta)}
                  />
                  <label htmlFor={`risposte-operazioni-gt10-${idDomanda}`} />
                </div>
                <div className="radio">
                  <input
                    name={`${RISPOSTE_OPERAZIONI.GT5.idRisposta}`}
                    type="radio"
                    id={`risposte-operazioni-gt5-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ operazioni: RISPOSTE_OPERAZIONI.GT5.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_OPERAZIONI.GT5.idRisposta)}
                  />
                  <label htmlFor={`risposte-operazioni-gt5-${idDomanda}`} />
                </div>
                <div className="radio">
                  <input
                    name={`${RISPOSTE_OPERAZIONI.LT5.idRisposta}`}
                    type="radio"
                    id={`risposte-operazioni-lt5-${idDomanda}`}
                    className="with-gap"
                    onChange={(value) => this.setState({ operazioni: RISPOSTE_OPERAZIONI.LT5.idRisposta })}
                    checked={fieldValue.includes(RISPOSTE_OPERAZIONI.LT5.idRisposta)}
                  />
                  <label htmlFor={`risposte-operazioni-lt5-${idDomanda}`} />
                </div>
                <div className="radio">
                  <input
                    name={`${RISPOSTE_OPERAZIONI.NO.idRisposta}`}
                    type="radio"
                    id={`risposte-operazioni-no-${idDomanda}`}
                    className="with-gap"
                    onChange={value => this.setState({ operazioni: RISPOSTE_OPERAZIONI.NO.idRisposta, percentuale: null })}
                    checked={fieldValue.includes(RISPOSTE_OPERAZIONI.NO.idRisposta)}
                  />
                  <label htmlFor={`risposte-operazioni-no-${idDomanda}`} />
                </div>
                <div className="check">
                  {this.state.percentuale && (
                    <Fragment>
                      <Input
                        name={`${RISPOSTE_OPERAZIONI.GT50.idRisposta}`}
                        type="checkbox"
                        label="> 50"
                        id={`risposte-operazioni-gt50-${idDomanda}`}
                        className="filled-in vertical"
                        checked={true}
                        disabled={fieldValue.includes(RISPOSTE_OPERAZIONI.NO.idRisposta)}
                        onChange={(ev, value) => this.setState({ percentuale: value ? RISPOSTE_OPERAZIONI.GT50.idRisposta : null })}
                      />
                      <label htmlFor={`risposte-operazioni-gt50-${idDomanda}`} />
                    </Fragment>
                  )}
                  {!this.state.percentuale && (
                    <Fragment>
                        <Input
                          name={`${RISPOSTE_OPERAZIONI.GT50.idRisposta}`}
                          type="checkbox"
                          label="> 50"
                          id={`risposte-operazioni-gt50-${idDomanda}`}
                          className="filled-in vertical"
                          checked={false}
                          disabled={fieldValue.includes(RISPOSTE_OPERAZIONI.NO.idRisposta)}
                          onChange={(ev, value) => this.setState({ percentuale: value ? RISPOSTE_OPERAZIONI.GT50.idRisposta : null })}
                        />
                        <label htmlFor={`risposte-operazioni-gt50-${idDomanda}`} />
                    </Fragment>
                  )}
                </div>
              </form>
            </div>
          </div>
        </div>
      )
    }
}

StrumentiRow.propTypes = {
    domanda: PropTypes.object,
    fieldValue: PropTypes.arrayOf(PropTypes.number),
    risposte: PropTypes.object,
    updateFieldStrumenti: PropTypes.func
}

export default StrumentiRow