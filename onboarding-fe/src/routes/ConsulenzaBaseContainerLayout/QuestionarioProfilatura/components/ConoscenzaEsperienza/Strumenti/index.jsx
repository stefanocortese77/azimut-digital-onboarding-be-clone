import React from 'react'
import { PropTypes } from 'prop-types'
import { fieldPropTypes } from '../../../../utils'
import withFieldCatch from '../../../../../../hocs/withFieldCatch/withFieldCatch'
import StrumentiRow from './StrumentiRow'

const Strumenti = ({ listaDomande, field, domini, getFieldValue, updateFieldStrumenti }) => {
  const strumentiTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const listaDominiRisposte = strumentiTemplate.strutturaInternaDomande.listaDominiRisposte

  const risposte = {
    RISPOSTE_CONOSCENZA: {
      SI: listaDominiRisposte[0].risposte.find(d => d.descRisposta === 'SI'),
      NO: listaDominiRisposte[0].risposte.find(d => d.descRisposta === 'NO'),
      PARZ: listaDominiRisposte[0].risposte.find(d => d.descRisposta === 'Solo parzialmente'),
    },
    RISPOSTE_OPERAZIONI: {
      GT10: listaDominiRisposte[1].risposte[0],
      GT5: listaDominiRisposte[1].risposte[1],
      LT5: listaDominiRisposte[1].risposte[2],
      NO: listaDominiRisposte[1].risposte[3],
      GT50: listaDominiRisposte[1].risposte[4],
    }
  }

  return (
    <div className="instruments-table">
      <div className="instruments-header">
        <div>
          <div className="instruments-col-border" />
        </div>
        <div>
          <div className="instruments-col-border">CONOSCENZA</div>
        </div>
        <div>
          <div className="instruments-col-border">OPERAZIONI PER TRIMESTRE (ultimi 12/18 mesi)</div>
        </div>
      </div>
      <div className="instruments-sub-header">
        <div>
          <div className="instruments-col-border" />
        </div>
        <div>
          <div className="instruments-col-border">
            <div>
              <div>Si</div>
              <div>No</div>
              <div>Parz.</div>
            </div>
          </div>
        </div>
        <div>
          <div className="instruments-col-border">
            <div>
              <div>Più di 10</div>
              <div>Tra 5 e 10</div>
              <div>Meno di 5</div>
              <div>No</div>
              <div>&gt; 50%</div>
            </div>
          </div>
        </div>
      </div>
      {strumentiTemplate.strutturaInternaDomande.domande.map(domanda => (
          <StrumentiRow
            key={domanda.idDomanda}
            domanda={domanda}
            risposte={risposte}
            updateFieldStrumenti={(value) => updateFieldStrumenti(domanda.idDomanda, value)}
            fieldValue={getFieldValue(domanda.idDomanda, true) || []}
          />
      ))}
    </div>
  )
}

Strumenti.propTypes = fieldPropTypes

export default withFieldCatch(Strumenti)
