import React from 'react'
import { PropTypes } from 'prop-types'
import TrueFalseQuestion from '../../../../../components/TrueFalseQuestion'
import fields from '../../fields'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
  fieldPropTypes,
  booleanToFieldId,
  fieldIdToBoolean,
  getFieldValue,
  updateField
} from '../../../utils'

const VeroFalso = ({
  listaDomande,
  field,
  domini,
  getFieldValue,
  updateField,
  dipendenze
}) => {
  const veroFalsoTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const trueId = veroFalsoTemplate.strutturaInternaDomande.listaDominiRisposte[0].risposte[0].idRisposta
  const falseId = veroFalsoTemplate.strutturaInternaDomande.listaDominiRisposte[0].risposte[1].idRisposta

  return veroFalsoTemplate
    .strutturaInternaDomande
    .domande
    .map(domanda => {
      const id = `${fields.VERO_FALSO.id}-${domanda.idDomanda}`

      let enabled = false
      if (domanda.listaDipendenze) {
        enabled = domanda
          .listaDipendenze
          .map(dipendenza => {
            let match = true

            if (!dipendenze[dipendenza.idDomandaDipendenza]) {
              return true
            } else if (Array.isArray(dipendenze[dipendenza.idDomandaDipendenza])) {
              match = dipendenze[dipendenza.idDomandaDipendenza].includes(dipendenza.idRispostaDipendenza)
            } else {
              match = dipendenze[dipendenza.idDomandaDipendenza] === dipendenza.idRispostaDipendenza
            }

            if (dipendenza.tipoDipendenza === 'disabled') {
              return !match
            } else if (dipendenza.tipoDipendenza === 'enabled') {
              return match
            }
          })
          .reduce((and, bool) => and && bool, true)
      }

      const booleanIdMap = booleanToFieldId(trueId, falseId)
      const idBooleanMap = fieldIdToBoolean(trueId, falseId)

      return (
        <TrueFalseQuestion
          key={id}
          title={domanda.testoDomanda}
          subTitle={domanda.subTestoDomanda}
          onChange={(value) => updateField(domanda.idDomanda, booleanIdMap(value))}
          value={idBooleanMap(getFieldValue(domanda.idDomanda))}
          disabled={!enabled} />
      )
    })
}

VeroFalso.propTypes = fieldPropTypes

export default withFieldCatch(VeroFalso)