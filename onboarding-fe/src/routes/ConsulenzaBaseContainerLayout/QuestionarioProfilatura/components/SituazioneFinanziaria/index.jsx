import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Button, Col, Input } from 'react-materialize'

import fields from '../../fields'
import { CollapsibleItem } from '../../../../../components/Collapsible'
import { filterQuestions, loadQuestions, getFieldValue, updateField } from '../../../utils'

import WrappedFonteReddito from './FonteReddito'
import WrappedFonteRedditoAltro from './FonteRedditoAltro'
import GenericRow from './GenericRow'

class SituazioneFinanziaria extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false
    }, loadQuestions(props.previousForm, props.template.listaDomande))

    this.updateField = this.updateField.bind(this)
    this.save = this.save.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.mifidSaveRequest.fetching && !this.props.mifidSaveRequest.fetching) {
        if (!this.props.mifidSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }
  }

  /**
   * 
   * Se si cambia la risposta alla prima domanda a 'Non ho reddito',
   * si annulla l'eventuale risposta alla seconda domanda
   */
  updateField(idDomanda, value) {
    if (idDomanda === 35 && value === 25) {
      this.setState({
        [36]: undefined,
        ...updateField(idDomanda, value)
      })
    } else {
      this.setState(updateField(idDomanda, value))
    }
    this.props.setDataPending()
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }

  render () {
    const { template, domini } = this.props
    const { descSezione, listaDomande } = template

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: this.updateField
    }

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="situazione-finanziaria"
        header={descSezione}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}
      >
        <Row>
          <Col s={6}>
            <WrappedFonteReddito
              field={fields.FONTE_REDDITO}
              {...sectionContext}
            />
          </Col>
          <Col s={6}>
            <WrappedFonteRedditoAltro
              field={fields.FONTE_REDDITO_ALTRO}
              fonteReddito={this.state[35]}
              {...sectionContext}
            />
          </Col>
        </Row>
        <GenericRow
          field={fields.REDDITO_FUTURO}
          formClassName="radio-horizontal"
          {...sectionContext}
        />
        <GenericRow
          field={fields.PATRIMONIO_FINANZIARIO}
          {...sectionContext}
        />
        <GenericRow
          field={fields.INVESTIMENTI_IMMOBILIARI}
          formClassName="check-vertical"
          inputType="checkbox"
          {...sectionContext}
        />
        <GenericRow
          field={fields.IMPEGNI_FINANZIARI_MENSILI}
          {...sectionContext}
        />
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

SituazioneFinanziaria.propTypes = {
  domini: PropTypes.object,
  onSave: PropTypes.func,
  setDataPending: PropTypes.func,
  template: PropTypes.object,
  previousForm: PropTypes.object,
  mifidSaveRequest: PropTypes.object,
}

export default(SituazioneFinanziaria)
