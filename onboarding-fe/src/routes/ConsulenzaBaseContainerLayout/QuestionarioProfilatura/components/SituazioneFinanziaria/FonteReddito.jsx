import React from 'react'
import { PropTypes } from 'prop-types'
import { Row } from 'react-materialize'

import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import { fieldPropTypes, getFieldValue, updateField } from '../../../utils'

const FonteReddito = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
    const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
    const idDomanda = obiettiviTemplate.idDomanda
  
    return (
      <Row s={6}>
        <h6>
          {obiettiviTemplate.testoDomanda}
        </h6>
        <form action="#" className="radio-vertical">
          {obiettiviTemplate
            .listaRisposte
            .map(({ idRisposta, descRisposta }) => {
              const id = `radio-${idDomanda}-${idRisposta}`
        
              return (
                <p key={idRisposta}>
                  <input
                    name={id}
                    type="radio"
                    id={id}
                    className="with-gap"
                    onChange={(value) => updateField(idDomanda, idRisposta)}
                    checked={getFieldValue(idDomanda) === idRisposta} />
                  <label htmlFor={id}>{descRisposta.toUpperCase()}</label>
                </p>
              )
          })}
        </form>
      </Row>
    )
}

FonteReddito.propTypes = fieldPropTypes

export default withFieldCatch(FonteReddito)