import React, { Fragment } from 'react'
import { PropTypes } from 'prop-types'
import { Row } from 'react-materialize'

import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import { fieldPropTypes, getFieldValue, updateField } from '../../../utils'

/**
 * 
 * fonteReddito è un array contenente la risposta alla prima domanda
 * In base al codice della risposta, si ha il seguente comportamento:
 *  - 18 (Derivante dalla mia professione): campo visibile, opzionale
 *  - 25 (Non ho reddito): campo invisible
 *  - altir: campo visibile, obbligatorio
 */
const FonteRedditoAltro = ({ listaDomande, field, fonteReddito = [], domini, getFieldValue, updateField }) => {
    const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
    const idDomanda = obiettiviTemplate.idDomanda
    const fonteRedditoId = fonteReddito[0]
    
    return (
      <Fragment>
        {(!fonteRedditoId || fonteRedditoId === 25) && ''}
        {(fonteRedditoId && fonteRedditoId !== 25) &&
          <Row s={6}>
            <h6>
              {fonteRedditoId === 18 && obiettiviTemplate.testoDomanda}
              {fonteRedditoId !== 18 && obiettiviTemplate.subTestoDomanda}
            </h6>
            <form action="#" className="radio-vertical">
              {obiettiviTemplate
                .listaRisposte
                .map(({ idRisposta, descRisposta }) => {
                  const id = `radio-${idDomanda}-${idRisposta}`
                  return (
                    <p key={idRisposta}>
                      <input
                        name={id}
                        type="radio"
                        id={id}
                        className="with-gap"
                        onChange={(value) => updateField(idDomanda, idRisposta)}
                        checked={getFieldValue(idDomanda) === idRisposta} />
                      <label htmlFor={id}>{descRisposta}</label>
                    </p>
                  )
                })}
            </form>
          </Row>
        }
      </Fragment>
    )
}

FonteRedditoAltro.propTypes = fieldPropTypes

export default withFieldCatch(FonteRedditoAltro)