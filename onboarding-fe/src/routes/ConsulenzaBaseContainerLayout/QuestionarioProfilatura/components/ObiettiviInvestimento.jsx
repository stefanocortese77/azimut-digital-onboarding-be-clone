import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Button } from 'react-materialize'

import fields from '../fields'
import { CollapsibleItem } from '../../../../components/Collapsible'
import withFieldCatch from '../../../../hocs/withFieldCatch/withFieldCatch'
import { fieldPropTypes, filterQuestions, loadQuestions, getFieldValue, updateField } from '../../utils'

const Obiettivi = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const idDomanda = obiettiviTemplate.idDomanda

  const radios = obiettiviTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta }) => {
      const id = `radio-${idDomanda}-${idRisposta}`

      return (
        <p key={idRisposta}>
          <input
            name={id}
            type="radio"
            id={id}
            className="with-gap"
            onChange={(value) => updateField(idDomanda, idRisposta)}
            checked={getFieldValue(idDomanda) === idRisposta} />
          <label htmlFor={id}>{descRisposta}</label>
        </p>
      )
    })

  return (
    <Row>
      <h6>
        {obiettiviTemplate.testoDomanda}
      </h6>
      <form action="#" className="radio-vertical">
        {radios}
      </form>
    </Row>
  )
}

Obiettivi.propTypes = fieldPropTypes
const WrappedObiettivi = withFieldCatch(Obiettivi)

const Rischio = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const rischioTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const idDomanda = rischioTemplate.idDomanda

  const radios = rischioTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta }) => {
      const id = `radio-${idDomanda}-${idRisposta}`

      return (
        <p key={idRisposta}>
          <input
            name={id}
            type="radio"
            id={id}
            className="with-gap"
            onChange={(value) => updateField(idDomanda, idRisposta)}
            checked={getFieldValue(idDomanda) === idRisposta} />
          <label htmlFor={id}>{descRisposta}</label>
        </p>
      )
    })

  return (
    <Row>
      <h6>
        {rischioTemplate.testoDomanda}
      </h6>
      <form action="#" className="radio-vertical">
        {radios}
      </form>
    </Row>
  )
}

Rischio.propTypes = fieldPropTypes
const WrappedRischio = withFieldCatch(Rischio)

const OrizzonteTemporale = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
  const orizzonteTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  const idDomanda = orizzonteTemplate.idDomanda

  const radios = orizzonteTemplate
    .listaRisposte
    .map(({ idRisposta, descRisposta, descSubRisposta }) => {
      const id = `radio-${orizzonteTemplate.idDomanda}-${idRisposta}`

      return (
        <div key={id}>
          <input
            name={id}
            type="radio"
            id={id}
            className="with-gap"
            onChange={(value) => updateField(idDomanda, idRisposta)}
            checked={getFieldValue(idDomanda) === idRisposta} />
          <label htmlFor={id}>{descRisposta}</label>
          <div className="subtitle">{descSubRisposta}</div>
        </div>
      )
    })

  return (
    <Row>
      <h6>
        {orizzonteTemplate.testoDomanda}
      </h6>
      <form action="#" className="big-radio">
        {radios}
      </form>
    </Row>
  )
}

OrizzonteTemporale.propTypes = fieldPropTypes
const WrappedOrizzonteTemporale = withFieldCatch(OrizzonteTemporale)

class ObiettiviInvestimento extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
    }, loadQuestions(this.props.previousForm, this.props.template.listaDomande))

    this.save = this.save.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.mifidSaveRequest.fetching && !this.props.mifidSaveRequest.fetching) {
        if (!this.props.mifidSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }

  render () {
    const { template, domini } = this.props
    const { descSezione, listaDomande } = template

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: (idDomanda, value) => {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      }
    }

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="obiettivo-investimento"
        header={descSezione}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}
      >
        <WrappedObiettivi field={fields.OBIETTIVI} {...sectionContext} />
        <WrappedRischio field={fields.RISCHIO} {...sectionContext} />
        <WrappedOrizzonteTemporale
          field={fields.ORIZZONTE_TEMPORALE}
          {...sectionContext} />
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

ObiettiviInvestimento.propTypes = {
  domini: PropTypes.object,
  onSave: PropTypes.func,
  setDataPending: PropTypes.func,
  template: PropTypes.object,
  previousForm: PropTypes.object,
  mifidSaveRequest: PropTypes.object,
}

export default(ObiettiviInvestimento)
