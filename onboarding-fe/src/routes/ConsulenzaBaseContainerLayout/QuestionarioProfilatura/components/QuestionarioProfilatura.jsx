import React from 'react'
import { PropTypes } from 'prop-types'
import { Link } from 'react-router-dom'
import { Row } from 'react-materialize'

import dataSB from '../../../../static-data/data-servizio-base.json'
import { Collapsible } from '../../../../components/Collapsible'
import YesNoQuestion from '../../../../components/YesNoQuestion'

import ConoscenzaEsperienza from './ConoscenzaEsperienza'
import ObiettiviInvestimento from './ObiettiviInvestimento'
import SituazioneFinanziaria from './SituazioneFinanziaria'
import AltreInformazioni from './AltreInformazioni'
import { loadQuestions, saveQuestions } from '../../utils'

export class QuestionarioProfilatura extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      openCollapsible: 'conoscenza-esperienza',
      formSnapshot: null,
      mifidMandatory: true,
    }

    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)
    this.handleSetDataPending = this.handleSetDataPending.bind(this)
    this.handleSaveSurvey = this.handleSaveSurvey.bind(this)
    this.nextStep = this.nextStep.bind(this)
    this.nextStepNoSave = this.nextStepNoSave.bind(this)
  }

  componentDidMount () {
    const dominioRequest = {
      'listaDomini': [
        {
          'idDominio': 'DOM_TITOLO_STUDIO'
        }, {
          'idDominio': 'DOM_SETTORE_OCCUPAZIONE_I'
        }, {
          'idDominio': 'DOM_PROFESSIONE_ATTUALE'
        }
      ]
    }

    this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
   // this.props.//(this.props.customer.idCliente, this.props.customer.idIncarico)
    this.props.dominio(dominioRequest)
    this.props.fetchSurveyTemplate()
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.getDataSummaryRequest.fetching === true && this.props.getDataSummaryRequest.fetching === false && this.props.getDataSummaryRequest.error === null) {
      if (this.props.summaryData.statusEndPoint) {
        const fetchSurveyBool = this.checkSatusService(this.props.summaryData.statusEndPoint, 'mifid')

        this.props.fetchSurvey(this.props.customer.idCliente, false, fetchSurveyBool)
      }
    }


    if (!prevProps.previousForm.idCliente && this.props.previousForm.idCliente) {
      this.setState({
        formSnapshot: loadQuestions(this.props.previousForm)
      })

      console.log(this.props.previousForm)
    }
  }
  checkSatusService (services, serviceName) {
    let active
    services.forEach(function (servizio) {
      if (servizio.name === serviceName) {
        active = servizio.active
      }
    })
    return active
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({
      openCollapsible: itemKey
    })
  }

  handleSaveSurvey (listaDomande) {
    // TODO: inserire logica per domande disabilitate
    debugger
    // this.props.template.sezioni.find(sezione => sezione.idSezione === 'CONOSCENZA_ESPERIENZA').listaDomande[6].strutturaInternaDomande.domande.forEach(d => {
    //   d.listaDipendenze.forEach(e => {
    //     if (listaDomande[e.idDomandaDipendenza] && listaDomande[e.idDomandaDipendenza].includes(e.idRispostaDipendenza)) {
    //       delete listaDomande[d.idDomanda]
    //     }
    //   })
    // })
    const updatedSnapshot = Object.assign({},
      this.state.formSnapshot,
      listaDomande
    )

    this.props.saveSurvey(this.props.customer.idCliente, saveQuestions(updatedSnapshot))
    console.error(updatedSnapshot)
    this.setState({ formSnapshot: updatedSnapshot })
    this.props.setDataPending(false)
  }

  handleSetDataPending () {
    if (!this.props.dataPending) {
      this.props.setDataPending(true)
    }
  }

  nextStep (path) {
    if (this.props.dataPending === true) {
      this.toggleModal()
      this.setState({ nextPath: path })
    } else {
      this.props.history.push(path)
    }
  }

  nextStepNoSave () {
    this.props.history.push(this.state.nextPath)
  }

  render () {
    const torna = dataSB.button_back
    const title = dataSB.title

    const { template, domini } = this.props

    if (!Object.keys(template).length || !Object.keys(domini).length) {
      return null
    }

    const { titoloSezione, titoloSubSezione, titoloSubSubSezione, sezioni } = template

    let content = null

    if (this.state.mifidMandatory === false) {
      content = (
        <div className="no-survey">
          {'Se il cliente non sarà intestatario non è necessario compilare il questionario'}
        </div>
      )
    }

    if (this.state.mifidMandatory === true && this.props.mifidRequest.fetching === false) {
      content = (
        <Collapsible
          open={this.state.openCollapsible}
          onChange={this.handleOpenCollapsibleChange}
        >
          <ConoscenzaEsperienza
            template={sezioni.find(sezione => sezione.idSezione === 'CONOSCENZA_ESPERIENZA')}
            domini={domini}
            previousForm={this.props.previousForm}
            setDataPending={this.handleSetDataPending}
            mifidSaveRequest={this.props.mifidSaveRequest}
            onSave={this.handleSaveSurvey} />
          <ObiettiviInvestimento
            template={sezioni.find(sezione => sezione.idSezione === 'INVESTIMENTO')}
            domini={domini}
            previousForm={this.props.previousForm}
            setDataPending={this.handleSetDataPending}
            mifidSaveRequest={this.props.mifidSaveRequest}
            onSave={this.handleSaveSurvey} />
          <SituazioneFinanziaria
            template={sezioni.find(sezione => sezione.idSezione === 'FINANZIARIA')}
            domini={domini}
            previousForm={this.props.previousForm}
            setDataPending={this.handleSetDataPending}
            mifidSaveRequest={this.props.mifidSaveRequest}
            onSave={this.handleSaveSurvey} />
          <AltreInformazioni
            template={sezioni.find(sezione => sezione.idSezione === 'ALTRO')}
            domini={domini}
            previousForm={this.props.previousForm}
            setDataPending={this.handleSetDataPending}
            mifidSaveRequest={this.props.mifidSaveRequest}
            onSave={this.handleSaveSurvey} />
        </Collapsible>
      )
    }

    return (

      <section className="main-body consulenza-base questionario-profilatura">
        <header>
          <Link to="/" className="torna-pratiche"><i className="icon-arrow-back" />{torna}</Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>

        <article className="page-left-content">
          <div className="background_pattern" />
          <h2 className="title-sezione">{titoloSezione}</h2>
          <div className="subtitle-sezione">
            <div className="question">
              <h5>{titoloSubSezione}</h5>
              <h6>{titoloSubSubSezione}</h6>
            </div>
            <YesNoQuestion
              label=""
              onChange={(value) => {
                this.setState({ mifidMandatory: value })
              }}
              value={this.state.mifidMandatory} />
          </div>

          { content }

          <Row className="right-align padding-r-20 margin-t-20">
            <Link onClick={() => this.nextStep('/consulenza-base/sottoscrizione-servizi')} to="#">{dataSB.link_continue}
              <i className="icon-arrow" />
            </Link>
          </Row>
        </article>

      </section>
    )
  }
}

QuestionarioProfilatura.propTypes = {
  customer: PropTypes.object,
  domini: PropTypes.object,
  dominio: PropTypes.func,
  fetchSurvey: PropTypes.func,
  fetchSurveyTemplate: PropTypes.func,
  saveSurvey: PropTypes.func,
  template: PropTypes.object,
  mifidRequest: PropTypes.object,
  previousForm: PropTypes.object,
  getDataSummaryLight: PropTypes.func,
  // getDocumentsData: PropTypes.func,
  setDataPending: PropTypes.func,
  dataPending: PropTypes.bool,
  mifidSaveRequest: PropTypes.object,
  summaryData: PropTypes.object,
  getDataSummaryRequest: PropTypes.object,
  history: PropTypes.object
}

export default(QuestionarioProfilatura)
