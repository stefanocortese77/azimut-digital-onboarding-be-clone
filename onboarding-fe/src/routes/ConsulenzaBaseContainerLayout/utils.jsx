import React from 'react'
import PropTypes from 'prop-types'

const getValoriDominio = (dominio) => (dominio || {}).valoriDominio
export const getDominio = ({ listaDomini }, key) => {
  return getValoriDominio(listaDomini.find(({ idDominio }) => idDominio === key))
}

/**
 * Check the status of specific serviceName
 */
export const checkStatusService = (services, serviceName) => {
  let servizioAttivo = false
  services.forEach(function (servizio) {
    if (servizio.name === serviceName) {
      servizioAttivo = servizio.active
    }
  })
  return servizioAttivo
}


/**
 * Maps an array of values to an <options /> array
 */
export const mapDominiToOptions = (dominiList, defaultOption) => {
  let array = defaultOption ? [<option key={-1} value={-1} disabled>{'-'}</option>] : []
  return Object.keys(dominiList || {}).reduce((res, key) => {
    return [
      ...res,
      <option key={`${key}`} value={key}>{dominiList[key]}</option>
    ]
  }, array)
}

/**
 * Maps an array of values to an Object like {Italia:null, "Svizzera:null, "Svezia:null}
 */
export const mapDominiToAutocomplete = (dominiList, dominioId) => {
  let dominioAutocompleteList = []
  for (var key in dominiList) {
    if (dominiList.hasOwnProperty(key)) {
      dominioAutocompleteList.push({
        key: dominiList[key],
        value: key,
        label: dominiList[key],
      })
    }
  }
  return dominioAutocompleteList
}

/**
 * Return the key of the value passed for the specific Domain
 */
export const getKeyDominioFromValue = (dominiList, dominioId, dominioItemValue) => {
  let keyMatch = ''
  dominiList.forEach(function (element) {
    if (element.idDominio === dominioId) {
      for (var key in element.valoriDominio) {
        if (element.valoriDominio.hasOwnProperty(key)) {
          if (element.valoriDominio[key] === dominioItemValue) {
            keyMatch = key
          }
        }
      }
    }
  })
  return keyMatch
}

/**
 * Return the value of the key passed for the specific Domain
 */
export const getValueDominioFromKey = (dominiList, dominioId, dominioItemKey) => {
  let valueMatch = ''
  dominiList.forEach(function (element) {
    if (element.idDominio === dominioId) {
      // TODO da verificare con chiamate reali
      valueMatch = element.valoriDominio[dominioItemKey]

      // for (var key in element.valoriDominio) {
      //   if (element.valoriDominio.hasOwnProperty(key)) {
      //     if (element.valoriDominio[key] === dominioItemKey[0]) {
      //       valueMatch = key
      //     }
      //   }
      // }
    }
  })
  return valueMatch
}

/**
 * Maps a boolean value to an arbitrary value
 * @param {*} trueValue the arbitrary value for true
 * @param {*} falseValue the arbitrary value for false
 */
export const booleanToFieldId = (trueValue, falseValue) => {
  return (value) => {
    if (value === true) {
      return trueValue
    } else if (value === false) {
      return falseValue
    }
  }
}

/**
 * Maps an arbitrary value to a boolean
 * @param {*} trueValue the arbitrary value for true
 * @param {*} falseValue the arbitrary value for false
 */
export const fieldIdToBoolean = (trueValue, falseValue) => {
  return (id) => {
    if (id === trueValue) {
      return true
    } else if (id === falseValue) {
      return false
    }
  }
}

export const loadQuestions = (previousForm, listaDomande) => {
  if (!previousForm || !previousForm.listaDomande) {
    return []
  }

  return previousForm.listaDomande.reduce((o, domanda) => {
    if (listaDomande) {
      // In the the question can't be found in the template, do not add the question state
      if (!listaDomande.find(domandaTemplate => domandaTemplate.idDomanda === domanda.idDomanda)) {
        return o
      }
    }

    o[domanda.idDomanda] = domanda.listaRisposte.map(risposta => risposta.idRisposta)
    return o
  }, { })
}

// Cleans a state object from all the properties not rearding the questions
export const filterQuestions = (formState) => {
  return Object
    .keys(formState)
    .filter(key => !isNaN(parseInt(key)))
    .reduce((o, key) => Object.assign(o, { [key]: formState[key] }), {})
}

export const saveQuestions = (listaDomande) => {
  const questions = Object
    .keys(listaDomande)
    .map(idDomanda => {
      const domanda = {
        idDomanda: idDomanda
      }

      const answers = listaDomande[idDomanda]

      if (answers && Array.isArray(answers)) {
        domanda.listaRisposte = answers
          .filter(a => a)
          .map(a => ({ idRisposta: a }))
      } else {
        domanda.listaRisposte = [
          {
            idRisposta: answers
          }
        ]
      }

      return domanda
    })

  return questions
}

export const updateField = (idDomanda, value) => {
  if (Array.isArray(value)) {
    return {
      dirty: true,
      [idDomanda]: value
    }
  }

  return {
    dirty: true,
    [idDomanda]: [value]
  }
}

export const getFieldValue = (idDomanda, multiValue, state) => {
  if (idDomanda && state[idDomanda] !== undefined) {
    if (multiValue) {
      return state[idDomanda]
    }

    return state[idDomanda][0]
  }

  return undefined
}

export const fieldPropTypes = {
  listaDomande: PropTypes.array,
  field: PropTypes.object,
  domini: PropTypes.object,
  getFieldValue: PropTypes.func,
  updateField: PropTypes.func
}
