import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-materialize'
import { Route, Switch } from 'react-router-dom'

import { AcquisizioneDocumenti } from './AcquisizioneDocumenti'
import { QuestionarioProfilatura } from './QuestionarioProfilatura'
import { CensimentoAnagrafico } from './CensimentoAnagrafico'
import { SottoscrizioneServizi } from './SottoscrizioneServizi'
import { RiepilogoCliente } from './RiepilogoCliente'
import Aside from '../../components/Aside/'
import { ConsulenzaBase } from './ConsulenzaBase/'

export class ConsulenzaBaseContainerLayout extends React.Component {
  render () {
    // console.log('this.props', this.props)
    let content = ''
    let col = 9
    if (this.props.location.pathname !== '/consulenza-Base/create') {
      content = <Col s={3} className="paddingless">
        <Aside history={this.props.history} />
      </Col>
    } else {
      col = 12
      content = ''
    }
    return (<Row className="container-home paddingless marginless">
      <Col s={col} className="container-home-left">
        <Switch>
          <Route path={`${this.props.match.url}/acquisizione-documenti`} component={AcquisizioneDocumenti} />
          <Route path={`${this.props.match.url}/censimento-anagrafico`} component={CensimentoAnagrafico} />
          <Route path={`${this.props.match.url}/create`} component={ConsulenzaBase} />
          <Route path={`${this.props.match.url}/questionario-profilatura`} component={QuestionarioProfilatura} />
          <Route path={`${this.props.match.url}/sottoscrizione-servizi`} component={SottoscrizioneServizi} />
          <Route path={`${this.props.match.url}/riepilogo-cliente`} component={RiepilogoCliente} />
        </Switch>
      </Col>
      {content}
    </Row>
    )
  }
}

ConsulenzaBaseContainerLayout.propTypes = {
  match: PropTypes.object,
  location: PropTypes.object,
  history: PropTypes.object
}

export default ConsulenzaBaseContainerLayout
