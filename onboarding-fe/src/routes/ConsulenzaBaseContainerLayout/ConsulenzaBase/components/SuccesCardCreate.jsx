import React from 'react'
import PropTypes from 'prop-types'
import { Button, Row, Col } from 'react-materialize'
import dataSB from '../../../../static-data/data-servizio-base.json'

export class SuccesCardCreate extends React.Component {
  render () {
    let successTitle = dataSB.verifica_cf.success_title
    let successSubtitle = dataSB.verifica_cf.success_subtitle
    let buttonProsegui
    let goTo
    if (this.props.label === null) {
      successTitle = dataSB.verifica_cf.success_title
      successSubtitle = dataSB.verifica_cf.success_subtitle
      goTo = '/consulenza-base/acquisizione-documenti'
      buttonProsegui = dataSB.button_prosegui
    } else {
      successTitle = ''
      successSubtitle = this.props.label
      goTo = '/'
      buttonProsegui = 'Torna alla Home'
    }

    return (

      <div className="box-container box-codice-fiscale">
        <Row>
          <Col s={12}>
            <i className="icon-check-codice-fiscale-success" />
            <h2 className="title-success">{successTitle}</h2>
            <p className="subtitle-success">{successSubtitle}</p>
          </Col>
        </Row>
        <Row className="center-align margin-t-40">
          <Col s={12}>
            <Button waves="light" onClick={() => this.props.history.push(goTo)} > {buttonProsegui} </Button>
          </Col>
        </Row>
      </div>
    )
  }
}

SuccesCardCreate.propTypes = {
  history:PropTypes.array,
  label: PropTypes.string
}

export default SuccesCardCreate
