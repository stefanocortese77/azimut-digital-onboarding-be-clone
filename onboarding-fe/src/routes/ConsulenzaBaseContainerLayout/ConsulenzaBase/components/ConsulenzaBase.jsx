import React from 'react'
import { PropTypes } from 'prop-types'
import dataSB from '../../../../static-data/data-servizio-base.json'
import './consulenzaBase.scss'
import { Link } from 'react-router-dom'
import { Input, Button, Row, Col } from 'react-materialize'
import SuccesCardCreate from './SuccesCardCreate'
import Quagga from 'quagga' // ES6
import ImageTools from './ImageTools'

// import Loading from '../../../../components/Loading'

export class ConsulenzaBase extends React.Component {
  constructor (props) {
    super(props)
    this.onChange = this.onChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.getNewInput = this.getNewInput.bind(this)

    const randomCF = () => {
      const randomLetter = () => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[Math.floor(Math.random() * 26)]
      const randomNumber = () => Math.floor(Math.random() * 10)

      return 'LLLLLLNNLNNLNNNL'
        .split('')
        .map(type => type === 'L' ? randomLetter() : randomNumber())
        .join('')
    }

    this.state = {
      inputVal: randomCF(),
      imageupload: '',
      active: false,
      customer: {},
      showElementFile: true,
      src: '',
      err: null,
      codiceFiscale: null,
      error: null
    }
  }

  componentWillReceiveProps (prevProps) {
    if (this.props.customerCreateRequest.fetching && !prevProps.customerCreateRequest.fetching && this.props.customerCreateRequest.error === null) {
      this.setState({ showElementFile: false })
      this.props.setCodiceFiscale(this.state.inputVal)
    } else if (this.props.customerCreateRequest.error !== null) {
      this.setState({ error:this.props.customerCreateRequest.error })
    }
  }

  handleFiles (event) {
    this.setState({
      ...this.state,
      [name]: event.target.value
    })
  }

  handleSubmit (event) {
    event.preventDefault()
    if (this.state.inputVal === null || this.state.inputVal.length !== 16) {
      console.log('pippo')
    } else {
      this.props.customerCreate(this.state.inputVal)
    }
  }
  handleCamera () {
    let context = this

    const file = this.fileInput.files[0]
    this.fileInput.value = ''

    ImageTools.resize(file, {
      width: 1000, // maximum width
      height: 750 // maximum height
    }, function (blob, didItResize) {
      // didItResize will be true if it managed to resize it, otherwise false (and
      // will return the original file as 'blob')
      let data = document
        .getElementById('preview')
        .src = window
        .URL
        .createObjectURL(blob)

      context.getCodiceFiscale(data)
      context.setState({ imageupload: data })
      // you can also now upload this blob using an XHR.
    })
  }

  getCodiceFiscale (fileSrc) {
    const quaggaConfig = {
      size: '1600',
      locate: false,
      patchSize: 'small'
    }

    this.getQuagga(quaggaConfig, fileSrc)
  }

  getQuagga (arrConfig, fileSrc) {
    const context = this
    Quagga.decodeSingle({
      decoder: {
        readers: ['code_39_reader', 'code_128_reader'] // List of active readers
      },
      patchSize: arrConfig.patchSize,
      numOfWorkers: 1,
      inputStream: {
        size: arrConfig.size // restrict input-size to be 800px in width (long-side)
      },
      locate: arrConfig.locate, // try to locate the barcode in the image
      src: fileSrc // or 'data:image/jpg;base64,' + data
    }, function (result) {
      if (result.codeResult) {
        context.getNewInput(result.codeResult.code)
      } else {
        context.getNewInput('si è verificato un errore nella codifica del codice fiscale')
      }
    })
  }

  getNewInput (value) {
    this.setState({ inputVal: value })
    this.setState({ active: true })
  }

  getBase64 (file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      // reader.onload = () => resolve(reader.result)

      var img = document.getElementById('preview')

      reader.onload = () => {
        // img.src = reader.result;
        this.setState({ imageupload: reader.result })

        var canvas = document.createElement('canvas')
        // var canvas = $("<canvas>", {"id":"testing"})[0];
        var ctx = canvas.getContext('2d')
        ctx.drawImage(img, 0, 0)
        var MAX_WIDTH = 400
        var MAX_HEIGHT = 400
        var width = img.width
        var height = img.height

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width
            width = MAX_WIDTH
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height
            height = MAX_HEIGHT
          }
        }
        canvas.width = width
        canvas.height = height

        // eslint-disable-next-line
        var ctx = canvas.getContext('2d')
        ctx.drawImage(img, 0, 0, width, height)

        var dataurl = canvas.toDataURL('image/jpeg')

        resolve(dataurl)
      }
    })
  }
  onChange (name, event) {
    if (event.target.value.length !== 16) {
      this.setState({
        ...this.state,
        [name]: event.target.value
      })
    } else {
      this.setState({
        ...this.state,
        [name]: event.target.value
      })
    }
  }

  openCamera () {
    let context = this
    this
      .fileInput
      .click()

    // this.handleSubmit()

    this.fileInput.onchange = function (e) {
      context.handleCamera()
    }
  }
  render () {
    // const { inputVal } = this.props
    const torna = dataSB.button_back
    const title = dataSB.title
    const inputCamera = dataSB.verifica_cf.placeholder_cf
    const buttonVerifica = dataSB.verifica_cf.button_verifica
    // const button_prosegui = dataSB.button_prosegui

    let activeClassName = ''
    if (this.state.active) {
      activeClassName += ' active'
    }

    let content
    let label
    if (this.props.customer.statusResponse.code === '-1') {
      label = this.props.customer.statusResponse.description
    } else {
      label = null
    }

    let fiscalCodeError = null
    let buttonDisabled = false

    if (this.state.inputVal.length !== 16) {
      buttonDisabled = true
      fiscalCodeError = (
        <div className="error-label">Attenzione! Il codice fiscale inserito non è valido. Inserisci altro codice fiscale</div>
      )
    }

    if (this.state.showElementFile || this.state.error !== null) {
      content = (
        <div className="box-container box-codice-fiscale">
          <Row>
            <Col s={11}>
              <Input
                s={12}
                validate
                minlength={16}
                maxLength={16}
                labelClassName={activeClassName}
                label={inputCamera}
                value={this.state.inputVal}
                onChange={e => this.onChange('inputVal', e)} />
              {fiscalCodeError}
              <form onSubmit={this.handleSubmit} id="form1" encType="multipart/form-data">
                <input
                  name="image"
                  type="file"
                  ref={input => {
                    this.fileInput = input
                  }}
                  // accept="image/*"
                  accept=".jpg, .png, .jpeg, .bmp, .tif, .tiff|images/*"
                  capture="camera" />
              </form>
            </Col>
            <Col s={1}>
              <i
                className="icon-barcode"
                onClick={() => {
                  this.openCamera()
                }} />
            </Col>
          </Row>
          <Row className="center-align margin-t-40">
            <Col s={12}>
              <Button waves="light" type="submit"
                form="form1"
                value="Submit"
                disabled={buttonDisabled}>{buttonVerifica}</Button>
            </Col>
          </Row>
        </div>
      )
    } else if (this.state.error === null && !this.state.showElementFile) {
      content = <SuccesCardCreate
        history={this.props.history}
        className={` ${this.state.showElement}`}
        label={label}
        error={this.state.error} />
    }

    return (
      <section className="main-body no-aside">
        <header>
          <Link to="/" className="torna-pratiche"><i className="icon-arrow-back" />{torna}</Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>
        <article className="page-left-content">
          <div className="background_pattern" />
          <h2 className="title-sezione">Intestatario</h2>
          {/* <h2 className="title-sezione">{titleSection}</h2> */}
          {content}
        </article>
        <img id="preview" src={this.state.imageupload} />
      </section>

    )
  }
}

ConsulenzaBase.propTypes = {
  customerCreate: PropTypes.func,
  customerCreateRequest: PropTypes.func,
  inputVal: PropTypes.string,
  history: PropTypes.object,
  setCodiceFiscale: PropTypes.func,
  customer: PropTypes.object
}

export default(ConsulenzaBase)
