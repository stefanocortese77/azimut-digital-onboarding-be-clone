import { connect } from 'react-redux'
import ConsulenzaBase from '../components/ConsulenzaBase'
import { customerCreate, setCodiceFiscale } from '../../../../store/modules/customers/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    customerCreate : (value) => dispatch(customerCreate(value)),
    setCodiceFiscale: (value) => dispatch(setCodiceFiscale(value))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    customerCreateRequest: state.customers.customerCreateRequest
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsulenzaBase)
