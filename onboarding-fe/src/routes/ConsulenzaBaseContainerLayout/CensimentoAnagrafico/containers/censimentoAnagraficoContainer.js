import { connect } from 'react-redux'
import CensimentoAnagrafico from '../components/CensimentoAnagrafico'
import { getDataRegistry, getDataSummaryLight, saveDataRegistry, postFatca } from '../../../../store/modules/customers/actions'
import { dominio } from '../../../../store/modules/dominio/actions'
import { fetchSurveyAml, saveAml } from '../../../../store/modules/survey/actions'
import { validationCreateOtp, validationVerifyOtp } from '../../../../store/modules/validation/actions'
import { setDataPending } from '../../../../store/modules/dataPending/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataRegistry : (codiceFiscale, idCliente, idIncarico, dataRegistryBool, fatcacrsBool) => dispatch(getDataRegistry(codiceFiscale, idCliente, idIncarico, dataRegistryBool, fatcacrsBool)),
    dominio : (queryParam) => dispatch(dominio(queryParam)),
    fetchSurveyAml : (idCliente, idIncarico, amlBool) => dispatch(fetchSurveyAml(idCliente, idIncarico, amlBool)),
    validationCreateOtp : (cellulare) => dispatch(validationCreateOtp(cellulare)),
    validationVerifyOtp : (otp) => dispatch(validationVerifyOtp(otp)),
    getDataSummaryLight: (idCliente, idIncarico) => dispatch(getDataSummaryLight(idCliente, idIncarico)),
    saveDataRegistry: (queryParam, idCliente, idIncarico, active) => dispatch(saveDataRegistry(queryParam, idCliente, idIncarico, active)),
    setDataPending: (boolean) => dispatch(setDataPending(boolean)),
    postFatca: (idCliente, idIncarico, listaDomande) => dispatch(postFatca(idCliente, idIncarico, listaDomande)),
    saveAml: (idCliente, idIncarico, listaDomande) => dispatch(saveAml(idCliente, idIncarico, listaDomande))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    survey: state.survey.aml,
    domini: state.dominio.domini,
    dominioRequest : state.dominio.dominioRequest,
    templateAml: state.survey.templateAml,
    dataRegistry: state.customers.dataRegistry,
    getDataRegistryRequest: state.customers.getDataRegistryRequest,
    amlRequest: state.survey.amlRequest,
    validation: state.validation,
    summaryData : state.customers.summaryData,
    verifyOtpRequest: state.validation.verifyOtpRequest,
    postInfoPersonaliRequest: state.customers.postInfoPersonaliRequest,
    dataPending: state.dataPendings.dataPendingState,
    postFatcaRequest: state.customers.postFatcaRequest,
    amlPreviousForm: state.survey.amlPreviousForm,
    amlSaveRequest: state.survey.amlSaveRequest,
    getDataSummaryRequest : state.customers.getDataSummaryRequest
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CensimentoAnagrafico)
