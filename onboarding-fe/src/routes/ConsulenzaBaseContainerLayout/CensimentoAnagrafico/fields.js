export default {
  TITOLO_STUDIO: {
    id: 'titoloStudio',
    templateId: 44,
  },
  NATURA_RAPPORTO: {
    id: 'naturaRapporto',
    templateId: 45,
  },
  SETTORE_OCCUPAZIONE: {
    id: 'settoreOccupazione',
    templateId: 46,
  },
  PROFESSIONE_ATTUALE: {
    id: 'professioneAttuale',
    templateId: 47,
  },
  NAZIONE_INSEDIAMENTO: {
    id: 'nazioneInsediamento',
    templateId: 48,
  },
  PROVINCIA_ATTIVITA: {
    id: 'provinciaAttivita',
    templateId: 49,
  },
  ANNO_INIZIO: {
    id: 'annoInizio',
    templateId: 50,
  },
  RADIO_PEP: {
    id: 'radioPep',
    templateId: 51,
  },
  RADIO_PEP_PERCHE: {
    id: 'RadioPepPerche',
    templateId: 55,
  },
  RADIO_PEP_REDDITO: {
    id: 'radioPepReddito',
    templateId: 56,
  },
  YES_NO_PEP_PARTECIPAZIONI_SOC: {
    id: 'yesNoPepPartecipazioniSoc',
    templateId: 57,
  },
  YES_NO_PEP_PROCEDIMENTI: {
    id: 'yesNoPepProcedimenti',
    templateId: 58,
  },
  RADIO_PEP_RAPPORTI_PERCHE: {
    id: 'radioPepRapportiPerche',
    templateId: 59,
  },
  // YES_NO_PEP_SOGGETTI: {
  //   id: 'yesNoPepSoggetti',
  //   templateId: 55,
  // },
  YES_NO_CARICHE_POLITICHE: {
    id: 'yesNoCarichePolitiche',
    templateId: 60,
  },
  YES_NO_CARICHE_EXTRA: {
    id: 'yesNoCaricheExtra',
    templateId: 61,
  },
  YES_NO_CARICHE_PUBBLICHE: {
    id: 'yesNoCarichePubbliche',
    templateId: 62,
  },
  YES_NO_FONDI_PUBBLICI: {
    id: 'yesNoFondiPubblici',
    templateId: 63,
  },
  DATI_PERSONALI: {
    id: 'datiPersonali',
    templateId: 64,
  },
  ATTIVITA_MARKETING: {
    id: 'attivitaMarketing',
    templateId: 65,
  }

}
