import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Input, Button, Col } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'

import OtpVerify from '../../../../components/OtpVerify'
import { mapDominiToAutocomplete, getDominio } from '../../utils'

import * as dataSB from '../../../../static-data/data-servizio-base.json'
import ResidenzaAggiuntiva from './ResidenzaAggiuntiva'


const infoResidenza = dataSB.censimento_anagrafica.residenza
const infoContatti = dataSB.censimento_anagrafica.contatti


class Contatti extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
      optHide: false,
      optHideEmail: false,
      questions: {
        prefissoCellulare: '',
        numeroCellulare: '',
        email: '',
        indirizzoComunicazione: '',
        civicoInvioComunicazioni: '',
        codComuneInvioComunicazioni: '',
        codNazioneInvioComunicazioni: '',
        codProvinciaInvioComunicazioni: '',
        indirizzoInvioComunicazioni: '',
        capInvioComunicazioni: '',
      },
      enablements: {
        comune: false,
        cap: false,
      },
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE_CONTATTI')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONE_CONTATTI')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE_CONTATTI'))
      }
    }/* , loadQuestions(this.props.previousForm, this.props.template.listaDomande) */)

    this.save = this.save.bind(this)
    this.handleQuestionValue = this.handleQuestionValue.bind(this)
    this.handlePrefissoChange = this.handlePrefissoChange.bind(this)
    this.handleChangeCellulare = this.handleChangeCellulare.bind(this)
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.addResidenzaToggle = this.addResidenzaToggle.bind(this)
    this.optHideToggle = this.optHideToggle.bind(this)
    this.optHideToggleEmail = this.optHideToggleEmail.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.postInfoPersonaliRequest.fetching && !this.props.postInfoPersonaliRequest.fetching) {
        if (!this.props.postInfoPersonaliRequest.error) {
          this.setState({ dirty: false })
        }
      }

      if (
        prevProps.verifyOtpRequest.fetching &&
        this.props.verifyOtpRequest.fetching === false
      ) {
        this.optHideToggle(true)
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this
        .props
        .onSave(this.state.questions)
    }
  }

  handleQuestionValue (question, value) {
    this.setState({ questions: Object.assign({}, this.state.questions, { [question]: value }) })
  }

  handlePrefissoChange (event) {
    this.handleQuestionValue('prefissoCellulare', event.target.value)
  }

  handleChangeCellulare (event) {
    this.handleQuestionValue('numeroCellulare', event.target.value)
  }

  handleChangeEmail (event) {
    this.handleQuestionValue('email', event.target.value)
  }

  addResidenzaToggle () {
    this.setState({ addResidenza: !this.state.addResidenza })
  }

  optHideToggle (forceClose) {
    if (
      !(
        this.props.customer.cellulare !== '' &&
        `${this.state.questions.cellulare}` === `${this.props.customer.cellulare}`
      ) ||
      forceClose
    ) {
      this.setState({ optHide: !this.state.optHide, forceClose: true })
    }
  }

  optHideToggleEmail () {
    if (
      !(
        this.props.customer.email !== '' &&
        `${this.state.questions.email}` === `${this.props.customer.email}`
      )
    ) {
      this.setState({ optHideEmail: !this.state.optHideEmail })
    }
  }

  render () {
    // connteggio errori in pagina per disabilitare bottone 'Salva'
    let errori = 0

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }


    let actionBtnCellulare = ''

    if (
      (this.state.questions !== null &&
      (this.props.customer.cellulare !== '' && `${this.state.questions.cellulare}` === `${this.props.customer.cellulare}`)) ||
      this.state.forceClose
    ) {
      actionBtnCellulare = infoContatti.suggestion_pin_verificato
    } else {
      actionBtnCellulare = infoContatti.button_link_certifica
    }

    let actionBtnEmail = ''

    if (this.state.questions !== null &&
      this.props.customer.email !== '' &&
      `${this.state.questions.email}` === `${this.props.customer.email}`
    ) {
      actionBtnEmail = infoContatti.suggestion_pin_verificato
    } else {
      actionBtnEmail = infoContatti.button_link_certifica
    }

    let residenzaAggiuntiva = null
    if (this.state.addResidenza) {
      residenzaAggiuntiva = (
        <ResidenzaAggiuntiva
          dominio={this.props.dominio}
          domini={this.props.domini}
          residenza={infoResidenza}
          updateDominio={this.props.updateDominio}
          dominioRequest={this.props.dominioRequest}
          capInvioComunicazioni={this.state.questions.capInvioComunicazioni}
          addResidenza={this.state.addResidenza}
          value={this.state.questions}
          handleQuestionValue={this.handleQuestionValue}
          customer={this.props.customer}
        />
      )
    }

    // controllo che il numero di telefono sia valido (solo numeri 10 caratteri) per renderizzare il link 'convalida'
    let linkConvalidaTelefono
    if (this.state.questions.numeroCellulare.length === 10) {
      let numRegex = /[0-9]/g
      let valueMatch = this.state.questions.numeroCellulare.match(numRegex)
      if (valueMatch.length === 10) {
        linkConvalidaTelefono = (
          <a onClick={this.optHideToggleEmail} className="certifica">
            {actionBtnEmail.toUpperCase()}
          </a>
        )
      } else {
        errori = errori + 1
      }
    } else if (this.state.questions.numeroCellulare.length !== 0) {
      errori = errori + 1
    }

    // controllo che l'email sia valida per renderizzare il link 'convalida'
    let linkConvalidaEmail
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.questions.email)) {
      linkConvalidaEmail = (
        <a onClick={this.optHideToggleEmail} className="certifica">
          {actionBtnEmail.toUpperCase()}
        </a>
        )
    } else if (this.state.questions.email !== '') {
      errori = errori + 1
    }

    return (
      <CollapsibleItem
        itemKey="contatti"
        header={infoContatti.title_accordion}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}>
        <Row>
          <Col s={2}>
            <Input
              s={12}
              type="select"
              onChange={this.handlePrefissoChange}
              value={this.state.questions.prefissoCellulare}
            >
              <option value="1">+39</option>
              <option value="2">+29</option>
              <option value="3">+17</option>
            </Input>
          </Col>
          <Col s={4} className="col-position-relative">
            <Input
              s={12}
              label={infoContatti.label_cellulare}
              value={this.state.questions.cellulare}
              onChange={this.handleChangeCellulare}
            />
            {linkConvalidaTelefono}
          </Col>
          <Col s={6} className="col-position-relative">
            <Input
              type="email"
              s={12}
              label={infoContatti.label_email}
              value={this.state.questions.email}
              onChange={this.handleChangeEmail} />
            {linkConvalidaEmail}
          </Col>
        </Row>
        <Row>
          <Col s={12}>
            <hr />
            <span className="indirizzo-alternativo">
              <b>{infoContatti.label_comunicazioni}</b>{' '}
              {infoContatti.label_comunicazioni_value}
            </span>
            <a onClick={this.addResidenzaToggle} className="indirizzo-alternativo-button">
              {infoContatti.button_link_modifica}
            </a>
            {residenzaAggiuntiva}
          </Col>
        </Row>
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save} disabled={errori > 0}>Salva</Button>
        </Row>
        <OtpVerify
          labelTitle="Cellulare"
          prefisso="+39"
          value={this.state.cellulare}
          callBack={this.optHideToggle}
          invia={this.props.validationCreateOtp}
          verifica={this.props.validationVerifyOtp}
          hide={this.state.optHide}
        />
        <OtpVerify
          labelTitle="Email"
          prefisso=""
          value={this.state.questions.email}
          callBack={this.optHideToggleEmail}
          invia={this.props.validationCreateOtp}
          verifica={this.props.validationVerifyOtp}
          hide={this.state.optHideEmail}
        />
      </CollapsibleItem>
    )
  }
}

Contatti.propTypes = {
  domini: PropTypes.object,
  dominio: PropTypes.object,
  updateDominio: PropTypes.func,
  dominioRequest: PropTypes.object,
  onSave: PropTypes.func,
  postInfoPersonaliRequest: PropTypes.object,
  customer: PropTypes.object,
  validationCreateOtp: PropTypes.func,
  validationVerifyOtp: PropTypes.func,
  verifyOtpRequest: PropTypes.object,
}

export default(Contatti)
