import React from 'react'
import { PropTypes } from 'prop-types'
import { Row } from 'react-materialize'

import './ConsensiPrivacy.scss'
import {
  getFieldValue,
  fieldPropTypes,
  updateField
} from '../../../utils'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'

const DatiPersonali = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
    const obiettiviTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
    const idDomanda = obiettiviTemplate.idDomanda
  
    const isRispostaErrata = () => {
      return getFieldValue(idDomanda) === obiettiviTemplate.listaRisposte[1].idRisposta;
    }
  
    const radios = obiettiviTemplate
      .listaRisposte
      .map(({ idRisposta, descRisposta }, i) => {
        const id = `radio-${idDomanda}-${idRisposta}`
  
        return (
          <p key={idRisposta}>
            <input
              name={id}
              type="radio"
              id={id}
              className={`with-gap ${(i === 1) ? 'negative' : ''}`}
              onChange={(value) => updateField(idDomanda, idRisposta)}
              checked={getFieldValue(idDomanda) === idRisposta} />
            <label htmlFor={id}>{descRisposta}</label>
          </p>
        )
      })
  
    return (
      <Row>
        <h6>
          {obiettiviTemplate.testoDomanda}
        </h6>
        <form action="#" className="radio-horizontal">
          {radios}
          {isRispostaErrata() && <div className="consenso-obbligatorio">Per proseguire è obbligatorio prestare il consenso</div>}
        </form>
      </Row>
    )
}

DatiPersonali.propTypes = fieldPropTypes
export default withFieldCatch(DatiPersonali)