import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Button } from 'react-materialize'
import { CollapsibleItem } from '../../../../../components/Collapsible'

import './ConsensiPrivacy.scss'
import {
  getFieldValue,
  updateField,
  filterQuestions
} from '../../../utils'
import fields from '../../fields'

import * as dataSB from '../../../../../static-data/data-servizio-base.json'

import WrappedAttivitaMarketing from './AttivitaMarketing'
import WrappedDatiPersonali from './DatiPersonali'

const consensiPrivacy = dataSB.censimento_anagrafica.consensi_privacy

class ConsensiPrivacy extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
      questions: {
        capResidenza: '',
        civicoResidenza: '',
        codComuneResidenza: '',
        codNazioneResidenza: '',
        codProvinciaResidenza: '',
        indirizzoResidenza: '',
      },
      form: {
        comuneResidenza: '',
        nazioneResidenza: '',
        provinciaResidenza: '',
      },
      enablements: {
        comune: false,
        cap: false,
      },
      domini: { }
    })
    this.checkConsenso = this.checkConsenso.bind(this)
    this.save = this.save.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.amlSaveRequest.fetching && !this.props.amlSaveRequest.fetching) {
        if (!this.props.amlSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }
  }

  checkConsenso() {
    const { template } = this.props;
    if (template && template.listaDomande &&
      Array.isArray(template.listaDomande) &&
      Array.isArray(template.listaDomande[0].listaRisposte)) {
        return getFieldValue(template.listaDomande[0].idDomanda, false, this.state) === template.listaDomande[0].listaRisposte[0].idRisposta;
    }
    return false;
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }

  render () {
    const { template, domini } = this.props
    const { listaDomande } = template

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: (idDomanda, value) => {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      }
    }

    return (
      <CollapsibleItem
        header={consensiPrivacy.title_accordion}
        icon="keyboard_arrow_down"
        itemKey="privacy"
        labelInfo={accordionInfo}

      >
        <WrappedDatiPersonali
          field={fields.DATI_PERSONALI}
          {...sectionContext}
        />
        <WrappedAttivitaMarketing
          field={fields.ATTIVITA_MARKETING}
          {...sectionContext}
        />

        <Row className="right-align submit-collapsible-item">
          <Button
            flat
            waves="light"
            disabled={!this.checkConsenso()}
            onClick={this.save}>
            Salva
          </Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

ConsensiPrivacy.propTypes = {
  domini: PropTypes.object,
  onSave: PropTypes.func,
  template: PropTypes.object,
  amlSaveRequest: PropTypes.object,
  setDataPending: PropTypes.func
}

export default(ConsensiPrivacy)
