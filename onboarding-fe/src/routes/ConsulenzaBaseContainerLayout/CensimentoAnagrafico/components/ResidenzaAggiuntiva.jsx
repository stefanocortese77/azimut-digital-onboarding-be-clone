import React from 'react'
import PropTypes from 'prop-types'
import Autocomplete from '../../../../components/Autocomplete/Autocomplete'
import { Row, Col, Input } from 'react-materialize'
import { mapDominiToAutocomplete, getDominio } from '../../utils'
import './ResidenzaAggiuntiva.scss'

export class ResidenzaAggiuntiva extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dirty: null,
      saving: false,
      form: {
        nazioneInvioComunicazioni: '',
        provinciaInvioComunicazioni: '',
        comuneInvioComunicazioni: '',
      },
      enablements: {
        comune: false,
        cap: false,
        provincia: false,
        altraCittadinanza: false,
      },
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONI')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE')),
        cap: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CAP')),
      }
    }

    this.dominioCustomRequest = this.dominioCustomRequest.bind(this)
    this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    this.handleComuneChange = this.handleComuneChange.bind(this)
    this.handleCAPChange = this.handleCAPChange.bind(this)
    this.handleNazioneChange = this.handleNazioneChange.bind(this)
  }

  dominioCustomRequest (codDominio, codFilter, value) {
    const request = {
      cod_domain: codDominio,
      filter: {
        cod_domain: codFilter,
        value: value
      }
    }
    this.props.dominio(request)
  }

  /* gestisce l'input con la nazione di residenza:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleNazioneChange (value, suggestion) {
    if (suggestion) {
      const isItalian = suggestion.label.toLowerCase().indexOf('ital') >= 0

      this.setState({
        form: {
          ...this.state.form,
          nazioneInvioComunicazioni: value
        },
        enablements: {
          ...this.state.enablements,
          provincia: isItalian
        },
      })
      this.props.handleQuestionValue('codNazioneInvioComunicazioni', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          nazioneInvioComunicazioni: value
        },
        enablements: {
          ...this.state.enablements,
          comune: false,
          provincia: false
        }
      })
    }
  }

  /* gestisce l'input con la provincia di residenza:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleProvinciaChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        form: {
          ...this.state.form,
          provinciaInvioComunicazioni: value,
          comuneInvioComunicazioni: ''
        },
        enablements: {
          ...this.state.enablements,
          comune: true,
          cap: false
        }
      })
      this.props.updateDominio('DOM_COMUNE', suggestion.value)
      this.props.handleQuestionValue('codProvinciaInvioComunicazioni', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          provinciaInvioComunicazioni: value
        },
        enablements: {
          ...this.state.enablements,
          comune: false
        }
      })
    }
  }

  /* gestisce l'input con il comune di residenza:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleComuneChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        form: {
          ...this.state.form,
          comuneInvioComunicazioni: value,
        },
        enablements: {
          ...this.state.enablements,
          cap: true
        }
      })
      this.props.handleQuestionValue('codComuneInvioComunicazioni', suggestion.value)
      this.props.updateDominio('DOM_CAP', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          comuneInvioComunicazioni: value
        },
        enablements: {
          ...this.state.enablements,
          cap: false
        }
      })
    }
  }

  /* gestisce l'input con il CAP di residenza:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleCAPChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        form: {
          ...this.state.form,
          capInvioComunicazioni: value,
        }
      })
      this.props.handleQuestionValue('capInvioComunicazioni', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          capInvioComunicazioni: value
        }
      })
    }
  }


  render () {
    // controllo che il nome non contenga numeri altrimenti mostro il messaggio
    let messaggioErroreNome
    if (!/^[a-zA-Z]*$/g.test(this.props.value.nomeComunicazioni)) {
      messaggioErroreNome = (
        <p className="errorMessage">Nome errato</p>
        )
    }

    // controllo che il cognome non contenga numeri altrimenti mostro il messaggio
    let messaggioErroreCognome
    if (!/^[a-zA-Z]*$/g.test(this.props.value.cognomeComunicazioni)) {
      messaggioErroreCognome = (
        <p className="errorMessage">Cognome errato</p>
          )
    }


    return (
      <div>
        <Row>
          <Input
            s={6}
            label="Nome"
            value={this.props.value.nomeComunicazioni}
            onChange={({ target: { value } }) => this.props.handleQuestionValue('nomeComunicazioni', value)}
            />
          <Input
            s={6}
            label="Cognome"
            value={this.props.value.cognomeComunicazioni}
            onChange={({ target: { value } }) => this.props.handleQuestionValue('cognomeComunicazioni', value)}
            />
        </Row>
        <Row>
          <Col s={6}>
            {messaggioErroreNome}
          </Col>
          <Col s={6}>
            {messaggioErroreCognome}
          </Col>
        </Row>
        <Row>
          <Col s={4}>
            <Autocomplete
              id="nazione-invio-comunicazioni"
              label={this.props.residenza.label_nazione}
              suggestions={this.state.domini.nazioni}
              value={this.state.form.nazioneInvioComunicazioni}
              onChange={this.handleNazioneChange}
            />
          </Col>
        </Row>
        <Row>
          <Col s={4}>
            <Autocomplete
              id="provincia-comunicazioni"
              label={this.props.residenza.label_provincia}
              threshold={0}
              suggestions={this.state.domini.province}
              value={this.state.form.provinciaInvioComunicazioni}
              onChange={this.handleProvinciaChange}
              disabled={!this.state.enablements.provincia}
            />
          </Col>
          <Col s={4}>
            <Autocomplete
              id="comune-comunicazioni"
              label={this.props.residenza.label_comune}
              threshold={0}
              suggestions={this.state.domini.comuni}
              value={this.state.form.comuneInvioComunicazioni}
              onChange={this.handleComuneChange}
              disabled={!this.state.enablements.comune}
            />
          </Col>
          <Col s={4}>
            <Autocomplete
              id="cap"
              label={this.props.residenza.label_cap}
              threshold={0}
              suggestions={this.state.domini.cap}
              value={this.props.capInvioComunicazioni}
              onChange={this.handleCAPChange}
              disabled={!this.state.enablements.cap}
            />
          </Col>
        </Row>
        <Row>
          <Input
            s={6}
            label={this.props.residenza.label_indirizzo}
            value={this.props.value.indirizzoInvioComunicazioni}
            onChange={({ target: { value } }) => this.props.handleQuestionValue('indirizzoInvioComunicazioni', value)}
          />
          <Input
            s={4}
            label={this.props.residenza.label_civico}
            value={this.props.value.civicoInvioComunicazioni}
            onChange={({ target: { value } }) => this.props.handleQuestionValue('civicoInvioComunicazioni', value)}
          />
        </Row>
      </div>
    )
  }
}
ResidenzaAggiuntiva.propTypes = {
  domini: PropTypes.object,
  dominioRequest: PropTypes.object,
  residenza: PropTypes.object,
  province: PropTypes.object,
  comuni: PropTypes.object,
  nazioni: PropTypes.object,
  cap: PropTypes.object,
  addResidenza: PropTypes.bool,
  dominio: PropTypes.func,
  indirizzoInvioComunicazioni: PropTypes.string,
  value: PropTypes.object,
  handleQuestionValue: PropTypes.func,
  updateDominio: PropTypes.func
}
export default ResidenzaAggiuntiva
