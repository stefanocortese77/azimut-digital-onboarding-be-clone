import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Input, Button, Col } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'

import {
  mapDominiToAutocomplete,
  getDominio,
} from '../../utils'

// import Strumenti from './Strumenti'
import * as dataSB from '../../../../static-data/data-servizio-base.json'
import Autocomplete from '../../../../components/Autocomplete/Autocomplete'

const fatcaCrs = dataSB.censimento_anagrafica.fatca_crs


class Fatca extends React.Component {
  constructor (props) {
    super(props)

    const listaResidenzaFiscale = props.customer.listaResidenzaFiscale || [{
      codNazione: '',
      codFiscale: ''
    }]

    this.state = Object.assign({
      dirty: null,
      saving: false,
      listaResidenzaFiscale: listaResidenzaFiscale,
      // addResidenzaFiscale: listaResidenzaFiscale[1] !== null,
      questions: {
      },
      form: {
      },
      enablements: {
      },
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONI')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE')),
      },
      firstCodNazione: '',
      firstCodNazioneText: '',
      firstCodFiscale: '',
      secondCodNazione: '',
      secondCodNazioneText: '',
      secondCodFiscale: '',
      hasSecondTaxResidence: false,
      tin: '',
      hasTin: false

    }/* , loadQuestions(this.props.previousForm, this.props.template.listaDomande) */)

    this.save = this.save.bind(this)
    this.updateFieldsAllegati = this.updateFieldsAllegati.bind(this)
    this.contentTextRF = this.contentTextRF.bind(this)
    this.addResidenzaFiscaleToggle = this.addResidenzaFiscaleToggle.bind(this)
    this.handleFirstNazioneChange = this.handleFirstNazioneChange.bind(this)
    this.handleFirstCodFiscaleChange = this.handleFirstCodFiscaleChange.bind(this)
    this.handleSecondNazioneChange = this.handleSecondNazioneChange.bind(this)
    this.handleSecondCodFiscaleChange = this.handleSecondCodFiscaleChange.bind(this)
    this.handleTinChange = this.handleTinChange.bind(this)
    this.addTinToggle = this.addTinToggle.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.postFatcaRequest.fetching && !this.props.postFatcaRequest.fetching) {
        if (!this.props.postFatcaRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }
  }


  handleFirstNazioneChange (value, matchElement) {
    if (matchElement) {
      this.setState({
        firstCodNazioneText: value,
        firstCodNazione: value,
      })
    } else {
      this.setState({
        firstCodNazioneText: value
      })
    }
  }


  handleFirstCodFiscaleChange (event) {
    const value = event.target.value
    this.setState({
      firstCodFiscale: value
    })
  }


  handleSecondNazioneChange (value, matchElement) {
    if (matchElement) {
      this.setState({
        secondCodNazioneText: value,
        secondCodNazione: value,
      })
    } else {
      this.setState({
        secondCodNazioneText: value
      })
    }
  }


  handleSecondCodFiscaleChange (event) {
    const value = event.target.value
    this.setState({
      secondCodFiscale: value
    })
  }

  handleTinChange (event) {
    this.setState({
      tin: event.target.value
    })
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      // this.props.onSave(this.state.listaResidenzaFiscale)
      this.props.onSave([{
        codNazione: this.state.firstCodNazione,
        codFiscale: this.state.firstCodFiscale,
      }, {
        codNazione: this.state.secondCodNazione,
        codFiscale: this.state.secondCodFiscale,
      }])
    }
  }

  updateFieldsAllegati (index, newPropertiesObj) {
    let newDataInfo = Object.assign({}, this.state.listaResidenzaFiscale[index], newPropertiesObj)
    let newlistaResidenzaFiscale = []
    this.state.listaResidenzaFiscale.forEach(function (element) {
      newlistaResidenzaFiscale.push(element)
    })
    newlistaResidenzaFiscale[index] = newDataInfo
    this.setState({ listaResidenzaFiscale: newlistaResidenzaFiscale })
  }


  addResidenzaFiscaleToggle () {
    this.setState({ hasSecondTaxResidence: !this.state.hasSecondTaxResidence })
  }

  // show TIN input
  addTinToggle (param) {
    if (param.target.name === 'residenzaSi') {
      if (!this.state.hasTin) {
        this.setState({ hasTin: !this.state.hasTin })
      }
    } else {
      if (this.state.hasTin) {
        this.setState({ hasTin: !this.state.hasTin })
        this.setState({ tin: '' })
      }
    }
  }

  // mostra nasconde aggiunta/rimozione altra residenza fiscale
  contentTextRF () {
    let domTemp
    if (this.state.hasSecondTaxResidence) {
      domTemp = <span><i className="icon-minus" /> Rimuovi altra residenza fiscale</span>
    } else if (this.state.firstCodNazione === '' || this.state.firstCodFiscale === '') {
      domTemp = null
    } else {
      domTemp = <span><i className="icon-plus" /> Inserisci altra residenza fiscale</span>
    }
    return domTemp
  }

  render () {
    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    if (!this.state.listaResidenzaFiscale) {
      return null
    }

    let secondTaxResidenceElement = null
    if (this.state.hasSecondTaxResidence) {
      secondTaxResidenceElement = (
        <Row>
          <Col s={6}>
            <Autocomplete
              id="nazione-fiscale"
              label={fatcaCrs.label_stato}
              suggestions={this.state.domini.nazioni}
              value={this.state.secondCodNazioneText}
              onChange={this.handleSecondNazioneChange}
          />
          </Col>
          <Input
            s={6}
            label={fatcaCrs.label_cf}
            value={this.state.secondCodFiscale}
            onChange={this.handleSecondCodFiscaleChange}
          />
        </Row>
      )
    }


    // handle change of TIN input element
    let tinElement = null
    if (this.state.hasTin) {
      tinElement = (
        <Row>
          <Input
            s={12}
            label="TIN"
            value={this.state.tin}
            onChange={this.handleTinChange}
          />
        </Row>
      )
    }

    // connteggio errori in pagina per disabilitare bottone 'Salva'
    let errori = 0

    if (this.state.firstCodNazioneText !== '' && (this.state.firstCodNazione !== this.state.firstCodNazioneText)) {
      errori = errori + 1
    }

    if (this.state.secondCodNazioneText !== '' && (this.state.secondCodNazioneText !== this.state.secondCodNazione)) {
      errori = errori + 1
    }

    return (
      <CollapsibleItem
        itemKey="fatca"
        header={fatcaCrs.title_accordion}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}>
        <Row>
          <Col s={12}>
            <p>
              <b>{fatcaCrs.label_dichiarazione_fatca}</b>
            </p>
            <p>{fatcaCrs.label_dichiarazione_residenza_USA}</p>
            <a name="residenzaSi" className="btn-floating btn-large waves-effect waves-light green" onClick={this.addTinToggle}>SI</a>
            <a name="residenzaNo" className="btn-floating btn-large waves-effect waves-light red" onClick={this.addTinToggle}>NO</a>
          </Col>
        </Row>
        <Row>
          { tinElement }
        </Row>
        <Row>
          <Col s={12}>
            <p>{fatcaCrs.label_dichiarazione_residenza}</p>
          </Col>
        </Row>
        <Row>
          <Col s={6}>
            <Autocomplete
              id="nazione-fiscale"
              label={fatcaCrs.label_stato}
              suggestions={this.state.domini.nazioni}
              value={this.state.firstCodNazioneText}
              onChange={this.handleFirstNazioneChange}
            />
            {console.log('robe a caso')}
            {console.log(this.state)}
          </Col>
          <Input
            s={6}
            label={fatcaCrs.label_cf}
            value={this.state.firstCodFiscale}
            onChange={this.handleFirstCodFiscaleChange}
          />
        </Row>
        { secondTaxResidenceElement }
        <Row>
          <p />
          <a onClick={this.addResidenzaFiscaleToggle}>{this.contentTextRF()}</a>
        </Row>

        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save} disabled={errori > 0}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

Fatca.propTypes = {
  domini: PropTypes.object,
  customer: PropTypes.object,
  onSave: PropTypes.func,
  postFatcaRequest: PropTypes.object
}

export default(Fatca)
