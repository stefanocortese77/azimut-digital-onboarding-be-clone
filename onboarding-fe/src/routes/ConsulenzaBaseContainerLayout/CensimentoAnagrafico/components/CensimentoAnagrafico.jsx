import React from 'react'
import PropTypes from 'prop-types'
import { loadQuestions, saveQuestions, checkStatusService } from '../../utils'
import { Link } from 'react-router-dom'
import dataSB from '../../../../static-data/data-servizio-base.json'
import { Collapsible } from '../../../../components/Collapsible'
import { Row } from 'react-materialize'

import AntiRiciclaggio from './AntiRiciclaggio'
import InformazioniPersonali from './InformazioniPersonali'
import Residenza from './Residenza'
import Contatti from './Contatti'
import Fatca from './Fatca'
import ConsensiPrivacy from './ConsensiPrivacy'

export class CensimentoAnagrafico extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      openCollapsible: 'informazioni-personali',
      dataRegistryFormSnapshot: null, //
      amlSnapshot: null,
      dataRegistry: this.props.dataRegistry
    }

    this.dominioCustomRequest = this.dominioCustomRequest.bind(this)
    this.handleSaveDataRegistry = this.handleSaveDataRegistry.bind(this)
    this.handleSaveFatca = this.handleSaveFatca.bind(this)
    this.handleSaveAml = this.handleSaveAml.bind(this)
    this.showAccordionInfo = this.showAccordionInfo.bind(this)
    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)
    this.nextStep = this.nextStep.bind(this)
    this.nextStepNoSave = this.nextStepNoSave.bind(this)
    this.updateDominio = this.updateDominio.bind(this)
  }

  dominioCustomRequest (codDominio, codFilter, value) {
    const request = {
      cod_domain: codDominio,
      filter: {
        cod_domain: codFilter,
        value: value
      }
    }
    this.props.dominio(request)
  }

  handleSaveDataRegistry (questions) {
    const updatedSnapshot = Object.assign({},
      this.state.dataRegistryFormSnapshot,
      questions
    )
    const dataRegistryBool = checkStatusService(this.props.summaryData.statusEndPoint, 'dataRegistry')
    this.props.saveDataRegistry(updatedSnapshot, this.props.customer.idCliente, this.props.customer.idIncarico, dataRegistryBool)
    console.log(updatedSnapshot)
    this.setState({ dataRegistryFormSnapshot: updatedSnapshot })
    this.props.setDataPending(false)
  }

  handleSaveFatca (listaDomande) {
    const { idCliente, idIncarico } = this.props.customer
    this.props.postFatca(idCliente, idIncarico, listaDomande)
    this.props.setDataPending(false)
  }

  handleSaveAml (listaDomande) {
    const updatedSnapshot = Object.assign({},
      this.state.amlSnapshot,
      listaDomande
    )

    const { idCliente, idIncarico } = this.props.customer

    this.props.saveAml(idCliente, idIncarico, saveQuestions(updatedSnapshot))
    console.log(updatedSnapshot)
    this.setState({ amlSnapshot: updatedSnapshot })
    this.props.setDataPending(false)
  }


  componentDidMount () {
    this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
    // this.props.getDataRegistry(
    //   this.props.customer.codiceFiscale,
    //   this.props.customer.idCliente,
    //   this.props.customer.idIncarico
    // )
    // this.props.fetchSurveyAml(this.props.customer.idCliente, this.props.customer.idIncarico)

    const dominioRequest = {
      'listaDomini': [
        {
          'idDominio': 'DOM_PROVINCE_NASCITA'
        },
        {
          'idDominio': 'DOM_NAZIONE_NASCITA'
        },
        {
          'idDominio': 'DOM_PROVINCE_CONTATTI'
        },
        {
          'idDominio': 'DOM_NAZIONE_CONTATTI'
        },
        {
          'idDominio': 'DOM_PROVINCE_RESIDENZA'
        },
        {
          'idDominio': 'DOM_NAZIONE_RESIDENZA'
        },
        {
          'idDominio': 'DOM_CITTADINANZE'
        },
        {
          'idDominio': 'DOM_TITOLO_STUDIO'
        },
        {
          'idDominio': 'DOM_SETTORE_OCCUPAZIONE_I'
        },
        {
          'idDominio': 'DOM_PROFESSIONE_ATTUALE'
        },
        {
          'idDominio': 'DOM_NATURA_RAPPORTO'
        },
        {
          'idDominio': 'DOM_ORIGINE_VERSAMENTO'
        },
        {
          'idDominio': 'DOM_TIPO_DOCUMENTO'
        },
        {
          'idDominio': 'DOM_CAP',
          'filtro': {
            'codice': '166009'
          }
        },
        {
          'idDominio': 'DOM_COMUNE',
          'filtro': {
            'codice': '1'
          }
        }
      ]
    }
    this.props.dominio(dominioRequest)
  }

  showAccordionInfo (accordionKey) {
    let accordionDataToCheck = 'accordionDataPending' + accordionKey
    let labelInfoElement = null
    if (this.state[accordionDataToCheck] === true) {
      labelInfoElement = <span className="accordion-info accordion-pending">Dati non salvati</span>
    } else if (this.state[accordionDataToCheck] === false) {
      labelInfoElement = <span className="accordion-info accordion-saved">Dati salvati</span>
    }
    return labelInfoElement
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({
      openCollapsible: itemKey
    })
  }

  nextStep (path) {
    if (this.props.dataPending === true) {
      this.toggleModal()
      this.setState({ nextPath: path })
    } else {
      this.props.history.push(path)
    }
  }

  nextStepNoSave () {
    this.props.history.push(this.state.nextPath)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.getDataSummaryRequest.fetching === true && this.props.getDataSummaryRequest.fetching === false && this.props.getDataSummaryRequest.error === null) {
      if (this.props.summaryData.statusEndPoint) {
        const dataRegistryBool = checkStatusService(this.props.summaryData.statusEndPoint, 'dataRegistry')
        const amlBool = checkStatusService(this.props.summaryData.statusEndPoint, 'aml')
        const fatcacrsBool = checkStatusService(this.props.summaryData.statusEndPoint, 'fatcacrs')

        this.props.getDataRegistry(
          this.props.customer.codiceFiscale,
          this.props.customer.idCliente,
          this.props.customer.idIncarico,
          dataRegistryBool,
          fatcacrsBool
        )
        this.props.fetchSurveyAml(this.props.customer.idCliente, this.props.customer.idIncarico, amlBool)
      }
    }
    if (prevProps.getDataRegistryRequest.fetching === true && this.props.getDataRegistryRequest.fetching === false && this.props.getDataRegistryRequest.error === null) {
      this.setState({ dataRegistry: this.props.dataRegistry })
    }

    if (!prevProps.amlPreviousForm.idCliente && this.props.amlPreviousForm.idCliente) {
      this.setState({
        amlSnapshot: loadQuestions(this.props.amlPreviousForm)
      })

      console.log(this.props.amlPreviousForm)
    }
  }

  updateDominio (name, filter) {
    this.props.dominio({
      'listaDomini': [{
        idDominio: name,
        filtro: {
          codice: filter
        }
      }]
    })
  }

  render () {
    if (!this.props.customer) {
      return null
    }

    const { templateAml, domini } = this.props

    if (!Object.keys(templateAml).length) {
      return null
    }

    if (!Object.keys(domini).length) {
      return null
    }

    // Check if there a is the requested domain
    if (!Object.keys(domini.listaDomini).find(dominioKey => domini.listaDomini[dominioKey].idDominio === 'DOM_PROVINCE_NASCITA')) {
      return null
    }

    const torna = dataSB.button_back
    const title = dataSB.title
    const titleSection = dataSB.censimento_anagrafica.title_section

    return (
      <section className="main-body consulenza-base">
        <header>
          <Link to="/" className="torna-pratiche">
            <i className="icon-arrow-back" />
            {torna}
          </Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>

        <article className="page-left-content">
          <div className="background_pattern" />
          <h2 className="title-sezione">{titleSection}</h2>
          <Collapsible
            open={this.state.openCollapsible}
            onChange={this.handleOpenCollapsibleChange}
          >
            <InformazioniPersonali
              // template={sezioni[0]}
              domini={domini}
              updateDominio={this.updateDominio}
              dominioRequest={this.props.dominioRequest}
              questions={this.state.dataRegistry}
              // previousForm={this.props.previousForm}
              setDataPending={this.handleSetDataPending}
              postInfoPersonaliRequest={this.props.postInfoPersonaliRequest}
              onSave={this.handleSaveDataRegistry} />
            <Residenza
              // template={sezioni[0]}
              domini={domini}
              updateDominio={this.updateDominio}
              dominioRequest={this.props.dominioRequest}
              // previousForm={this.props.previousForm}
              questions={this.state.dataRegistry}
              setDataPending={this.handleSetDataPending}
              postInfoPersonaliRequest={this.props.postInfoPersonaliRequest}
              onSave={this.handleSaveDataRegistry} />
            <Contatti
              // template={sezioni[0]}
              domini={domini}
              updateDominio={this.updateDominio}
              dominioRequest={this.props.dominioRequest}
              // previousForm={this.props.previousForm}
              setDataPending={this.handleSetDataPending}
              postInfoPersonaliRequest={this.props.postInfoPersonaliRequest}
              onSave={this.handleSaveDataRegistry}
              questions={this.state.dataRegistry}
              customer={this.props.customer}
              validationCreateOtp={this.props.validationCreateOtp}
              validationVerifyOtp={this.props.validationVerifyOtp}
              verifyOtpRequest={this.props.verifyOtpRequest}
            />
            <Fatca
              // template={sezioni[0]}
              domini={domini}
              updateDominio={this.updateDominio}
              // previousForm={this.props.previousForm}
              setDataPending={this.handleSetDataPending}
              postFatcaRequest={this.props.postFatcaRequest}
              onSave={this.handleSaveFatca}
              customer={this.props.customer}
            />
            <AntiRiciclaggio
              template={templateAml.sezioni[0]}
              domini={domini}
              updateDominio={this.updateDominio}
              onSave={(question) => { this.handleSaveAml(question) }}
              itemKey="aml"
              labelInfo={this.showAccordionInfo}
              previousForm={this.props.amlPreviousForm}
              setDataPending={this.props.setDataPending}
              amlSaveRequest={this.props.amlSaveRequest}
            />
            <ConsensiPrivacy
              template={templateAml.sezioni[1]}
              domini={domini}
              onSave={(question) => { this.handleSaveAml(question) }}
              itemKey="aml"
              labelInfo={this.showAccordionInfo}
              previousForm={this.props.amlPreviousForm}
              setDataPending={this.props.setDataPending}
              amlSaveRequest={this.props.amlSaveRequest}
            />

          </Collapsible>
          <Row className="right-align padding-r-20 margin-t-20">
            <Link onClick={() => this.nextStep('/consulenza-base/questionario-profilatura')} to="#">{dataSB.link_continue}
              <i className="icon-arrow" />
            </Link>
          </Row>
        </article>
      </section>
    )
  }
}

CensimentoAnagrafico.propTypes = {
  dominio: PropTypes.func,
  dominioRequest: PropTypes.object,
  customer: PropTypes.object,
  getDataSummaryLight: PropTypes.func,
  getDataRegistry: PropTypes.func,
  fetchSurveyAml: PropTypes.func,
  verifyOtpRequest: PropTypes.object,
  templateAml: PropTypes.object,
  domini: PropTypes.object,
  validationCreateOtp: PropTypes.func,
  validationVerifyOtp: PropTypes.func,
  saveDataRegistry: PropTypes.func,
  setDataPending:PropTypes.func,
  postInfoPersonaliRequest: PropTypes.object,
  postFatca: PropTypes.func,
  postFatcaRequest: PropTypes.object,
  amlPreviousForm: PropTypes.object,
  amlSaveRequest: PropTypes.object,
  saveAml: PropTypes.func,
  getDataSummaryRequest: PropTypes.object,
  history: PropTypes.object,
  dataPending: PropTypes.bool,
  summaryData: PropTypes.object,
  dataRegistry: PropTypes.object
}
export default CensimentoAnagrafico
