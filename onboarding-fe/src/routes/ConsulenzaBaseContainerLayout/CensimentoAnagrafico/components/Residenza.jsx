import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Input, Button, Col } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'

import Autocomplete from '../../../../components/Autocomplete/Autocomplete'
import { mapDominiToAutocomplete, getDominio } from '../../utils'

// import Strumenti from './Strumenti'
import * as dataSB from '../../../../static-data/data-servizio-base.json'


const infoResidenza = dataSB.censimento_anagrafica.residenza


class Residenza extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
      questions: {
        capResidenza: '',
        civicoResidenza: '',
        codComuneResidenza: '',
        codNazioneResidenza: '',
        codProvinciaResidenza: '',
        indirizzoResidenza: '',
      },
      form: {
        comuneResidenza: '',
        nazioneResidenza: '',
        provinciaResidenza: '',
      },
      enablements: {
        comune: false,
        cap: false,
        provincia: false,
        altraCittadinanza: false,
      },
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE_RESIDENZA')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONE_RESIDENZA')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE_RESIDENZA')),
        cap: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CAP_RESIDENZA')),
      }
    }/* , loadQuestions(this.props.previousForm, this.props.template.listaDomande) */)

    this.save = this.save.bind(this)
    this.handleQuestionValue = this.handleQuestionValue.bind(this)
    this.handleIndirizzoChange = this.handleIndirizzoChange.bind(this)
    this.handleCivicoChange = this.handleCivicoChange.bind(this)
    this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    this.handleComuneChange = this.handleComuneChange.bind(this)
    this.handleCAPChange = this.handleCAPChange.bind(this)
    this.handleNazioneChange = this.handleNazioneChange.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.postInfoPersonaliRequest.fetching && !this.props.postInfoPersonaliRequest.fetching) {
        if (!this.props.postInfoPersonaliRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }

    if (prevProps.dominioRequest.fetching && !this.props.dominioRequest.fetching) {
      if (!this.props.dominioRequest.error) {
        this.updateDomini()
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this
        .props
        .onSave(this.state.questions)
    }
  }

  updateDomini (domini) {
    this.setState({
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_CITTADINANZE')),
        nazioni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_NAZIONE_RESIDENZA')),
        province: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_PROVINCE_RESIDENZA')),
        comuni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_COMUNE_RESIDENZA')),
        cap: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_CAP_RESIDENZA'))
      }
    })
  }

  handleQuestionValue (question, value) {
    this.setState({ questions: Object.assign({}, this.state.questions, { [question]: value }) })
  }

  handleIndirizzoChange (event) {
    this.handleQuestionValue('indirizzoResidenza', event.target.value)
  }

  handleCivicoChange (event) {
    this.handleQuestionValue('civicoResidenza', event.target.value)
  }

  handleNazioneChange (value, suggestion) {
    if (suggestion) {
      const isItalian = suggestion.label.toLowerCase().indexOf('ital') >= 0

      this.setState({
        questions: {
          ...this.state.questions,
          codNazioneResidenza: suggestion.value
        },
        form: {
          ...this.state.form,
          nazioneResidenza: value
        },
        enablements: {
          ...this.state.enablements,
          provincia: isItalian
        }
      })
    } else {
      this.setState({
        form: {
          ...this.state.form,
          nazioneResidenza: value
        },
        enablements: {
          ...this.state.enablements,
          comune: false,
          provincia: false
        }
      })
    }
  }


  handleProvinciaChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        questions: {
          ...this.state.questions,
          codProvinciaResidenza: suggestion.value,
          codComuneResidenza: null,
          capResidenza: null,
        },
        form: {
          ...this.state.form,
          provinciaResidenza: value,
          comuneResidenza: ''
        },
        enablements: {
          ...this.state.enablements,
          comune: true,
          cap: false
        }
      })
      this.props.updateDominio('DOM_COMUNE_RESIDENZA', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          provinciaResidenza: value
        },
        enablements: {
          ...this.state.enablements,
          comune: false
        }
      })
    }
  }

  handleComuneChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        questions: {
          ...this.state.questions,
          codComuneResidenza: suggestion.value,
          capResidenza: '',
        },
        form: {
          ...this.state.form,
          comuneResidenza: value,
        },
        enablements: {
          ...this.state.enablements,
          cap: true
        }
      })
      this.props.updateDominio('DOM_CAP_RESIDENZA', suggestion.value)
    } else {
      this.setState({
        form: {
          ...this.state.form,
          comuneResidenza: value
        },
        enablements: {
          ...this.state.enablements,
          cap: false
        }
      })
    }
  }

  handleCAPChange (value, suggestion) {
    this.setState({
      questions: {
        ...this.state.questions,
        capResidenza: value
      },
    })
  }


  render () {
    // const { domini } = this.props const sectionContext = {   domini: domini,
    // getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue,
    // this.state),   updateField: (idDomanda, value) => {
    // this.setState(updateField(idDomanda, value))     this       .props
    // .setDataPending()   } }

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="residenza"
        header={infoResidenza.title_accordion}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}>
        <Row>
          <Col s={6}>
            <Autocomplete
              id="nazione-residenza"
              placeholder={infoResidenza.label_nazione}
              threshold={3}
              suggestions={this.state.domini.nazioni}
              value={this.state.form.nazioneResidenza}
              onChange={this.handleNazioneChange}
            />
          </Col>
          <Col s={6}>
            <Autocomplete
              id="provincia-residenza"
              placeholder={infoResidenza.label_provincia}
              threshold={0}
              suggestions={this.state.domini.province}
              onChange={this.handleProvinciaChange}
              value={this.state.form.provinciaResidenza}
              disabled={!this.state.enablements.provincia}
            />
          </Col>
        </Row>
        <Row>
          <Col s={6}>
            <Autocomplete
              id="comune-rilascio"
              placeholder={infoResidenza.label_comune}
              threshold={0}
              suggestions={this.state.domini.comuni}
              onChange={this.handleComuneChange}
              value={this.state.form.comuneResidenza}
              disabled={!this.state.enablements.comune}
            />
          </Col>
          <Col s={6}>
            <Autocomplete
              id="cap-residenza"
              placeholder={infoResidenza.label_cap}
              threshold={0}
              suggestions={this.state.domini.cap}
              value={this.state.questions.capResidenza}
              onChange={this.handleCAPChange}
              disabled={!this.state.enablements.cap}
            />
          </Col>
        </Row>
        <Row>
          <Input
            s={8}
            label={infoResidenza.label_indirizzo}
            value={this.state.questions.indirizzoResidenza}
            onChange={this.handleIndirizzoChange} />
          <Input
            s={4}
            label={infoResidenza.label_civico}
            value={this.state.questions.civicoResidenza}
            onChange={this.handleCivicoChange} />
        </Row>
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

Residenza.propTypes = {
  domini: PropTypes.object,
  dominioRequest: PropTypes.object,
  onSave: PropTypes.func,
  // setDataPending: PropTypes.func, previousForm: PropTypes.object,
  postInfoPersonaliRequest: PropTypes.object,
  updateDominio: PropTypes.func
}

export default(Residenza)
