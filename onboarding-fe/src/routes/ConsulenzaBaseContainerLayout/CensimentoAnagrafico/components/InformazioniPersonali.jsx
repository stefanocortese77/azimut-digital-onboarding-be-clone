import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Input, Button, Col } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'

import moment from 'moment'
import Autocomplete from '../../../../components/Autocomplete/Autocomplete'
import DatePicker from '../../../../components/DatePicker/DatePicker'
import {
  mapDominiToAutocomplete,
  getDominio,
} from '../../utils'

// import Strumenti from './Strumenti'
import * as dataSB from '../../../../static-data/data-servizio-base.json'
import './InformazioniPersonali.scss'

const infoPersonali = dataSB.censimento_anagrafica.info_personali

class InformazioniPersonali extends React.Component {
  constructor (props) {
    super(props)
    const dominioCittadinanze = getDominio(props.domini, 'DOM_CITTADINANZE')
    const { questions } = this.props

    this.state = Object.assign({
      dirty: null,
      saving: false,
      questions: {
        nome: (questions && questions.dataRegistry) ? this.props.questions.dataRegistry.nome : '',
        cognome: (questions && questions.dataRegistry) ? this.props.questions.dataRegistry.cognome : '',
        codProvincia: (questions && questions.dataRegistry) ? this.props.questions.dataRegistry.codProvincia : '',
        codComune: (questions) ? this.props.questions.codComune : '',
        codCittadinanza: Object.keys(dominioCittadinanze).find((id) => dominioCittadinanze[id].toLowerCase().indexOf('italiana') === 0),
        codAltraCittadinanza: (questions && questions.dataRegistry) ? this.props.questions.dataRegistry.codAltraCittadinanza : '',
        dataNascita: (questions && questions.dataRegistry) ? this.props.questions.dataRegistry.dataNascita : null,
      },
      form: {
        provincia: '',
        comune: '',
        cittadinanza: 'Italiana',
        altraCittadinanza: '',
      },
      enablements: {
        comune: false,
        provincia: false,
        altraCittadinanza: false,
      },
      domini: {
        cittadinanze: mapDominiToAutocomplete(dominioCittadinanze),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE_NASCITA')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONE_NASCITA')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE_NASCITA')),
      }
    }/* , loadQuestions(this.props.previousForm, this.props.template.listaDomande) */)

    this.save = this.save.bind(this)
    this.updateDomini = this.updateDomini.bind(this)
    this.handleQuestionValue = this.handleQuestionValue.bind(this)
    this.handleNomeChange = this.handleNomeChange.bind(this)
    this.handleCognomeChange = this.handleCognomeChange.bind(this)
    this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    this.handleComuneChange = this.handleComuneChange.bind(this)
    this.handleCittadinanzaChange = this.handleCittadinanzaChange.bind(this)
    this.handleAltraCittadinanzaChange = this.handleAltraCittadinanzaChange.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.postInfoPersonaliRequest.fetching && !this.props.postInfoPersonaliRequest.fetching) {
        if (!this.props.postInfoPersonaliRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }

    if (prevProps.dominioRequest.fetching && !this.props.dominioRequest.fetching) {
      if (!this.props.dominioRequest.error) {
        this.updateDomini()
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(this.state.questions)
    }
  }

  updateDomini (domini) {
    this.setState({
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_COMUNE_NASCITA')),
        nazioni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_NAZIONE_NASCITA')),
        province: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_PROVINCE_NASCITA'))
      }
    })
  }

  handleQuestionValue (question, value) {
    this.setState({ questions: Object.assign({}, this.state.questions, { [question]: value }) })
  }

  /* gestisce l'input con il nome
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleNomeChange (event) {
    this.handleQuestionValue('nome', event.target.value)
  }

  /* gestisce l'input con il cognome
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleCognomeChange (event) {
    this.handleQuestionValue('cognome', event.target.value)
  }

  /* gestisce l'input con la provincia
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleProvinciaChange (value, suggestion) {
    if (suggestion) {
      // Reset comune too

      this.setState({
        questions: Object.assign({ }, this.state.questions, {
          codProvincia: suggestion.value,
          codComune: null,
        }),
        form: Object.assign({ }, this.state.form, {
          provincia: value,
          comune: ''
        }),
        enablements: Object.assign({ }, this.state.enablements, {
          comune: true
        }),
      })
      this.props.updateDominio('DOM_COMUNE_NASCITA', suggestion.value)
    } else {
      this.setState({
        form: Object.assign({ }, this.state.form, {
          provincia: value
        }),
        enablements: Object.assign({ }, this.state.enablements, {
          comune: false
        })
      })
    }
  }

  /* gestisce l'input con il comune
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleComuneChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        questions: Object.assign({}, this.state.questions, {
          codComune: suggestion.value
        }),
        form: Object.assign({ }, this.state.form, {
          comune: value
        }),
      })
    } else {
      this.setState({
        form: Object.assign({}, this.state.form, {
          comune: value
        }),
      })
    }
  }

  /* gestisce l'input con la prima cittadinanza
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleCittadinanzaChange (value, suggestion) {
    if (suggestion) {
      const isItalian = suggestion.label.toLowerCase().indexOf('italian') >= 0

      this.setState({
        questions: Object.assign({}, this.state.questions, {
          codCittadinanza: suggestion.value
        }),
        form: Object.assign({ }, this.state.form, {
          cittadinanza: value
        }),
        enablements: Object.assign({ }, this.state.enablements, {
          provincia: isItalian,
          altraCittadinanza: isItalian
        }),
      })
    } else {
      this.setState({
        form: Object.assign({}, this.state.form, {
          cittadinanza: value
        }),
      })
    }
  }

  /* gestisce l'input con la seconda cittadinanza
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleAltraCittadinanzaChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        questions: Object.assign({}, this.state.questions, {
          codAltraCittadinanza: suggestion.value
        }),
        form: Object.assign({ }, this.state.form, {
          altraCittadinanza: value
        }),
      })
    } else {
      this.setState({
        form: Object.assign({}, this.state.form, {
          altraCittadinanza: value
        }),
      })
    }
  }

  render () {
    // connteggio errori in pagina per disabilitare bottone 'Salva'
    let errori = 0

    // controllo che il nome non contenga numeri altrimenti mostro il messaggio
    let messaggioErroreNome
    if (!/^[a-zA-Z]*$/g.test(this.state.questions.nome)) {
      errori = errori + 1
      messaggioErroreNome = (
        <p className="errorMessage">Nome errato</p>
        )
    }

    // controllo che il cognome non contenga numeri altrimenti mostro il messaggio
    let messaggioErroreCognome
    if (!/^[a-zA-Z]*$/g.test(this.state.questions.cognome)) {
      errori = errori + 1
      messaggioErroreCognome = (
        <p className="errorMessage">Cognome errato</p>
          )
    }


    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        itemKey="informazioni-personali"
        header={infoPersonali.title_accordion}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}>
        <Row>
          <Input
            s={6}
            label={infoPersonali.label_nome}
            value={this.state.questions.nome}
            onChange={this.handleNomeChange} />
          <Input
            s={6}
            label={infoPersonali.label_cognome}
            value={this.state.questions.cognome}
            onChange={this.handleCognomeChange} />
        </Row>
        <Row>
          <Col s={6}>
            {messaggioErroreNome}
          </Col>
          <Col s={6}>
            {messaggioErroreCognome}
          </Col>
        </Row>
        <Row>
          <Col s={6}>
            <Autocomplete
              id="cittadinanza"
              placeholder={infoPersonali.label_cittadinanza}
              suggestions={this.state.domini.cittadinanze}
              onChange={this.handleCittadinanzaChange}
              value={this.state.form.cittadinanza}
              threshold={0}
            />
          </Col>
          <Col s={6}>
            <Autocomplete
              id="altra-cittadinanza"
              placeholder={infoPersonali.label_cittadinanza2}
              suggestions={this.state.domini.cittadinanze}
              onChange={this.handleAltraCittadinanzaChange}
              value={this.state.form.altraCittadinanza}
              disabled={!this.state.enablements.altraCittadinanza}
              threshold={0}
            />
          </Col>
        </Row>
        <Row>
          <Col s={4}>
            <Autocomplete
              id="provincia-rilascio"
              label={infoPersonali.label_provincia}
              threshold={0}
              suggestions={this.state.domini.province}
              onChange={this.handleProvinciaChange}
              value={this.state.form.provincia}
              disabled={!this.state.enablements.provincia}
          />
          </Col>
          <Col s={4}>
            <Autocomplete
              id="comune-rilascio"
              label={infoPersonali.label_comune}
              threshold={0}
              suggestions={this.state.domini.comuni}
              onChange={this.handleComuneChange}
              value={this.state.form.comune}
              disabled={!this.state.enablements.comune}
          />
          </Col>
          <Col s={4}>
            <DatePicker
              placeholder={infoPersonali.label_data}
              isOutsideRange={(date) => moment(date).isAfter(moment().subtract(18, 'years')) || moment(date).isBefore(moment().subtract(120, 'years'))}
              initialVisibleMonth={() => moment().subtract(18, 'years')}
              alignLeft
              readOnly
              defaultDate={this.state.questions.dataNascita}
            />
            <span className="suggestion-for-input">
              {infoPersonali.suggestion_data}
            </span>
          </Col>
        </Row>
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save} disabled={errori > 0}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

InformazioniPersonali.propTypes = {
  domini: PropTypes.object,
  dominioRequest: PropTypes.object,
  onSave: PropTypes.func,
  postInfoPersonaliRequest: PropTypes.object,
  updateDominio: PropTypes.func,
  questions: PropTypes.object
}

export default(InformazioniPersonali)
