import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import YesNoQuestion from '../../../../../components/YesNoQuestion'
import {
    fieldPropTypes,
    getFieldValue,
    getDominio,
    booleanToFieldId,
    fieldIdToBoolean,
    updateField
  } from '../../../utils'

const YesNoQuestionAntiriciclaggio = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
    const YesNoTemplate = listaDomande.find(c => c.idDomanda === field.templateId)

    const trueId = YesNoTemplate
        .listaRisposte
        .find(d => d.descRisposta === 'SI')
        .idRisposta
    const falseId = YesNoTemplate
        .listaRisposte
        .find(d => d.descRisposta === 'NO')
        .idRisposta

    const booleanIdMap = booleanToFieldId(trueId, falseId)
    const idBooleanMap = fieldIdToBoolean(trueId, falseId)

    return (
        <YesNoQuestion
            label={YesNoTemplate.testoDomanda}
            onChange={(value) => updateField(YesNoTemplate.idDomanda, booleanIdMap(value))}
            value={idBooleanMap(getFieldValue(YesNoTemplate.idDomanda))}
        />
    )
}

YesNoQuestionAntiriciclaggio.propTypes = fieldPropTypes

export default withFieldCatch(YesNoQuestionAntiriciclaggio)