import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
    mapDominiToOptions,
    fieldPropTypes,
    getFieldValue,
    getDominio,
    updateField
  } from '../../../utils'

/**
 * 
 * Select sulla natura del rapporto
 * Text Input in caso la natura del rapporto sia 'altro'
 * Mappa in option la lista di risposte ottenute tramite getDominio sul DOM_NATURA_RAPPORTO
 * Il campo altro viene abilitato se il flag enableAltro dello stato del componente è true
 */
class NaturaRapporto extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
        enableAltro: false,
        naturaRapportoIdDomanda: props.listaDomande.find(c => c.idDomanda === props.field.templateId).idDomanda,
        dominioNaturaRapporto: getDominio(this.props.domini, 'DOM_NATURA_RAPPORTO')
      }
  
      this.handleNaturaRapportoChange = this.handleNaturaRapportoChange.bind(this)
    }
  
    handleNaturaRapportoChange (event) {
      const value = event.target.value
      this.props.updateField(this.state.naturaRapportoIdDomanda, value)
  
      if (this.state.dominioNaturaRapporto[value] === 'Altro') {
        this.setState({ enableAltro: true })
      } else {
        this.setState({ enableAltro: false })
      }
    }
  
    render () {
      const {
        listaDomande,
        field,
        getFieldValue,
        updateField,
        altroField
      } = this.props
  
      const naturaRapportoTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
      const naturaRapportoOptions = mapDominiToOptions(this.state.dominioNaturaRapporto, true)
  
  
      // TODO: remove this...
      const naturaRapportoAltroTemplate = {
        idDomanda: 1046,
        testoDomanda: 'Altro*'
      }
      // ...and uncomment this
      // const naturaRapportoTemplate = listaDomande.find(c => c.idDomanda === altroField.templateId)
  
  
      return (
        <Row>
          <Col s={6} key={1}>
            <Input
              s={12}
              type="select"
              label={naturaRapportoTemplate.testoDomanda}
              defaultValue={-1}
              onChange={this.handleNaturaRapportoChange}
              value={getFieldValue(naturaRapportoTemplate.idDomanda)}
            >
              {naturaRapportoOptions}
            </Input>
          </Col>
          <Col s={6} key={2}>
            <Input
              s={12}
              type="text"
              label={naturaRapportoAltroTemplate.testoDomanda}
              onBlur={({ target: { value } }) => updateField(naturaRapportoAltroTemplate.idDomanda, value)}
              onChange={({ target: { value } }) => this.setState({ naturaRapportoAltro: value })}
              value={this.state.naturaRapportoAltro}
              disabled={!this.state.enableAltro}
            />
          </Col>
        </Row>
      )
    }
}

NaturaRapporto.propTypes = {
    ...fieldPropTypes,
    altroField: PropTypes.object
}

export default withFieldCatch(NaturaRapporto)