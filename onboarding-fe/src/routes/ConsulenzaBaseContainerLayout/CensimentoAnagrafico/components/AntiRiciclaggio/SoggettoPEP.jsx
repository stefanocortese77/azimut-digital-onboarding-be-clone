import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import fields from '../../fields'
import {
    fieldPropTypes,
    getFieldValue,
    getDominio,
    updateField
  } from '../../../utils'
import DatePicker from '../../../../../components/DatePicker/DatePicker'
import WrappedYesNoQuestionAntiriciclaggio from './YesNoQuestionAntiriciclaggio'

class SoggettoPEP extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
        caricaRicoperta: '',
        fonteReddito: '',
        settoreOccupazioneIdDomanda: props.listaDomande.find(c => c.idDomanda === props.field.templateId).idDomanda,
        dominioSoggettoPEP: getDominio(this.props.domini, 'DOM_SETTORE_OCCUPAZIONE_I'),
        soggettoPEPTemplate: props.listaDomande.find(c => c.idDomanda === props.field.templateId)
      }
  
      this.handleSoggettoPEPChange = this.handleSoggettoPEPChange.bind(this)
      this.handleSoggettoPEPChange = this.handleSoggettoPEPChange.bind(this)
    }
  
    handleSoggettoPEPChange (event) {
      const value = event.target.value
      this.props.updateField(this.state.settoreOccupazioneIdDomanda, value)
  
      if (this.state.dominioSoggettoPEP[value] === 'Altro') {
        this.setState({ enableAltro: true })
      } else {
        this.setState({ enableAltro: false })
      }
    }
  
  
    render () {
      const {
        listaDomande,
        field,
        getFieldValue,
        updateField
      } = this.props
  
      const caricaRicopertaTemplate = listaDomande.find(c => c.idDomanda === 52)
      const dataInizioCaricaTemplate = listaDomande.find(c => c.idDomanda === 53)
      const fonteRedditoTemplate = listaDomande.find(c => c.idDomanda === 54)
      // TODO: ...and uncomment this:
      // const caricaRicopertaTemplate = listaDomande.find(c => c.idDomanda === fieldCaricaRicoperta.templateId)
      // const dataInizioCaricaTemplate = listaDomande.find(c => c.idDomanda === fieldDataInizioCarica.templateId)
      // const fonteRedditoTemplate = listaDomande.find(c => c.idDomanda === fieldFonteReddito.templateId)
  
      const RadioBigTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
  
      // di seguito mapping degli id che arrivano dal template BE
      // const noPepId = 63
      const siPepNazionaleId = 64
      const siPepEsteroId = 65
      const siPepRapportiId = 66
  
      const RadioPepPerche = listaDomande.find(c => c.idDomanda === 55)
      const RadioPepReddito = listaDomande.find(c => c.idDomanda === 56)
      const RadioPepRapportiPerche = listaDomande.find(c => c.idDomanda === 59)
  
      // const trueId = RadioBigTemplate.listaRisposte.find(d => d.descRisposta === 'SI, PEP NAZIONALE').idRisposta
      // const falseId = RadioBigTemplate.listaRisposte.find(d => d.descRisposta === 'NO').idRisposta
      //
      // const booleanIdMap = booleanToFieldId(trueId, falseId)
      // const idBooleanMap = fieldIdToBoolean(trueId, falseId)
  
      const pepValue = getFieldValue(RadioBigTemplate.idDomanda)
  
      let pepActiveClass = ''// classe per mostrare il blocco di info PEP
      if (pepValue === siPepNazionaleId || pepValue === siPepEsteroId || pepValue === siPepRapportiId) pepActiveClass = 'pep-active'
  
      let hideClassPep = ''// classe per nascondere il radio di motivazione Pep nazionale o estero
      let hideClassPepRapporti = ''// classe per nascondere il radio di motivazione Pep Rapporti
      if (pepValue === siPepRapportiId) hideClassPep = 'hide-question'
      if (pepValue === siPepNazionaleId || pepValue === siPepEsteroId) hideClassPepRapporti = 'hide-question'
  
      const radioPepOptions = RadioBigTemplate.listaRisposte.map((risposta, i) => {
        return (
          <div key={i}>
            <input name="pep-type" type="radio"
              id={'type-' + i} className="with-gap"
              onChange={(value) => updateField(RadioBigTemplate.idDomanda, risposta.idRisposta)}
              checked={risposta.idRisposta === pepValue} />
            <label htmlFor={'type-' + i}>{risposta.descRisposta}</label>
          </div>
        )
      })
  
      // TODO aggiornamento dello state al change dei radio interni
  
      const RadioPepPercheOptions = RadioPepPerche.listaRisposte.map((risposta, i) => {
        return (
          <p key={`p-pep-perche-${i}`}>
            <input name="pep-perche" type="radio" id={'pep-perche-' + i} className="with-gap" />
            <label htmlFor={'pep-perche-' + i}>{risposta.descRisposta}</label>
          </p>
        )
      })
  
      const RadioPepRedditoOptions = RadioPepReddito.listaRisposte.map((risposta, i) => {
        return (
          <p key={`p-pep-reddito-${i}`}>
            <input name="pep-reddito" type="radio" id={'pep-reddito-' + i} className="with-gap" />
            <label htmlFor={'pep-reddito-' + i}>{risposta.descRisposta}</label>
          </p>
        )
      })
  
      const RadioPepRapportiPercheOptions = RadioPepRapportiPerche.listaRisposte.map((risposta, i) => {
        return (
          <p key={`p-pep-rapporti-perche-${i}`}>
            <input name="pep-rapporti-perche" type="radio" id={'pep-rapporti-' + i} className="with-gap" />
            <label htmlFor={'pep-rapporti-' + i}>{risposta.descRisposta}</label>
          </p>
        )
      })
  
      return (
        <div className="pep-section">
          <Row>
            <Col s={12}>
              <h6>{RadioBigTemplate.testoDomanda}</h6>
              <form action="#" className="big-radio">
                {radioPepOptions}
              </form>
            </Col>
          </Row>
          <div className={'pep-more-info-box ' + pepActiveClass}>
            <Row>
              <Col s={12}>
                <div className="inner-shadow-top-box" />
              </Col>
            </Row>
            <Row>
              <Col s={6}>
                <Input
                  s={12}
                  type="text"
                  label={caricaRicopertaTemplate.testoDomanda}
                  onBlur={({ target: { value } }) => updateField(caricaRicopertaTemplate.idDomanda, value)}
                  onChange={({ target: { value } }) => this.setState({ caricaRicoperta: value })}
                  value={this.state.caricaRicoperta}
                />
              </Col>
              <Col s={6}>
                <DatePicker
                  s={12}
                  label={dataInizioCaricaTemplate.testoDomanda}
                  openDirection="down"
                  onDateChange={value => updateField(dataInizioCaricaTemplate.idDomanda, value)}
                  isOutsideRange={(date) => date.isAfter(moment())}
                />
              </Col>
            </Row>
            <Row>
              <Col s={12}>
                <Input
                  s={12}
                  type="text"
                  label={fonteRedditoTemplate.testoDomanda}
                  onBlur={({ target: { value } }) => updateField(fonteRedditoTemplate.idDomanda, value)}
                  onChange={({ target: { value } }) => this.setState({ fonteReddito: value })}
                  value={this.state.fonteReddito}
                />
              </Col>
            </Row>
            <Row className={hideClassPep}>
              <Col s={12}>
                <p>{RadioPepPerche.testoDomanda}</p>
                <form action="#" className="radio-vertical">
                  {RadioPepPercheOptions}
                </form>
              </Col>
            </Row>
            <Row className={hideClassPepRapporti}>
              <Col s={12}>
                <p>{RadioPepRapportiPerche.testoDomanda}</p>
                <form action="#" className="radio-vertical">
                  {RadioPepRapportiPercheOptions}
                </form>
              </Col>
            </Row>
            <Row>
              <Col s={12}>
                <p>{RadioPepReddito.testoDomanda}</p>
                <form action="#" className="radio-vertical">
                  {RadioPepRedditoOptions}
                </form>
              </Col>
            </Row>
            <WrappedYesNoQuestionAntiriciclaggio
              field={fields.YES_NO_PEP_PARTECIPAZIONI_SOC}
              {...this.props.sectionContext}
            />
            <WrappedYesNoQuestionAntiriciclaggio
              field={fields.YES_NO_PEP_PROCEDIMENTI}
              {...this.props.sectionContext}
            />
  
            <Row>
              <Col s={12}>
                <div className="inner-shadow-bottom-box" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }
}

SoggettoPEP.propTypes = {
...fieldPropTypes,
altroField: PropTypes.object
}

export default withFieldCatch(SoggettoPEP)