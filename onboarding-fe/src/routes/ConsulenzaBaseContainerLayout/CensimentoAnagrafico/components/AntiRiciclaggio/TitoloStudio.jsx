import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
    mapDominiToOptions,
    fieldPropTypes,
    getFieldValue,
    getDominio,
    updateField
  } from '../../../utils'

/**
 * 
 * Select sul titolo di studio
 * Mappa in option la lista di risposte ottenute tramite getDominio sul DOM_TITOLO_STUDIO
 */
const TitoloStudio = ({ listaDomande, field, domini, getFieldValue, updateField }) => {
    const titoloStudioTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
    const titoloStudioOptions = mapDominiToOptions(getDominio(domini, 'DOM_TITOLO_STUDIO'), true)
  
    return (
      <Row>
        <Col s={6}>
          <Input
            s={12}
            type="select"
            label={titoloStudioTemplate.testoDomanda}
            defaultValue={-1}
            onChange={({ target: {
              value
            } }) => updateField(titoloStudioTemplate.idDomanda, value)}
            value={getFieldValue(titoloStudioTemplate.idDomanda)}>
            {titoloStudioOptions}
          </Input>
        </Col>
      </Row>
    )
}
  
TitoloStudio.propTypes = fieldPropTypes

export default withFieldCatch(TitoloStudio)