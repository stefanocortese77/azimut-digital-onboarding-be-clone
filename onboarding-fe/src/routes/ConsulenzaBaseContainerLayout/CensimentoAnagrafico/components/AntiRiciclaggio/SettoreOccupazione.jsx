import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import {
    mapDominiToOptions,
    fieldPropTypes,
    getFieldValue,
    getDominio,
    updateField
  } from '../../../utils'

/**
 * 
 * Select sul settore di occupazione
 * Select sul settore di occupazione alternativa, in caso sia 'altro'
 * Mappa in option la lista di risposte ottenute tramite getDominio sul DOM_SETTORE_OCCUPAZIONE_I
 * La seconda select si abilita se il flag enableAltro dello stato del componente è true
 */
class SettoreOccupazione extends React.Component {
    constructor (props) {
      super(props)
      this.state = {
        enableAltro: false,
        settoreOccupazioneIdDomanda: props.listaDomande.find(c => c.idDomanda === props.field.templateId).idDomanda,
        dominioSettoreOccupazione: getDominio(this.props.domini, 'DOM_SETTORE_OCCUPAZIONE_I')
      }
  
      this.handleSettoreOccupazioneChange = this.handleSettoreOccupazioneChange.bind(this)
    }
  
    handleSettoreOccupazioneChange (event) {
      const value = event.target.value
      this.props.updateField(this.state.settoreOccupazioneIdDomanda, value)
  
      if (this.state.dominioSettoreOccupazione[value] === 'Altro') {
        this.setState({ enableAltro: true })
      } else {
        this.setState({ enableAltro: false })
      }
    }
  
    render () {
      const {
        listaDomande,
        field,
        getFieldValue,
        updateField
      } = this.props
  
      const settoreOccupazioneTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
      const settoreOccupazioneOptions = mapDominiToOptions(this.state.dominioSettoreOccupazione, true)
  
      let settoreOccupazioneAltroOptions = []
  
      // TODO: remove this...
      const settoreOccupazioneAltroTemplate = {
        idDomanda: 1046,
        testoDomanda: 'Altro*'
      }
  
      if (this.state.enableAltro) {
        settoreOccupazioneAltroOptions = [
          <option key={-1} value="">{'-'}</option>,
          <option key={1001} value={1001}>{'Investimento in binary options'}</option>,
          <option key={1002} value={1002}>{'Investimento in diamanti'}</option>
        ]
      }
      // ...and uncomment this:
      // const settoreOccupazioneTemplate = listaDomande.find(c => c.idDomanda === altroField.templateId)
      // if (this.state.enableAltro) {
      //   settoreOccupazioneAltroOptions = mapDominiToOptions(getDominio(domini, 'DOM_CODICE_SETTOR'))
      // }
  
  
      return (
        <Row>
          <Col s={6} key={1}>
            <Input
              s={12}
              type="select"
              defaultValue={-1}
              label={settoreOccupazioneTemplate.testoDomanda}
              onChange={this.handleSettoreOccupazioneChange}
              value={getFieldValue(settoreOccupazioneTemplate.idDomanda)}
            >
              {settoreOccupazioneOptions}
            </Input>
          </Col>
          <Col s={6} key={2}>
            <Input
              s={12}
              type="select"
              defaultValue={-1}
              label={settoreOccupazioneAltroTemplate.testoDomanda}
              onChange={({ target: { value } }) => updateField(settoreOccupazioneAltroTemplate.idDomanda, value)}
              value={getFieldValue(settoreOccupazioneAltroTemplate.idDomanda)}
              disabled={!this.state.enableAltro}
            >
              {settoreOccupazioneAltroOptions}
            </Input>
          </Col>
        </Row>
      )
    }
}

SettoreOccupazione.propTypes = {
...fieldPropTypes,
altroField: PropTypes.object
}

export default withFieldCatch(SettoreOccupazione)