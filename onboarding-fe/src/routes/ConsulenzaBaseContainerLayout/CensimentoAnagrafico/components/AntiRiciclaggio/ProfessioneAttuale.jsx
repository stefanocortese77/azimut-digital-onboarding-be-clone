import React from 'react'
import { PropTypes } from 'prop-types'
import { Row, Col, Input } from 'react-materialize'
import withFieldCatch from '../../../../../hocs/withFieldCatch/withFieldCatch'
import Autocomplete from '../../../../../components/Autocomplete/Autocomplete'
import {
    mapDominiToOptions,
    mapDominiToAutocomplete,
    fieldPropTypes,
    getFieldValue,
    getDominio,
    updateField
} from '../../../utils'

const fakeProfessioni = {
  1003: 'Poeta',
  1004: 'Venditore di ombrelli',
  1006: 'Altro lavoratore/rice in proprio',
  1005: 'Pensionato/a'
}

/**
 * 
 * Select sulla professione attuale
 * Select sulla professione precedente, in caso quella attuale sia 'Pensionato'
 * Autocomplete su Nazione/Provincia/Anno di insediamento in caso la prima select sia 'in proprio'
 * Mappa in option la lista di risposte ottenute tramite getDominio su:
 *  - DOM_PROFESSIONE_ATTUALE
 *  - DOM_PROFESSIONE_PRECEDENTE
 *  - DOM_NAZIONI
 *  - DOM_PROVINCE
 * La seconda select si abilita se il flag enablePrecedente dello stato del componente è true
 * In caso gli autocomplete della Nazione e Provincia andassero in errore, si chiama una callback
 * per settare l'errore del componente padre.
 */
class ProfessioneAttuale extends React.Component {
    constructor (props) {
      super(props)
  
      this.state = {
        enablePrecedente: false,
        enableInProprio: false,
        nazioneInsediamento: '',
        dominioNazioni: getDominio(this.props.domini, 'DOM_NAZIONI'),
        dominioProvince: getDominio(this.props.domini, 'DOM_PROVINCE'),
  
        // TODO: remove this...
        dominioProfessioneAttuale: fakeProfessioni,
        professionePrecedenteIdDomanda: 1010,
        dominioProfessionePrecedente: Object.keys(fakeProfessioni).reduce((res, cur) => {
          if (cur !== '1005') {
          return { ...res, [cur]: fakeProfessioni[cur] };
          } else return res;
          }, {}),
        // TODO: ...and uncomment this:
        // dominioProfessioneAttuale: getDominio(this.props.domini, 'DOM_PROFESSIONE_ATTUALE'),
        // professionePrecedenteIdDomanda: props.listaDomande.find(c => c.idDomanda === props.fieldPrecedente.templateId).idDomanda,
        // dominioProfessionePrecedente: getDominio(this.props.domini, 'DOM_PROFESSIONE_PRECEDENTE'),
      }
      
      this.handleProfessioneAttualeChange = this.handleProfessioneAttualeChange.bind(this)
      this.handleProfessionePrecedenteChange = this.handleProfessionePrecedenteChange.bind(this)
      this.handleNazioneChange = this.handleNazioneChange.bind(this)
      this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    }
  
    handleProfessioneAttualeChange (event) {
      const value = event.target.value
      const { listaDomande, field, updateField } = this.props
      updateField(listaDomande.find(c => c.idDomanda === field.templateId).idDomanda, value)
  
      if (this.state.dominioProfessioneAttuale[value].indexOf('Pensionato') === 0) {
        this.setState({
          enablePrecedente: true,
          enableInProprio: false,
        })
      } else if (this.state.dominioProfessioneAttuale[value].indexOf('in proprio') > 0) {
        this.setState({
          enablePrecedente: false,
          enableInProprio: true,
        })
      } else {
        this.setState({
          enablePrecedente: false,
          enableInProprio: false,
          nazioneInsediamento: '',
          provinciaInsediamento: '',
          annoInizioTemplate: ''
        })
      }
    }
  
    handleProfessionePrecedenteChange (event) {
      // TODO: rimuovere this.state.professionePrecedenteIdDomande e usare quella delle props
      const { updateField } = this.props
      updateField(this.state.professionePrecedenteIdDomanda, event.target.value)
    }
  
    handleNazioneChange (value, suggestion) {
      const {
        updateField,
        listaDomande,
        nazioneInsediamento,
        provinciaInsediamento,
        setAutocompleteError } = this.props
      if (suggestion) {
        this.setState({ nazioneInsediamento: suggestion.key })
        setAutocompleteError('nazione', false)
        updateField(listaDomande.find(c => c.idDomanda === nazioneInsediamento.templateId).idDomanda, value)
      } else {
        setAutocompleteError('nazione', true)
        this.setState({ nazioneInsediamento: value, provinciaInsediamento: '' })
        if (this.state.provinciaInsediamento) {
          updateField(listaDomande.find(c => c.idDomanda === provinciaInsediamento.templateId).idDomanda, '')
        }
      }
    }
  
    handleProvinciaChange (value, suggestion) {
      const { updateField, listaDomande, provinciaInsediamento, setAutocompleteError } = this.props
      if (suggestion) {
        this.setState({ provinciaInsediamento: suggestion.key })
        setAutocompleteError('provincia', false)
        updateField(listaDomande.find(c => c.idDomanda === provinciaInsediamento.templateId).idDomanda, value)
      } else {
        setAutocompleteError('provincia', true)
        this.setState({ provinciaInsediamento: value })
      }
    }
  
    render () {
      const {
        listaDomande,
        field,
        professionePrecedente,
        nazioneInsediamento,
        provinciaInsediamento,
        annoInizio,
        getFieldValue,
        updateField
      } = this.props
  
      const professioneAttualeTemplate = listaDomande.find(c => c.idDomanda === field.templateId)
      // TODO:
      const professionePrecedenteTemplate = {
        idDomanda: 1050,
        testoDomanda: 'Professione precedente'
      }
      // const professionePrecedenteTemplate = listaDomande.find(d => d.idDomanda === professionePrecedente.templateId)
      const nazioneInsediamentoTemplate = listaDomande.find(d => d.idDomanda === nazioneInsediamento.templateId)
      const provinciaInsediamentoTemplate = listaDomande.find(d => d.idDomanda === provinciaInsediamento.templateId)
      const annoInizioTemplate = listaDomande.find(d => d.idDomanda === annoInizio.templateId)
  
      return (
        <div>
          <Row>
            <Col s={6} key={1}>
              <Input
                s={12}
                type="select"
                defaultValue={-1}
                label={professioneAttualeTemplate.testoDomanda}
                onChange={this.handleProfessioneAttualeChange}
                value={getFieldValue(professioneAttualeTemplate.idDomanda)}
              >
                {mapDominiToOptions(this.state.dominioProfessioneAttuale, true)}
              </Input>
            </Col>
            <Col s={6} key={2}>
              <Input
                s={12}
                type="select"
                defaultValue={-1}
                label={professionePrecedenteTemplate.testoDomanda}
                onChange={this.handleProfessionePrecedenteChange}
                value={getFieldValue(this.state.professionePrecedenteIdDomanda)}
                disabled={!this.state.enablePrecedente}
              >
                {this.state.enablePrecedente ? mapDominiToOptions(this.state.dominioProfessionePrecedente, true) : []}
              </Input>
            </Col>
          </Row>
          <Row>
            <Col s={6}>
              <Autocomplete
                id="nazione-insediamento"
                s={12}
                label={nazioneInsediamentoTemplate.testoDomanda}
                suggestions={mapDominiToAutocomplete(this.state.dominioNazioni)}
                onChange={this.handleNazioneChange}
                value={this.state.nazioneInsediamento}
                disabled={!this.state.enableInProprio}
              />
            </Col>
            <Col s={3}>
              <Autocomplete
                id="provincia-insediamento"
                s={12}
                label={provinciaInsediamentoTemplate.testoDomanda}
                suggestions={mapDominiToAutocomplete(this.state.dominioProvince)}
                onChange={this.handleProvinciaChange}
                value={this.state.provinciaInsediamento}
                disabled={!this.state.enableInProprio || this.state.nazioneInsediamento !== 'Italia'}
              />
            </Col>
            <Col s={3}>
              <Input
                s={12}
                type="number"
                label={annoInizioTemplate.testoDomanda}
                onBlur={({ target: { value } }) => updateField(annoInizioTemplate.idDomanda, value)}
                onChange={({ target: { value } }) => this.setState({ annoInizioTemplate: value })}
                value={this.state.annoInizioTemplate}
                disabled={!this.state.enableInProprio}
              />
            </Col>
          </Row>
        </div>
      )
    }
}

ProfessioneAttuale.propTypes = {
...fieldPropTypes,
altroField: PropTypes.object
}

export default withFieldCatch(ProfessioneAttuale)