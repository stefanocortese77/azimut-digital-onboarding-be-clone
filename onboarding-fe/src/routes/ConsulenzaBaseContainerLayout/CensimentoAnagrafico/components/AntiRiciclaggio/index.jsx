import React from 'react'
import { PropTypes } from 'prop-types'
import moment from 'moment'

import fields from '../../fields'
import {
  getFieldValue,
  updateField,
  loadQuestions,
  filterQuestions
} from '../../../utils'
import { Row, Col, Button } from 'react-materialize'
import { CollapsibleItem } from '../../../../../components/Collapsible'

import WrappedTitoloStudio from './TitoloStudio'
import WrappedNaturaRapporto from './NaturaRapporto'
import WrappedSettoreOccupazione from './SettoreOccupazione'
import WrappedProfessioneAttuale from './ProfessioneAttuale'
import WrappedYesNoQuestionAntiriciclaggio from './YesNoQuestionAntiriciclaggio'
import WrappedSoggettoPEP from './SoggettoPEP'


/**
 * Ciascun sottocomponente corrisponde ad uno o più campi.
 */
class AntiRiciclaggio extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      errors: {},
      saving: false
    }, loadQuestions(this.props.previousForm, this.props.template.listaDomande))

    this.updateField = this.updateField.bind(this)
    this.getFieldValue = this.getFieldValue.bind(this)
    this.setAutocompleteError = this.setAutocompleteError.bind(this)
    this.save = this.save.bind(this)
  }

  updateField (fieldId, idDomanda, value) {
    const updateObj = {}
    const domanda = {}
    domanda[idDomanda] = value
    updateObj[`field-${fieldId}`] = Object.assign(
      {},
      this.state[`field-${fieldId}`],
      domanda
    )
    this.setState(updateObj)
  }

  getFieldValue (fieldId, idDomanda) {
    if (this.state[`field-${fieldId}`]) {
      if (
        idDomanda &&
        this.state[`field-${fieldId}`][idDomanda] !== undefined
      ) {
        return this.state[`field-${fieldId}`][idDomanda]
      }
    }

    return undefined
  }

  setAutocompleteError (id, error) {
    this.setState({ errors: {
      ...this.state.errors,
      [id]: error
    }})
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      this.props.onSave(filterQuestions(this.state))
    }
  }


  render () {
    const { template, domini } = this.props
    const { listaDomande } = template

    const sectionContext = {
      listaDomande: listaDomande,
      domini: domini,
      getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
      updateField: (idDomanda, value) => {
        this.setState(updateField(idDomanda, value))
        this.props.setDataPending()
      }
    }

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }

    return (
      <CollapsibleItem
        header="Dati antiriciclaggio"
        icon="keyboard_arrow_down"
        itemKey={this.props.itemKey}
        labelInfo={accordionInfo}
      >
        <Row>
          <Col s={6}>
            <p>Il soggetto dichiara di essere un titolare effettivo</p>
          </Col>
        </Row>
        <WrappedTitoloStudio
          field={fields.TITOLO_STUDIO}
          {...sectionContext}
        />

        <WrappedNaturaRapporto
          field={fields.NATURA_RAPPORTO}
          {...sectionContext}
        />

        <WrappedSettoreOccupazione
          field={fields.SETTORE_OCCUPAZIONE}
          {...sectionContext}
        />

        <WrappedProfessioneAttuale
          field={fields.PROFESSIONE_ATTUALE}
          professionePrecedente={fields.PROFESSIONE_PRECEDENTE}
          nazioneInsediamento={fields.NAZIONE_INSEDIAMENTO}
          provinciaInsediamento={fields.PROVINCIA_ATTIVITA}
          annoInizio={fields.ANNO_INIZIO}
          setAutocompleteError={this.setAutocompleteError}
          {...sectionContext}
        />

        <Row>
          <WrappedSoggettoPEP
            field={fields.RADIO_PEP}
            {...sectionContext}
            sectionContext={sectionContext}
          />
          <WrappedYesNoQuestionAntiriciclaggio
            field={fields.YES_NO_CARICHE_POLITICHE}
            {...sectionContext}
          />
          <WrappedYesNoQuestionAntiriciclaggio
            field={fields.YES_NO_CARICHE_EXTRA}
            {...sectionContext}
          />
          <WrappedYesNoQuestionAntiriciclaggio
            field={fields.YES_NO_CARICHE_PUBBLICHE}
            {...sectionContext}
          />
          <WrappedYesNoQuestionAntiriciclaggio
            field={fields.YES_NO_FONDI_PUBBLICI}
            {...sectionContext}
          />
        </Row>

        <Row className="right-align submit-collapsible-item">
          <Button
            flat
            waves="light"
            disabled={Object.keys(this.state.errors).some(el => this.state.errors[el])}
            onClick={this.save}>
            Salva
          </Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

AntiRiciclaggio.propTypes = {
  templateAml: PropTypes.array,
  listaDomande: PropTypes.object,
  onSave: PropTypes.func,
  domini: PropTypes.object,
  itemKey: PropTypes.string,
  setDataPending: PropTypes.func,
  template: PropTypes.object,
  previousForm: PropTypes.object,
  labelInfo: PropTypes.func
}

export default AntiRiciclaggio
