
import AcquisizioneDocumenti from '../components/AcquisizioneDocumenti'
import { getDataSummaryLight, getDocumentsData, postIdCardDataAllegatiCF, postIdCardDataAllegatiDOC, postIdCardData, postIdCardDataHide } from '../../../../store/modules/customers/actions'
import { setDataPending } from '../../../../store/modules/dataPending/actions'
import { dominio } from '../../../../store/modules/dominio/actions'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente, idIncarico) => dispatch(getDataSummaryLight(idCliente, idIncarico)),
    getDocumentsData : (idCliente, idIncarico, dataIdCardActive, dataIdCardAllegati) => dispatch(getDocumentsData(idCliente, idIncarico, dataIdCardActive, dataIdCardAllegati)),
    postIdCardDataAllegatiCF : (idCliente, queryBody, activeService) => dispatch(postIdCardDataAllegatiCF(idCliente, queryBody, activeService)),
    postIdCardDataAllegatiDOC : (idCliente, queryBody, activeService) => dispatch(postIdCardDataAllegatiDOC(idCliente, queryBody, activeService)),
    postIdCardData : (idCliente, queryBody, activeService) => dispatch(postIdCardData(idCliente, queryBody, activeService)),
    postIdCardDataHide : (idCliente, queryBody, activeService) => dispatch(postIdCardDataHide(idCliente, queryBody, activeService)),
    // postDocumentsScreenshot : (idCliente, idIncarico, query, active) => dispatch(postDocumentsScreenshot(idCliente, idIncarico, query, active)),
    // postDocumentsDataAndFiles: (idCliente, idIncarico, queryFile, queryData, dataActive, allegatiActive) => dispatch(postDocumentsDataAndFiles(idCliente, idIncarico, queryFile, queryData, dataActive, allegatiActive)),
    dominio : (queryParam) => dispatch(dominio(queryParam)),
    setDataPending : (value) => dispatch(setDataPending(value))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    dominioRequest : state.dominio.dominioRequest,
    summaryData: state.customers.summaryData,
    documentsData: state.customers.documentsData,
    getDataSummaryRequest: state.customers.getDataSummaryRequest,
    getDocumentsDataRequest: state.customers.getDocumentsDataRequest,
    // postDocumentsScreenRequest: state.customers.postDocumentsScreenRequest,
    // postDocumentsDataAndFilesRequest: state.customers.postDocumentsDataAndFilesRequest,
    postDataAllegatiCFRequest:state.customers.postDataAllegatiCFRequest,
    postDataAllegatiDOCRequest:state.customers.postDataAllegatiDOCRequest,
    postDataDocumentoRequest:state.customers.postDataDocumentoRequest,
    postDataDocumentoHideRequest:state.customers.postDataDocumentoHideRequest,

    domini: state.dominio.domini,
    dataPendingState: state.dataPendings.dataPendingState
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AcquisizioneDocumenti)
