import React from 'react'
import { PropTypes } from 'prop-types'
import { Link } from 'react-router-dom'
import { Row, Col, Input, Button } from 'react-materialize'
import moment from 'moment'

import { Collapsible, CollapsibleItem } from '../../../../components/Collapsible'
import ModalePopup from '../../../../components/ModalePopup'
import dataSB from '../../../../static-data/data-servizio-base.json'
import InputImage from '../../../../components/InputImage'
// import AutocompleteCustom from '../../../../components/AutocompleteCustom'
import Autocomplete from '../../../../components/Autocomplete/Autocomplete'
import DatePicker from '../../../../components/DatePicker/DatePicker'
import { getDominio, mapDominiToOptions, mapDominiToAutocomplete, getKeyDominioFromValue, checkStatusService } from '../../utils'

export class AcquisizioneDocumentiWrapper extends React.Component {
  componentDidMount () {
    this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
    // this.props.getDocumentsData(this.props.customer.idCliente, this.props.customer.idIncarico)

    const dominioRequest = {
      'listaDomini': [
        {
          'idDominio': 'DOM_NAZIONE_RILASCIO'
        }, {
          'idDominio': 'DOM_TIPO_DOCUMENTO'
        }
      ]
    }
    this.props.dominio(dominioRequest)
  }

  componentDidUpdate (prevProps) {
    // if(this.props.summaryData !== null){
    if (prevProps.getDataSummaryRequest.fetching === true && this.props.getDataSummaryRequest.fetching === false && this.props.getDataSummaryRequest.error === null) {
      if (this.props.summaryData.statusEndPoint) {
        const dataIdCardActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardData')
        // const dataIdCardAllegatiActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardAllegati')

        this.props.getDocumentsData(this.props.customer.idCliente, this.props.customer.idIncarico, dataIdCardActive)
      }
    }
  }

  render () {
    if (Object.keys(this.props.domini).length === 0) { return null }

    if (this.props.documentsData === null) { return null }

    return (
      <AcquisizioneDocumenti {...this.props} />
    )
  }
}

class AcquisizioneDocumenti extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      documentsDataPrevius: props.documentsData, // oggetto per avere lo stato precedente
      documentsData: props.documentsData, // oggetto per il salvataggio delle info documento
      listaAllegati: [], // oggetto per il salvataggio degli allegati,
      listaAllegatiCF: [], // oggetto per il salvataggio degli allegati CF
      listaAllegatiDocumento: [], // oggetto per il salvataggio degli allegati Documento identità

      nazioneValue:null,
      provinciaValue:null,
      comuneValue:null,

      accordionDataPending0: null,
      accordionDataPending1: null,
      openCollapsible: 'codice-fiscale',
      nextPath: '/consulenza-base/censimento-anagrafico',
      showModal: false,
      prov_disable: (this.props.documentsData.nazioneRilascio) ? (this.props.documentsData.nazioneRilascio.chiave === 0) : true,
      comuni_disable: (this.props.documentsData.provinciaRilascio) ? (this.props.documentsData.nazioneRilascio.chiave === 0) : true,
      tipoDocumento: '1'
    }

    this.dominioCustomRequest = this.dominioCustomRequest.bind(this)
    this.updateAllegati = this.updateAllegati.bind(this)
    this.updateFields = this.updateFields.bind(this)
    this.nextStep = this.nextStep.bind(this)
    this.nextStepNoSave = this.nextStepNoSave.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.checkAllegatoEsistente = this.checkAllegatoEsistente.bind(this)
    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)
    this.showAccordionInfo = this.showAccordionInfo.bind(this)
    this.handleNazioneChange = this.handleNazioneChange.bind(this)
    this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    this.handleComuneChange = this.handleComuneChange.bind(this)
    this.getDominioMatchLabel = this.getDominioMatchLabel.bind(this)
    this.handleTipologia = this.handleTipologia.bind(this)

    this.saveAllegatiDOC = this.saveAllegatiDOC.bind(this)
    this.saveAllegatiCF = this.saveAllegatiCF.bind(this)
    this.saveDataDocumentoPrevious = this.saveDataDocumentoPrevious.bind(this)
    this.saveDataDocumento = this.saveDataDocumento.bind(this)
    this.saveSecondAccordion = this.saveSecondAccordion.bind(this)
  }

  getDominioMatchLabel (objectKey) {
    let dominio = getDominio(this.props.domini, objectKey.codiceDominio)
    for (var key in dominio) {
      if (dominio.hasOwnProperty(key)) {
        if (key === objectKey.chiave.toString()) {
          return dominio[key]
        }
      }
    }
  }

  checkAllegatoEsistente (tipologiaAllegato) {
    // ESEMPIO call method: fileIn={this.checkAllegatoEsistente('CF_FRONTE')}
    let imgAllegato = ''

    this.state.listaAllegatiCF.forEach(function (allegato) {
      if (allegato.tipoAllegato === tipologiaAllegato) {
        imgAllegato = allegato.file
      }
    })

    this.state.listaAllegatiDocumento.forEach(function (allegato) {
      if (allegato.tipoAllegato === tipologiaAllegato) {
        imgAllegato = allegato.file
      }
    })

    return imgAllegato
  }

  componentDidUpdate (prevProps, prevState) {
    // SE IL SALVATAGGIO DELLE IMMAGINI CF VA IN SUCCESS ALLORA RICHIAMA savIdCardData PER I SALVARE I FLAG IMM
    if (prevProps.postDataAllegatiCFRequest.fetching && this.props.postDataAllegatiCFRequest.fetching === false && this.props.postDataAllegatiCFRequest.error == null) {
      let newFlags = {}
      if (this.props.postDataAllegatiCFRequest.dataSuccessFlags.flagCFFronte) newFlags.flagCFFronte = this.props.postDataAllegatiCFRequest.dataSuccessFlags.flagCFFronte
      if (this.props.postDataAllegatiCFRequest.dataSuccessFlags.flagCFRetro) newFlags.flagCFRetro = this.props.postDataAllegatiCFRequest.dataSuccessFlags.flagCFRetro

      this.saveDataDocumentoPrevious(newFlags)
    }

    // SE IL SALVATAGGIO NASCOSTO DEL PRIMO ACCORDION VA IN SUCCESS ALLORA APRE IL SECONDO
    if (prevProps.postDataDocumentoHideRequest.fetching && this.props.postDataDocumentoHideRequest.fetching === false && this.props.postDataDocumentoHideRequest.error == null) {
      let newFlags = {}
      newFlags.flagCFFronte = prevProps.postDataDocumentoHideRequest.dataPreviousPosted.flagCFFronte
      newFlags.flagCFRetro = prevProps.postDataDocumentoHideRequest.dataPreviousPosted.flagCFRetro
      newFlags.flagDOCFronte = prevProps.postDataDocumentoHideRequest.dataPreviousPosted.flagDOCFronte
      newFlags.flagDOCRetro = prevProps.postDataDocumentoHideRequest.dataPreviousPosted.flagDOCRetro

      let docDataFlags = Object.assign({}, this.state.documentsData, newFlags)
      // SETTA documentsDataPrevius CON I DATI APPENA SALVATI
      // SETTA documentsData con I DATI DELLO STATE Aggiornati solo con i flag - oggetto docDataFlags
      // eslint-disable-next-line
      this.setState({
        openCollapsible: 'documento-identita',
        accordionDataPending0: false,
        documentsDataPrevius:this.props.postDataDocumentoHideRequest.dataPreviousPosted,
        documentsData:docDataFlags
      })
      this.props.setDataPending(false)
    }

    // SE IL SALVATAGGIO DELLE IMMAGINI DOC VA IN SUCCESS ALLORA RICHIAMA savIdCardData PER I SALVARE I DATI E I FLAG
    if (prevProps.postDataAllegatiDOCRequest.fetching && this.props.postDataAllegatiDOCRequest.fetching === false && this.props.postDataAllegatiDOCRequest.error == null) {
      let newFlags = {}
      if (this.props.postDataAllegatiDOCRequest.dataSuccessFlags.flagDOCFronte) newFlags.flagCFFronte = this.props.postDataAllegatiDOCRequest.dataSuccessFlags.flagDOCFronte
      if (this.props.postDataAllegatiDOCRequest.dataSuccessFlags.flagDOCRetro) newFlags.flagCFRetro = this.props.postDataAllegatiDOCRequest.dataSuccessFlags.flagDOCRetro

      this.saveDataDocumento(newFlags)
    }

    // SE IL SALVATAGGIO DEL SECONDO ACCORDION VA IN SUCCESS ALLORA PASSA ALLO STEP SUCCESSIVO
    if (prevProps.postDataDocumentoRequest.fetching && this.props.postDataDocumentoRequest.fetching === false && this.props.postDataDocumentoRequest.error == null) {
      this.props.setDataPending(false)
      this.props.history.push('/consulenza-base/censimento-anagrafico')
    }

    // CHECK DATA PENDING NON salvati sui due accordion
    if (prevState.listaAllegatiCF !== null && prevState.listaAllegatiCF !== this.state.listaAllegatiCF) {
      // TODO da inserire la logica per verificare quale allegato è stato modificato
      // eslint-disable-next-line
      this.setState({accordionDataPending0: true})
      this.props.setDataPending(true)
    }

    if (prevState.documentsData !== null && prevState.documentsData !== this.state.documentsData) {
      // eslint-disable-next-line
      this.setState({accordionDataPending1: true})
      this.props.setDataPending(true)
    }

    if (prevState.listaAllegatiDocumento !== null && prevState.listaAllegatiDocumento !== this.state.listaAllegatiDocumento) {
      // eslint-disable-next-line
      this.setState({accordionDataPending1: true})
      this.props.setDataPending(true)
    }
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({ openCollapsible: itemKey })
  }

  updateFields (newPropertiesObj) {
    // Aggiorna le proprietà dell'oggetto documentsData dello state con i valori degli input
    let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)
    this.setState({ documentsData: newDocumentsData })
  }

  // gestisce la tipologia del documento
  handleTipologia (value) {
    let newPropertiesObj = {
      tipoDocumenti:{
        'codiceDominio': 'DOM_TIPO_DOCUMENTO',
        'chiave': value
      }
    }
    let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)
    console.log('value', newDocumentsData)
    this.setState({ documentsData: newDocumentsData,
      tipoDocumento: newDocumentsData.tipoDocumenti.chiave })
    this.setState({})
  }

  /* gestisce l'input con la nazione di rilascio:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleNazioneChange (value, matchSuggestion) {
    let newPropertiesObj = { nazioneRilascio: matchSuggestion }
    let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)

    // sblocco lista province solo per l'Italia
    if (matchSuggestion && matchSuggestion.key === 'Italia') {
      this.setState({ nazioneValue:value, documentsData: newDocumentsData, prov_disable:false })
      this.dominioCustomRequest('DOM_PROVINCE_RILASCIO', 'DOM_NAZIONE_RILASCIO', value)
    } else {
      this.setState({ nazioneValue:value, documentsData: newDocumentsData, prov_disable:true, comuni_disable:true })
    }
  }

  /* gestisce l'input con la provincia di rilascio:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleProvinciaChange (value, matchSuggestion) {
    let newPropertiesObj = { provinciaRilascio: matchSuggestion }
    let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)

    if (matchSuggestion) {
      this.setState({ provinciaValue:value, documentsData: newDocumentsData, comuni_disable:false })
      this.dominioCustomRequest('DOM_COMUNE_RILASCIO', 'DOM_PROVINCE_RILASCIO', value)
    } else {
      this.setState({ provinciaValue:value, documentsData: newDocumentsData, comuni_disable:true })
    }
  }

  /* gestisce l'input con il comune di rilascio:
  * 'value', valore in input
  * 'suggestion', click elemento lista opzioni possibili
  */
  handleComuneChange (value, matchSuggestion) {
    let newPropertiesObj = { comuneRilascio: matchSuggestion }
    let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)

    if (matchSuggestion) {
      this.setState({ comuneValue:value, documentsData: newDocumentsData })
    } else {
      this.setState({ comuneValue:value, documentsData: newDocumentsData })
    }
  }

  updateData (dataProp, newData) {
    // Aggiorna le proprietà dell'oggetto documentsData dello state con i valori degli input
    if (newData && newData !== null) {
      let newPropertiesObj = {}
      newPropertiesObj[dataProp] = newData.format()
      let newDocumentsData = Object.assign({}, this.state.documentsData, newPropertiesObj)
      this.setState({ documentsData: newDocumentsData })
    }
  }

  updateAllegati (newAllegatoObj) {
    let newlistaAllegati = []
    let replaceImage = false

    if (newAllegatoObj.tipoAllegato === 'CF_FRONTE' || newAllegatoObj.tipoAllegato === 'CF_RETRO') {
      this.state.listaAllegatiCF.forEach(function (element) {
        if (element.tipoAllegato === newAllegatoObj.tipoAllegato) {
          replaceImage = true
          newlistaAllegati.push(newAllegatoObj)
        } else {
          newlistaAllegati.push(element)
        }
      })
      // caso iniziale e caso di nuova img
      if (replaceImage === false) { newlistaAllegati.push(newAllegatoObj) }
      this.setState({ listaAllegatiCF: newlistaAllegati })
    } else if (newAllegatoObj.tipoAllegato === 'DOC_FRONTE' || newAllegatoObj.tipoAllegato === 'DOC_RETRO') {
      this.state.listaAllegatiDocumento.forEach(function (element) {
        if (element.tipoAllegato === newAllegatoObj.tipoAllegato) {
          replaceImage = true
          newlistaAllegati.push(newAllegatoObj)
        } else {
          newlistaAllegati.push(element)
        }
      })
      // caso iniziale e caso di nuova img
      if (replaceImage === false) { newlistaAllegati.push(newAllegatoObj) }
      this.setState({ listaAllegatiDocumento: newlistaAllegati })
    }
  }

  dominioCustomRequest (codDominio, codDominioFilter, value) {
    let keyFilter = getKeyDominioFromValue(this.props.domini.listaDomini, codDominioFilter, value)
    const dominioRequest = {
      'listaDomini': [
        {
          'idDominio': codDominio,
          'filtro': {
            'codice': keyFilter
          }
        }
      ]
    }
    this.props.dominio(dominioRequest)
  }

  toggleModal () {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  nextStep (path) {
    if (this.props.dataPendingState === true) {
      this.toggleModal()
      this.setState({ nextPath: path })
    } else {
      this.props.history.push(path)
    }
  }

  nextStepNoSave () {
    this.props.history.push(this.state.nextPath)
  }

  saveAllegatiCF () {
    const allegatiQuery = this.state.listaAllegatiCF
    const queryBody = {
      'idIncarico': this.props.customer.idIncarico,
      'listaAllegati': allegatiQuery
    }
    const dataIdCardAllegatiActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardAllegati')

    this.props.postIdCardDataAllegatiCF(this.props.customer.idCliente, queryBody, dataIdCardAllegatiActive)
  }

  saveAllegatiDOC () {
    const allegatiQuery = this.state.listaAllegatiDocumento
    const queryBody = {
      'idIncarico': this.props.customer.idIncarico,
      'listaAllegati': allegatiQuery
    }
    const dataIdCardAllegatiActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardAllegati')

    this.props.postIdCardDataAllegatiDOC(this.props.customer.idCliente, queryBody, dataIdCardAllegatiActive)
  }

  saveDataDocumentoPrevious (newFlags) {
    // merge di documentsDataPrevius con i flag in arrivo dalla chiamata di postIdCardAllegatiCF
    let queryData = Object.assign({}, this.state.documentsDataPrevius, newFlags)
    const queryComplete = {
      'idIncarico': this.props.customer.idIncarico,
      ...queryData
    }

    const dataIdCardActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardData')
    this.props.postIdCardDataHide(this.props.customer.idCliente, queryComplete, dataIdCardActive)
  }

  saveDataDocumento (newFlags) {
    // merge di documentsDataPrevius con i flag in arrivo dalla chiamata di postIdCardAllegatiCF
    let queryData = Object.assign({}, this.state.documentsData, newFlags)
    const queryComplete = {
      'idIncarico': this.props.customer.idIncarico,
      ...queryData
    }

    const dataIdCardActive = checkStatusService(this.props.summaryData.statusEndPoint, 'idCardData')
    this.props.postIdCardData(this.props.customer.idCliente, queryComplete, dataIdCardActive)
  }

  saveSecondAccordion () {
    // se non c'è nessuna immagine DOC da salavre lancia direttamente il salvataggio dei data
    // TODO Approfondire questo controllo
    if (this.state.listaAllegatiDocumento.length === 0) {
      this.saveDataDocumento()
    } else {
      this.saveAllegatiDOC()
    }
  }

  showAccordionInfo (accordionKey) {
    let accordionDataToCheck = 'accordionDataPending' + accordionKey
    let labelInfoElement = null
    if (this.state[accordionDataToCheck] === true) {
      labelInfoElement = <span className="accordion-info accordion-pending">Dati non salvati</span>
    } else if (this.state[accordionDataToCheck] === false) {
      labelInfoElement = <span className="accordion-info accordion-saved">Dati salvati</span>
    }

    return labelInfoElement
  }

  render () {
    // connteggio errori in pagina per disabilitare bottone 'Salva'
    let errori = 0

    let collapsibleToOpen = this.state.openCollapsible
    if (this.props.location.state) collapsibleToOpen = this.props.location.state.collapsibleToOpen

    let nazioneDefault = this.state.nazioneValue
    let provinciaDefault = this.state.provinciaValue
    let comuneDefault = this.state.comuneValue

    // per il primo mapping
    if (nazioneDefault === null && this.props.documentsData.nazioneRilascio) nazioneDefault = this.getDominioMatchLabel(this.props.documentsData.nazioneRilascio)
    if (provinciaDefault === null && this.props.documentsData.provinciaRilascio) provinciaDefault = this.getDominioMatchLabel(this.props.documentsData.provinciaRilascio)
    if (comuneDefault === null && this.props.documentsData.comuneRilascio) comuneDefault = this.getDominioMatchLabel(this.props.documentsData.comuneRilascio)

    const suggestionData = dataSB.acquisizione.documento_identita.suggestionData
    const torna = dataSB.button_back
    const title = dataSB.title
    const titleSection = dataSB.acquisizione.title_section
    var nazioni = {}
    var province = {}
    var comuni = {}

    // POPOLAMENTO DOMIINI
    nazioni = mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_NAZIONE_RILASCIO'))
    province = mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_PROVINCE_RILASCIO'))
    comuni = mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_COMUNE_RILASCIO'))

    let messsaggioErroreDocumento
    // controllo che sia stato digitato qualcosa
    if (this.state.documentsData.numeroDocumenti &&
      this.state.documentsData.numeroDocumenti.length > 0) {
      // controllo che sia una carta d'identità
      if (this.state.tipoDocumento === '1') {
        const ciRegex = /[a-z]{2}\d{7}/i
        // controllo che sia da 9 caratteri e il formato corretto
        if (this.state.documentsData.numeroDocumenti.length === 9 &&
          ciRegex.test(this.state.documentsData.numeroDocumenti)) {
        } else {
          errori = errori + 1
          messsaggioErroreDocumento = (
            <div className="error-label">Il numero del documento non è valido</div>
          )
        }
      } else if (this.state.tipoDocumento === '2') {
        const patRegex = /[a-z]{2}\d{7}[a-z]/i
        // controllo che sia da 10 caratteri e il formato corretto
        if (this.state.documentsData.numeroDocumenti.length === 10 &&
          patRegex.test(this.state.documentsData.numeroDocumenti)) {
        } else {
          errori = errori + 1
          messsaggioErroreDocumento = (
            <div className="error-label">Il numero del documento non è valido</div>
          )
        }
      } else if (this.state.tipoDocumento === '11') {
        console.log('LIBRETTO PENSIONE')
      }
    }


    return (
      <section className="main-body">
        <ModalePopup visible={this.state.showModal} title="Attenzione!" text="Le modifiche non sono state salvate. Se prosegui perderai i dati inseriti" primaryActionLabel="Continua" secondaryActionLabel="Annulla" secondaryAction={this.toggleModal} primaryAction={this.nextStepNoSave} />
        <header>
          <Link onClick={() => this.nextStep('/')} to="#" className="torna-pratiche"><i className="icon-arrow-back" />{torna}</Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>
        <article className="page-left-content">
          <div className="background_pattern" />
          <h2 className="title-sezione">{titleSection}</h2>
          <Collapsible open={collapsibleToOpen} onChange={this.handleOpenCollapsibleChange}>
            <CollapsibleItem itemKey="codice-fiscale" header="Codice fiscale" icon="keyboard_arrow_down" labelInfo={this.showAccordionInfo(0)}>
              <Row>
                <Col s={4} offset={'s1'}>
                  <InputImage idImg="imgCFRONTE" label="FRONTE documento*" fileIn={this.checkAllegatoEsistente('CF_FRONTE')} onChange={(fileupload, fileThumb) => this.updateAllegati({ file: fileupload, fileThumb: fileThumb, tipoAllegato: 'CF_FRONTE' })} />
                </Col>
                <Col s={4} offset={'s2'}>
                  <InputImage idImg="imgCFRETRO" label="RETRO documento*" fileIn={this.checkAllegatoEsistente('CF_RETRO')} onChange={(fileupload, fileThumb) => this.updateAllegati({ file: fileupload, fileThumb: fileThumb, tipoAllegato: 'CF_RETRO' })} />
                </Col>
              </Row>
              <Row className="right-align">
                <Button flat={true} waves="light" disabled={this.state.accordionDataPending0 === false || this.state.accordionDataPending0 === null} onClick={this.saveAllegatiCF}>Salva</Button>
              </Row>
            </CollapsibleItem>
            <CollapsibleItem header="Documenti d'identità" itemKey="documento-identita" icon="keyboard_arrow_down" labelInfo={this.showAccordionInfo(1)}>
              <Row>
                <Col s={6}>
                  <Input s={12} type="select" label="Tipologia documento *" onChange={(e, value) => this.handleTipologia(value)} defaultValue={this.state.documentsData.tipoDocumenti.chiave}>
                    {mapDominiToOptions(getDominio(this.props.domini, 'DOM_TIPO_DOCUMENTO'))}
                  </Input>
                </Col>
                <Col s={6}>
                  <Input s={12} label="Numero documento *" key="value_input" value={this.state.documentsData.numeroDocumenti} onChange={({ target: {
                      value
                    } }) => this.updateFields({ numeroDocumenti: value })} />
                  {messsaggioErroreDocumento}
                </Col>
              </Row>
              <Row>
                <Col s={6}>
                  <Autocomplete id="nazione-rilascio"
                    label="Nazione di rilascio *"
                    suggestions={nazioni}
                    value={nazioneDefault}
                    onChange={this.handleNazioneChange}
                    threshold={1}
                  />
                </Col>
                <Col s={6}>
                  <Autocomplete id="provincia-rilascio"
                    label="Provincia di rilascio *"
                    suggestions={province}
                    value={provinciaDefault}
                    onChange={this.handleProvinciaChange}
                    threshold={1}
                    disabled={this.state.prov_disable}
                  />
                </Col>
              </Row>
              <Row>
                <Col s={6}>
                  <Autocomplete id="comune-rilascio"
                    label="Comune di rilascio *"
                    suggestions={comuni}
                    value={comuneDefault}
                    onChange={this.handleComuneChange}
                    threshold={1}
                    disabled={this.state.comuni_disable}
                  />
                </Col>
              </Row>
              <Row>
                <Col s={3}>
                  <InputImage s={12} idImg="imgDOCFRONTE" label="FRONTE documento*" onChange={(fileupload, fileThumb) => this.updateAllegati({ file: fileupload, fileThumb: fileThumb, tipoAllegato: 'DOC_FRONTE' })} />
                </Col>
                <Col s={3}>
                  <InputImage s={12} idImg="imgDOCRETRO" label="RETRO documento*" onChange={(fileupload, fileThumb) => this.updateAllegati({ file: fileupload, fileThumb: fileThumb, tipoAllegato: 'DOC_RETRO' })} />
                </Col>
                <Col s={3}>
                  <DatePicker label="Data rilascio*" placeholder={moment(this.state.documentsData.dataRilascio).format('DD/MM/YYYY')} openDirection="up" dataDefault={moment(this.state.documentsData.dataRilascio)} onDateChange={(date) => this.updateData('dataRilascio', date)} />
                  <span className="suggestion-for-input">{suggestionData}</span>
                </Col>
                <Col s={3}>
                  <DatePicker label="Data scadenza*" placeholder={moment(this.state.documentsData.dataScadenza).format('DD/MM/YYYY')} openDirection="up" anchorDirection="right" dataDefault={moment(this.state.documentsData.dataScadenza)} onDateChange={(date) => this.updateData('dataScadenza', date)} />
                  <span className="suggestion-for-input">{suggestionData}</span>
                </Col>

              </Row>
              <Row className="right-align">
                <Button flat={true} waves="light" disabled={(this.state.accordionDataPending1 === false || this.state.accordionDataPending1 === null) || (errori > 0)} onClick={this.saveSecondAccordion}>Salva</Button>
              </Row>
            </CollapsibleItem>
          </Collapsible>
          <Row className="right-align padding-r-20 margin-t-20">
            <Link onClick={() => this.nextStep('/consulenza-base/censimento-anagrafico')} to="#">{dataSB.link_continue}
              <i className="icon-arrow" /></Link>
          </Row>
        </article>
      </section>)
  }
}

AcquisizioneDocumenti.propTypes = {
  customer: PropTypes.object,
  dominio: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  history: PropTypes.object,
  location: PropTypes.object,
  documentsData: PropTypes.object,
  domini: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  setDataPending: PropTypes.func,
  dataPendingState: PropTypes.bool,
  summaryData:PropTypes.object,
  postIdCardData: PropTypes.func,
  postIdCardDataHide: PropTypes.func,
  postIdCardDataAllegatiCF: PropTypes.func,
  postIdCardDataAllegatiDOC: PropTypes.func,
  postDataDocumentoRequest: PropTypes.object,
  postDataDocumentoHideRequest: PropTypes.object,
  postDataAllegatiCFRequest: PropTypes.object,
  postDataAllegatiDOCRequest: PropTypes.object
}

AcquisizioneDocumentiWrapper.propTypes = {
  getDocumentsData: PropTypes.func,
  getDataSummaryLight: PropTypes.func,
  customer: PropTypes.object,
  dominio: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  history: PropTypes.object,
  documentsData: PropTypes.object,
  dominioRequest: PropTypes.object,
  domini: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  setDataPending: PropTypes.func,
  dataPendingState: PropTypes.bool,
  getDataSummaryRequest: PropTypes.object,
  summaryData:PropTypes.object
}

export default (AcquisizioneDocumentiWrapper)
