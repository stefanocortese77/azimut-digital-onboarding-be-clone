import React from 'react'
import { PropTypes } from 'prop-types'
import { Link } from 'react-router-dom'
import { Input, Button, Col, Row } from 'react-materialize'
import InputImage from '../../../../components/InputImage'
import ModalePopup from '../../../../components/ModalePopup'
import dataSB from '../../../../static-data/data-servizio-base.json'
import { checkStatusService } from '../../utils'

export class Sottoscrizione extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      serviziData: null,

      salvataggio:false, // per inibire il redirect in caso di percentuale al 99
      showModal:false,
      formready:false
    }

    this.handleRadioTipologiaChange = this.handleRadioTipologiaChange.bind(this)
    this.sendServiziData = this.sendServiziData.bind(this)
    this.updateFields = this.updateFields.bind(this)
    this.updateImg = this.updateImg.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.nextStep = this.nextStep.bind(this)
    this.nextStepNoSave = this.nextStepNoSave.bind(this)
  }

  componentDidMount () {
    this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
    // this.props.getServiziDigitali(this.props.customer.idCliente, this.props.customer.idIncarico)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.getDataSummaryRequest.fetching === true && this.props.getDataSummaryRequest.fetching === false && this.props.getDataSummaryRequest.error === null) {
      // SE TUTTI I DATI SONO COMPLETI AL 99% && E' STATO CLICCATO IL BOTTONE SALVA, ALLORA PASSA AL RIEPILOGO
      if (this.props.summaryData.percentuale === 99 && this.state.salvataggio === true) {
        this.props.history.push('/consulenza-base/riepilogo-cliente')
      }
      if (this.props.summaryData.statusEndPoint) {
        const servDigActive = checkStatusService(this.props.summaryData.statusEndPoint, 'servdig')
        this.props.getServiziDigitali(this.props.customer.idCliente, this.props.customer.idIncarico, servDigActive)
      }
    }

    // check se sono terminate le chiamate per popolare i campi, setta i default dei campi
    if (prevProps.getServiziDigitaliRequest.fetching === true && this.props.getServiziDigitaliRequest.fetching === false && this.props.getServiziDigitaliRequest.error === null) {
      // eslint-disable-next-line
      this.setState({ serviziData:this.props.serviziData, formready: true })
    }

    // CONTROLLO AL SUCCESS DEL SALVATAGGIO DATI
    if (prevProps.postServiziDigitaliRequest.fetching &&
      this.props.postServiziDigitaliRequest.fetching === false &&
      this.props.postServiziDigitaliRequest.error == null) {
      this.props.getDataSummaryLight(this.props.customer.idCliente, this.props.customer.idIncarico)
      this.props.setDataPending(false)
    }

    // CONTROLLO AL SUCCESS DEL DATA_SUMMARY (Aside)
    if (prevProps.getDataSummaryRequest.fetching && this.props.getDataSummaryRequest.fetching === false &&
      this.props.getDataSummaryRequest.error === null) {
      // SE TUTTI I DATI SONO COMPLETI AL 99% && E' STATO CLICCATO IL BOTTONE SALVA, ALLORA PASSA AL RIEPILOGO
      if (this.props.summaryData.percentuale === 99 && this.state.salvataggio === true) {
        this.props.history.push('/consulenza-base/riepilogo-cliente')
      }
    }

      // CHECK DATA PENDING NON salvati
    if (prevState.serviziData !== null && prevState.serviziData !== this.state.serviziData) {
      this.props.setDataPending(true)
    }
  }

  setupProp () {

  }

  updateFields (newPropertiesObj) {
    // Aggiorna le proprietà dell'oggetto serviziData dello state con i valori degli input
    let newServiceData = Object.assign({}, this.state.serviziData, newPropertiesObj)
    this.setState({ serviziData: newServiceData })
  }

  updateImg (fileupload, fileThumb) {
    let newServiceData = {}
    if (fileupload === '') {
      newServiceData = Object.assign({}, this.state.serviziData, { flagImg:false, immagineFirma: fileupload, immagineFirmaThumb: fileThumb })
    } else {
      newServiceData = Object.assign({}, this.state.serviziData, { flagImg:true, immagineFirma: fileupload, immagineFirmaThumb: fileThumb })
    }
    this.setState({ serviziData: newServiceData })
  }

  handleRadioTipologiaChange (event) {
    const collocamento = event.target.id === 'flagCollocamento'
    const consulenzaCollocamento = event.target.id === 'flagConsulenzaCollocamento'
    let objRadio = {}
    objRadio.flagCollocamento = collocamento
    objRadio.flagConsulenzaCollocamento = consulenzaCollocamento
    this.updateFields(objRadio)
  }

  toggleModal () {
    this.setState({ showModal: !this.state.showModal })
  }

  sendServiziData () {
    this.setState({ salvataggio:true })
    const servDigActive = checkStatusService(this.props.summaryData.statusEndPoint, 'servdig')
    // const dataToSave = {}
    // dataToSave.flagCollocamento = this.state.flagCollocamento
    // dataToSave.flagConsulenzaCollocamento = this.state.flagConsulenzaCollocamento
    // dataToSave.immagineFirma = this.state.immagineFirma
    this.props.postServiziDigitali(this.props.customer.idCliente, this.state.serviziData, servDigActive)
  }

  nextStep () {
    if (this.props.dataPendingState === true) {
      this.toggleModal()
    } else {
      this.props.history.push('/consulenza-base/riepilogo-cliente')
    }
  }

  nextStepNoSave () {
    this.props.history.push('/consulenza-base/riepilogo-cliente')
  }

  render () {
    const torna = dataSB.button_back
    const title = dataSB.title
    const titleSection = dataSB.servizi.title_section
    const titleSubsection1 = dataSB.servizi.servizi_investimento.title_subsection
    const titleSubsection2 = dataSB.servizi.servizi_digitali.title_subsection
    const radioTitle = dataSB.servizi.servizi_investimento.radio_tipologia_servizio.title
    const radioName = dataSB.servizi.servizi_investimento.radio_tipologia_servizio.name
    const radioOption = dataSB.servizi.servizi_investimento.radio_tipologia_servizio.options

    if (this.state.formready === false) { return null }

    let labelInfoData = null
    if (this.props.dataPendingState === true) { labelInfoData = <label className="box-data-info">Dati non salvati</label> }

    return (
      <section className="main-body">
        <ModalePopup
          visible={this.state.showModal}
          title="Attenzione!"
          text="Le modifiche non sono state salvate. Se prosegui perderai i dati inseriti"
          primaryActionLabel="Continua"
          secondaryActionLabel="Annulla"
          secondaryAction={this.toggleModal}
          primaryAction={this.nextStepNoSave}
        />
        <header>
          <Link to="/" className="torna-pratiche"><i className="icon-arrow-back" />{torna}</Link>
          <h1 className="title-macrosezione">{title}</h1>
        </header>
        <article className="page-left-content">
          <div className="background_pattern" />
          <h2 className="title-sezione">{titleSection}</h2>

          <div className="box-container">
            {labelInfoData}
            <h4 className="title-subsection">{titleSubsection1}</h4>
            <Row >
              <Col s={12}>
                <div className="padding-10">
                  <p>{radioTitle}</p>
                  <div className="radio-horizontal">
                    <p>
                      <input name={radioName} type="radio" id="flagConsulenzaCollocamento" className="with-gap"
                        checked={this.state.serviziData.flagConsulenzaCollocamento === true}
                        onChange={this.handleRadioTipologiaChange}
                      />
                      <label htmlFor="flagConsulenzaCollocamento">{radioOption[0].label}</label>
                    </p>
                    <p>
                      <input name={radioName} type="radio" id="flagCollocamento" className="with-gap"
                        checked={this.state.serviziData.flagCollocamento === true}
                        onChange={this.handleRadioTipologiaChange} />
                      <label htmlFor="flagCollocamento">{radioOption[1].label}</label>
                    </p>
                  </div>
                </div>
              </Col>
            </Row>

            <h4 className="title-subsection">{titleSubsection2}</h4> <label className="text-servizio-my-azimut">Il cliente dichiara di aderire al <strong>servizio My Azimut</strong></label>

            <Row>
              <Col s={6}>
                <p className="padding-l-10">{dataSB.servizi.servizi_digitali.label_contatti}</p>
                <Input s={12} label="Cellulare" disabled
                  value={this.props.customer.cellulare != null ? this.props.customer.cellulare : 'Da inserire in CENSIMENTO ANAGRAFICO'} />
                <Input s={12} label="Email" disabled
                  value={this.props.customer.email != null ? this.props.customer.email : 'Da inserire in CENSIMENTO ANAGRAFICO'} />
              </Col>
              <Col s={6}>
                <p className="margin-b-none">{dataSB.servizi.servizi_digitali.label_modulo}</p>
                <p className="suggestion-text">{dataSB.servizi.servizi_digitali.suggestion_modulo}</p>
                <InputImage
                  s={12}
                  idImg="moduloImg"
                  fileIn={this.state.serviziData.immagineFirma}
                  onChange={(fileupload, fileThumb) => this.updateImg(fileupload, fileThumb)} />
              </Col>
            </Row>
            <Row className="right-align" >
              <Col s={12}>
                <Button flat waves="light" disabled={this.props.dataPendingState === false} onClick={this.sendServiziData} className="margin-20">{dataSB.button_save}</Button>
              </Col>
            </Row>
          </div>
          <Row className="right-align padding-r-20 margin-t-20">
            <Link onClick={this.nextStep} to="#">{dataSB.link_continue} <i className="icon-arrow" /></Link>
          </Row>

        </article>
      </section>
    )
  }
}

Sottoscrizione.propTypes = {
  customer : PropTypes.object,
  serviziData: PropTypes.object,
  summaryData: PropTypes.object,
  getDataSummaryLight: PropTypes.func,
  getServiziDigitali: PropTypes.func,
  postServiziDigitali: PropTypes.func,
  setDataPending: PropTypes.func,
  postServiziDigitaliRequest: PropTypes.object,
  getServiziDigitaliRequest: PropTypes.object,
  getDataSummaryRequest: PropTypes.object,
  dataPendingState: PropTypes.object,
  history: PropTypes.object
}

export default (Sottoscrizione)
