import { connect } from 'react-redux'
import SottoscrizioneServizi from '../components/SottoscrizioneServizi.jsx'
import { getDataSummaryLight, getServiziDigitali, postServiziDigitali } from '../../../../store/modules/customers/actions'
import { setDataPending } from '../../../../store/modules/dataPending/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente, idIncarico) => dispatch(getDataSummaryLight(idCliente, idIncarico)),
    getServiziDigitali : (idCliente, idIncarico, active) => dispatch(getServiziDigitali(idCliente, idIncarico, active)),
    postServiziDigitali : (idCliente, query, active) => dispatch(postServiziDigitali(idCliente, query, active)),
    setDataPending : (value) => dispatch(setDataPending(value))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    summaryData: state.customers.summaryData,
    serviziData: state.customers.serviziData,
    getServiziDigitaliRequest: state.customers.getServiziDigitaliRequest,
    postServiziDigitaliRequest: state.customers.postServiziDigitaliRequest,
    getDataSummaryRequest: state.customers.getDataSummaryRequest,
    dataPendingState: state.dataPendings.dataPendingState
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(SottoscrizioneServizi)
