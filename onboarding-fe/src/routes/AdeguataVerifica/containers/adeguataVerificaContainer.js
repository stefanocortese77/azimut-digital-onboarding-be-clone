import { connect } from 'react-redux'
import AdeguataVerificaComponent from '../components/AdeguataVerificaComponent'
import { getAdegVerTemplate, saveAdegVer } from '../../../store/modules/survey/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getAdegVerTemplate : () => dispatch(getAdegVerTemplate()),
    saveAdegVer: (idCliente, datiQuestionario) => dispatch(saveAdegVer(idCliente, datiQuestionario)),
  }
}

const mapStateToProps = (state) => {
  return {
    templateAV: state.survey.templateAV,
    adegVerSaveRequest: state.survey.adegVerSaveRequest,
    adegVerTemplateRequest: state.survey.adegVerTemplateRequest
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdeguataVerificaComponent)
