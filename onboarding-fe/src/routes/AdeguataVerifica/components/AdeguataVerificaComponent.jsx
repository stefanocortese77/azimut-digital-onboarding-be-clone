import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Button, Input } from 'react-materialize'
import { Link } from 'react-router-dom'
import { Collapsible, CollapsibleItem } from '../../../components/Collapsible'
import ModalePopup from '../../../components/ModalePopup'
import SliderComponent from '../../../components/Slider'
import YesNoQuestion from '../../../components/YesNoQuestion'
import staticData from '../static-data/data-adeguata-verifica.json'

class AdeguataVerificaComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showModal:false,
      openCollapsible:'info-contesto'
    }

    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)
    this.sendAdegVer = this.sendAdegVer.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.backHome = this.backHome.bind(this)
    this.backHomeNoSave = this.backHomeNoSave.bind(this)
  }

  componentDidMount () {
    this.props.getAdegVerTemplate()
  }

  backHome () {
    this.setState({ showModal:true })
  }

  backHomeNoSave () {
    this.props.history.push('/')
  }

  toggleModal () {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({ openCollapsible: itemKey })
  }

  sendAdegVer () {
    // TODO format data to send
    this.props.saveAdegVer()
  }

  render () {
    if (this.props.templateAV === null) { return null }

    const staticAV = staticData.adeguata_verifica
    const staticConoscenza = staticAV.conoscenza_cliente
    const staticContesto = staticAV.info_contesto

    let userBox = <div className="info-cliente">
      <i className="icon-user" />
      <div>
        <p className="label-user">Nome Cognome</p>
        <h4 className="value-user">Mario Rossi</h4>
        <h6 className="value-cf">MARRSS67LGL67L6986</h6>
      </div>
    </div>
    let contrattoBoxInfo = <div className="contratto-box-info">
      <p>CONSULENZA E COLLOCAMENTO</p>
      <p className="margin-t-5"><span>Mario Rossi</span><span> Rossella Bianchi</span></p>
    </div>

    return (
      <Row className="container-home paddingless marginless">
        <Col s={12} className="container-home-left">
          <section className="main-body no-aside">
            <ModalePopup visible={this.state.showModal} title="Attenzione!" text="Le modifiche non sono state salvate. Se prosegui perderai i dati inseriti"
              primaryActionLabel="Continua" secondaryActionLabel="Annulla" secondaryAction={this.toggleModal} primaryAction={this.backHomeNoSave} />
            <header>
              <Link onClick={this.backHome} to="#" className="torna-pratiche">
                <i className="icon-arrow-back" />{staticData.button_back}
              </Link>
              <h1 className="title-macrosezione">{staticData.title}</h1>
            </header>
            <article className="page-left-content">
              <div className="background_pattern" />
              <Row>
                <Col s={9}>
                  <h2 className="title-sezione">{staticAV.title_section}</h2>

                  <Collapsible open={this.state.openCollapsible} onChange={this.handleOpenCollapsibleChange}>
                    <CollapsibleItem itemKey="conoscenza-cliente" header={staticConoscenza.title_accordion} icon="keyboard_arrow_down" labelInfo="">
                      <SliderComponent
                        title={staticConoscenza.input_anni.title}
                        defaultValue={2}
                        min={1}
                        max={20} />
                      <Row >
                        <Col s={12}>
                          <div>
                            <p className="margin-none">{staticConoscenza.radio_primo_contatto.title}</p>
                            <div className="radio-horizontal">
                              <p>
                                <input
                                  name={staticConoscenza.radio_primo_contatto.name}
                                  type="radio"
                                  id={staticConoscenza.radio_primo_contatto.options[0].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticConoscenza.radio_primo_contatto.options[0].value}>
                                  {staticConoscenza.radio_primo_contatto.options[0].label}
                                </label>
                              </p>
                              <p>
                                <input
                                  name={staticConoscenza.radio_primo_contatto.name}
                                  type="radio"
                                  id={staticConoscenza.radio_primo_contatto.options[1].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticConoscenza.radio_primo_contatto.options[1].value}>
                                  {staticConoscenza.radio_primo_contatto.options[1].label}
                                </label>
                              </p>
                              <p>
                                <input
                                  name={staticConoscenza.radio_primo_contatto.name}
                                  type="radio"
                                  id={staticConoscenza.radio_primo_contatto.options[2].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticConoscenza.radio_primo_contatto.options[2].value}>
                                  {staticConoscenza.radio_primo_contatto.options[2].label}
                                </label>
                              </p>
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col s={12}>
                          <YesNoQuestion
                            label={staticConoscenza.yes_no_questions_contesto_famiglia.questions[0].label}
                            onChange=""
                            value=""
                          />
                        </Col>
                      </Row>
                      <SliderComponent
                        title={staticConoscenza.input_num_componenti.title}
                        defaultValue={3}
                        min={1}
                        max={10} />
                      <Row>
                        <h6>
                          {staticConoscenza.radio_tenore_vita.title}
                        </h6>
                        <form action="#" className="big-radio">
                          <div key={staticConoscenza.radio_tenore_vita.options[0].value} >
                            <input name={staticConoscenza.radio_tenore_vita.name} type="radio"
                              id={staticConoscenza.radio_tenore_vita.options[0].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticConoscenza.radio_tenore_vita.options[0].value}>{staticConoscenza.radio_tenore_vita.options[0].label}</label>
                          </div>
                          <div key={staticConoscenza.radio_tenore_vita.options[1].value} >
                            <input name={staticConoscenza.radio_tenore_vita.name} type="radio"
                              id={staticConoscenza.radio_tenore_vita.options[1].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticConoscenza.radio_tenore_vita.options[1].value}>{staticConoscenza.radio_tenore_vita.options[1].label}</label>
                          </div>
                          <div key={staticConoscenza.radio_tenore_vita.options[2].value} >
                            <input name={staticConoscenza.radio_tenore_vita.name} type="radio"
                              id={staticConoscenza.radio_tenore_vita.options[2].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticConoscenza.radio_tenore_vita.options[2].value}>{staticConoscenza.radio_tenore_vita.options[2].label}</label>
                          </div>
                        </form>
                      </Row>
                    </CollapsibleItem>
                    <CollapsibleItem itemKey="info-contesto" header={staticContesto.title_accordion} icon="keyboard_arrow_down" labelInfo="">
                      <Row>
                        <Col s={12}>
                          <div>
                            <p className="margin-none">{staticContesto.radio_ragionevolezza_attivita.title}</p>
                            <div className="radio-horizontal">
                              <p>
                                <input
                                  name={staticContesto.radio_ragionevolezza_attivita.name}
                                  type="radio"
                                  id={staticContesto.radio_ragionevolezza_attivita.options[0].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_ragionevolezza_attivita.options[0].value}>
                                  {staticContesto.radio_ragionevolezza_attivita.options[0].label}
                                </label>
                              </p>
                              <p>
                                <input
                                  name={staticContesto.radio_ragionevolezza_attivita.name}
                                  type="radio"
                                  id={staticContesto.radio_ragionevolezza_attivita.options[1].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_ragionevolezza_attivita.options[1].value}>
                                  {staticContesto.radio_ragionevolezza_attivita.options[1].label}
                                </label>
                              </p>
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col s={12}>
                          <div>
                            <p className="margin-none">{staticContesto.radio_ragionevolezza_residenza.title}</p>
                            <div className="radio-horizontal">
                              <p>
                                <input
                                  name={staticContesto.radio_ragionevolezza_residenza.name}
                                  type="radio"
                                  id={staticContesto.radio_ragionevolezza_residenza.options[0].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_ragionevolezza_residenza.options[0].value}>
                                  {staticContesto.radio_ragionevolezza_residenza.options[0].label}
                                </label>
                              </p>
                              <p>
                                <input
                                  name={staticContesto.radio_ragionevolezza_residenza.name}
                                  type="radio"
                                  id={staticContesto.radio_ragionevolezza_residenza.options[1].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_ragionevolezza_residenza.options[1].value}>
                                  {staticContesto.radio_ragionevolezza_residenza.options[1].label}
                                </label>
                              </p>
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col s={12}>
                          <div>
                            <p className="margin-none">{staticContesto.radio_comportamento.title}</p>
                            <div className="radio-horizontal">
                              <p>
                                <input
                                  name={staticContesto.radio_comportamento.name}
                                  type="radio"
                                  id={staticContesto.radio_comportamento.options[0].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_comportamento.options[0].value}>
                                  {staticContesto.radio_comportamento.options[0].label}
                                </label>
                              </p>
                              <p>
                                <input
                                  name={staticContesto.radio_comportamento.name}
                                  type="radio"
                                  id={staticContesto.radio_comportamento.options[1].value}
                                  className="with-gap"
                                />
                                <label htmlFor={staticContesto.radio_comportamento.options[1].value}>
                                  {staticContesto.radio_comportamento.options[1].label}
                                </label>
                              </p>
                            </div>
                          </div>
                        </Col>
                      </Row>
                      <YesNoQuestion
                        label={staticContesto.yes_no_questions_info.questions[0].label}
                        onChange=""
                        value=""
                          />
                      <YesNoQuestion
                        label={staticContesto.yes_no_questions_info.questions[1].label}
                        onChange=""
                        value=""
                          />
                      <YesNoQuestion
                        label={staticContesto.yes_no_questions_info.questions[2].label}
                        onChange=""
                        value=""
                          />
                      <Row>
                        <h6>
                          {staticContesto.radio_rischio_riciclaggio.title}
                        </h6>
                        <form action="#" className="big-radio">
                          <div key={staticContesto.radio_rischio_riciclaggio.options[0].value} >
                            <input name={staticContesto.radio_rischio_riciclaggio.name} type="radio"
                              id={staticContesto.radio_rischio_riciclaggio.options[0].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticContesto.radio_rischio_riciclaggio.options[0].value}>{staticContesto.radio_rischio_riciclaggio.options[0].label}</label>
                          </div>
                          <div key={staticContesto.radio_rischio_riciclaggio.options[1].value} >
                            <input name={staticContesto.radio_rischio_riciclaggio.name} type="radio"
                              id={staticContesto.radio_rischio_riciclaggio.options[1].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticContesto.radio_rischio_riciclaggio.options[1].value}>{staticContesto.radio_rischio_riciclaggio.options[1].label}</label>
                          </div>
                          <div key={staticContesto.radio_rischio_riciclaggio.options[2].value} >
                            <input name={staticContesto.radio_rischio_riciclaggio.name} type="radio"
                              id={staticContesto.radio_rischio_riciclaggio.options[2].value} className="with-gap"
                              onChange=""
                              checked="" />
                            <label htmlFor={staticContesto.radio_rischio_riciclaggio.options[2].value}>{staticContesto.radio_rischio_riciclaggio.options[2].label}</label>
                          </div>
                        </form>
                      </Row>
                      <Row>
                        <Input s={12} type="textarea" label={staticContesto.input_textarea_motivazioni.label} />
                        <Col s={12}><span className="suggestion-textarea">{staticContesto.input_textarea_motivazioni.help_text}</span></Col>
                      </Row>
                    </CollapsibleItem>
                  </Collapsible>
                  <Row className="center-align">
                    <Col s={12}>
                      <Button waves="light" className="margin-20" onClick={this.sendAdegVer}>{staticData.button_invia}</Button>
                    </Col>
                  </Row>
                </Col>
                <Col s={3} className="paddingless">
                  <aside className="aside-adeguata-verifica">
                    {userBox}
                    {contrattoBoxInfo}
                    {contrattoBoxInfo}
                  </aside>
                </Col>
              </Row>
            </article>
          </section>
        </Col>
      </Row>

    )
  }
}

AdeguataVerificaComponent.propTypes = {
  history: PropTypes.object,
  templateAV: PropTypes.object,
  getAdegVerTemplate: PropTypes.func,
  saveAdegVer: PropTypes.func,
  // adegVerTemplateRequest: PropTypes.object,
  // adegVerSaveRequest: PropTypes.object
}

export default AdeguataVerificaComponent
