import { connect } from 'react-redux'
import FirmaComponent from '../components/FirmaComponent'
import { utilityActions } from '../../../store/modules/utility'

const mapDispatchToProps = (dispatch) => {
  return {
    utilityDocFirmaSign : (idIncarico) => dispatch(utilityActions.utilityDocFirmaSign(idIncarico))
  }
}

const mapStateToProps = (state) => ({
  utility: state.utility,
  customer: state.customers.customer,
  utilityDocFirmaSignRequest: state.utility.utilityDocFirmaSignRequest
})

export default connect(mapStateToProps, mapDispatchToProps)(FirmaComponent)
