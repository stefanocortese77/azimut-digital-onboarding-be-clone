import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Input, Button } from 'react-materialize'
import { Link } from 'react-router-dom'
import UserCardFirma from '../../../components/UserCardFirma'
import PdfFile from './PdfFile'
import staticDataFirma from '../../../static-data/data-firma.json'

class FirmaComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      tokenNamirial :null,
      iframeDOM:null,
      utilityRender: null,
      checkboxOtp : false
    }
    this.openIframeNamirial = this.openIframeNamirial.bind(this)
    this.changeCheckBoxOtp = this.changeCheckBoxOtp.bind(this)
  }
  backHome () {
    this.props.history.push('/')
  }


  gotoParentDocument () {
    // const queryParams = new URLSearchParams(this.props.location.search)
    // console.log('assuming query param is id', queryParams.get('id'))

    let tokenNamirial = null
    const urlMatches = /tokenNamirial=([a-zA-Z0-9_.\-~]*)(&.*)*$/.exec(document.location.href)
    if (urlMatches && urlMatches[1] && urlMatches[1].length) {
      tokenNamirial = urlMatches[1]
      this.setState({ iframeDOM: null })
      this.setState({ tokenNamirial: tokenNamirial })
    }
  }
  openIframeNamirial (link) {
    console.log(link)
    const iframeDOM = <iframe id="namirialFrame" src="https://demo.xyzmo.com/workstepredirector/sign?identifier=TVHcHfen1a~JKrI1oIs2nQrn0bv1Nif8Sr7lIJw/I~zD5JZhD1LtxcWW4szMgqSJwP7Jde36UA4=" width="100%" height="600" />
    this.setState({ iframeDOM: iframeDOM })
  }
  componentDidMount () {
    this.gotoParentDocument()
    const idIncarico = this.props.customer.idIncarico || 'testdiProva'
    this.props.utilityDocFirmaSign(idIncarico)
  }
  componentDidUpdate (prevProps) {
    if (
      prevProps.utilityDocFirmaSignRequest.fetching &&
      this.props.utilityDocFirmaSignRequest.fetching === false
    ) {
        // eslint-disable-next-line
      this.setState({ utilityRender: true })
    }
  }
  sendDataDocument () {

  }
  changeCheckBoxOtp () {
    this.setState({ checkboxOtp: !this.state.checkboxOtp })
  }


  render () {
    if (!this.state.utilityRender) {
      return null
    }
    const { utility } = this.props
    let pdfFileItemList = []
    if (utility && utility.listaDocumenti) {
      pdfFileItemList = utility['listaDocumenti'].map((documentoPdf, i) => (
        <PdfFile key={i} documentoPdf={documentoPdf} />
      ))
    }
    let firmaNoIframe = null
    if (this.state.iframeDOM === null) {
      firmaNoIframe = (<div className="box-vertical-container ">
        <div className="box-vertical">
          <p>
            {staticDataFirma.description_file_list}
          </p>
          <ul>
            {pdfFileItemList}
          </ul>
        </div>
        <UserCardFirma callback={this.openIframeNamirial} staticDataFirma={staticDataFirma} />
      </div>)
    }
    let classDisable = 'disabled'
    if (utility.statusComplessivo === 'FIRMATO' && this.state.checkboxOtp) {
      classDisable = ''
    }
    return (
      <Row className="container-home paddingless marginless">
        <Col s={12} className="container-home-left">
          <section className="main-body no-aside">
            <header>
              <Link onClick={() => this.backHome()} to="#" className="torna-pratiche">
                <i className="icon-arrow-back" />Torna a elenco pratiche
              </Link>
              <h1 className="title-macrosezione">Firma</h1>
            </header>
            <article className="page-left-content">
              <div className="background_pattern" />
              {this.state.iframeDOM}
              {firmaNoIframe}
            </article>
            <Row className="action-send-file">
              <Col s={12}>
                <Input
                  name="group1-vertical"
                  type="checkbox"
                  value={this.state.checkboxOtp}
                  onChange={this.changeCheckBoxOtp}
                  label={staticDataFirma.otp_label}
                  className="filled-in" />
                <br />
                <Button disabled={classDisable} flat waves="light" onClick={() => this.sendDataDocument} > {staticDataFirma.invia}</Button>
              </Col>
            </Row>
          </section>
        </Col>
      </Row>

    )
  }
}

FirmaComponent.propTypes = {
  history: PropTypes.object,
  utilityDocFirmaSign: PropTypes.func,
  customer: PropTypes.object,
  utility: PropTypes.object,
  utilityDocFirmaSignRequest: PropTypes.object
}

export default FirmaComponent
