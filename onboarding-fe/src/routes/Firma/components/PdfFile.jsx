import React from 'react'
import PropTypes from 'prop-types'


const PdfFile = ({ documentoPdf }) => {
  return (
    <li>{documentoPdf} </li>
  )
}

PdfFile.propTypes = {
  documentoPdf: PropTypes.string
}

export default PdfFile
