import { connect } from 'react-redux'
import IntestatariView from '../components/IntestatariView'
import { getCustomerList } from '../../../../store/modules/customers/actions'

// importo la mia nuova funzione
import { saveAndProceed } from '../../../../store/modules/pratica/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getCustomerList : () => dispatch(getCustomerList()),

    // aggiungo il metodo ai metodi da utilizzare in pagina
    saveAndProceed : (paramObject) => dispatch(saveAndProceed(paramObject))
  }
}

// mappo ritorno chiamate
const mapStateToProps = (state) => ({
  users: state.customers.users,
  getClientiListRequest: state.customers.getClientiListRequest,
  // mappo il ritorno della mia chiamata allo state del componente
  savePraticaRequest: state.pratica.savePraticaRequest
})

export default connect(mapStateToProps, mapDispatchToProps)(IntestatariView)

