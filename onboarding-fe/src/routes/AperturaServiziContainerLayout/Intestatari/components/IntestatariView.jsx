import React from 'react'
import { PropTypes } from 'prop-types'
import { Button, Row, Col, Input } from 'react-materialize'
import AutocompleteIcon from '../../../../components/AutocompleteIcon/AutocompleteIcon'

export class IntestatariView extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      intestatarioValue: '',
      cointestatario1Value: '',
      cointestatario2Value: '',
      intestatario:null,
      cointestatario1: null,
      cointestatario2: null,

      formReady:false,

      // checkboxes fields
      check1:false,
      check2:false,
      check3:false,
      check4:false
    }

    this.handleIntestatarioChange = this.handleIntestatarioChange.bind(this)
    this.checkingElement1 = this.checkingElement1.bind(this)
    this.checkingElement2 = this.checkingElement2.bind(this)
    this.checkingElement3 = this.checkingElement3.bind(this)
    this.checkingElement4 = this.checkingElement4.bind(this)

    // ne effettuop il bind() nel costruttore per passargli i parametri
    this.savePratica = this.savePratica.bind(this)
  }

  componentDidMount () {
    this.props.getCustomerList()
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.getClientiListRequest.fetching === true && this.props.getClientiListRequest.fetching === false) {
      // eslint-disable-next-line
      this.setState({ formReady: true })
    }

    if (prevProps.savePraticaRequest.fetching === true && this.props.savePraticaRequest.fetching === false && this.props.savePraticaRequest.error === null) {
      this.props.history.push('configurazione')
    }
  }

  handleIntestatarioChange (value, matchSuggestion, ruoloCliente) {
    switch (ruoloCliente) {
      case 'intestatario':
        this.setState({ intestatario:matchSuggestion, intestatarioValue: value })
        break
      case 'cointestatario1':
        this.setState({ cointestatario1:matchSuggestion, cointestatario1Value: value })
        break
      case 'cointestatario2':
        this.setState({ cointestatario2:matchSuggestion, cointestatario2Value: value })
        break
    }
  }

  mapClientiToAutocomplete (arrayUsers) {
    let usersForAutocomplete = []
    arrayUsers.forEach(function (user) {
      const nomeCognome = user.nome + ' ' + user.cognome
      usersForAutocomplete.push({
        key: user.idCustomer,
        value: nomeCognome,
        label: nomeCognome,
      })
    })
    return usersForAutocomplete
  }

  // check 1st checkbox
  checkingElement1 () {
    this.setState({
      check1: !this.state.check1
    })
  }

  // check 2nd checkbox
  checkingElement2 () {
    this.setState({
      check2: !this.state.check2
    })
  }

  // check 3rd checkbox
  checkingElement3 () {
    this.setState({
      check3: !this.state.check3
    })
  }

  // check 4th checkbox
  checkingElement4 () {
    this.setState({
      check4: !this.state.check4
    })
  }

  // save user data and proceed to next page
  savePratica () {
    // array contenente i moduli selezionati
    let arrayModuli = []

    if (this.state.check1) {
      arrayModuli.push('1')
    }

    if (this.state.check2) {
      arrayModuli.push('2')
    }

    if (this.state.check3) {
      arrayModuli.push('3')
    }

    if (this.state.check4) {
      arrayModuli.push('4')
    }

    // array contenente l'intestatario e i cointestatari (se presenti)
    let arrayIntestatari = []

    // add intestatario
    arrayIntestatari.push({idCliente : this.state.intestatario.key, idRuolo: 1})

    // add cointestatario1
    if (this.state.cointestatario1 !== null) {
      arrayIntestatari.push({idCliente : this.state.cointestatario1.key, idRuolo: 2})
    }


    // add cointestatario2
    if (this.state.cointestatario2 !== null) {
      arrayIntestatari.push({idCliente : this.state.cointestatario2.key, idRuolo: 3})
    }


    // define request param
    let paramObject = {
      'codiciTipoServizio': arrayModuli,
      'intestatari': arrayIntestatari
    }

    // send request to web service
    this.props.saveAndProceed(paramObject)
  }

  render () {
    if (this.state.formReady === false) { return null }

    let clienti = this.mapClientiToAutocomplete(this.props.users)

    return (

      <div>
        <h2 className="title-sezione-config">CONFIGURAZIONE</h2>
        <div className="box-container box-intestatari">
          <Row className="left-align">
            <Col s={1}>
              <p className="intestazione">INTESTATARI</p>
            </Col>
          </Row>
          <Row>
            <Col s={6}>
              <AutocompleteIcon
                id="intestatario"
                label="intestatario"
                threshold={0}
                suggestions={clienti}
                onChange={(value, matched) => this.handleIntestatarioChange(value, matched, 'intestatario')}
                value={this.state.intestatarioValue}
            />
            </Col>
          </Row>
          <Row>
            <Col s={6}>
              <AutocompleteIcon
                id="cointestatario1"
                label="Cointestatario 1"
                threshold={0}
                suggestions={clienti}
                onChange={(value, matched) => this.handleIntestatarioChange(value, matched, 'cointestatario1')}
                value={this.state.cointestatario1Value}
                disabled={this.state.intestatario === null}
              />
            </Col>

            <Col s={6}>
              <AutocompleteIcon
                id="cointestatario1"
                label="Cointestatario 2"
                threshold={0}
                suggestions={clienti}
                onChange={(value, matched) => this.handleIntestatarioChange(value, matched, 'cointestatario2')}
                value={this.state.cointestatario2Value}
                disabled={this.state.intestatario === null}
              />
            </Col>
          </Row>
          <Row className="left-align">
            <Col s={1}>
              <p className="intestazione">SERVIZI</p>
            </Col>
          </Row>
          <div className="buttons-servizio-box">
            <div className="button-servizio-check" onClick={this.checkingElement1}>
              <i className="icon-prodotti-action-bar" />
              <p>MAX</p>
              <Input
                key={this.state.check1}
                name="check1"
                type="checkbox"
                checked={this.state.check1}
                label=" "
                className="filled-in" />
            </div>
            <div className="button-servizio-check" onClick={this.checkingElement2}>
              <i className="icon-prodotti-action-bar" />
              <p>MAX FUND</p>
              <Input
                key={this.state.check2}
                name="group2-horizontal"
                type="checkbox"
                checked={this.state.check2}
                label=" "
                className="filled-in vertical" />
            </div>
            <div className="button-servizio-check" onClick={this.checkingElement3}>
              <i className="icon-prodotti-action-bar" />
              <p>RTO<br />BANCO BPM S.P.A</p>
              <Input
                key={this.state.check3}
                name="group2-horizontal"
                type="checkbox"
                checked={this.state.check3}
                label=" "
                className="filled-in vertical" />
            </div>
            <div className="button-servizio-check" onClick={this.checkingElement4}>
              <i className="icon-prodotti-action-bar" />
              <p>RTO<br />UBS EUROPE SE</p>
              <Input
                key={this.state.check4}
                name="group2-horizontal"
                type="checkbox"
                checked={this.state.check4}
                label=" "
                className="filled-in vertical" />
            </div>
          </div>
          <Row className="center-align">
            <Col s={12}>
              <Button waves="light" disabled={this.state.intestatario === null} className=" margin-20" onClick={this.savePratica}>Salva e Prosegui</Button>
            </Col>
          </Row>

        </div>
      </div>)
  }
}

IntestatariView.propTypes = {
  history: PropTypes.object,
  users: PropTypes.array,
  getCustomerList: PropTypes.func,
  getClientiListRequest: PropTypes.object,
  saveAndProceed: PropTypes.func,
  savePraticaRequest: PropTypes.object
}

export default IntestatariView
