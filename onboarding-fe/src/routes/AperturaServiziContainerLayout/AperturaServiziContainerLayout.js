import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-materialize'
import { Route, Switch, Link } from 'react-router-dom'

import { ConfigurazioneServizi } from './ConfigurazioneServizi'
import { Intestatari } from './Intestatari'

export class AperturaServiziContainerLayout extends React.Component {
  backHome () {
    this.props.history.push('/')
  }

  render () {
    return (

      <Row className="container-home paddingless marginless">
        <Col s={12} className="container-home-left">
          <section className="main-body no-aside">
            <header>
              <Link onClick={() => this.backHome()} to="#" className="torna-pratiche">
                <i className="icon-arrow-back" />Torna a elenco pratiche
              </Link>
              <h1 className="title-macrosezione">Apertura Servizi</h1>
            </header>
            <article className="page-left-content">
              <div className="background_pattern" />

              <Switch>
                <Route path={`${this.props.match.url}/configurazione`} component={ConfigurazioneServizi} />
                <Route path={`${this.props.match.url}/intestatari`} component={Intestatari} />
              </Switch>

            </article>
          </section>
        </Col>
      </Row>

    )
  }
}

AperturaServiziContainerLayout.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object
}

export default AperturaServiziContainerLayout
