import React from 'react'
// import { PropTypes } from 'prop-types'
// import { Link } from 'react-router-dom'
import { Row } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'

export class Max extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      serviceData:null,

      formReady: false
    }
  }
  render () {
    return (
      <CollapsibleItem
        itemKey="max"
        header={'Max'}
        icon="keyboard_arrow_down"
        // labelInfo={accordionInfo}
      >
        <Row>

          <div className="padding-10">
            <p className="margin-none">Dichiarazione del regime di appartenenza*</p>
            <div className="radio-horizontal">
              <p>
                <input name="regime" type="radio" id="regimeRisparmio" className="with-gap"
                      />
                <label htmlFor="regimeRisparmio">risparmio amministrato</label>
              </p>
              <p>
                <input name="regime" type="radio" id="dichiarazione" className="with-gap"
                      />
                <label htmlFor="dichiarazione">dichiarazione</label>
              </p>
            </div>
          </div>
        </Row>
      </CollapsibleItem>
    )
  }
}
export default Max
