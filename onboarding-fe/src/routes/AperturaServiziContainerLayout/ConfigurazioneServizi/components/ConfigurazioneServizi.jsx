import React from 'react'
import { Row, Col } from 'react-materialize'
import { PropTypes } from 'prop-types'
import { Collapsible } from '../../../../components/Collapsible'

import SERVIZI from '../servizi'

import InformazioniGenerali from './InformazioniGenerali'
import Rto from './Rto'
import Max from './Max'
import MaxFund from './MaxFund'
// import { connect } from 'react-redux'
// import { Input, Button } from 'react-materialize'

export class ConfigurazioneServiziView extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      serviceData:null,
      formReady: false,
      openCollapsible: 'informazioni-generali'
    }

    this.handleOpenCollapsibleChange = this.handleOpenCollapsibleChange.bind(this)

    this.fetchDomini = this.fetchDomini.bind(this)
    this.updateDominio = this.updateDominio.bind(this)
  }

  componentDidMount () {
    // this.props.getServizioInfo('1234')
    this.fetchDomini([
      'DOM_CITTADINANZE',
      'DOM_NAZIONI',
      'DOM_PROVINCE',
      'DOM_ORIGINE_VERSAMENTO'
    ])
  }

  componentDidUpdate (prevProps) {
    if (prevProps.dominioRequest.fetching === true && this.props.dominioRequest.fetching === false) {
      // eslint-disable-next-line
      this.setState({ formReady:true })
    }
  }

  fetchDomini (domini) {
    this.props.dominio({
      'listaDomini': domini.map(dominio => ({
        idDominio: dominio
      }))
    })
  }

  updateDominio (name, filter) {
    this.props.dominio({
      'listaDomini': [{
        idDominio: name,
        filtro: {
          codice: filter
        }
      }]
    })
  }

  handleOpenCollapsibleChange (itemKey) {
    this.setState({
      openCollapsible: itemKey
    })
  }

  render () {
    const { domini } = this.props

    if (!domini.listaDomini) {
      return null
    }

    // Check if there a is the requested domain
    if (!Object.keys(domini.listaDomini).find(dominioKey => domini.listaDomini[dominioKey].idDominio === 'DOM_ORIGINE_VERSAMENTO')) {
      return null
    }

    if (!this.props.pratica) {
      return null
    }

    let userBox = <div className="info-cliente">
      <i className="icon-user" />
      <div>
        <p className="label-user">Nome Cognome</p>
        <h4 className="value-user">Mario Rossi</h4>
      </div>
    </div>

    let rto = null
    if (this.props.pratica.servizi.find(servizio => servizio.idTipoServizio === SERVIZI.RTO)) {
      rto = (
        <Rto
          domini={domini} />
      )
    }

    let max = null
    if (this.props.pratica.servizi.find(servizio => servizio.idTipoServizio === SERVIZI.MAX)) {
      max = (
        <Max
          domini={domini} />
      )
    }

    let maxFund = null
    if (this.props.pratica.servizi.find(servizio => servizio.idTipoServizio === SERVIZI.MAX_FUND)) {
      maxFund = (
        <MaxFund
          domini={domini} />
      )
    }

    return (
      <Row>
        <Col s={9}>
          <h2 className="title-sezione">Configurazione Servizi</h2>
          <div>

            <Collapsible
              open={this.state.openCollapsible}
              onChange={this.handleOpenCollapsibleChange}
            >
              <InformazioniGenerali
                domini={domini}
                dominio={this.props.dominio}
                // previousForm={this.props.previousForm}
                setDataPending={this.handleSetDataPending}
                // mifidSaveRequest={this.props.mifidSaveRequest}
                updateDominio={this.updateDominio}
                onSave={this.hadleSaveSurvey}
                dominioRequest={this.props.dominioRequest} />

              { rto }
              { max }
              { maxFund }
            </Collapsible>
          </div>
        </Col>
        <Col s={3} className="paddingless">
          <aside className="aside-service">
            <h4 className="title-intestatari-aside">Intestatari</h4>
            {userBox}
            {userBox}
          </aside>
        </Col>
      </Row>
    )
  }
}
ConfigurazioneServiziView.propTypes = {
  // getServizioInfo: PropTypes.func,
  // getServiceInfoRequest: PropTypes.object,
  dominioRequest: PropTypes.object,
  dominio: PropTypes.func,
  domini: PropTypes.array,
  pratica: PropTypes.object
}
export default ConfigurazioneServiziView
