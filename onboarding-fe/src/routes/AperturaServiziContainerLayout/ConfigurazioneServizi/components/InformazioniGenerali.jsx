import React from 'react'
import { PropTypes } from 'prop-types'
import { Col, Row, Input, Button } from 'react-materialize'
import { CollapsibleItem } from '../../../../components/Collapsible'
import Autocomplete from '../../../../components/Autocomplete/Autocomplete'

import { mapDominiToAutocomplete, getDominio } from '../../../ConsulenzaBaseContainerLayout/utils'

import * as dataSB from '../../../../static-data/data-servizio-base.json'
const infoResidenza = dataSB.censimento_anagrafica.residenza


class InformazioniGenerali extends React.Component {
  constructor (props) {
    super(props)

    this.state = Object.assign({
      dirty: null,
      saving: false,
      informazioniGenerali: null,

      nome: '',
      cognome: '',
      indirizzo: '',
      civico: '',
      idComune: null,
      comuneText: '',
      idProvincia: null,
      provinciaText: '',
      cap: null,
      idNazione: null,
      nazioneText: '',
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_COMUNE')),
        nazioni: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_NAZIONI')),
        province: mapDominiToAutocomplete(getDominio(props.domini, 'DOM_PROVINCE'))
      }
      // "comuneEstero": ''
    }/* , loadQuestions(this.props.previousForm, this.props.template.listaDomande) */)

    this.save = this.save.bind(this)
    this.updateDomini = this.updateDomini.bind(this)
    this.handleIndirizzoChange = this.handleIndirizzoChange.bind(this)
    this.handleCivicoChange = this.handleCivicoChange.bind(this)
    this.handleProvinciaChange = this.handleProvinciaChange.bind(this)
    this.handleComuneChange = this.handleComuneChange.bind(this)
    this.handleCAPChange = this.handleCAPChange.bind(this)
    this.handleNazioneChange = this.handleNazioneChange.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.saving === true) {
      if (prevProps.mifidSaveRequest.fetching && !this.props.mifidSaveRequest.fetching) {
        if (!this.props.mifidSaveRequest.error) {
          this.setState({ dirty: false })
        }
      }
    }

    if (prevProps.dominioRequest.fetching && !this.props.dominioRequest.fetching) {
      if (!this.props.dominioRequest.error) {
        this.updateDomini()
      }
    }
  }

  save () {
    if (this.props.onSave) {
      this.setState({ saving: true })
      // this.props.onSave(filterQuestions(this.state))
    }
  }

  updateDomini (domini) {
    this.setState({
      domini: {
        cittadinanze: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_CITTADINANZE')),
        comuni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_COMUNE')),
        nazioni: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_NAZIONI')),
        province: mapDominiToAutocomplete(getDominio(this.props.domini, 'DOM_PROVINCE'))
      }
    })
  }

  handleIndirizzoChange (event) {
    this.setState({ indirizzo: event.target.value })
  }

  handleCivicoChange (event) {
    this.setState({ civico: event.target.value })
  }

  handleProvinciaChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        idProvincia: suggestion.value,
        provinciaText: value,
      })
      this.props.updateDominio('DOM_COMUNE', suggestion.value)
    } else {
      this.setState({ provinciaText: value })
    }
  }

  handleComuneChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        idComune: suggestion.value,
        comuneText: value
      })
    } else {
      this.setState({ comuneText: value })
    }
  }

  handleCAPChange (value) {
    // if (suggestion) {
    //   this.setState({
    //     idProvincia: suggestion.value,
    //     provinciaText: value
    //   })
    // } else {
    this.setState({ cap: value })
    // }
  }

  handleNazioneChange (value, suggestion) {
    if (suggestion) {
      this.setState({
        idNazione: suggestion.value,
        nazioneText: value
      })
    } else {
      this.setState({ nazioneText: value })
    }
  }


  render () {
    // const { domini } = this.props

    // const sectionContext = {
    //   domini: domini,
    //   getFieldValue: (idDomanda, multiValue) => getFieldValue(idDomanda, multiValue, this.state),
    //   updateField: (idDomanda, value) => {
    //     this.setState(updateField(idDomanda, value))
    //     this.props.setDataPending()
    //   }
    // }

    // TODO: Use dominio
    const cap = [
      {
        key: '10090',
        label: '10090',
        value: '10090',
      },
      {
        key: '10100',
        label: '10100',
        value: '10100',
      },
      {
        key: '10141',
        label: '10141',
        value: '10141',
      },
    ]


    // const dipendenze = Object
    //   .keys(this.state)
    //   .filter(fieldName => !isNaN(parseInt(fieldName)))
    //   .reduce((a, field) => Object.assign(a, { [field]: this.state[field] }), {})

    let accordionInfo = null
    if (this.state.dirty === false) {
      accordionInfo = (
        <span className="accordion-info accordion-saved">Dati salvati</span>
      )
    } else if (this.state.dirty === true) {
      accordionInfo = (
        <span className="accordion-info accordion-pending">Dati non salvati</span>
      )
    }


    return (
      <CollapsibleItem
        itemKey="informazioni-generali"
        header={'Informazioni Generali'}
        icon="keyboard_arrow_down"
        labelInfo={accordionInfo}
      >
        <Row>
          <h6>
            Dichiarazione del regime di appartenenza*
          </h6>
          <form action="#" className="radio-horizontal">
            <p>
              <input
                name="risparmio-amministrato"
                type="radio"
                id="risparmio-amministrato"
                className="with-gap"
                onChange={({ target: { value } }) => this.setState({ informazioniGenerali: 'risparmio-amministrato' })}
                checked={this.state.informazioniGenerali === 'risparmio-amministrato'} />
              <label htmlFor="risparmio-amministrato">Risparmio amministrato</label>
            </p>
            <p>
              <input
                name={'dichiarazione'}
                type="radio"
                id={'dichiarazione'}
                className="with-gap"
                onChange={({ target: { value } }) => this.setState({ informazioniGenerali: 'dichiarazione' })}
                checked={this.state.informazioniGenerali === 'dichiarazione'} />
              <label htmlFor={'dichiarazione'}>Dichiarazione</label>
            </p>
            <hr />
            <b>Indirizzo per comunicazione:</b> uguale a indirizzo residenza intestatario
            <a href="javascript:void(0)"><b>Modifica</b></a>
          </form>
        </Row>
        <Row>
          <Input
            s={6}
            label={infoResidenza.label_indirizzo}
            value={this.state.indirizzo}
            onChange={this.handleIndirizzoChange}
          />
          <Input
            s={4}
            label={infoResidenza.label_civico}
            value={this.state.civico}
            onChange={this.handleCivicoChange}
          />
        </Row>
        <Row>
          <Col s={4}>
            <Autocomplete
              id="provincia-comunicazioni"
              label={infoResidenza.label_provincia}
              threshold={0}
              suggestions={this.state.domini.province}
              value={this.state.provinciaText}
              onChange={this.handleProvinciaChange}
            />
          </Col>
          <Col s={4}>
            <Autocomplete
              id="comune-comunicazioni"
              label={infoResidenza.label_comune}
              threshold={0}
              suggestions={this.state.domini.comuni || []}
              value={this.state.comuneText}
              onChange={this.handleComuneChange}
              disabled={this.state.comuniResidenzaDisable}
            />
          </Col>
          <Col s={4}>
            <Autocomplete
              id="cap"
              label={infoResidenza.label_cap}
              threshold={0}
              suggestions={cap}
              value={this.state.cap}
              onChange={this.handleCAPChange}
              disabled={this.state.capResidenzaDisable}
            />
          </Col>
        </Row>
        <Row>
          <Col s={4}>
            <Autocomplete
              id="nazione-rilascio"
              label={infoResidenza.label_nazione}
              suggestions={this.state.domini.nazioni}
              value={this.state.nazioneText}
              onChange={this.handleNazioneChange}
            />
          </Col>
        </Row>
        <Row className="right-align submit-collapsible-item">
          <Button flat waves="light" onClick={this.save}>Salva</Button>
        </Row>
      </CollapsibleItem>
    )
  }
}

InformazioniGenerali.propTypes = {
  // dominio: PropTypes.func,
  domini: PropTypes.object,
  onSave: PropTypes.func,
  // setDataPending: PropTypes.func,
  // template: PropTypes.object,
  // previousForm: PropTypes.object,
  mifidSaveRequest: PropTypes.object,
  updateDominio: PropTypes.func,
  dominioRequest: PropTypes.object,
}

export default(InformazioniGenerali)
