import React from 'react'
import { Link } from 'react-router-dom'
import { Row, Col, Input, Button } from 'react-materialize'
import { PropTypes } from 'prop-types'
import { getDominio, mapDominiToOptions } from '../../../ConsulenzaBaseContainerLayout/utils'
import InputImage from '../../../../components/InputImage'
import InputPlusMinus from '../../../../components/InputPlusMinus'
import { CollapsibleItem } from '../../../../components/Collapsible'
// import { connect } from 'react-redux'
// import { Input, Button } from 'react-materialize'

export class Rto extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      serviceData:null,

      formReady: false
    }
  }

  componentDidMount () {

  }

  // componentDidUpdate (prevProps) {
  //   if (prevProps.dominioRequest.fetching === true && this.props.dominioRequest.fetching === false) {
  //     // eslint-disable-next-line
  //     this.setState({ formReady:true })
  //   }
  // }

  render () {
    // if (this.state.formReady === false) { return null }

    // let userBox = <div className="info-cliente">
    //   <i className="icon-user" />
    //   <div>
    //     <p className="label-user">Nome Cognome</p>
    //     <h4 className="value-user">Mario Rossi</h4>
    //   </div>
    // </div>

    return (
      <CollapsibleItem
        itemKey="rto"
        header={'Rto UBS Europe SE'}
        icon="keyboard_arrow_down"
        // labelInfo={accordionInfo}
      >
        <Row >
          <Col s={12}>
            <div className="padding-10">
              <p className="margin-none">Dichiarazione del regime di appartenenza*</p>
              <div className="radio-horizontal">
                <p>
                  <input name="regime" type="radio" id="regimeRisparmio" className="with-gap"
                />
                  <label htmlFor="regimeRisparmio">risparmio amministrato</label>
                </p>
                <p>
                  <input name="regime" type="radio" id="dichiarazione" className="with-gap"
                />
                  <label htmlFor="dichiarazione">dichiarazione</label>
                </p>
              </div>
            </div>
          </Col>
        </Row>

        <h4 className="title-subsection">Banca di riferimento</h4>
        <Row >
          <Col s={12}>
            <div className="padding-10">
              <p className="margin-none">Banca incaricata al subdeposito degli Strumenti finanziari e della liquidità *</p>
              <div className="radio-horizontal">
                <p>
                  <input name="banca-riferimento" type="radio" id="banco-bpm" className="with-gap"
                />
                  <label htmlFor="banco-bpm">Banco BPM S.p.A</label><label className="margin-l-10" htmlFor="banco-bpm">filiale Milano AG. 12</label>
                </p>
                <p>
                  <input name="banca-riferimento" type="radio" id="banca-ubs" className="with-gap"
                />
                  <label htmlFor="banca-ubs">UBS Europe SE</label><label className="margin-l-10" htmlFor="banca-ubs">Succursale Italia, Via del Vecchio Politecnico 2, 20121 Milano</label>
                </p>
              </div>
            </div>
          </Col>
        </Row>

        <h4 className="title-subsection">Modalità di pagamento</h4>

        <Row>
          <Col s={12}><p className="padding-l-10">Mezzo di pagamento</p></Col>
          <Col s={12}>
            <Input
              name="mezzo-pagamento"
              type="checkbox"
              value="Trasferimento-titoli"
              label="Bonifico"
              className="filled-in" />
            <Input
              name="mezzo-pagamento"
              type="checkbox"
              value="assegno"
              label="Assegno bancario circolare non trasferibile"
              className="filled-in" />
          </Col>
        </Row>
        <Row className="margin-t-10">
          <Col s={6}>
            <Input s={12} label="Importo *" />
          </Col>
          <Col s={6}>
            <Input s={12} type="select" label="Origine versamento *" onChange="" value="">
              {mapDominiToOptions(getDominio(this.props.domini, 'DOM_ORIGINE_VERSAMENTO'))}
            </Input>
          </Col>
        </Row>

        <Row>
          <Col s={12}><p className="padding-l-10">Estremi assegno *</p></Col>
          <Col s={12} className="inputs-assegno">
            <Input s={2} label="ABI" />
            <Input s={2} label="CAB" />
            <Input s={4} label="Num. assegno" />
            <Input s={4} label="Num. conto" />
          </Col>
        </Row>

        <Row>
          <Col s={6} offset={'s3'}>
            <InputImage label="Foto assegno *" onChange="" />
          </Col>
        </Row>

        <Row>
          <Col s={12}><p className="padding-l-10">Mezzo di pagamento</p></Col>
          <Col s={4}>
            <Input
              name="mezzo-pagamento"
              type="checkbox"
              value="trasferimento-titoli"
              label="Trasferimento titoli"
              className="filled-in" />
          </Col>
          <Col s={8}>
            <label>
                    Gli strumenti finanziari indicati nella disposizione di trasferimento titoli da altro intermediario saranno subdepositati per conto del cliente presso il Banco Popolare Società Cooperativa, filiale Milano AG. 12 nel deposito conto terzi numero 8227864 intestato ad «AZIMUT Capital Management S.p.A. – Servizio Conto Deposito»
            </label>
          </Col>

        </Row>

        <h4 className="title-subsection">Commissioni</h4>
        <Row className="margin-b-20">
          <Col s={6}>
            <InputPlusMinus
              label="Commissione titoli azionari e diritti di opzione"
              stepValue={0.10}
              minValue={-0.50}
              maxValue={2.00}
              defaultValue={0.70}
              onChange="" />
          </Col>
          <Col s={6}>
            <InputPlusMinus
              label="Commissione obbligazionari e titoli di stato"
              stepValue={0.15}
              minValue={0}
              maxValue={1.20}
              defaultValue={0.60}
              onChange="" />
          </Col>
        </Row>

        <h4 className="title-subsection">Contatti</h4>
        <Row >
          <Col s={12}><p className="padding-l-10 margin-none">Cellulare dal quale verranno trasmessi gli ordini via telefono</p></Col>
          <Col s={2}>
            <Input value="+39" s={12} />
          </Col>
          <Col s={8}>
            <Input value="3485364287" s={12} />
          </Col>
        </Row>
        <Row >
          <Col s={12}><p className="padding-l-10 margin-b-none">Verrà utilizzato dalla SGR per comunicare le raccomandazioni formulate nell'ambito del seguente servizio</p></Col>
          <Col s={6} className="inline-checked-input-text">
            <Input
              name="cellulare-check"
              type="checkbox"
              value="cellulare-checked"
              label="-"
              className="filled-in" />
            <Input label="Cellulare" value="+39" s={4} />
            <Input value="348567923" s={8} />
          </Col>

          <Col s={6} className="inline-checked-input-text">
            <Input
              name="email-check"
              type="checkbox"
              value="email-checked"
              label="-"
              className="filled-in" />
            <Input label="Email" value="mario.rossi@azimuto.it" s={12} />
          </Col>
        </Row>
        <Row>
          <Col s={12}>
            <hr />
            <span className="indirizzo-alternativo">
              <b>Indirizzo per comunicazione:</b>{' '}
              Uguale a indirizzo di residenza
            </span>
            <a onClick={this.addResidenzaToggle} className="indirizzo-alternativo-button">
              MODIFICA
            </a>
          </Col>
        </Row>
        {/* </div> */}

        <Row className="center-align">
          <Col s={12}>
            <Button waves="light" className="margin-20" onClick="">Firma</Button>
          </Col>
        </Row>
        <Row className="center-align">
          <Link onClick="" to="#">Salva e firma dopo</Link>
        </Row>
      </CollapsibleItem>
    )
  }
}
Rto.propTypes = {
  // getServizioInfo: PropTypes.func,
  // getServiceInfoRequest: PropTypes.object,
  // dominioRequest: PropTypes.object,
  // dominio: PropTypes.func,
  domini: PropTypes.array
}
export default Rto
