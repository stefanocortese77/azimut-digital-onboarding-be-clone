import { connect } from 'react-redux'
import ConfigurazioneServiziView from '../components/ConfigurazioneServizi'
import { dominio } from '../../../../store/modules/dominio/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    dominio : (queryParam) => dispatch(dominio(queryParam))
  }
}

const mapStateToProps = (state) => ({
  dominioRequest : state.dominio.dominioRequest,
  domini: state.dominio.domini,
  pratica: {
    'idStatoPratica': 0,
    'idRegimeAppartenenza': 0,
    'servizi': [
      {
        'idServizio': '1253863_2',
        'idTipoServizio': 2
      },
      {
        'idServizio': '1253863_3',
        'idTipoServizio': 3
      },
      {
        'idServizio': '1253863_5',
        'idTipoServizio': 5
      }
    ],
    'intestatari': {
      'id': 'string',
      'firstName': 'string',
      'lastName': 'string',
      'codiceFiscale': 'string',
      'idStato': 0,
      'idRuolo': 0
    },
    'corrispondenza': {
      'nome': 'string',
      'cognome': 'string',
      'indirizzo': 'string',
      'civico': 'string',
      'cap': {
        'tipoDominio': 'string',
        'chiave': 'string'
      },
      'idComune': {
        'tipoDominio': 'string',
        'chiave': 'string'
      },
      'idProvincia': {
        'tipoDominio': 'string',
        'chiave': 'string'
      },
      'idNazione': {
        'tipoDominio': 'string',
        'chiave': 'string'
      },
      'comuneEstero': 'string'
    },
    'contatti': {
      'flagEmail': true,
      'email': 'string',
      'flagCellulare': true,
      'prefCellulare': 'string',
      'numcellulare': 'string'
    }
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfigurazioneServiziView)
