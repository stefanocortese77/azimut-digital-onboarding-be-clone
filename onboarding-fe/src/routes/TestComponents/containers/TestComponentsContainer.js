import { connect } from 'react-redux'
import TestComponents from '../components/TestComponents'

const mapDispatchToProps = {
}

const mapStateToProps = (state) => ({
  foo: state.testComponents.foo
})

export default connect(mapStateToProps, mapDispatchToProps)(TestComponents)
