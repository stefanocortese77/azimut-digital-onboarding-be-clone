/* eslint-disable */

import React from 'react'
import {
  Button,
  Card,
  Row,
  Col,
  Input,
  Chip,
  Tag,
  Modal
} from 'react-materialize'
import YesNoQuestion from '../../../components/YesNoQuestion'
import TrueFalseQuestion from '../../../components/TrueFalseQuestion'
import InputImage from '../../../components/InputImage'
import {Collapsible, CollapsibleItem} from '../../../components/Collapsible'
import {Autocomplete} from '../../../components/Autocomplete'

class TestComponents extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      collapsibleValue: 0
    }
    // this.toggle = this.toggle.bind(this)
  }

  componentWillMount() {}

  render() {

    let dataAV = {
      adeguata_verifica: {
        conoscenza_cliente: {
          input_anni: 0,
          radio_primo_contatto: {
            options: [
              {
                label: 'Nostrud incididunt labore eiusmod mollit sit ullamco tempor duis excepteur deser' +
                    'unt dolore.'
              }, {
                label: 'Ea aliqua ad esse aute ex aliqua ad eiusmod pariatur reprehenderit officia anim ' +
                    'cupidatat.'
              }, {
                label: 'Ipsum deserunt duis ipsum occaecat.'
              }
            ]
          }
        }
      }
    }

    const input_anni = dataAV.adeguata_verifica.conoscenza_cliente.input_anni
    const radio_primo_contatto = dataAV.adeguata_verifica.conoscenza_cliente.radio_primo_contatto
    const states = {
      'Albania': 'AL',
      'Algeria': null,
      'Arabia Saudita': null,
      'Armenia': null,
      'Argentina': null,
      'Austria': null,
      'Azerbaigian': null
    }

    const suggestions = [
      {
        key: 'Laborum',
        label: 'Laborum proident officia ex elit culpa aliquip minim consectetur velit est Lorem aute.',
        value: 'Laborum'
      },
      {
        key: 'Mollit',
        label: 'Mollit Lorem elit consequat consectetur ullamco adipisicing deserunt amet.',
        value: 'Mollit'
      },
      {
        key: 'Do',
        label: 'Do mollit minim duis proident cupidatat qui culpa dolore et magna consectetur.',
        value: 'Do'
      },
      {
        key: 'Minim',
        label: 'Minim occaecat aliquip adipisicing pariatur nulla dolor consequat consequat.',
        value: 'Minim'
      },
      {
        key: 'Culpa',
        label: 'Culpa nostrud quis culpa anim.',
        value: 'Culpa'
      },
      {
        key: 'Sunt',
        label: 'Sunt duis enim aliqua cillum.',
        value: 'Sunt'
      },
      {
        key: 'Aliquip',
        label: 'Aliquip laboris eiusmod consequat id quis officia labore do enim reprehenderit id culpa quis consequat.',
        value: 'Aliquip'
      },
      {
        key: 'Ea',
        label: 'Ea enim aute pariatur deserunt anim duis occaecat qui commodo commodo ut magna exercitation eiusmod.',
        value: 'Ea'
      },
      {
        key: 'Fugiat',
        label: 'Fugiat pariatur officia in adipisicing ipsum magna enim elit nulla duis aliqua.',
        value: 'Fugiat'
      },
      {
        key: 'Veniam',
        label: 'Veniam non ea irure consequat voluptate cupidatat.',
        value: 'Veniam'
      },
      {
        key: 'Minim',
        label: 'Minim anim culpa aliqua deserunt aute velit proident amet occaecat.',
        value: 'Minim'
      },
      {
        key: 'Sit',
        label: 'Sit non cupidatat veniam nisi ea mollit aliqua excepteur nisi velit cillum incididunt pariatur nisi.',
        value: 'Sit'
      },
      {
        key: 'Esse',
        label: 'Esse laboris aliqua elit laborum.',
        value: 'Esse'
      },
      {
        key: 'Fugiat',
        label: 'Fugiat dolore mollit veniam quis officia voluptate irure velit pariatur nulla fugiat.',
        value: 'Fugiat'
      },
      {
        key: 'Incididunt',
        label: 'Incididunt commodo aute amet qui.',
        value: 'Incididunt'
      },
      {
        key: 'Nulla',
        label: 'Nulla fugiat magna veniam incididunt voluptate aliqua adipisicing dolor ut minim in.',
        value: 'Nulla'
      },
      {
        key: 'Exercitation',
        label: 'Exercitation consectetur sint do elit deserunt excepteur non nulla.',
        value: 'Exercitation'
      },
      {
        key: 'Labore',
        label: 'Labore eu do aliquip et.',
        value: 'Labore'
      },
      {
        key: 'Duis',
        label: 'Duis laboris id irure reprehenderit voluptate.',
        value: 'Duis'
      },
      {
        key: 'Occaecat',
        label: 'Occaecat ea nulla minim aliqua veniam mollit aliqua reprehenderit est laboris esse quis.',
        value: 'Occaecat'
      },
      {
        key: 'Adipisicing',
        label: 'Adipisicing deserunt do esse exercitation deserunt consequat aliquip ipsum reprehenderit cillum nulla nisi duis exercitation.',
        value: 'Adipisicing'
      },
      {
        key: 'Id',
        label: 'Id esse eiusmod dolor labore quis deserunt quis aliquip quis exercitation irure dolor.',
        value: 'Id'
      },
      {
        key: 'Aute',
        label: 'Aute aliquip consequat nostrud nostrud exercitation occaecat et enim in id dolore qui.',
        value: 'Aute'
      },
    ]

    return (
      <div>
        <h4>ONBOARDING FORM ELEMENTS!</h4>

        <h5>INPUTS</h5>
        <p>{input_anni.title}</p>
        <Row>
          <Col s={3}><Input label={input_anni.label}/></Col>
          <Col s={9}><Input s={12} placeholder="Sarà uno slider"/></Col>
        </Row>

        <Row>
          <Col s={3}><Input s={12} label="disabled" disabled/></Col>
          <Col s={3}><Input s={12} placeholder="Qui un placeholder"/></Col>
          <Col s={3}><Input s={12} label="campo con suggerimento"/>
            <span className="suggestion-for-input">gg / mm / aaaa</span>
          </Col>
        </Row>
        <Row>
          <Col s={6}><Input
            s={12}
            label="campo con errore"
            validate
            error="input errore"
            success="ok boss"/></Col>
          <Col s={6}><Input s={12} label="Password" validate error="password errore" type="email"/>
            <span className="suggestion-for-input" data-error="wrong" data-success="right">Helper text</span>
          </Col>
        </Row>

        <Row>
          <Col s={6}><Input s={12} label="campo con errore"/>
            <span className="helper-text" data-error="wrong" data-success="right">Helper text</span>
          </Col>
          <Col s={6}><Input s={12} label="campo con suggerimento"/>
            <span className="suggestion-for-input">gg / mm / aaaa</span>
          </Col>
        </Row>
        <Row>
          <Input s={6} type="select" label="Tipologia documento *">
            <option value="1">Carta d'identità</option>
            <option value="2">Passaporto</option>
            <option value="3">Patente</option>
          </Input>
          <Autocomplete
            title="Nazione Autocomplete"
            data={states}
            minLength={2}
            limit={4}
            icon="search"
            defaultValue="al"
            suggestions={suggestions}/>
          <Autocomplete
            title="Nazione Autocomplete"
            data={states}
            minLength={2}
            limit={4}
            icon="search"
            defaultValue="al"
            suggestions={suggestions}
            value={this.state.autocompleteValue}
            onChange={(value) => this.setState({autocompleteValue: value})}
            onSelect={(selection) => this.setState({autoSelected: selection})}
          />
          <Autocomplete
            title="Nazione Autocomplete"
            data={states}
            minLength={2}
            limit={4}
            icon="search"
            defaultValue="al"
            suggestions={suggestions}
            disabled
          />

            <p>Selected: <pre>{ this.state.autoSelected? JSON.stringify(this.state.autoSelected) : '' }</pre></p>
        </Row>
        <Row/>
        <h5>IMAGE INPUT</h5>
        <Row>
          <Col s={3}><InputImage label="FRONTE documento*"/></Col>
          <Col s={3} offset={'s1'}><InputImage label="RETRO documento*"/></Col>
        </Row>

        <Modal header="Modal Header" trigger={< Button > MODAL </Button>}>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est laborum</p>
        </Modal>

        <h5>YES NO QUESTION</h5>
        <YesNoQuestion
          label="Testo domanda testo domanda Testo domanda testo domandaTesto domanda testo domanda Testo domanda testo domanda Testo domanda testo domanda"/>
        <YesNoQuestion
          label="Testo domanda testo domanda Testo domanda testo domandaTesto domanda testo domanda Testo domanda testo domanda Testo domanda testo domanda"/>

        <h5>TRUE FALSE QUESTION</h5>
        <TrueFalseQuestion
          title="Titolo Strumento"
          subtitle="Testo domanda testo domanda Testo domanda testo domandaTesto domanda testo domanda Testo domanda testo domanda Testo domanda testo domanda"/>
        <TrueFalseQuestion
          title="Titolo Strumento"
          subtitle="Testo domanda testo domanda Testo domanda testo domandaTesto domanda testo domanda Testo domanda testo domanda Testo domanda testo domanda"/>

        <h5>CHECKBOX</h5>
        <Row>
          <Col s={6}>
            <h6>horizontal</h6>
            <Input
              name="group1-vertical"
              type="checkbox"
              value="green"
              label="Green"
              className="filled-in"/>
            <Input
              name="group1-vertical"
              type="checkbox"
              value="green1"
              label="Green1"
              className="filled-in"/>
            <Input
              name="group1-vertical"
              type="checkbox"
              value="green2"
              label="Green2"
              className="filled-in"
              defaultChecked="checked"/>
            <Input
              name="group1-vertical"
              type="checkbox"
              value="brown"
              label="Brown"
              disabled="disabled"/>
          </Col>
          <Col s={6} className="check-vertical">
            <h6>vertical</h6>
            <Row>
              <Input
                name="group2-horizontal"
                type="checkbox"
                value="green"
                label="Green"
                className="filled-in vertical"/>
            </Row>
            <Row>
              <Input
                name="group2-horizontal"
                type="checkbox"
                value="green"
                label="Green"
                className="filled-in vertical"/>
            </Row>
          </Col>
        </Row>

        <h5>RADIO</h5>
        <p>Radio inline</p>
        <form action="#" className="radio-horizontal">
          <p>
            <input
              name={radio_primo_contatto.name}
              type={radio_primo_contatto.type}
              id="test1"
              className="with-gap"/>
            <label htmlFor="test1">{radio_primo_contatto.options[0].label}</label>
          </p>
          <p>
            <input
              name={radio_primo_contatto.name}
              type={radio_primo_contatto.type}
              id="test2"
              className="with-gap"/>
            <label htmlFor="test2">{radio_primo_contatto.options[1].label}</label>
          </p>
          <p>
            <input
              name={radio_primo_contatto.name}
              type={radio_primo_contatto.type}
              id="test3"
              className="with-gap"/>
            <label htmlFor="test3">{radio_primo_contatto.options[2].label}</label>
          </p>
        </form>

        <p>Radio vertical</p>
        <form action="#" className="radio-vertical">
          <p>
            <input name="radio-vert" type="radio" id="test1a" className="with-gap"/>
            <label htmlFor="test1a">{radio_primo_contatto.options[0].label}</label>
          </p>
          <p>
            <input name="radio-vert" type="radio" id="test2a" className="with-gap"/>
            <label htmlFor="test2a">{radio_primo_contatto.options[1].label}</label>
          </p>
          <p>
            <input name="radio-vert" type="radio" id="test3a" className="with-gap"/>
            <label htmlFor="test3a">{radio_primo_contatto.options[2].label}</label>
          </p>
        </form>

        <h5>ICONS</h5>
        <Row>
          <h4>
            <i className="icon-plus"/>
            <i className="icon-close"/>
            <i className="icon-accordion"/>
            <i className="icon-icona-per-ricerca-chiusa"/>
            <i className="icon-arrow"/>
            <i className="icon-meno"/>
            <i className="icon-doc-spalla-dx"/>
            <i className="icon-prodotti-action-bar"/>
            <i className="icon-doc"/>
            <i className="icon-alert-per-icona-non-completa-spalla-dx"/>
            <i className="icon-check-per-icona-doc-spalla-dx"/>
            <i className="icon-check-per-bottone-outline"/>
            <i className="icon-check-codice-fiscale-success"/>
            <i className="icon-calendar"/>
            <i className="icon-cerca"/>
            <i className="icon-compilazione"/>
            <i className="icon-password"/>
            <i className="icon-camera"/>
            <i className="icon-login"/>
            <i className="icon-user"/>
            <i className="icon-barcode"/>
            <i className="icon-document-pdf"/>
          </h4>
        </Row>

        <h5>BUTTONS</h5>
        <Button waves="light">Primary</Button>
        <Button flat waves="light">Flat button</Button>
        <Button flat disabled waves="light">Disabled</Button>
        <Button waves="light">Iconed
          <i className="icon-arrow"/></Button>

        <h5>ACCORDION</h5>

        <Collapsible
          value={this.state.collapsibleValue}
          onChange={(v) => this.setState({collapsibleValue: v})}>
          <CollapsibleItem header="Codice fiscale" icon="keyboard_arrow_down">
            <Row>
              <Col s={6}>
                <Card
                  className="corporate-blue lighten-1"
                  textClassName="white-text"
                  title="Card title"
                  actions={[ < a href = "#" > This is a link </a>]}>
                  I am a very simple card.
                </Card>
              </Col>
            </Row>
          </CollapsibleItem>
          <CollapsibleItem header="Documenti d'identità" icon="keyboard_arrow_down">
            <Row>
              <Input s={6} type="select" label="Tipologia documento *">
                <option value="1">Carta d'identità</option>
                <option value="2">Passaporto</option>
                <option value="3">Patente</option>
              </Input>
              <Input s={6} label="Numero documento *"/>
            </Row>
            <Row className="right-align">
              <Button flat waves="light">Flat button</Button>
            </Row>
          </CollapsibleItem>
          <CollapsibleItem header="Third" icon="keyboard_arrow_down">
            Lorem ipsum dolor sit amet.
          </CollapsibleItem>
        </Collapsible>
      </div>
    )

    return (
      <div></div>
    )
  }
}

export default TestComponents
