import React from 'react'
import PropTypes from 'prop-types'
import { Input, Button } from 'react-materialize'

class OtpVerify extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      n0: null,
      n1: null,
      n2: null,
      n3: null
    }
    this.verifica = this.verifica.bind(this)
    this.invia = this.invia.bind(this)
  }
  verifica () {
    this.props.verifica({
      otp: `${this.state.n0}${this.state.n1}${this.state.n2}${this.state.n3}`
    })
  }
  componentDidUpdate (prevProps) {
    // check se sono terminate le chiamate per popolare i campi, setta i default dei campi se esistono nella get
  }
  invia () {
    // eslint-disable-next-line
    this.props.invia({ value: `${this.props.prefisso}${this.props.value}` })
  }
  render () {
    if (!this.props.hide) {
      return null
    }
    let btnVerifica
    if (
      this.state.n0 === null ||
      this.state.n1 === null ||
      this.state.n2 === null ||
      this.state.n3 === null ||
      this.state.n0 === '' ||
      this.state.n1 === '' ||
      this.state.n2 === '' ||
      this.state.n3 === ''
    ) {
      btnVerifica = (
        <Button disabled waves="light" onClick={() => this.verifica()}>
          verifica
        </Button>
      )
    } else {
      btnVerifica = (
        <Button waves="light" onClick={() => this.verifica()}>
          verifica
        </Button>
      )
    }
    return (
      <div className="otpVerify-modal">
        <div className="otpVerify-container">
          <div className="otpVerify-close">
            <i className="icon-plus" onClick={this.props.callBack} />
          </div>
          <div className="otpVerify-content">
            <h6>Certifica {this.props.labelTitle}</h6>
            <p>Inserisci codice PIN ricevuto al numero indicato</p>
            <div className="otpVerify-body">
              <Input
                maxlength="1"
                value={this.state.n0}
                onChange={({ target: { value } }) => {
                  var regExpr = new RegExp('^[0-9]$')
                  if (!regExpr.test(value)) {
                    // Case of error
                    this.setState({ n0: '' })
                  } else {
                    this.setState({ n0: value })
                  }
                }}
              />

              <Input
                maxlength="1"
                value={this.state.n1}
                onChange={({ target: { value } }) => {
                  var regExpr = new RegExp('^[0-9]$')
                  if (!regExpr.test(value)) {
                    // Case of error
                    this.setState({ n1: '' })
                  } else {
                    this.setState({ n1: value })
                  }
                }}
              />
              <Input
                maxlength="1"
                value={this.state.n2}
                onChange={({ target: { value } }) => {
                  var regExpr = new RegExp('^[0-9]$')
                  if (!regExpr.test(value)) {
                    // Case of error
                    this.setState({ n2: '' })
                  } else {
                    this.setState({ n2: value })
                  }
                }}
              />
              <Input
                maxlength="1"
                value={this.state.n3}
                onChange={({ target: { value } }) => {
                  var regExpr = new RegExp('^[0-9]$')
                  if (!regExpr.test(value)) {
                    // Case of error
                    this.setState({ n3: '' })
                  } else {
                    this.setState({ n3: value })
                  }
                }}
              />
            </div>
          </div>
          <div className="otpVerify-footer">
            <Button flat waves="light" onClick={() => this.invia()}>
              invia di nuovo
            </Button>
            {btnVerifica}
          </div>
        </div>
      </div>
    )
  }
}

OtpVerify.propTypes = {
  verifica: PropTypes.func,
  invia: PropTypes.func,
  prefisso: PropTypes.string,
  hide: PropTypes.bool,
  callBack: PropTypes.func,
  labelTitle: PropTypes.string
}

export default OtpVerify
