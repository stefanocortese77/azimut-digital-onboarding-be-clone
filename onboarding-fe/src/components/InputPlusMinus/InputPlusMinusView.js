import React from 'react'
import PropTypes from 'prop-types'

export class InputPlusMinus extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 0,
      stepValue: 0.1
    }
    this.handleOnChange = this.handleOnChange.bind(this)
    this.stepPlus = this.stepPlus.bind(this)
    this.stepMinus = this.stepMinus.bind(this)
  }

  componentDidMount () {
    if (this.props.defaultValue) {
      this.setState({ value:this.props.defaultValue.toFixed(2) })
    }
  }

  handleOnChange (e) {
    console.log(e.target.value)
    // const newValue = parseFloat(e.target.value)
    // this.setState({ value:newValue })
  }

  stepPlus () {
    const newValue = (parseFloat(this.state.value) + this.props.stepValue).toFixed(2)
    this.setState({ value: newValue })
    if (this.props.callback) {
      this.props.callback(newValue)
    }
  }

  stepMinus () {
    const newValue = (parseFloat(this.state.value) - this.props.stepValue).toFixed(2)
    this.setState({ value: newValue })
  }

  render () {
    return (
      <div>
        <p s={12} className="label-plus-minus">{this.props.label}</p>
        <div s={12} className="box-input-plus-minus">
          <button
            className="button-step button-minus"
            disabled={this.state.value <= this.props.minValue}
            onClick={this.stepMinus}>-</button>
          <input type="text" value={this.state.value + '%'} onChange={this.handleOnChange} />
          <button
            className="button-step button-plus"
            disabled={this.state.value >= this.props.maxValue}
            onClick={this.stepPlus}>+</button>
        </div>
      </div>
    )
  }
}

InputPlusMinus.propTypes = {
  label: PropTypes.string,
  stepValue : PropTypes.number,
  defaultValue : PropTypes.number,
  minValue : PropTypes.number,
  maxValue : PropTypes.number,
  callback: PropTypes.function
}
export default InputPlusMinus
