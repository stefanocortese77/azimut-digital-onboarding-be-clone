import Loading from './LoadingView'
import { setDataLoading } from '../../store/modules/dataLoading/actions'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch) => {
  return {
    setDataLoading : (value) => dispatch(setDataLoading(value))
  }
}

const mapStateToProps = (state) => {
  return {
    dataLoading: state.dataLoading.dataLoadingState
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loading)
