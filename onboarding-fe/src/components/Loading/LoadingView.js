import React from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'

export class Loading extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: ''
    }
    this.toggle = this.toggle.bind(this)
    this.setCall = this.setCall.bind(this)
    this.setStateLoading = this.setStateLoading.bind(this)
  }
  toggle () {
    this.setState({ active: !this.state.active })
  }
  setCall () {
    let context = this
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      console.log('init call')
      context.props.setDataLoading('fixedloading spinner')
     // this.setStateLoading('fixedloading')
     // this.setState({ loading: 'fixedloading' })

      // this.props.setDataLoading(true)
      return config
    }, function (error) {
      // context.setStateLoading('spinner')
      context.props.setDataLoading('spinner')
      // Do something with request error
     // this.setState({ loading: '' })
      return Promise.reject(error)
    })

    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      console.log('end call')
      context.props.setDataLoading('spinner')
      return response
    }, function (error) {
      console.log('end error')
      context.props.setDataLoading('spinner')


      // context.setStateLoading('spinner')
      // this.setState({ loading: '' })
      // Do something with response error
      return Promise.reject(error)
    })
  }
  componentDidMount () {
    this.setCall()
  }
  setStateLoading (value) {
    this.setState({ loading: value })
  }
  render () {
    return (
      <div className={this.props.dataLoading}>
        <div className="loader-inner ball-spin-fade-loader">
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    )
  }
}
Loading.propTypes = {
  dataLoading: PropTypes.string
}
export default Loading
