import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-rangeslider'
import { Row, Col } from 'react-materialize'

class SliderComponent extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = {
      value: 1
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleInputBlur = this.handleInputBlur.bind(this)
    this.handleChangeStart = this.handleChangeStart.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeComplete = this.handleChangeComplete.bind(this)
  }

  componentDidMount () {
    if (this.props.defaultValue) this.setState({ value: this.props.defaultValue })
  }

  handleInputChange (e) {
    this.setState({ value: e.target.value })
  };

  handleInputBlur (e) {
    let newValue = e.target.value
    if (newValue > this.props.max) {
      this.setState({ value: this.props.max })
    } else if (newValue < this.props.min) {
      this.setState({ value: this.props.min })
    }
  };

  handleChangeStart () {
    console.log('Change event started')
  };

  handleChange (value) {
    this.setState({ value: value })
  };

  handleChangeComplete () {
    console.log('Change event completed')
  };

  render () {
    const { value } = this.state
    return (
      <div className="slider">
        <h4 className="title-slider">{this.props.title}</h4>
        <Row>
          <Col s={2}>
            <input type="number" value={value} onChange={this.handleInputChange} onBlur={this.handleInputBlur} />
          </Col>
          <Col s={10}>
            <Slider
              min={this.props.min}
              max={this.props.max}
              defaultValue={this.props.defaultValue}
              value={value}
              onChangeStart={this.handleChangeStart}
              onChange={this.handleChange}
              onChangeComplete={this.handleChangeComplete} />
          </Col>
        </Row>
      </div>)
  }
}

SliderComponent.propTypes = {
  title: PropTypes.string,
  defaultValue: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number
}

export default SliderComponent
