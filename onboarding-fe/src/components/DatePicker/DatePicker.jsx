import React from 'react'
import { SingleDatePicker } from 'react-dates'
import { Icon } from 'react-materialize'
import { PropTypes } from 'prop-types'
import 'react-dates/lib/css/_datepicker.css'
import moment from 'moment'

/**
 * This replaces the monnth header to show a year selection menu
 * when the user clicks on the date. As f 31/05/18 there is no
 * way to do it simply using the react-dates API.
 */
class SelectYearMonthCaption extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      yearMenuOpen: false
    }

    this._handleYearSelect = this._handleYearSelect.bind(this)
    this._handleDateClick = this._handleDateClick.bind(this)
  }

  _handleYearSelect (year) {
    this.props.changeYear(moment().set('year', year))
    this.setState({ yearMenuOpen: !this.state.yearMenuOpen })
  }

  _handleDateClick () {
    this.setState({ yearMenuOpen: true })
  }

  render () {
    const { month } = this.props
    const currentYear = month.year()

    let yearMenu = null

    // Year menu opens only if the range validation is defined (to only show valid years)
    if (this.state.yearMenuOpen && this.props.isOutsideRange) {
      const years = []

      for (let i = currentYear + 120; i > currentYear - 120; i--) {
        if (!this.props.isOutsideRange(moment().set('year', i))) {
          years.push((
            <div key={i} onClick={() => this._handleYearSelect(i)}>
              {i}
            </div>
          ))
        }
      }
      yearMenu = (
        <div className="years-panel">
          {years}
        </div>
      )
    }

    return (
      <div className="CalendarMonth_caption CalendarMonth_caption_1 year-select-month-caption">
        <strong>{moment(month).format('MMMM ')}</strong>
        <strong>
          <span onClick={this._handleDateClick}>
            {moment(month).format('YYYY')}
          </span>
          { yearMenu }
        </strong>
      </div>
    )
  }
}

SelectYearMonthCaption.propTypes = {
  changeYear: PropTypes.func,
  month: PropTypes.object,
  isOutsideRange: PropTypes.func
}


class DatePicker extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      focused: false,
      date: (props.defaultDate) ? moment(props.defaultDate) : null,
      selectedYear: 0,
      refocus: false
    }

    this._handleChangeYear = this._handleChangeYear.bind(this)
    this.initialVisibleMonth = this.initialVisibleMonth.bind(this)
    this._handleChange = this._handleChange.bind(this)
  }

  componentDidUpdate () {
    // To refresh the date in the menu this workaround is needed (as of 31/05/18:
    // the menu must be closed and then reopened
    if (this.state.refocus) {
      this.setState({
        focused: true,
        refocus: false
      })
    }
  }

  _handleChange (date) {
    this.setState({ date: date })
    if (this.props.onDateChange) {
      this.props.onDateChange(date)
    }
  }

  /**
   * Sets the year chosen in the year select menu
   */
  _handleChangeYear (year) {
    let currentDate = moment()

    if (this.state.date) {
      currentDate = moment(this.state.date)
    }

    this.setState({
      date: currentDate.set('year', year.get('year')),
      focused: false,
      refocus: true
    })
  }

  /**
   * Returns the default month shown when the menu opens.
   * If a date is already selected in the text box, this is
   * the month to show. Otherwise, show the one passed from
   * the props
   */
  initialVisibleMonth () {
    if (this.state.date) {
      return this.state.date
    }

    if (this.props.initialVisibleMonth) {
      return this.props.initialVisibleMonth()
    }

    return moment()
  }

  render () {
    let labelContent = ''

    if (this.props.label) {
      labelContent = <p className="date-picker-label">{this.props.label}</p>
    }

    const className = 'date-picker ' +
      (this.props.s ? ('col input-field s' + this.props.s + ' ') : '') +
      (this.state.focused ? 'active ' : '') +
      (this.props.alignLeft ? 'align-left ' : '')

    return (
      <div className={className}>
        <SingleDatePicker
          placeholder={this.props.placeholder}
          openDirection={this.props.openDirection}
          anchorDirection={this.props.anchorDirection}
          date={this.state.date} // momentPropTypes.momentObj or null
          onDateChange={date => this._handleChange(date)} // PropTypes.func.isRequired
          focused={this.state.focused} // PropTypes.bool
          onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
          numberOfMonths={1}
          showDefaultInputIcon
          customInputIcon={<Icon className="prefix">date_range</Icon>}
          disabled={this.props.disabled || false}
          isOutsideRange={this.props.isOutsideRange}
          initialVisibleMonth={this.initialVisibleMonth}
          readOnly={this.props.readOnly}
          renderMonth={(month) => (
            <SelectYearMonthCaption
              month={month}
              changeYear={this._handleChangeYear}
              isOutsideRange={this.props.isOutsideRange}
            />
          )}
         />
        {labelContent}
      </div>
    )
  }
}

DatePicker.propTypes = {
  alignLeft: PropTypes.bool,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  onDateChange: PropTypes.func,
  openDirection: PropTypes.string,
  anchorDirection: PropTypes.string,
  disabled: PropTypes.bool,
  s: PropTypes.number,
  isOutsideRange: PropTypes.func,
  initialVisibleMonth: PropTypes.func,
  readOnly: PropTypes.bool,
  defaultDate: PropTypes.object
}

export default DatePicker
