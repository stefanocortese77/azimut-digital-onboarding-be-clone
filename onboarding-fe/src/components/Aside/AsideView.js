import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import ModalePopup from '../ModalePopup'

export class Aside extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      asideDataReady: false,
      showModal:false,
      nextStepPath:''
    }
    this.nextStepNoSave = this.nextStepNoSave.bind(this)
    this.checkChangeStep = this.checkChangeStep.bind(this)
  }

  componentDidMount () {
  }

  componentDidUpdate (prevProps) {
    // check se sono terminate le chiamate e se sono recuperati i dati per la side bar
    if (prevProps.getDataSummaryRequest.fetching && this.props.getDataSummaryRequest.fetching === false) {
      // eslint-disable-next-line
      this.setState({ asideDataReady: true })
    }
  }

  nextStepNoSave (path) {
    this.props.history.push(path)
    this.props.setDataPending(false)
    this.setState({ showModal:false })
  }

  checkChangeStep (path) {
    // verifica di dataPending non ancora salvati
    if (this.props.dataPendingState === true) {
      this.setState({ nextStepPath:path, showModal:true })
    } else {
      this.props.history.push(path)
    }
  }

  render () {
    if (this.state.asideDataReady === false) { return false }
    if (this.props.summaryData === null) { return false }


    const percentageValue = this.props.summaryData.percentuale + '%'

    let infoCliente = <div>
      <p className="label-user">Codice Fiscale</p>
      <h4 className="value-cf">{this.props.customer.codiceFiscale}</h4>
    </div>

    if (this.props.customer.nome != null && this.props.customer.cognome != null) {
      infoCliente = <div>
        <p className="label-user">Nome Cognome</p>
        <h4 className="value-user">{this.props.customer.nome + ' ' + this.props.customer.cognome}</h4>
        <p className="value-cf-small">{this.props.customer.codiceFiscale}</p>
      </div>
    }

    let hideRiepilogo = ''


    // ABILITARE QUANDO FUNZIONERANNO I SERVIZI
    // this.props.summaryData.sezioni.forEach(function (item) {
    //   if (item.codSezione !== 'RIEPILOGO' && item.stato !== 'COMPLETATA') {
    //     hideRiepilogo = 'hiding-riepilogo'
    //   }
    // })


    return (
      <aside className="aside navigation">
        <ModalePopup
          visible={this.state.showModal}
          title="Attenzione!"
          text="Le modifiche non sono state salvate. Se prosegui perderai i dati inseriti"
          primaryActionLabel="Continua"
          secondaryActionLabel="Annulla"
          secondaryAction={() => this.setState({ showModal: false })}
          primaryAction={() => this.nextStepNoSave(this.state.nextStepPath)}
        />
        <div className="info-cliente">
          <i className="icon-user" />
          {infoCliente}
        </div>
        <div className="progress-info">
          <p className="percentage-value">{percentageValue}</p>
          <div className="progress-bar-background">
            <div className="progress-bar-complete" style={{ width: percentageValue }} />
          </div>
        </div>
        <ul>
          <li><Link onClick={() => this.checkChangeStep('/consulenza-base/acquisizione-documenti')} to="#" className={'navigation-item stato-' + this.props.summaryData.sezioni[0].stato} ><i className="icon-doc" />{this.props.summaryData.sezioni[0].nomeSezione}</Link></li>
          <li><Link onClick={() => this.checkChangeStep('/consulenza-base/censimento-anagrafico')} to="#" className={'navigation-item stato-' + this.props.summaryData.sezioni[1].stato} ><i className="icon-doc" />{this.props.summaryData.sezioni[1].nomeSezione}</Link></li>
          <li><Link onClick={() => this.checkChangeStep('/consulenza-base/questionario-profilatura')} to="#" className={'navigation-item stato-' + this.props.summaryData.sezioni[2].stato} ><i className="icon-doc" />{this.props.summaryData.sezioni[2].nomeSezione}</Link></li>
          <li><Link onClick={() => this.checkChangeStep('/consulenza-base/sottoscrizione-servizi')} to="#" className={'navigation-item stato-' + this.props.summaryData.sezioni[3].stato} ><i className="icon-doc" />{this.props.summaryData.sezioni[3].nomeSezione}</Link></li>
          <li><Link onClick={() => this.checkChangeStep('/consulenza-base/riepilogo-cliente')} to="#" className={hideRiepilogo + ' navigation-item stato-' + this.props.summaryData.sezioni[4].stato} ><i className="icon-doc" />{this.props.summaryData.sezioni[4].nomeSezione}</Link></li>
        </ul>
      </aside>
    )
  }
}

Aside.propTypes = {
  history: PropTypes.object,
  customer : PropTypes.object,
  summaryData: PropTypes.object,
  getDataSummaryRequest: PropTypes.object,
  dataPendingState: PropTypes.bool,
  setDataPending: PropTypes.func
}
export default Aside
