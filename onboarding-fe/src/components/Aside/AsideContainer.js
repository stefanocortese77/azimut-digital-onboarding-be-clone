import Aside from './AsideView'
import { connect } from 'react-redux'
import { getDataSummaryLight } from '../../store/modules/customers/actions'
import { setDataPending } from '../../store/modules/dataPending/actions'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente) => dispatch(getDataSummaryLight(idCliente)),
    setDataPending: (boolean) => dispatch(setDataPending(boolean))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer,
    getDataSummaryRequest: state.customers.getDataSummaryRequest,
    summaryData: state.customers.summaryData,
    dataPendingState: state.dataPendings.dataPendingState
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Aside)
