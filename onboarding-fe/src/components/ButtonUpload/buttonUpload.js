
 import React from 'react'
 import PropTypes from 'prop-types'
 import { Link } from 'react-router-dom'
 import { Navbar, NavItem } from 'react-materialize'

 export const Aside = () => (
  <i className='icon-camera' />
)
 Aside.propTypes = {
  children: PropTypes.node,
}
 export default Aside
