import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Navbar, NavItem } from 'react-materialize'
export const ButtoUpload = () => (
  <i className='icon-camera' />
)
ButtoUpload.propTypes = {
  children: PropTypes.node,
}
export default ButtoUpload
