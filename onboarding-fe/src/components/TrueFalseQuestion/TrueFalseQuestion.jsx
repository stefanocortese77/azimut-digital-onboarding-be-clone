import React from 'react'
import PropTypes from 'prop-types'

const TrueFalseQuestion = ({ label, title, subTitle, onChange, value, disabled = false }) => {
  let yesNoClassName = 'box-input'

  const onChangeFn = (value) => {
    if (!disabled) {
      if (onChange) {
        onChange(value)
      }
    }
  }

  if (value) {
    yesNoClassName += ' yes-selected'
  } else if (value === false) {
    yesNoClassName += ' no-selected'
  }

  let labelElement = null
  if (label) {
    labelElement = (
      <div>
        <p>{label}</p>
      </div>
    )
  }

  let titleElement = null
  if (title) {
    titleElement = (
      <p className="title">{title}</p>
    )
  }

  let subTitleElement = null
  if (subTitle) {
    subTitleElement = (
      <p className="subtitle">{subTitle}</p>
    )
  }

  return (
    <div className={`true-false-question ${disabled ? 'disabled' : ''}`}>
      { titleElement }
      { subTitleElement }
      { labelElement }
      <div className={yesNoClassName}>
        <button className="yes-button" onClick={() => onChangeFn(true)}>V</button>
        <button className="no-button" onClick={() => onChangeFn(false)}>F</button>
      </div>
    </div>
  )
}

TrueFalseQuestion.propTypes = {
  disabled: PropTypes.bool,
  label: PropTypes.string,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.bool
}

export default TrueFalseQuestion
