import ModalePopup from './ModalePopupView'
import { connect } from 'react-redux'
import { getDataSummaryLight } from '../../store/modules/customers'

const mapDispatchToProps = (dispatch) => {
  return {
    getDataSummaryLight : (idCliente) => dispatch(getDataSummaryLight(idCliente))
  }
}

const mapStateToProps = (state) => {
  return {
    customer: state.customers.customer
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalePopup)
