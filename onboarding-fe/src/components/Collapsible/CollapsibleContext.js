import React from 'react'

export const CollapsibleContext = React.createContext({
  openItem: null,
  onChange: () => {},
  autoToggle: false
})
