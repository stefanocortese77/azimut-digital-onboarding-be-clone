import React from 'react'
import PropTypes from 'prop-types'
import { Autocomplete } from 'react-materialize'

export class AutocompleteCustom extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: '',
      error:false
    }
    this.autocompleteInput = React.createRef()
    this.handleOnBlur = this.handleOnBlur.bind(this)
    this.handleOnAutocomplete = this.handleOnAutocomplete.bind(this)
    this.disableDependentInput = this.disableDependentInput.bind(this)
    this.setDisabled = this.setDisabled.bind(this)
  }

  componentDidMount () {
    this.setDisabled()
  }

  componentDidUpdate () {
    this.setDisabled()
  }

  setDisabled () {
    let elem = this.autocompleteInput.current.querySelectorAll('input')
    elem[0].disabled = this.props.disabled
  }

  disableDependentInput (value) {
    // Return true SE IL VALORE DIGITATO FA PARTE DEL DATA, SE COMBACIA IN MODO CORRETTO CON UNO DEI VALORI
    let disableDependentInput = true

    let dataArray = Object.keys(this.props.data)
    for (var i = 0; i < dataArray.length; i++) {
      // check case insensitive
      if (value.toLowerCase() === dataArray[i].toLowerCase()) { disableDependentInput = false }
    }

    if (!disableDependentInput) {
      this.setState({ error:true })
    }

    return disableDependentInput
  }

  handleOnAutocomplete (valueComplete) {
    let disable = this.disableDependentInput(valueComplete)
    if (this.props.callback) {
      this.props.callback([valueComplete])
    }

    if (this.props.onAutocomplete) { this.props.onAutocomplete(disable, valueComplete) }
  }

  handleOnBlur () {
    let disable = this.disableDependentInput(this.state.value)

    if (this.props.onBlur) { this.props.onBlur(disable, this.state.value) }
  }

  handleOnChange (ev, value) {
    this.setState({ value: value })
    if (this.props.onChange) { this.props.onChange(ev, value) }
  }

  render () {
    let errorSpan = null
    let classCustom = 'autocomplete-custom'

    let showError = true
    let dataArray = Object.keys(this.props.data)

    for (var i = 0; i < dataArray.length; i++) {
      if (dataArray[i].toLowerCase().indexOf(this.state.value.toLowerCase()) >= 0) { showError = false }
    }

    if (showError) {
      errorSpan = (
        <span className="error-label">Nessuna corrispondenza</span>
      )
      classCustom += ' error-input-custom'
    }

    return (
      <div ref={this.autocompleteInput} className={classCustom}>
        <Autocomplete
          minLength={1}
          limit={4}
          icon="search"
          {...this.props}
          onChange={(e, value) => this.handleOnChange(e, value)}
          onBlur={this.handleOnBlur}
          onAutocomplete={this.handleOnAutocomplete}
        />
        {errorSpan}
      </div>
    )
  }
}
AutocompleteCustom.propTypes = {
  data: PropTypes.object,
  callback: PropTypes.func,
  children: PropTypes.node,
  disabled: PropTypes.bool,
  onAutocomplete: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
}
export default AutocompleteCustom
