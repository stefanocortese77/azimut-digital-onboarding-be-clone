import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'


import ConsulenzaBaseContainerLayout from '../routes/ConsulenzaBaseContainerLayout/ConsulenzaBaseContainerLayout'
import { Home } from '../routes/Home'
// import { MaxContainerLayout } from '../routes/MaxContainerLayout/MaxContainerLayout'
// import { MaxFundContainerLayout } from '../routes/MaxFundContainerLayout/MaxFundContainerLayout'
// import { RtoContainerLayout } from '../routes/RtoContainerLayout/RtoContainerLayout'
import { AperturaServiziContainerLayout } from '../routes/AperturaServiziContainerLayout/AperturaServiziContainerLayout'
import { AdeguataVerifica } from '../routes/AdeguataVerifica'
import TestComponents from '../routes/TestComponents/components/TestComponents'
import { Firma } from '../routes/Firma'
import { FirmaWrapper } from '../routes/FirmaWrapper'
import Loading from '../components/Loading'
import ModalError from '../components/ModalError'


export class App extends React.Component {
  shouldComponentUpdate () {
    return false
  }
  componentDidMount () {

  }

  render () {
    return (
      <Provider store={this.props.store}>

        <div style={{ height: '100%' }}>
          <Loading />
          <ModalError />
          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/consulenza-base" component={ConsulenzaBaseContainerLayout} />
              {/* <Route path="/max" component={MaxContainerLayout} /> */}
              {/* <Route path="/max-fund" component={MaxFundContainerLayout} /> */}
              {/* <Route path="/rto" component={RtoContainerLayout} /> */}
              <Route path="/apertura-servizi" component={AperturaServiziContainerLayout} />
              <Route path="/adeguata-verifica" component={AdeguataVerifica} />
              <Route path="/test-components" component={TestComponents} />
              <Route path="/Firma" component={Firma} />
              <Route path="/Firma-namiral" component={FirmaWrapper} />
            </Switch>
          </Router>
          {/* <Router history={browserHistory} children={this.props.routes} /> */}
        </div>
      </Provider>
    )
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default(App)
