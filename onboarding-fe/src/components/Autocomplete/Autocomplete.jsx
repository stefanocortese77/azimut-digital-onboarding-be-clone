import React from 'react'
import PropTypes from 'prop-types'

import { CollectionItem, Collection } from 'react-materialize'

class Autocomplete extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      value: '',
      loseFocus: false
    }

    this._handleChange = this._handleChange.bind(this)
    this._handleSelect = this._handleSelect.bind(this)
  }

  // handleItemChange (i) {
  //   if (this.props.onChange) {
  //     this.props.onChange(i)
  //   }
  // }

  _handleChange ({ target: { value } }) {
    if (this.props.onChange) {
      const matchingSuggestion = this.props.suggestions.find(({ key }) => key.toLowerCase() === value.toLowerCase())

      this.props.onChange(value, matchingSuggestion || null)
    } else {
      this.setState({
        value
      })
    }
  }

  _handleSelect (suggestion) {
    if (this.props.onChange) {
      this.props.onChange(suggestion.label, suggestion)
      this.setState({ loseFocus: true })
    } else {
      this.setState({
        value: suggestion.label,
        loseFocus: true
      })
    }
  }

  render () {
    const {
      disabled = false,
      label = '',
      maxSuggestions = 5,
      placeholder = '',
      suggestions = [],
      threshold = 3,
    } = this.props

    const value = this.props.value || this.state.value || ''

    let suggestionsElement = null
    let filteredSuggestionsElements = null
    let noMatchElement = null

    if (value.length >= threshold) {
      filteredSuggestionsElements = suggestions
        .filter(suggestion => suggestion.label.toLowerCase().indexOf(value.toLowerCase()) >= 0)
        .slice(0, maxSuggestions)
        .map(suggestion => {
          const prefixEnd = suggestion.label.toLowerCase().indexOf(value.toLowerCase())
          const suffixStart = prefixEnd + value.length

          const prefix = suggestion.label.slice(0, prefixEnd)
          const infix = suggestion.label.slice(prefixEnd, suffixStart)
          const suffix = suggestion.label.slice(suffixStart, suggestion.label.length)

          return (
            <CollectionItem key={suggestion.key} onClick={() => this._handleSelect(suggestion)}>
              { prefix }
              <b>
                { infix }
              </b>
              { suffix }
            </CollectionItem>
          )
        })

      if (filteredSuggestionsElements.length > 0 && !this.state.loseFocus) {
        suggestionsElement = (
          <div className={'suggestions'}>
            <Collection>
              { filteredSuggestionsElements }
            </Collection>
          </div>
        )
      }
    }

    // The input is invalid if there is no match and the field is enabled
    const invalid = (filteredSuggestionsElements && filteredSuggestionsElements.length === 0) && !disabled

    if (invalid) {
      noMatchElement = (
        <div className="error-label">Nessuna corrispondenza</div>
      )
    }

    return (
      <div className={`autocomplete ${invalid ? 'invalid' : ''} ${disabled ? 'disabled' : ''} ${value.length ? 'filled' : ''}`} >
        <div className="input-field">
          <label htmlFor={this.props.id}>{label}</label>
          <input
            id={this.props.id}
            type="text"
            placeholder={placeholder}
            label={label}
            value={value || this.state.value}
            onChange={this._handleChange}
            onFocus={() => this.setState({ loseFocus: false })}
            disabled={disabled}
            autoComplete="off"
           />
          { suggestionsElement }
          { noMatchElement }
        </div>
      </div>
    )
  }
}

Autocomplete.propTypes = {
  id: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  maxSuggestions: PropTypes.number,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  suggestions: PropTypes.arrayOf(PropTypes.object),
  threshold: PropTypes.number,
  value: PropTypes.string,
}

export default Autocomplete
