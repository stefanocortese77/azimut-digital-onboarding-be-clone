import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Navbar, NavItem } from 'react-materialize'
export const Header = () => (
  <Navbar right>
    <Link to='/test-components' activeClassName='page-layout__nav-item--active'>testComponents</Link>
  </Navbar>
)
Header.propTypes = {
  children: PropTypes.node,
}
export default Header
