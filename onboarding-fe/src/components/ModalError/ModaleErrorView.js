import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
// import { Link } from 'react-router-dom'

export class ModaleError extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      error: '',
      error2: '',
      error3: '',
      visible: false
    }

    this.handlePrimaryClick = this.handlePrimaryClick.bind(this)
    this.handleSecondaryClick = this.handleSecondaryClick.bind(this)
  }

  componentDidMount () {
    let context = this
    axios.interceptors.request.use(function (config) {
      return config
    }, function (error) {
      let errorMessage = ''
      let errorMessage2 = ''
      let errorMessage3 = ''
      if (error.response) {
        errorMessage += (error.response.status) ? `${error.response.status}: ` : ''
        errorMessage += (error.response.statusText) ? `${error.response.statusText}` : ''

        errorMessage2 += (error.response.data.code) ? `${error.response.data.code}: ` : ''
        errorMessage2 += (error.response.data.description) ? error.response.data.description : ''
      }


      errorMessage3 += (error.message) ? `\n${error.message}\n` : ''


      context.setState({ error: errorMessage, error2: errorMessage2, error3: errorMessage3, visible: true })
      return error
    })
    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      return response
    }, function (error) {
      console.log('end error')
      let errorMessage = ''
      let errorMessage2 = ''
      let errorMessage3 = ''
      if (error.response) {
        errorMessage += (error.response.status) ? `${error.response.status}: ` : ''
        errorMessage += (error.response.statusText) ? `${error.response.statusText}` : ''

        errorMessage2 += (error.response.data.code) ? `${error.response.data.code}: ` : ''
        errorMessage2 += (error.response.data.description) ? error.response.data.description : ''
      }


      errorMessage3 += (error.message) ? `\n${error.message}\n` : ''


      context.setState({ error: errorMessage, error2: errorMessage2, error3: errorMessage3, visible: true })
      return Promise.reject(error)
    })
  }

  componentDidUpdate (prevProps) {

  }

  handlePrimaryClick (e) {
    let context = this
    context.setState({ visible: false })
  }

  handleSecondaryClick (e) {
    if (this.props.secondaryAction) { this.props.secondaryAction() }
  }

  render () {
    if (this.state.visible === false) { return null }

    return (
      <div className="modal-background">
        <div className="modal-box-container">
          <div className="modal-box-content">
            <h1 className="modal-title">attenzione</h1>
            <p className="modal-text">{this.state.error}</p>
            <p className="modal-text">{this.state.error2}</p>
            <p className="modal-text">{this.state.error3}</p>
          </div>
          <div className="modal-box-actions">
            <button className="modal-action primary btncenter" onClick={this.handlePrimaryClick}>CHIUDI</button>
          </div>
        </div>
      </div>
    )
  }
}

ModaleError.propTypes = {
  secondaryAction: PropTypes.func
}

export default ModaleError
