import React from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Row } from 'react-materialize'

export class Footer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      active: false
    }
    this.toggle = this.toggle.bind(this)
  }
  toggle () {
    this.setState({ active: !this.state.active })
  }
  render () {
    return (
      <footer className={`footer-bar${this.state.active ? ' active' : ''}`}>
        <div className="nav-links-box">
          <Link
            to="/consulenza-Base/create"
            className={`nav-link-item${this.state.active ? ' page-layout__nav-item--active' : ''}`}>
            <i className="icon-login" />
            <p>Acquisizione Cliente</p>
          </Link>
          <Link
            to="/apertura-servizi/intestatari"
            className={`nav-link-item${this.state.active ? ' page-layout__nav-item--active' : ''}`}>
            <i className="icon-prodotti-action-bar" />
            <p>Apertura Servizi</p>
          </Link>
          {/* <Link to="/max/intestatari" activeClassName="page-layout__nav-item--active" className="nav-link-item">
            <i className="icon-doc" />
            <p>Nuovo MAX</p>
          </Link>
          <Link to="/max-fund/intestatari" activeClassName="page-layout__nav-item--active" className="nav-link-item">
            <i className="icon-doc" />
            <p>Nuovo MAX FUND</p>
          </Link> */}
        </div>
        <Row className="button-container">
          <button className="button-toogle" onClick={this.toggle}><i className="icon-plus" /></button>
        </Row>
      </footer>
    )
  }
}
Footer.propTypes = {
  // children: PropTypes.node,
}
export default Footer
