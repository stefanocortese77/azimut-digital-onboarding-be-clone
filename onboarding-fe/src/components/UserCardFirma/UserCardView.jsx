import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Button } from 'react-materialize'
export const UserCard = ({ listaClienti, callback, staticDataFirma }) => {
  const items = listaClienti.map((user, i) => {
    let userIcon = null
    userIcon = (
      <span>
        <img src="/img/user_complete.png" />
        <Icon>done</Icon>
      </span>
    )


    let userText = null
    let userText2 = null

    userText = (
      <div className="user-card-nome">{ `${user.nomeCliente}` }</div>

      )
    userText2 = (
      <div className="user-card-cf">{ `${user.codiceFiscale}` }</div>
      )
    let userStatoPratica = null
    let labelUserStato = staticDataFirma.firmato
    if (user.stato !== 'FIRMATO') {
      labelUserStato = staticDataFirma.da_firmare
    }
    userStatoPratica = (
      <div>
        <div className="user-card-statopratica-titolo">{staticDataFirma.stato_label}</div>
        <div className="user-card-statopratica-dati">{ `${labelUserStato}` }</div>
      </div>
        )

    let buttonAction = null
    const classNameStato = `color-${[user.stato]}`
    if (user.stato !== 'FIRMATO') {
      buttonAction = (
        <Button flat waves="light" onClick={() => callback(user.urlIframe)} > {staticDataFirma.prendi_visione}</Button>
      )
    }


    return (
      <div key={i} className={`user-list-item padding-10 ${classNameStato}`}>
        <div className="user-icon margin-r-15">
          { userIcon }
        </div>
        <div className="user-name  padding-b-5">
          { userText }
          { userText2 }
          {userStatoPratica}
        </div>
        {buttonAction}
      </div>
    )
  })
  return (
    <div className="box-vertical box-vertical-right">
      {items}
    </div>
  )
}

UserCard.propTypes = {
  listaClienti: PropTypes.array,
  callback: PropTypes.func,
  staticDataFirma: PropTypes.object
}

export default UserCard
