
import { connect } from 'react-redux'
import UserCard from './UserCardView.jsx'
// import { servicesActions } from '../../store/modules/services'

const mapDispatchToProps = (dispatch) => {
  return { }
}

const mapStateToProps = (state) => {
  return ({
    listaClienti: state.utility.listaClienti,
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(UserCard)
