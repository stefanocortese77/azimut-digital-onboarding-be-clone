import React from 'react'
import PropTypes from 'prop-types'

export const AsideUsers = ({ clienti }) => {
  const items = clienti.map((user, i) => {
    let userIcon = null

    // check key idStato for the user and render the correct icon
    switch (user.idStato.chiave) {
      case '1':
        userIcon = (
          <span className="icon-user draft" />
        )
        break
      case '2':
        userIcon = (
          <span>
            <span className="icon-user complete" />
          </span>
        )
        break
      default:
        userIcon = (
          <span>
            <span className="icon-user other" />
          </span>
        )
    }

    let userText = null

    // check if fisrtName and lastName aren't undefined to change user view
    if (user.firstName && user.lastName) {
      userText = (
        <div>
          <div>Nome e Cognome</div>
          <div>{ `${user.firstName} ${user.lastName}` }</div>
        </div>
      )
    } else {
      userText = (
        <div>
          <div>Codice Fiscale</div>
          <div>{ `${user.codiceFiscale}` }</div>
        </div>
      )
    }

    return (
      <div key={i} className="user-list-item padding-10">
        <div className="user-icon margin-r-15">
          { userIcon }
        </div>
        <div className="">
          { userText }
        </div>
      </div>
    )
  })

  return (
    <aside className="aside aside-home">
      {items}
    </aside>
  )
}

AsideUsers.propTypes = {
  clienti: PropTypes.array,
}

export default AsideUsers
