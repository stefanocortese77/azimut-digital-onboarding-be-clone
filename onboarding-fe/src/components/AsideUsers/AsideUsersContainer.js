
import { connect } from 'react-redux'
import AsideUsers from './AsideUsersView.jsx'
// import { servicesActions } from '../../store/modules/services'

const mapDispatchToProps = (dispatch) => {
  return { }
}

const mapStateToProps = (state) => {
  console.log(state)
  return ({
    clienti: state.services.clienti,
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(AsideUsers)
