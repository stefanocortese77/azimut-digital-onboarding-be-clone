import React from 'react'
import { PropTypes } from 'prop-types'

const withFieldCatch = function withFieldCatch (WrappedComponent) {
  class ErrorField extends React.Component {
    constructor (props) {
      super(props)
      this.state = { error: null, errorInfo: null }
    }

    componentDidCatch (error, errorInfo) {
      this.setState({ error, errorInfo })
    }

    render () {
      if (this.state.error) {
        return (
          <div className="field-error">
            <h6 className="field">{ this.props.field }</h6>
            <pre>{this.state.error.message}</pre>
            <pre>{ this.state.errorInfo.componentStack }</pre>
          </div>
        )
      }

      return this.props.children
    }
  }

  ErrorField.propTypes = {
    field: PropTypes.string,
    children:  PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.element
    ])
  }

  class FieldCatch extends React.Component {
    render () {
      return (
        <ErrorField field={this.props.field.id}>
          <WrappedComponent {...this.props} />
        </ErrorField>
      )
    }
  }

  FieldCatch.propTypes = {
    field: PropTypes.object
  }

  return FieldCatch
}

export default withFieldCatch
